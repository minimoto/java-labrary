package com.tplhk.generic.genericclass;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 * <p>
 * 此处T可以随便写为任意标识，常见的如T、E、K、V等形式的参数常用于表示泛型
 * 在实例化泛型类时，必须指定T的具体类型
 * <p>
 * 不能对确切的泛型类型使用instanceof操作。如下面的操作是非法的，编译时会出错。
 * if(ex_num instanceof Generic<Number>){ }
 */
public class GenericClass<T> {

    //key这个成员变量的类型为T,T的类型由外部指定
    private T key;

    public GenericClass(T key) { //泛型构造方法形参key的类型也为T，T的类型由外部指定
        this.key = key;
    }

    public T getKey() { //泛型方法getKey的返回值类型为T，T的类型由外部指定
        return key;
    }

}
