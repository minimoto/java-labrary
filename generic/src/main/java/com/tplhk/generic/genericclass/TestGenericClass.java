package com.tplhk.generic.genericclass;

import lombok.extern.slf4j.Slf4j;
import sun.net.www.content.text.Generic;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
@Slf4j
public class TestGenericClass {


    public static void main(String[] args) {
        //泛型的类型参数只能是类类型（包括自定义类），不能是简单类型
        //传入的实参类型需与泛型的类型参数类型相同，即为Integer.
        GenericClass<Integer> genericInteger = new GenericClass<>(123456);

        //传入的实参类型需与泛型的类型参数类型相同，即为String.
        GenericClass<String> genericString = new GenericClass<>("key_vlaue");

        log.info("泛型测试,key is {}", genericInteger.getKey());
        log.info("泛型测试,key is {}", genericString.getKey());

    }
}
