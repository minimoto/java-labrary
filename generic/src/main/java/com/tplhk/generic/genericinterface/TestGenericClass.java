package com.tplhk.generic.genericinterface;

import lombok.extern.slf4j.Slf4j;

import javax.sound.midi.Soundbank;
import java.util.LinkedList;
import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
@Slf4j
public class TestGenericClass {


    public static void main(String[] args) {
        List<String> list = new LinkedList<>();
        list.add("111");
        list.add("222");
        list.add("333");
        // 构建对象时指定类型
        GenericClass<List<String>> genericInteger = new GenericClass<>(list);
        System.out.println(genericInteger.next());

        // 构建对象时，不用指定类型
        GenericClass2 genericClass2 = new GenericClass2();
        System.out.println(genericClass2.next());
    }
}
