package com.tplhk.generic.genericinterface;

import java.util.Collection;

/**
 * 未传入泛型实参时，与泛型类的定义相同，在声明类的时候，需将泛型的声明也一起加到类中
 * 即：class FruitGenerator<T> implements Generator<T>{
 * 如果不声明泛型，如：class FruitGenerator implements Generator<T>，编译器会报错："Unknown class"
 */
public class GenericClass<T> implements GeneratInterface<T> {

    //key这个成员变量的类型为T,T的类型由外部指定
    private T key;

    public GenericClass(T key) { //泛型构造方法形参key的类型也为T，T的类型由外部指定
        this.key = key;
    }

    public T getKey() { //泛型方法getKey的返回值类型为T，T的类型由外部指定
        return key;
    }

    @Override
    public T next() {
        return ((Collection<? extends T>) getKey()).iterator().next();
    }
}
