package com.tplhk.generic.genericinterface;

import java.util.Collection;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 *
 * 应用场景：
 * 当实现类的入参或返回值是不同的类型时，
 * 可以使用泛型，而不是 object 做入参或返回值
 *
 * disruptor 项目的“模型处理器”就是用这样解决的
 * public interface CommandProcessor<T extends Command> {
 *
 *   Class<T> getMatchClass();
 *
 *   void process(T command);
 *
 * }
 *
 *  /////////////// ItemAmountUpdateCommand 模型处理 ///////////////
 * @Slf4j
 * public class ItemAmountUpdateCommandProcessor implements CommandProcessor<ItemAmountUpdateCommand> {
 *
 *   @Override
 *   public Class<ItemAmountUpdateCommand> getMatchClass() {
 *     return ItemAmountUpdateCommand.class;
 *   }
 *
 *   @Override
 *   public void process(ItemAmountUpdateCommand command) {
 *     int index = RandomUtil.randomInt(producerCount);
 *     commandEventProducerList[index].onData(command);
 *   }
 * }
 *
 * /////////////// OrderInsertCommand 模型处理 ///////////////
 * @Slf4j
 * public class OrderInsertCommandProcessor implements CommandProcessor<OrderInsertCommand> {
 *
 *   @Override
 *   public Class<OrderInsertCommand> getMatchClass() {
 *     return OrderInsertCommand.class;
 *   }
 *
 *   @Override
 *   public void process(OrderInsertCommand command) {
 *     int index = RandomUtil.randomInt(producerCount);
 *     commandEventProducerList[index].onData(command);
 *   }
 * }
 */
public interface GeneratInterface<T> {

    public T next();
}
