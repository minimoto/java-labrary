package com.tplhk.generic.tuple;

import java.io.File;

/**
 * @ClassName : TwoTuple
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 **/
public class TwoTuple<A,B> {
    public final A first;
    public final B second;

    public TwoTuple(A first, B second) {
        this.first = first;
        this.second = second;
    }
}
