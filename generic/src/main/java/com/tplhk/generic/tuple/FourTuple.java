package com.tplhk.generic.tuple;

/**
 * @ClassName : TwoTuple
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 **/
public class FourTuple<A,B,C,D> extends ThreeTuple<A,B,C>{
    public final D fourth;

    public FourTuple(A first, B second, C third, D fourth) {
        super(first, second, third);
        this.fourth = fourth;
    }
}
