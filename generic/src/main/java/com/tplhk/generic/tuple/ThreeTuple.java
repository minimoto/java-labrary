package com.tplhk.generic.tuple;

/**
 * @ClassName : TwoTuple
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 **/
public class ThreeTuple<A,B,C> extends TwoTuple<A,B>{
    public final C third;

    public ThreeTuple(A first, B second, C third) {
        super(first, second);
        this.third = third;
    }
}
