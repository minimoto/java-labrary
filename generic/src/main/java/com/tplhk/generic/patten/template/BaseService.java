package com.tplhk.generic.patten.template;

import com.tplhk.generic.patten.template.vo.Result;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public abstract class BaseService<T> {

    /**
     * 定义了程序的主要实现流程-骨架
     *
     * @return
     */
    public Result handle() {
        Result result = new Result();
        method1();
//        result.setResponse(method2());
        T response = initResponse();//获取子类初始化的响应实例
        result.setResponse(response);
        return result;
    }

    /**
     * 返回值-泛型，根据不同的业务返回不同的响应类型
     * @return
     */
//    private T method2() {
//        T response = initResponse();//获取子类初始化的响应实例
//        System.out.println("BaseService method2");
//        return response;
//    }

    /**
     * 公共处理业务
     */
    private void method1() {
        System.out.println("BaseService method1");
    }

    /**
     * 响应类型-泛型，提供出去给具体实现类进行初始化
     *
     * @return
     */
    protected abstract T initResponse();
}
