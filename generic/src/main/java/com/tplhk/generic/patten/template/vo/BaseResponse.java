package com.tplhk.generic.patten.template.vo;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class BaseResponse {
    private int age;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
