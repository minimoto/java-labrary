package com.tplhk.generic.patten.template.vo;

import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class ChinaResponse extends BaseResponse {

    List<String> province;

    public List<String> getProvince() {
        return province;
    }

    public void setProvince(List<String> province) {
        this.province = province;
    }
}
