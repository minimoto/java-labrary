package com.tplhk.generic.patten.template;

import com.tplhk.generic.patten.template.vo.AmericaResponse;
import com.tplhk.generic.patten.template.vo.ChinaResponse;
import com.tplhk.generic.patten.template.vo.Result;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class TestTemplate {

    public static void main(String[] args) {
        BaseService<ChinaResponse> baseService = new ChinaServiceImpl();
        Result<ChinaResponse> result = baseService.handle();
        ChinaResponse chinaResponse = result.getResponse();
        System.out.println(chinaResponse.toString());

        BaseService<AmericaResponse> americaService = new AmericaServiceImpl();
        Result<AmericaResponse> americaResult = americaService.handle();
        AmericaResponse americaResponse = americaResult.getResponse();
        System.out.println(americaResponse.toString());
    }
}
