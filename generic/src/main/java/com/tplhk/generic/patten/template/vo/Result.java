package com.tplhk.generic.patten.template.vo;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class Result<T> {
    T response;

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }
}
