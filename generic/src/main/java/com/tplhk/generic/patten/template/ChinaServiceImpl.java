package com.tplhk.generic.patten.template;

import com.tplhk.generic.patten.template.vo.ChinaResponse;
import com.tplhk.generic.patten.template.vo.Result;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class ChinaServiceImpl extends BaseService<ChinaResponse> {

    @Override
    public Result<ChinaResponse> handle() {
        Result result = super.handle();
        return result;
    }

    /**
     * 实现抽象方法，初始化返回值类型
     *
     * @return
     */
    @Override
    protected ChinaResponse initResponse() {
        return new ChinaResponse();
    }
}
