package com.tplhk.generic.patten.template.vo;

import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class AmericaResponse extends BaseResponse {
    List<String> state;

    public List<String> getState() {
        return state;
    }

    public void setState(List<String> state) {
        this.state = state;
    }
}
