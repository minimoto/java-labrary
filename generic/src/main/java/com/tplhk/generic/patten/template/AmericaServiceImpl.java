package com.tplhk.generic.patten.template;

import com.tplhk.generic.patten.template.vo.AmericaResponse;

/**
 * create by xiao_qiang_01@163.com
 * 2022/3/6
 */
public class AmericaServiceImpl extends BaseService<AmericaResponse> {

    /**
     * 实现抽象方法，初始化返回值类型
     *
     * @return
     */
    @Override
    protected AmericaResponse initResponse() {
        return new AmericaResponse();
    }
}
