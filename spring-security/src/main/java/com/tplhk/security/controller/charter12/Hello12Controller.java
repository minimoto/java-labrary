package com.tplhk.security.controller.charter12;

import com.tplhk.security.domain.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : LoginController
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/22 17:12
 **/
@RestController
@RequestMapping("/v12")
public class Hello12Controller {

    // /hello 是任何人都可以访问的接口
    @GetMapping("/hello")
    public String hello() {
        return "hello-v12";
    }

    ///admin/hello 是具有 admin 身份的人才能访问的接口
    @GetMapping("/admin/hello")
    public String admin() {
        return "admin-v12";
    }

    // /user/hello 是具有 user,admin 身份的人才能访问的接口
    @GetMapping("/user/hello")
    public String user() {
        return "user-v12";
    }

}
