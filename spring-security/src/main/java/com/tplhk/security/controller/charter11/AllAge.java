package com.tplhk.security.controller.charter11;

import lombok.Data;

import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2021/6/27
 */
@Data
public class AllAge {
    java.util.List<Integer> ages;
    List<String> users;
}
