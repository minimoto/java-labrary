package com.tplhk.security.controller.charter6;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName : LoginController
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/22 17:12
 **/
@RestController
@RequestMapping("/v6")
public class Hello6Controller {

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

}
