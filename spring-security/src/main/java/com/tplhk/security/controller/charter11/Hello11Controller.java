package com.tplhk.security.controller.charter11;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @ClassName : LoginController
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/22 17:12
 **/
@RestController
@RequestMapping("/v11")
public class Hello11Controller {

    @Autowired
    HelloService11 helloService11;

    // /hello 是任何人都可以访问的接口
    @GetMapping("/hello")
    public String hello() {
        return helloService11.hello();
    }

    ///admin/hello 是具有 admin 身份的人才能访问的接口
    @GetMapping("/admin/hello")
    public String admin() {
        return helloService11.admin();
    }


    @GetMapping("/getAge")
    public String getAge(@RequestParam("age") Integer age) {
        return helloService11.getAge(age);
    }


    @GetMapping("/getAllUser")
    public List<String> getAllUser() {
        return helloService11.getAllUser();
    }


    @PostMapping("/getAllAge")
    public String user(@RequestBody AllAge allAge) {
        helloService11.getAllAge(allAge.getAges(), allAge.getUsers());
        return "ok";
    }
}
