package com.tplhk.security.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Map;
import java.util.Set;

/**
 * @ClassName : UserLoginDetails
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/24 14:25
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserLoginDetails implements UserDetails {
    private Long userId;
    /**
     * 账户
     */
    private String account;
    /**
     * 用户名称
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 性别： F 女，M 男
     */
    private String gender;
    /**
     * 平台集
     */
    private Map<String, String> platforms;
    /**
     * 权限集
     */
    private Set<GrantedAuthority> authorities;
    /**
     * 账户未过期：true 是， false 过期
     */
    private boolean accountNonExpired;
    /**
     * 账户未锁定：true 是， false 已锁定
     */
    private boolean accountNonLocked;
    /**
     * 账户有效：true 是， false 无效
     */
    private boolean enabled;
    /**
     * 密码未过期：true 是， false 过期
     */
    private boolean credentialsNonExpired;
    /**
     * 平台访问权限：true 有，false 没有
     */
    private boolean accountCanEnterPlatform;
    /**
     * 密码状态：0 未过期， 1 将过期， 2 过期
     */
    private Byte pwdStatus;
    /**
     * 进一步提示
     */
    private String hint;
    /**
     * 密码错误重试次数
     */
    private Short retryCount;
    /**
     * 登录次数
     */
    private Integer loginCount;
    /**
     * 隐私条款：0 未读， 1 已读
     */
    private Byte readPrivacy;
    /**
     * 平台编码
     */
    private String platformCode;
    /**
     * 用户 IP
     */
    private String remoteIp;
    /**
     * 异常提示
     */
    private String exceptionHint;
    /**
     * 用户来源
     */
    private String source;
    /**
     * 是否显示隐私条款,1=显示，0=不显示
     */
    private Integer reminder;

    @Override
    public boolean equals(Object rhs) {
        if (rhs instanceof UserLoginDetails) {
            return username.equals(((UserLoginDetails) rhs).username);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return username.hashCode();
    }
}
