package com.tplhk.security.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: jiangfei
 * @time: 2020/9/25 16:28
 * @Description: Application
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_role", schema = "tplhk_train")
public class Role {
    @Id
    private Long id;
    private String name;
    private String nameZh;

}
