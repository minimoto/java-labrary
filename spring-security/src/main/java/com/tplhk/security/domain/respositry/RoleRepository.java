package com.tplhk.security.domain.respositry;

import com.tplhk.security.domain.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @ClassName :   ApplicationImageRepository
 * @Description:
 * @Author: tq
 * @CreateDate: 2021/1/27 16:33
 * @Version: 1.0
 */
@Repository
public interface RoleRepository extends JpaRepository<Role, String> {
    List<Role> findAllByIdIn(List<Long> ids);
}
