package com.tplhk.security.domain.respositry;

import com.tplhk.security.domain.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * @ClassName :   ApplicationImageRepository
 * @Description:
 * @Author: tq
 * @CreateDate: 2021/1/27 16:33
 * @Version: 1.0
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, String> {

    List<UserRole> findAllByUserId(Long userId);
}
