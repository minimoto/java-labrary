package com.tplhk.security.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

/**
 * @ClassName : UserAuthority
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/25 10:23
 **/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthority implements GrantedAuthority {

    private String authority;


}
