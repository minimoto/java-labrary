package com.tplhk.security.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Author: jiangfei
 * @time: 2020/9/25 16:28
 * @Description: Application
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_user", schema = "tplhk_train")
public class User {
    @Id
    private String id;
    private Long userId;
    private String username;
    private String password;
    private Integer accountNonExpired;
    private Integer accountNonLocked;
    private Integer credentialsNonExpired;
    private Integer enabled;


}
