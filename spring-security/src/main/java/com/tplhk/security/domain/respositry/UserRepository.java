package com.tplhk.security.domain.respositry;

import com.tplhk.security.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * @ClassName :   ApplicationImageRepository
 * @Description:
 * @Author: tq
 * @CreateDate: 2021/1/27 16:33
 * @Version: 1.0
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
    User findByUsername(String username);
}
