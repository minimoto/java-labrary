//package com.tplhk.security.config;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.crypto.password.NoOpPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//
//import java.io.PrintWriter;
//
///**
// * @ClassName : SecurityConfig
// * @Description : TODO
// * @Author : taiping
// * @Date: 2021/6/22 17:24
// **/
//@Configuration
//public class SecurityConfigCharter1 extends WebSecurityConfigurerAdapter {
//
//    @Bean
//    PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        // 内置账密在内存中
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("123").roles("admin")
//        ;
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // --------------1. 配置所有请求都要鉴权
//        http.authorizeRequests()
//                .anyRequest()
//                .authenticated()
//                .and()
//                // -----------2. 配置登陆请求
//                .formLogin()
////                -----2.1 定义请求账密的参数
//                .usernameParameter("username")
//                .passwordParameter("password")
////                --------2.2 请求接口, 不需要在 controller 中实现:
////                http://localhost:8082/v1/login?username=javaboy.org&password=123
//                .loginPage("/v1/login")
////                --------2.3 请求成功后返回字符串给前端
//                .successHandler((req, resp, authentication) -> {
//                    Object principal = authentication.getPrincipal();
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write(new ObjectMapper().writeValueAsString(principal));
//                    out.flush();
//                    out.close();
//                })
////                --------2.4 请求失败后返回字符串给前端
//                .failureHandler((req, resp, e) -> {
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write(e.getMessage());
//                    out.flush();
//                    out.close();
//                })
////                ------2.5 对于 /v1/hello/login 地址不鉴权: http://localhost:8082/logout
//                .permitAll()
//                .and()
////             3. 配置退出请求接口
//                .logout()
////                -------------3.1 成功退出，返回字符串给前端
//                .logoutSuccessHandler((req, resp, authentication) -> {
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write("注销成功");
//                    out.flush();
//                    out.close();
//                })
////                ----------3.2 对于 /logout 地址不鉴权
//                .permitAll()
//                .and()
//                .csrf().disable()
//                // 4. 未登陆时访问其它地址时，返回字符串给前端 : http://localhost:8082/v1/hello
//                .exceptionHandling()
//                .authenticationEntryPoint((req, resp, authException) -> {
//                            resp.setContentType("application/json;charset=utf-8");
//                            PrintWriter out = resp.getWriter();
//                            out.write("尚未登录，请先登录");
//                            out.flush();
//                            out.close();
//                        }
//                );
//
//        ;
//    }
//
//
//}
