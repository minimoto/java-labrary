//package com.tplhk.security.config;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
//import org.springframework.security.access.hierarchicalroles.RoleHierarchyImpl;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.User;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.password.NoOpPasswordEncoder;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.security.provisioning.InMemoryUserDetailsManager;
//
//import java.io.PrintWriter;
//
///**
// * @ClassName : SecurityConfig
// * @Description : TODO
// * @Author : taiping
// * @Date: 2021/6/22 17:24
// **/
//@Configuration
//public class SecurityConfigCharter2 extends WebSecurityConfigurerAdapter {
//
//    @Bean
//    PasswordEncoder passwordEncoder() {
//        return NoOpPasswordEncoder.getInstance();
//    }
//
////    /**
////     * 配置用户信息方式 1
////     * @param auth
////     * @throws Exception
////     */
////    @Override
////    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
////        // 内置账密在内存中
////        auth.inMemoryAuthentication()
////                .withUser("javaboy.org")
////                .password("123").roles("admin")
////        ;
////    }
//
//    /**
//     * 配置用户信息方式 2
//     * @throws Exception
//     */
//    @Override
//    @Bean
//    protected UserDetailsService userDetailsService() {
//        InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
//        manager.createUser(User.withUsername("admin").password("123").roles("admin").build());
//        manager.createUser(User.withUsername("xjq").password("123").roles("user").build());
//        return manager;
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // --------------1. 配置所有请求都要鉴权
//        http.authorizeRequests()
////                ------------1.1 配置以下不同的 url 需要不同的角色
////                ------** 表示多层，* 表示一层， ?表示单个字符
//                .antMatchers("/v2/admin/**").hasRole("admin")
//                .antMatchers("/v2/user/**").hasRole("user")
////                --------------1.2 anyRequest 必须放在最后面！！ 注意顺序，上往下的顺序来匹配，一旦匹配到了就不继续匹配了
//                .anyRequest()
//                .authenticated()
//                .and()
//                // -----------2. 配置登陆请求
//                .formLogin()
////                -----2.1 定义请求账密的参数
//                .usernameParameter("username")
//                .passwordParameter("password")
////                --------2.2 请求接口, 不需要在 controller 中实现:
////                http://localhost:8082/v1/hello/login?username=javaboy.org&password=123
//                .loginPage("/v1/login")
////                --------2.3 请求成功后返回字符串给前端
//                .successHandler((req, resp, authentication) -> {
//                    Object principal = authentication.getPrincipal();
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write(new ObjectMapper().writeValueAsString(principal));
//                    out.flush();
//                    out.close();
//                })
////                --------2.4 请求失败后返回字符串给前端
//                .failureHandler((req, resp, e) -> {
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write(e.getMessage());
//                    out.flush();
//                    out.close();
//                })
////                ------2.5 对于 /v1/hello/login 地址不鉴权: http://localhost:8082/logout
//                .permitAll()
//                .and()
////             3. 配置退出请求接口
//                .logout()
////                -------------3.1 成功退出，返回字符串给前端
//                .logoutSuccessHandler((req, resp, authentication) -> {
//                    resp.setContentType("application/json;charset=utf-8");
//                    PrintWriter out = resp.getWriter();
//                    out.write("注销成功");
//                    out.flush();
//                    out.close();
//                })
////                ----------3.2 对于 /logout 地址不鉴权
//                .permitAll()
//                .and()
//                .csrf().disable()
//                // 4. 未登陆时访问其它地址时，返回字符串给前端 : http://localhost:8082/v1/hello/hello
//                .exceptionHandling()
//                .authenticationEntryPoint((req, resp, authException) -> {
//                            resp.setContentType("application/json;charset=utf-8");
//                            PrintWriter out = resp.getWriter();
//                            out.write("尚未登录，请先登录");
//                            out.flush();
//                            out.close();
//                        }
//                );
//    }
//
//    /**
//     * 角色继承: 所有 user 能够访问的资源，admin 都能够访问
//     * 在配置时，需要给角色手动加上 ROLE_ 前缀
//     * @return
//     */
//    @Bean
//    RoleHierarchy roleHierarchy() {
//        RoleHierarchyImpl hierarchy = new RoleHierarchyImpl();
//        hierarchy.setHierarchy("ROLE_admin > ROLE_user");
//        return hierarchy;
//    }
//
//}
