DROP TABLE IF EXISTS "tplhk_train"."t_role";
CREATE TABLE "tplhk_train"."t_role" (
                                   "id" SERIAL NOT NULL,
                                   "name" VARCHAR ( 32 ) COLLATE "pg_catalog"."default" DEFAULT NULL :: CHARACTER VARYING,
                                   "name_zh" VARCHAR ( 32 ) COLLATE "pg_catalog"."default" DEFAULT NULL :: CHARACTER VARYING
);
COMMENT ON TABLE "tplhk_train"."t_role" IS '角色表';
ALTER TABLE "tplhk_train"."t_role" ADD CONSTRAINT "pk_t_role" PRIMARY KEY ( "id" );


DROP TABLE IF EXISTS "tplhk_train"."t_user";
CREATE TABLE "tplhk_train"."t_user" (
                                   "id" SERIAL NOT NULL,
                                   "user_id" int2 DEFAULT NULL,
                                   "username" VARCHAR ( 32 ) COLLATE "pg_catalog"."default" DEFAULT NULL :: CHARACTER VARYING,
                                   "password" VARCHAR ( 32 ) COLLATE "pg_catalog"."default" DEFAULT NULL :: CHARACTER VARYING,
                                   "account_non_expired" int2 DEFAULT NULL,
                                   "account_non_locked" int2 DEFAULT NULL,
                                   "credentials_non_expired" int2 DEFAULT NULL,
                                   "enabled" int2 DEFAULT NULL
);
COMMENT ON TABLE "tplhk_train"."t_user" IS '用户表';
ALTER TABLE "tplhk_train"."t_user" ADD CONSTRAINT "pk_t_user" PRIMARY KEY ( "id" );

DROP TABLE IF EXISTS "tplhk_train"."t_user_roles";
CREATE TABLE "tplhk_train"."t_user_roles" (
                                         "id" SERIAL NOT NULL,
                                         "user_id" int2 DEFAULT NULL,
                                         "role_id" int2 DEFAULT NULL
);
COMMENT ON TABLE "tplhk_train"."t_user_roles" IS '用户角色表';
ALTER TABLE "tplhk_train"."t_user_roles" ADD CONSTRAINT "pk_t_user_roles" PRIMARY KEY ( "id" );