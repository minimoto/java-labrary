INSERT INTO "tplhk_train"."t_role"("id", "name", "name_zh") VALUES (1, 'admin', NULL);
INSERT INTO "tplhk_train"."t_role"("id", "name", "name_zh") VALUES (2, 'user', NULL);

INSERT INTO "tplhk_train"."t_user"("id", "user_id", "username", "password", "account_non_expired", "account_non_locked", "credentials_non_expired", "enabled") VALUES (1, 1, 'admin', '123', 0, 0, 0, 1);
INSERT INTO "tplhk_train"."t_user"("id", "user_id", "username", "password", "account_non_expired", "account_non_locked", "credentials_non_expired", "enabled") VALUES (2, 2, 'user', '123', 0, 0, 0, 1);

INSERT INTO "tplhk_train"."t_user_roles"("id", "user_id", "role_id") VALUES (1, 1, 1);
INSERT INTO "tplhk_train"."t_user_roles"("id", "user_id", "role_id") VALUES (2, 2, 2);

