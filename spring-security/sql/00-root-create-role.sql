-- root执行

-- 创建角色和用户

CREATE ROLE "learning_ddl_role";
CREATE ROLE "learning_dml_role";
CREATE ROLE "learning_readonly_role";

CREATE ROLE "learning_ddl_user" with password 'Taiping@9999' LOGIN;
CREATE ROLE "learning_dml_user" with password 'Taiping@9999' LOGIN;
CREATE ROLE "learning_readonly_user" with password 'Taiping@9999' LOGIN;

GRANT "learning_ddl_role" TO "learning_ddl_user";
GRANT "learning_dml_role" TO "learning_dml_user";
GRANT "learning_readonly_role" TO "learning_readonly_user";


