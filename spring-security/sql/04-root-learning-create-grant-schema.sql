create schema if not  exists  tplhk_train;

ALTER SCHEMA tplhk_train OWNER TO learning_ddl_user;

GRANT USAGE ON SCHEMA tplhk_train TO learning_ddl_user,learning_dml_user,learning_readonly_user;
