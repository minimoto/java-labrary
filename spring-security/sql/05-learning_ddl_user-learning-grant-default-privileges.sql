begin;
-- 默认权限
ALTER DEFAULT PRIVILEGES IN SCHEMA tplhk_train GRANT SELECT ON TABLES to learning_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA tplhk_train GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to learning_ddl_user,learning_dml_user;
-- sequence默认权限
ALTER DEFAULT PRIVILEGES IN SCHEMA tplhk_train GRANT USAGE,SELECT ON SEQUENCES to learning_ddl_user,learning_dml_user,learning_readonly_user;
-- function默认权限
ALTER DEFAULT PRIVILEGES IN SCHEMA tplhk_train GRANT EXECUTE ON FUNCTIONS to learning_ddl_user,learning_dml_user,learning_readonly_user;

commit;





