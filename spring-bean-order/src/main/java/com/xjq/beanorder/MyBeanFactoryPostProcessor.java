package com.xjq.beanorder;

import com.xjq.service.MyBean1;
import com.xjq.service.MyBean2;
import com.xjq.service.MyBean3;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * 启动的顺序：myBean1 -> myBean2 -> myBean3
 * create by xiao_qiang_01@163.com
 * 2024/8/14
 */
public class MyBeanFactoryPostProcessor implements BeanDefinitionRegistryPostProcessor {

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        AbstractBeanDefinition beanDefinition1 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition1.setBeanClass(MyBean1.class);
        registry.registerBeanDefinition("myBean1", beanDefinition1);

        AbstractBeanDefinition beanDefinition2 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition2.setBeanClass(MyBean2.class);
        registry.registerBeanDefinition("myBean2", beanDefinition2);

        AbstractBeanDefinition beanDefinition3 = BeanDefinitionBuilder.genericBeanDefinition().getBeanDefinition();
        beanDefinition3.setBeanClass(MyBean3.class);
        registry.registerBeanDefinition("myBean3", beanDefinition3);

    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
