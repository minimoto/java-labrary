package com.xjq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBeanOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBeanOrderApplication.class, args);
    }

}
