package com.tplhk.camunda.starter.model.param.task;

import lombok.Data;

/**
 * task查询参数
 *
 * @Author : jqxu
 * @Date: 2022/5/18 12:36
 */
@Data
public class ReceiveTaskParam {

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 流程实例Key
     */
    private String processInstanceKey;

    /**
     * 事件名称(全局唯一)
     */
    private String eventName;

}
