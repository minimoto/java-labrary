package com.tplhk.camunda.starter.model.param.process;

import lombok.Builder;
import lombok.Data;

/**
 * 流程查询参数
 *
 * @Author : jqxu
 * @Date: 2022/5/6 17:36
 */
@Data
@Builder
public class ProcessVariablesQueryParam{
    /**
     * 流程实例ID
     */
    private String processInstanceId;
    /**
     * 任务ID
     */
    private String taskId;
    /**
     * 流程变量名称
     */
    private String variableName;

    /**
     * 租户ID
     */
    private String tenantId;

}
