package com.tplhk.camunda.starter.model.param.process;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * task查询参数
 *
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 */
@Data
public class ProcessInstanceQueryParam{

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 流程实例 busiKey
     */
    private String processInstanceBusinessKey;


}
