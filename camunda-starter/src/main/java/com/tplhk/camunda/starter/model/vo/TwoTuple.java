package com.tplhk.camunda.starter.model.vo;

import lombok.Data;

/**
 * @ClassName : TwoTuple
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 **/
@Data
public class TwoTuple<A,B> {
    public final A first;
    public final B second;

    public TwoTuple(A first, B second) {
        this.first = first;
        this.second = second;
    }


}
