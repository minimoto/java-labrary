package com.tplhk.camunda.starter.anno;

import com.tplhk.camunda.starter.config.CamundaCommonConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启动Camunda通用工具
 *
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:27
 */
@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(CamundaCommonConfig.class)
public @interface EnableCamundaCommon {
}
