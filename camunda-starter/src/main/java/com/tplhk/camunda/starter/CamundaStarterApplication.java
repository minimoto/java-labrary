package com.tplhk.camunda.starter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class CamundaStarterApplication {

    public static void main(String... args) {
        SpringApplication.run(CamundaStarterApplication.class, args);
    }

}
