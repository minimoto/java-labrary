package com.tplhk.camunda.starter.listener;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.DelegateTask;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.delegate.TaskListener;

/**
 * @ClassName : TplhkExecutionListener
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/25 14:35
 **/
@Slf4j
public class TplhkExecutionListener implements ExecutionListener, TaskListener {
    @Override
    public void notify(DelegateExecution execution) throws Exception {
        String eventName = execution.getEventName();
        // ActivitiEventType.PROCESS_STARTED
        if ("start".equals(eventName)) {
            // 流程开始
            log.info("start......{}", eventName);
        } else if ("end".equals(eventName)) {
            // 流程结束
            log.info("end......{}", eventName);
        } else if ("take".equals(eventName)) {
            // 连线监听器
            log.info("take......{}", eventName);
        }
    }

    @Override
    public void notify(DelegateTask delegateTask) {
        String assignee = delegateTask.getAssignee();
        String eventName = delegateTask.getEventName();
        String taskDefinitionKey = delegateTask.getTaskDefinitionKey();

        // ActivitiEventType.PROCESS_STARTED
        if ("create".endsWith(eventName)) {
            log.info("create=========");
//            delegateTask.setAssignee("jqxu");
        } else if ("assignment".endsWith(eventName)) {
            log.info("assignment========");
        } else if ("complete".endsWith(eventName)) {
            log.info("complete===========");
        } else if ("delete".endsWith(eventName)) {
            log.info("delete=============");
        }
        //todo delegateTask 可以操作activiti引擎的一些东西
    }
}
