package com.tplhk.camunda.starter.model.param.task;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * task查询参数
 *
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 */
@Data
public class UserTaskConfirmParam {

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 任务ID
     */
    private String taskId;

    /**
     * 业务KEY: 如果需要根据 bizKey 返回业务数据，则必传.
     */
    private String bizKey;
}
