package com.tplhk.camunda.starter.model.param.process;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * task查询参数
 *
 * @Author : jqxu
 * @Date: 2022/5/6 16:36
 */
@Data
public class TaskQueryParam extends BasePageParam {
    /**
     * 任务处理者
     */
    private String assignee;

    /**
     * 流程实例ID
     */
    private String processInstanceId;

    /**
     * 流程定义ID
     */
    private String processDefinitionKey;
    /**
     * 任务定义ID
     */
    private String taskDefinitionKey;
    /**
     * 租户ID
     */
    private String tenantId;
    /**
     * 是否附加查询流程变量
     */
    private Boolean withProcessVariables = false;

    /**
     * 开始时间 - 起始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startedAfter;
    /**
     * 开始时间 - 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startedBefore;

    /**
     * 变量名称: 通过变量名称取得值，根据变量值回调业务数据
     */
    private String taskBizKeyVariableName;

    /**
     * 是否已完成，用于
     */
    private Boolean finished;

}
