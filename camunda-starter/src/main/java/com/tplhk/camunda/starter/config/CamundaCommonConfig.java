package com.tplhk.camunda.starter.config;

import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import org.springframework.context.annotation.Bean;

/**
 * Camunda通用工具 - 配置类
 *
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:27
 */
public class CamundaCommonConfig {

    @Bean
    public CamundaCommonService camundaCommonService() {
        return new CamundaCommonService();
    }
}
