package com.tplhk.camunda.starter.model.param.process;

import lombok.Data;

/**
 * @ClassName : BasePageParam
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/6 16:27
 **/
@Data
public class BasePageParam {
    private Integer pageNo = 1;
    private Integer pageSize = 10;
    private String orderBy;
    private String orderSort;
}
