package com.tplhk.retry.service.impl;

import com.tplhk.retry.vo.MessageBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName : SmsSendService
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2023/2/13 10:30
 **/
@Slf4j
@Service
public class SmsSendService extends AbstractSendService {


    @Override
    public Object doSend(MessageBean messageBean) throws Exception {
        log.info("发送消息，id: {}", messageBean.key());
        return null;
    }

}
