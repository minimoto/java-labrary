package com.tplhk.retry.service;

import com.tplhk.retry.vo.MessageBean;


public interface SendService {

    /**
     * 发送消息
     *
     * @param messageBean 消息内容
     * @return
     */
    Object send(MessageBean messageBean);

}
