package com.tplhk.retry.service.impl;

import com.tplhk.retry.vo.MessageBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

/**
 * @ClassName : SmsSendService
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2023/2/13 10:30
 **/

/**
 * @EnableRetry 使用：
 * 注解 @Retryable 的方式必须声明
 * RetryTemplate 方式不用声明
 */
@Slf4j
@Service
@EnableRetry
public class EmailSendService {


    @Retryable(value = RuntimeException.class, maxAttempts = 5,
            backoff= @Backoff(value = 1500, maxDelay = 5000, multiplier = 2))
    public Object send(MessageBean messageBean){
        log.info("发送消息，id: {}", messageBean.key());
        if(true) {
            throw new RuntimeException("测试异常情况");
        }
        return null;
    }

    /**
     * 注意：
     * 报错：Cannot locate recovery method
     * 解决办法：
     * 异常类型需要与Recover方法参数类型保持一致
     * recover方法返回值需要与重试方法返回值保证一致
     * @param e
     * @return
     */
    @Recover
    public Object recover(RuntimeException e){
        System.out.println("service retry after Recover => " + e.getMessage());
        log.info("完蛋了，重试都失败了");
        return null;
    }


}
