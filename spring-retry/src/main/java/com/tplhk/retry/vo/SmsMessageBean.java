package com.tplhk.retry.vo;

import cn.hutool.core.util.RandomUtil;

import java.util.Random;

/**
 * @ClassName : MessageBean
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2023/2/13 10:11
 **/
public class SmsMessageBean implements MessageBean{

    /**
     * 返回消息主键（需具有唯一约束）。
     * 如：保单号，业务流水号...
     *
     * @return 消息主键
     */
    @Override
    public String key(){
        return RandomUtil.randomString(10);
    }

}
