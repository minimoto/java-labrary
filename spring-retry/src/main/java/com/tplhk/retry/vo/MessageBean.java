package com.tplhk.retry.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName : MessageBean
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2023/2/13 10:11
 **/
public interface MessageBean extends Serializable {

    /**
     * 返回消息主键（需具有唯一约束）。
     * 如：保单号，业务流水号...
     *
     * @return 消息主键
     */
    String key();

}
