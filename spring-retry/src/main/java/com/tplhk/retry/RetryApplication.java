package com.tplhk.retry;

import com.tplhk.retry.service.impl.SmsSendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

/**
 * @ClassName : RetryApplication
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2023/2/13 10:27
 **/
@SpringBootApplication
public class RetryApplication {

    public static void main(String[] args) {
        SpringApplication.run(RetryApplication.class, args);

    }

}
