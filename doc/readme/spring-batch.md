# spring batch

### 重要说明

* 依赖包中 batch 与 task 组件的关系

  * 如果只是编写 batch ，依赖包只需要 springbatch 即可。
   
     但是在部署到 dataflow 环境下，由于 dataflow 是基于 task ，所以需要引入 task 组件, 
     
     否则 batch 任务虽然仍可正常执行，但是 dataflow 界面无法显示执行日志。
     
  * 如果没有 task 组件，该模块做为应用程序，瞬间结束退出了，无法执行 batch 任务
  
* batch 任务不能依赖 web，否则就是一个服务，无法结束。      

> 所有示例参考 "【千锋】Java Spring Batch实战教程"

* readStr 
  
  * 一个入门程序，该示例比较完整的 read-processor-writer 模型的 demo 
  
   * 基于 mybatis-plus 双数据源配置。 
   
   * Processor2 中有获取 JobParameters 写法, 需要加上 @StepScope 注解。
   
   * retry 和 skip 机制： 
   
    * 异常处理
    
   |-|使用范围|read 异常|process 异常|write 异常|-|
   |:---|:---|:---|:---|:---|:---|
   |skip|Read, Processor 和 Writer 中使用|skip 异常数据，重复执行 read ，<br> 直到返回数据达到 chuck size </br>|skip 异常数据， 然后重新 scan 这批 chuck 数据， <br> 就是说，会重复执行这批数据，除异常数据外</br> |回到 process 处理器，重新 scan 这批 chuck 数据，逐条执行结果传给 write ，即 write 单条处理,<br>如果单条处理发生异常，直接 skip 数据， 不影响后续数据处理 </br> |-|
   |retry|Processor 和 Writer 中使用|-|重新 scan 这批 chuck 数据， <br> 当执行到异常数据时，会重试 </br> <br> 如果再发生异常，不断重复上述过程，直到 retryLimit 而中断 job </br>|回到 process 处理器，重新 scan 这批 chuck 数据，并将这批 chuck 数据一次性传给 write , 即 write 仍是处理一批数据, <br>如果发生异常，不断重复上述过程，直到 retryLimit 而中断 job </br>|-|
   
    * 如果同时设置，retry 设置失效
    
  
* usejob

  * 如何构建 job 时指定 steps 
  
* useFlow

  * flow 是一组 step
  
  * flow 可以被多个 job 复用
  
* useSplit

  * 并发执行
 
* useDecider

  * 决策器
 
* useJobNested

  * job 嵌套。
  
  > 教程提示需要配置 spring.batch.job.names = nestedJobDemoJob , 
   但是我没有配置也可以成功
  
* useListen

  * 示例见 readStr. 错误重试 retry 不知道什么原因，以前测试没有问题的，现在 retry 不起作用

  * 创建监听可以通过实现接口或注解
  
  * JobExecutionListener(before, after) 监听job的执行开始和结束；
  
    * 两种实现方式 : 1. 使用注解@BeforeJob、@AfterJob  2. 实现接口 JobExecutionListener
  
  * StepExecutionListener(before, after) 监听step的执行开始和结束；
  
    * 两种实现方式 : 1. 使用注解@BeforeStep、@AfterStep  2. 实现接口 StepExecutionListener
  
  * ChunkListener(before, after, error) 监听的chunk的执行开始和结束，或者监听chunk出错
  
  * ItemReadListener 监听 读之前、读之后、读的时候发生错误
  
  * ItemProcessListener 监听 处理之前、处理之后、处理的时候发生错误
  
  * ItemWriteListener(before, after, error) 监听 写之前、写之后、写的时候发生错误

  * SkipListener   在读、处理、写的过程中，监听跳过的异常；
  
* useParameter

  * 使用参数, step 之间传参通过单独定义一个类 (spring 默认单例)

* taskletDemoJob
  * 学习 tasklet 使用。这种模型适用于按步骤执行的批处理