# spring-security

* charter1: 前后端分离的认证示例
  * 配置登陆(登陆成功/失败)返回字符串给前端
  * 配置退出
  * 未登陆时调用其他接口时，返回字符串给前端
  * 初始化用户是配置在内存中的（生产中不可能这样做）  
  * 代码：charter1/HelloController, SecurityConfigCharter1

* charter2: 授权(不同的 role 访问不同的 url)
  * antMatchers 的使用
  * 重写 userDetailsService 类：可以通过不同的数据源取得用户源。
    在实际应用中很重要，本例初始化 admin ,user 两种用户和角色
  * 重载 roleHierarchy 实现角色继承
  * 初始化用户是配置在内存中的（生产中不可能这样做）  
  * 代码：charter2/Hello2Controller, SecurityConfigCharter2

* charter3: 账密信息配置在数据库中（生产做法, 根据权限标识 AnyAuthority 访问不同的 url）
  * UserLoginDetails：扩展 security 的用户信息模型
  * UserService: 实现 userDetailsService 接口, 重载 loadUserByUsername，返回 UserLoginDetails 模型。
    登陆时，框架自动获取用户名，并调用该方法加载用户基本信息，然后认证。
  * configure(AuthenticationManagerBuilder auth) : 配置用户
  * 修改 configure(HttpSecurity http)
    * .antMatchers("/v3/admin/**").hasAnyAuthority("admin")
    * .antMatchers("/v3/user/**").hasAnyAuthority("user")
  * 增加 remember-me 功能: ( 用postman 无法测试，待验证)
    * 只需要在 configure(HttpSecurity http) 增加: rememberMe().key("setKey") 即可
  * 代码：charter3/*, SecurityConfigCharter3, domain/*
  

* charter4: 自定义认证（生产做法, 增加验证码）
  * 由于security 只能验证账密，所以需要通过自定义认证增加验证码功能
  * 本示例代码在 charter3 上修改，但去掉 remember-me 功能
  * 步骤：
    * VerifyCodeConfig : 验证码配置类
    * Hello4Controller : 增加获取验证码接口
    * [重要] VerifyCodeAuthenticationProvider : 
      重写 DaoAuthenticationProvider.additionalAuthenticationChecks (主要是密码的校验)，
      增加验证码的判断
    * SecurityConfigCharter4 : 配置代码  
    ```java
        @Bean
            VerifyCodeAuthenticationProvider verifyCodeAuthenticationProvider() {
                VerifyCodeAuthenticationProvider verifyCodeAuthenticationProvider = new VerifyCodeAuthenticationProvider();
                // 父类 DaoAuthenticationProvider 在判断账密的时候，需要 passwordEncoder，userDetailsService 两个实现类
                verifyCodeAuthenticationProvider.setPasswordEncoder(passwordEncoder());
                verifyCodeAuthenticationProvider.setUserDetailsService(userService);
                return verifyCodeAuthenticationProvider;
            }
        
            @Override
            @Bean
            protected AuthenticationManager authenticationManager(){
                ProviderManager manager = new ProviderManager(Arrays.asList(verifyCodeAuthenticationProvider()));
                return manager;
            }
    ```
  * 代码：
    * charter4/*, 
    * SecurityConfigCharter4，
    * provider/VerifyCodeAuthenticationProvider,  
    * VerifyCodeConfig
  
  
  * charter5: 获取IP信息及扩展( AuthenticationDetailsSource 信息扩展接口 )
    * 本示例代码在 charter4 上修改，没有单独的 charter5 目录
    * 原理：
      * IP 是存在 Authentication.getDetails() 返回对象 WebAuthenticationDetails 中。
        该返回对象是通过 AuthenticationDetailsSource 接口的实现类 WebAuthenticationDetailsSource 获取的。
      * 只要我们自定义一个实现类( 实现 AuthenticationDetailsSource 接口)，返回对象扩展 WebAuthenticationDetails 类就行了。
    * 本例把 charter4 验证码逻辑移到自定义扩展类中     
                   
    * 步骤：
      * WebAuthenticationDetailsSourceExt : 自定义信息扩展类
      * WebAuthenticationDetailsEx : 返回自定义扩展对象，增加验证码的校验结果
      * 修改 additionalAuthenticationChecks 方法
      * configure(HttpSecurity http) 中配置 .authenticationDetailsSource(webAuthenticationDetailsSourceExt)
      * 自定义信息扩展核心代码  
      ```java
          public class MyWebAuthenticationDetails extends WebAuthenticationDetails {
          
              private boolean isPassed;
          
              public MyWebAuthenticationDetails(HttpServletRequest req) {
                  super(req);
                  String code = req.getParameter("code");
                  String verify_code = (String) req.getSession().getAttribute("verify_code");
                  if (code != null && verify_code != null && code.equals(verify_code)) {
                      isPassed = true;
                  }
              }
          
              public boolean isPassed() {
                  return isPassed;
              }
          }
          @Component
          public class MyWebAuthenticationDetailsSource implements AuthenticationDetailsSource<HttpServletRequest,MyWebAuthenticationDetails> {
              @Override
              public MyWebAuthenticationDetails buildDetails(HttpServletRequest context) {
                  return new MyWebAuthenticationDetails(context);
              }
          }
      ```
    * 代码：
      * charter4 所有代码 
      * WebAuthenticationDetailsSourceExt
      * WebAuthenticationDetailsEx

  * charter6: 单点登陆
    * 本示例代码复制 charter3 后修改
    * 踢掉已经登录用户
      * configure(HttpSecurity http) 
      ```java
          .csrf().disable()
          .sessionManagement()
          .maximumSessions(1);
      ```
    * 如果用户已经登录，禁止新的登录   
      ```java
            .csrf().disable()
            .sessionManagement()
            .maximumSessions(1)
            .maxSessionsPreventsLogin(true);
      ```       
      同时需要增加
      ```java
            @Bean
            HttpSessionEventPublisher httpSessionEventPublisher() {
                return new HttpSessionEventPublisher();
            }
      ```    
     * 无论是哪一种单点登陆情况，当高并发时，会同时登陆成功。
        因为 session 注册时，保存在 ConcurrentMap<Object, Set<String>> principals; 
        key 值是用户对象，value 值是 sessionId。
        所以需要重写 UserLoginDetails 的 equals 和 hashCode
               
    * 代码
      * charter6/*
      * SecurityConfigCharter6
        

  * charter7: 自带防火墙
    * 有两种模式：DefaultHttpFirewall 和 StrictHttpFirewall（严格模式）。  默认使用的是 StrictHttpFirewall
    * StrictHttpFirewall 有以下限制
      * 只允许白名单中的方法:  
        HTTP 请求方法必须是 DELETE、GET、HEAD、OPTIONS、PATCH、POST 以及 PUT 中的一个，请求才能发送成功，
        否则的话，就会抛出 RequestRejectedException 异常。
        如果你想发送其他 HTTP 请求方法,配置如下：
        ```java
            @Bean
            HttpFirewall httpFirewall() {
                StrictHttpFirewall firewall = new StrictHttpFirewall();
                firewall.setUnsafeAllowAnyHttpMethod(true);
                return firewall;
            }
        ```
      * 请求地址不能有分号
        比如 http://localhost:8080/hello;name=haha  是不行的。
        如果需要解除限制，配置如下：
        ```java
        @Bean
        HttpFirewall httpFirewall() {
            StrictHttpFirewall firewall = new StrictHttpFirewall();
            firewall.setAllowSemicolon(true);
            return firewall;
        }
        ```
      * 必须是标准化 URL
        路径不能包含： "./", "/../" or "/." 字符串
      * 必须是可打印的 ASCII 字符
      * 双斜杠不被允许
        如果需要解除限制，配置如下：
        ```java
            @Bean
            HttpFirewall httpFirewall() {
                StrictHttpFirewall firewall = new StrictHttpFirewall();
                firewall.setAllowUrlEncodedDoubleSlash(true);
                return firewall;
            }
        ```
      * % 不被允许 
        如果需要解除限制，配置如下：
        ```java
            @Bean
            HttpFirewall httpFirewall() {
                StrictHttpFirewall firewall = new StrictHttpFirewall();
                firewall.setAllowUrlEncodedPercent(true);
                return firewall;
            }
        ```
      * 正反斜杠不被允许  
        如果需要解除限制，配置如下：
        ```java
            @Bean
            HttpFirewall httpFirewall() {
                StrictHttpFirewall firewall = new StrictHttpFirewall();
                firewall.setAllowBackSlash(true);
                firewall.setAllowUrlEncodedSlash(true);
                return firewall;
            }
        ```      
      * . 不被允许
        ```java
            @Bean
            HttpFirewall httpFirewall() {
                StrictHttpFirewall firewall = new StrictHttpFirewall();
                firewall.setAllowUrlEncodedPeriod(true);
                return firewall;
            }
        ```   
      * 需要强调一点，上面所说的这些限制，都是针对请求的 requestURI 进行的限制，
        而不是针对请求参数。以下请求是合法的：
        http://localhost:8080/hello?param=aa%2ebb
        
  * charter8: 加密  
    * 没有示例代码 
    * BCryptPasswordEncoder (推荐)    
    * 步骤
      * 配置代码
      ```java
        @Bean
        PasswordEncoder passwordEncoder() {
            // 参数 10 就是 strength，即密钥的迭代次数。不配置，默认为 10
            return new BCryptPasswordEncoder(10);
        }
      ```    
      * 注册用户时需要加密
      ```java
          @Service
          public class RegService {
              public int reg(String username, String password) {
                  BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(10);
                  String encodePasswod = encoder.encode(password);
                  return saveToDb(username, encodePasswod);
              }
          }
      ```
      
  * charter9: 资源拦截
    * configure(WebSecurity web) : 拦截静态资源
    ```java
        @Override
        public void configure(WebSecurity web) throws Exception {
            web.ignoring().antMatchers("/css/**", "/js/**", "/index.html", "/img/**", "/fonts/**", "/favicon.ico", "/verifyCode");
        }
    ```  
          
    * configure(HttpSecurity http) : 拦截接口请求
    ```java
        http.authorizeRequests()
                .antMatchers("/hello").permitAll()
                .anyRequest().authenticated()
    ```    
  
  * charter10: 获取登录用户信息    
    * SecurityContextHolder.getContext().getAuthentication();
    * 在 controller 的方法注入 Authentication 对象
    ```java
        @GetMapping("/hr/info")
        public Hr getCurrentHr(Authentication authentication) {
            return ((Hr) authentication.getPrincipal());
        }
    ```    
    * 修改用户信息
      * 
    * 在子线程中获取用户信息
      * 默认情况下是不支持的。<br>
        需要在项目启动时通过修改系统变量: -Dspring.security.strategy=MODE_INHERITABLETHREADLOCAL
      * SecurityContextHolder 定义了三种存储策略
      ```java
        public class SecurityContextHolder {
         public static final String MODE_THREADLOCAL = "MODE_THREADLOCAL";
         public static final String MODE_INHERITABLETHREADLOCAL = "MODE_INHERITABLETHREADLOCAL";
         public static final String MODE_GLOBAL = "MODE_GLOBAL";
            ...
            ...
        }
      ```
    
  * charter11:四种权限控制方式   
    * 本示例采用 表达式控制方法权限   
    * 表达式控制 URL 路径权限
    
    |表达式|备注|
    |:---|:---|
    |hasRole|用户具备某个角色即可访问资源|
    |hasAnyRole|用户具备多个角色中的任意一个即可访问资源|
    |hasAuthority|类似于 hasRole|
    |hasAnyAuthority|类似于 hasAnyRole|
    |permitAll|统统允许访问|
    |denyAll|统统拒绝访问|
    |isAnonymous|判断是否匿名用户|
    |isAuthenticated|判断是否认证成功|
    |isRememberMe|判断是否通过记住我登录的|
    |isFullyAuthenticated|判断是否用户名/密码登录的|
    |principle|当前用户|
    |authentication|从 SecurityContext 中提取出来的用户对象| 
    
    配置方法：
      ```java
        protected void configure(HttpSecurity http) throws Exception {
            http.authorizeRequests()
                    .antMatchers("/admin/**").hasRole("admin")
                    .antMatchers("/user/**").hasAnyRole("admin", "user")
                    .anyRequest().authenticated()
                    .and()
                    ...
        }
       ``` 
    
    * 表达式控制方法权限【推荐】   
      * 需要开启注解的使用            
    ```java
        @Configuration
        @EnableGlobalMethodSecurity(prePostEnabled = true,securedEnabled = true)
        public class SecurityConfig extends WebSecurityConfigurerAdapter {
    
        }
    ```
    * 动态权限
    
    * 代码
      * charter11/*
      
  * charter12: 异常处理
    * 自定义认证异常
    
    * 自定义授权异常
    
    * configure(HttpSecurity http) 中配置自定义处理异常
    
    * 代码 
      * charter12/*, 
      * config/AccessDeniedHandlerExt.java, 
      * config/AuthenticationEntryPointExt.java
      
  * charter13: 配置多个过滤器链
    * 配置代码
      ```java
        @Configuration
        public class SecurityConfig {
            @Bean
            protected UserDetailsService userDetailsService() {
                InMemoryUserDetailsManager manager = new InMemoryUserDetailsManager();
                manager.createUser(User.withUsername("javaboy").password("{bcrypt}$2a$10$Sb1gAUH4wwazfNiqflKZve4Ubh.spJcxgHG8Cp29DeGya5zsHENqi").roles("admin", "aaa", "bbb").build());
                manager.createUser(User.withUsername("sang").password("{noop}123").roles("admin").build());
                manager.createUser(User.withUsername("江南一点雨").password("{MD5}{Wucj/L8wMTMzFi3oBKWsETNeXbMFaHZW9vCK9mahMHc=}4d43db282b36d7f0421498fdc693f2a2").roles("user", "aaa", "bbb").build());
                return manager;
            }
        
            @Configuration
            @Order(1)
            static class DefaultWebSecurityConfig extends WebSecurityConfigurerAdapter {
        
                @Override
                protected void configure(HttpSecurity http) throws Exception {
                    http.antMatcher("/foo/**")
                            .authorizeRequests()
                            .anyRequest().hasRole("admin")
                            .and()
                            .csrf().disable();
                }
            }
        
            @Configuration
            @Order(2)
            static class DefaultWebSecurityConfig2 extends WebSecurityConfigurerAdapter {
        
                @Override
                protected void configure(HttpSecurity http) throws Exception {
                    http.antMatcher("/bar/**")
                            .authorizeRequests()
                            .anyRequest().hasRole("user")
                            .and()
                            .formLogin()
                            .permitAll()
                            .and()
                            .csrf().disable();
                }
            }
        }
      ```  
  
  * charter14  
