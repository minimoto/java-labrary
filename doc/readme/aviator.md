# aviator-source 源码

* 原 aviator 不支持嵌套表达式，所以修改了源码
  * 新增 ExpressCustomize.java : 自定义用于内嵌的表达式模型
  * 新增 ILoadExpressCustomize.java : 用于提供加载自定义内嵌表达式的接口，通常是从数据库加载
  * 修改源码 AviatorJavaType.java ：
   ```java
    public Object getValueFromEnv(final String name, final boolean nameContainsDot,
                                 final Map<String, Object> env, final boolean throwExceptionNotFound) {
        if (env != null) {
          if (nameContainsDot && !env.containsKey(name) && RuntimeUtils.getInstance(env)
                  .getOptionValue(Options.ENABLE_PROPERTY_SYNTAX_SUGAR).bool) {
            if (this.subNames == null) {
              // cache the result
              this.subNames = Constants.SPLIT_PAT.split(name);
            }
            return getProperty(name, this.subNames, env, throwExceptionNotFound, false);
          }
          //todo 修改以下代码 by xujunqiang
          if(null == AviatorEvaluator.getInstance() || null == AviatorEvaluator.getInstance().getLoadExpressCustomize()){
            return env.get(name);
          }
          List<ExpressCustomize> expressCustomizes = AviatorEvaluator.getInstance().getLoadExpressCustomize().loadExpressCustomize();
          if(CollectionUtils.isEmpty(expressCustomizes)){
            return env.get(name);
          }
          String express = null;
          for(ExpressCustomize expressCustomize: expressCustomizes){
            if(expressCustomize.getName().equalsIgnoreCase(name)){
              express = expressCustomize.getExpress();
            }
          }
          if(null != express){
            return this.getValueFromFun(express, env);
          }
          return env.get(name);
        }
        return null;
      }

   ```

    * 修改源码 AviatorObject.java ：
    ```java
        public Object getValueFromFun(String express, Map<String, Object> env){
          return AviatorEvaluator.execute(express, env);
        }
    ```
  
    * 修改源码 AviatorEvaluatorInstance.java
    ```java
      private ILoadExpressCustomize loadExpressCustomize;
      
        public ILoadExpressCustomize getLoadExpressCustomize() {
          return loadExpressCustomize;
        }
      
        public void setLoadExpressCustomize(ILoadExpressCustomize loadExpressCustomize) {
          this.loadExpressCustomize = loadExpressCustomize;
        }
   ```
# aviator 测试
 * 测试代码见 aviator 模块 test 包下
   * 测试代码 AviatorApplicationTests.java
    ```java
       @SpringBootTest
        class AviatorApplicationTests {
        
            @Autowired
            AviatorExecutor aviatorExecutor;
        
            @Test
            public void test(){
                Map<String, Object> env1 = AviatorEvaluator.newEnv(
                        "x", 1, "y", 4, "z", 8,
                        "a", 20, "b", 40, "c", 80, "d", 2);
                //        System.out.println(AviatorExecutor.execute(AviatorContext.builder().expression("fun1").env(env1).build()));
                //        System.out.println(AviatorExecutor.execute(AviatorContext.builder().expression("fun2").env(env1).build()));
                System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("min(x)+fun2").env(env1).build()));
            }
        
        }
    ```
   * 创建 LoadExpress.java 
   * 创建 AviatorConfig.java
     ```java
        @Configuration
        public class AviatorConfig {
        
            @Bean
            AviatorExecutor aviatorExecutor(){
                AviatorExecutor aviatorExecutor = new AviatorExecutor(new LoadExpress());
                return aviatorExecutor;
            }
        }
     ```

   
