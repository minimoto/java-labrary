# camunda

* service task: 服务任务
   * 调用java ： 见 com.tplhk.task.invokjava.*
   * 外部服务：见 com.tplhk.task.external.*
  
*  receive Task (接收任务) 和 messageEvent (消息事件)
    * 示例代码：见 com.tplhk.app.receive.*
    * 流程图：见 doc.bpmn.receiveTask.*    
    * 都是接收消息
    * receive Task 是阻塞型的
    * receive Task 做为阻塞任务，需要先启动流程实例。
        *  本例(下图)通过 startReceiveProcess 模拟启动流程实例， 然后再执行 mockConsume : processInstanceId 必传
    *  ![image](../bpmn/receiveTask/学习ReceiveTask.png)
    * 做为事件（message start event），此时 message 名称全局唯一, 不需要启动流程实例。
        * 本例(下图)直接执行 mockConsume: processInstanceId 和 processInstanceKey 都不用传，但建议传 processInstanceKey
    *  ![image](../bpmn/receiveTask/学习ReceiveTask2.png)    
    * 接收任务和消息事件区别：一般来说，消息任务代替消息中间事件和结束事件，因为任务可以扩展事件。消息中间件通常与事件网关配合使用。

*  message event(消息事件)
    * 示例代码：无
    * 流程图：无   
    * 消息中间事件
        * 可以用 receive Task (接收任务) 代替，因为接收任务可以扩展各种事件。消息中间事件一般结合事件网关使用
    * 消息抛出事件
        * 可以用receive Task (接收任务) 代替
    * 消息结束事件
        * 可以用receive Task (接收任务) 代替 


*  error event(错误事件)
    * 示例代码：见 com.tplhk.app.trip.*
    * 流程图：   
    * 错误开始事件
        * 主流程的错误开始事件，只能捕获主流程的异常
        * 子流程的错误开始事件，只能捕获子流程的异常
    * 错误边界事件
        * 用于捕获错误结束事件
        * 用于捕获嵌入式子流程或调用活动的异常
        * 往上级抛异常 ( 实践中，我觉得应该用错误子流程来捕获异常，错误边界事件仅用于向上级抛异常或自定义异常)
        * 不能触发补偿事件，也不能捕获补偿事件的异常
    * 错误结束事件
        * 优先被错误子流程捕获
        * 被错误边界事件捕获
        
    * 实践1 
        * 示例图
            ![image](../bpmn/trip/trip错误事件捕获示例.png)
        * 如果 book hotal 抛异常，流程如下：book car -> book hotel -> 搞事1 -> 搞事2 -> 搞事3
        * 如果代码无异常，流程如下：book car -> book hotel -> book flight -> 搞事1 -> 搞事2 -> 搞事3

    * 实践2 
        * 示例图, 修改错误子流程最后状态为正常状态，所以本示例图的错误边界事件是没有意义的。  
            ![image](../bpmn/trip/trip错误事件捕获示例2.png)           
        * 如果 book hotal 抛异常，流程如下：book car -> book hotel -> 搞事1 -> 搞事5 -> 搞事4
   
*  error event && compensation event (错误事件,补偿事件)
    * 示例代码：见 com.tplhk.app.trip.*
    * 流程图：    
    * 本例主要讲解补偿事件
    * 子流程的异常由错误子流程捕获，捕获后通过抛出补偿事件来触发子流程补偿
    * 主流程的异常由主流程的错误子流程捕获，捕获后通过抛出补偿事件触发主流程的补偿
    * 补偿会向子流程补偿,如果子流程已补偿过，那就不会再补偿子流程了(实践1，2 得出的结论)
    * 只能补偿（子）任务成功的(示例6是不能补偿的，示例7才是正确的补偿方式)
    * 实践1 
        * 示例图
            ![image](../bpmn/trip/trip错误与补偿.png)
        * 如果 book hotal 抛异常，流程如下：
            book car -> book hotel -> cancelCar
        * 如果 book hotal , doSomething2 抛异常，流程如下：
            book car -> book hotel -> cancelCar -> doSomething1 -> doSomething2 -> cancel1
             
    * 实践2 
        * 示例图, 修改错误子流程,去掉补偿事件。  
            ![image](../bpmn/trip/trip错误与补偿2.png)           
        * 如果 book hotal , doSomething2 抛异常，流程如下：
            book car -> book hotel -> doSomething1 -> doSomething2 -> cancel1 -> cancelCar
        
*  cancel event && error event && compensation event (取消事件, 错误事件, 补偿事件)
    * 示例代码：见 com.tplhk.app.cancel.*
    * 流程图：    
    * 本例主要讲解取消事件
    * 取消结束事件只能与事务子流程结合使用。它会被取消边界事件捕获，然后自动触发补偿事件，
      对所有已完成的任务补偿后，沿着取消边界事件下一个任务继续执行。
   
    * 实践1 
        * 示例图
            ![image](../bpmn/cancel/取消&错误&补偿事件.png)
        * 流程如下：
            搞点事件5 -> 抛个错误 AppExcetion2 -> 回滚1 -> 搞点事件1 -> 回到接收消息任务
    * 实践2 
        * 示例图 不用取消事件也实现了相同的功能，但是取消事件会自动触发补偿事件
            ![image](../bpmn/cancel/取消&错误&补偿事件2.png)
        * 流程如下：
            搞点事件5 -> 抛个错误 AppExcetion2 -> 回滚1 -> 搞点事件1 -> 回到接收消息任务
    * 实践3 
        * 示例图 
            ![image](../bpmn/cancel/取消事件应用1.png)
        * 知识点：
            取消事件自动触发事务子流程里的补偿事件
    * 实践4 
        * 示例图 
            ![image](../bpmn/cancel/取消事件应用2.png)
        * 知识点：
            取消事件自动触发事务子流程里的补偿事件和主流程的回滚          
    * 实践5 
        * 示例图 
            ![image](../bpmn/cancel/取消事件应用3.png)
        * 知识点：
            错误中断事件不会触发补偿，并且补偿事件异常也不会被捕获到!         
    * 实践6 
        * 示例图 
            ![image](../bpmn/cancel/取消事件应用4.png)
        * 知识点：
            补偿事件是在任务成功执行完成之后，有可能会触发。
            这里子流程任务失败了，所以子流程中的补偿事件子流程不会触发 
    * 实践7 
        * 示例图 
            ![image](../bpmn/cancel/取消事件应用5.png)
            ![image](../bpmn/cancel/取消事件应用6.png)
        * 代码
        ```groovy
          def id=execution.getProcessInstanceId()
          // 是否出现异常信息
          def hasError=false
          if(execution.hasVariable("vrMsg")){
          	def errorMsg = execution.getVariable("vrMsg");
          	hasError=true;
          	println "流程实例:"+id+",出现异常："+errorMsg
          }
          //异常标识设置到变量
          execution.setVariable("hasError",hasError)
        ```    
        * 知识点：
            子流程的异常内部吃了，不报出来,补偿事件会触发。
            这里子流程任务失败了，所以子流程中的补偿事件子流程不会触发

* Camunda 分布式事务一致性2种实现
    * 事务子流程， 推荐使用
    * 事件子流程(错误事件+补偿事件),需要额外判断
     
*  escalation event (升级事件)
    * 示例代码：
    * 流程图：    
    * 升级开始事件有两个可选属性，escalationRef 和 escalationCodeVariable
   
    * 实践1 
        * 示例图
          ![image](../bpmn/escalation/升级事件1.png)
          ![image](../bpmn/escalation/升级事件2.png)
          
                      
*  callactivity (调用子活动) 
    * 示例代码：见 com.tplhk.app.callactivity.*
    * 流程图：见 doc.bpmn.callActivity.*     
    * 子流程可以取得主流程变量，但子流程变量需要在模型的 out mappings 映射出去才可以
    * 子流程无法修改主流程的变量值
    *  ![image](../bpmn/callActivity/CallActivity主流程.png)
    *  ![image](../bpmn/callActivity/CallActivity子流程.png)

*  payment (支付流程) 
    * 示例代码：见 com.tplhk.app.payment.*
    * 流程图：见 doc.bpmn.payment.*      
    * 一个服务任务与用户任务结合的示例
    * 子流程无法修改主流程的变量值
    *  ![image](../bpmn/payment/payment.png)

