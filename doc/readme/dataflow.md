#  dataflow

* 用于测试 192.168.1.126 k8s 集群环境中 dataflow 的例子
  
* 打包镜像名称: dataflow:param 
  * 测试多个 task 组合的场景传参
  * 测试异常时，通过入参返回码(exitCode) 决定是否执行下一个 task
  * dataflow-param 'COMPLETED'->dataflow 'FAILED'->dataflow2
  * --composed-task-arguments=--exitcode=fail,--spring.profiles.active=sit

* 元素表(用于异常记录)
  * 经过多次测试，先说 task_execution（exitcode, exitmessage, errormessage）
    如果批启动的时候报错，才会进入 onTaskFailed 方法，此时 exitcode ,errormessage  才会被系统自动赋值。
    目前发现只有多个实例启动时才会出现这种情况，因为我们限制了单个实例运行。
    而这种异常，是不会启动job 的，也就是说 batch_job_execution 不会有值。

  * 再说 batch_job_execution（status, exitcode, exitmessage) ，
    当我们的业务代码出现异常（如oom, 事务失败无法提交db ，或我们希望抛的自定义异常）会写入这个表。

  * 我觉得，批处理如果出现业务异常，系统会自动写 batch_job_execution,
    但是 task 却是正常结束
  （我还没有找到如何发出一个失败通知，让springbatch 监听回调 onTaskFailed ，
    只会这样springbatch 才会将此 task 状态设置为 fail  ）。

  * 在代码中，我们直接抛异常， 由系统 batch_job_execution（status, exitcode, exitmessage) 记录异常信息。
    在 onTaskEnd 方法中设置 task_execution.exitmessage (可以理解为只是一个标识字符串) ，
    用于决定依赖批中是否向下执行。
