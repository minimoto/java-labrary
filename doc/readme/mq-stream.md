# mq-stream

* rabbitmq
  * inputProductAdd， outputProductAdd， mytestInput， outputmytestInput  这两对生产消费者
    由于 routing-key-expression 表达式错误导致MQ发送不能接收，或发送错误的 routkey 也能接收, 原因未知。
    这个 demo 在 spring-cloud-stream-rabbitmq 这个 demo 中是没问题的，
    区别在于 spring-cloud-stream-rabbitmq 使用的是 1.5.2.RELEASE 版本，而本项目使用的是 2.3.1.RELEASE 版本

* rocketmq
  * 该demo 测试以下技术点
    * 发送普通消息
    * 发送延时消息
    * 发送事务消息
    * Tag 过滤消息
    * 发送顺序消息
    * 处理消费时异常
    * 死信队列
    * 重新消费历史数据 ( 通过界面 reset consumer offset, 重新处理的数据不会出现在重试队列)
  * 延时配置参数图
    * ![image](./doc/rocketmq-delay.png)  
    
  * 未解决
    * 消息轨迹  
  
  * mq 规范
    服务间消息(1:1)topic:
    hk_producerService_consumerService
    producerService是生产服务名
    consumerService是消费服务名
    eg:hk_tplhk_nb_tplhk_payment
    扇出型消息(1:N)topic：
    hk_producerService_public
    eg:hk_tplhk_policy_public
    扇入型消息(N:1)topic:
    hk_public_consumerService
    eg:hk_public_tplhk_log
    如果是有特殊要求加后缀，单独建topic
    _schedule(定时)  、 _sequence（顺序） 、 _transaction（事务）
    
    生产者：
    hk_producerService_producer
    eg:hk_tplhk_nb_producer

    消费者：
    hk_consumerService_consumer
    /mo_consumerService_consumer
    eg:hk_tplhk_payment_consumer
