# drools

* 示例 1

  * 把 excel 规则通过 freemarker 生成 drools 文件，然后将规则文件写入内存

  *　缺点：不能动态修改 excel 实时生成 drools 文件

* 示例 2

  * 直接导入整个 excel 文件到内存，可以动态修改 excel 规则。

  * 缺点：还未解决对多个 excel 文件导入的问题，目前一次只能导入一个 excel 文件

* core.service
 
  * 可以动态的增加规则到内存规则库
  * 支持多个规则库
  
   

