/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : provision

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:33:50
*/


-- ----------------------------
-- Table structure for t_collection_order
-- ----------------------------
DROP TABLE IF EXISTS "provision"."t_collection_order";
CREATE TABLE "provision"."t_collection_order" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "policy_no" varchar(50) COLLATE "pg_catalog"."default",
  "policy_id" varchar(50) COLLATE "pg_catalog"."default",
  "payment_no" varchar(50) COLLATE "pg_catalog"."default",
  "business_no" varchar(50) COLLATE "pg_catalog"."default",
  "business_type" varchar(4) COLLATE "pg_catalog"."default",
  "product_code" varchar(50) COLLATE "pg_catalog"."default",
  "policy_year" varchar(50) COLLATE "pg_catalog"."default",
  "fee_type" int2,
  "next_payment_date" timestamp(6),
  "currency" varchar(10) COLLATE "pg_catalog"."default",
  "amount" numeric(30,2) DEFAULT 0,
  "provision_status" varchar(1) COLLATE "pg_catalog"."default",
  "provision_time" date,
  "write_off_time" date,
  "send_provision_times" int2 DEFAULT 0,
  "send_write_off_times" int2 DEFAULT 0,
  "create_by" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "update_by" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "update_time" timestamp(6) NOT NULL
)
;
COMMENT ON COLUMN "provision"."t_collection_order"."id" IS '主键UUID';
COMMENT ON COLUMN "provision"."t_collection_order"."policy_no" IS '保单号';
COMMENT ON COLUMN "provision"."t_collection_order"."policy_id" IS '保单ID';
COMMENT ON COLUMN "provision"."t_collection_order"."payment_no" IS '收款订单号';
COMMENT ON COLUMN "provision"."t_collection_order"."business_no" IS '业务流水号';
COMMENT ON COLUMN "provision"."t_collection_order"."business_type" IS '业务类型:01承保02缴费频率变更03续期';
COMMENT ON COLUMN "provision"."t_collection_order"."product_code" IS '产品编号';
COMMENT ON COLUMN "provision"."t_collection_order"."policy_year" IS '保单年度';
COMMENT ON COLUMN "provision"."t_collection_order"."fee_type" IS '收费类型';
COMMENT ON COLUMN "provision"."t_collection_order"."next_payment_date" IS '到期日';
COMMENT ON COLUMN "provision"."t_collection_order"."currency" IS '币别';
COMMENT ON COLUMN "provision"."t_collection_order"."amount" IS '计提金额';
COMMENT ON COLUMN "provision"."t_collection_order"."provision_status" IS '计提状态:N等审核，Y计提，X提冲';
COMMENT ON COLUMN "provision"."t_collection_order"."provision_time" IS '计提入参时间';
COMMENT ON COLUMN "provision"."t_collection_order"."write_off_time" IS '提冲入参时间';
COMMENT ON COLUMN "provision"."t_collection_order"."send_provision_times" IS '发送计提次数';
COMMENT ON COLUMN "provision"."t_collection_order"."send_write_off_times" IS '发送提冲次数';
COMMENT ON TABLE "provision"."t_collection_order" IS '收费记录表';

-- ----------------------------
-- Primary Key structure for table t_collection_order
-- ----------------------------
ALTER TABLE "provision"."t_collection_order" ADD CONSTRAINT "t_collection_order_pkey" PRIMARY KEY ("id");
