-- 切换到新建的库下，建schema
create schema if not  exists  advance_interest;
create schema if not  exists  annual;
create schema if not  exists  data_flow;
create schema if not  exists  ilog;
create schema if not  exists  provision;
create schema if not  exists  renewal;
create schema if not  exists  slq;
create schema if not  exists  underwriting;

ALTER SCHEMA "advance_interest" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema advance_interest to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "annual" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema annual to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "data_flow" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema data_flow to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "ilog" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema ilog to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "provision" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema provision to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "public" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema public to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "renewal" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema renewal to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "slq" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema slq to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

ALTER SCHEMA "underwriting" OWNER TO "tplmo_data_flow_ddl_user";

GRANT USAGE on  schema underwriting to  tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;


