CREATE ROLE "tplhk_ddl_user" with password 'Taiping@8888' LOGIN;
CREATE ROLE "tplhk_dml_user" with password 'Taiping@8888' LOGIN;
CREATE ROLE "thksales" with password 'Taiping@8888' LOGIN;
CREATE ROLE "readonly" with password 'Taiping@8888' LOGIN;

--1. root
create database cntaiping_admin WITH OWNER tplhk_ddl_user ENCODING = UTF8;



-- 2. ddl-cntaiping_admin_uat1
create schema if not exists cntaiping_admin_uat1;
ALTER
SCHEMA cntaiping_admin_uat1 OWNER TO tplhk_ddl_user;

GRANT USAGE ON SCHEMA
cntaiping_admin_uat1 TO tplhk_ddl_user,tplhk_dml_user,thksales,readonly;


-- tableĬ��Ȩ��
ALTER DEFAULT PRIVILEGES IN SCHEMA cntaiping_admin_uat1 grant SELECT ON TABLES to readonly;
ALTER DEFAULT PRIVILEGES IN SCHEMA cntaiping_admin_uat1 grant SELECT,UPDATE,DELETE,INSERT ON TABLES to tplhk_ddl_user,tplhk_dml_user,thksales;
-- sequenceĬ��Ȩ��
ALTER DEFAULT PRIVILEGES IN SCHEMA  cntaiping_admin_uat1 GRANT USAGE,SELECT ON SEQUENCES to tplhk_ddl_user,tplhk_dml_user,thksales,readonly;
-- functionĬ��Ȩ��
ALTER DEFAULT PRIVILEGES IN SCHEMA  cntaiping_admin_uat1 GRANT EXECUTE ON FUNCTIONS to tplhk_ddl_user,tplhk_dml_user,thksales,readonly;



-- 3. root-cntaiping_admin_uat1-public
-- ��չ��װ��public �²ſ���ͨ���ڸ���schema
CREATE
EXTENSION if not exists pgcrypto;
CREATE
EXTENSION if not exists "uuid-ossp";



