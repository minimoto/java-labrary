begin;



-- ----------------------------
-- Table structure for app_registration
-- ----------------------------
DROP TABLE IF EXISTS "public"."app_registration";
CREATE TABLE "public"."app_registration" (
  "id" int8 NOT NULL,
  "object_version" int8,
  "default_version" bool,
  "metadata_uri" text COLLATE "pg_catalog"."default",
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "type" int4,
  "uri" text COLLATE "pg_catalog"."default",
  "version" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table app_registration
-- ----------------------------
ALTER TABLE "public"."app_registration" ADD CONSTRAINT "app_registration_pkey" PRIMARY KEY ("id");






-- ----------------------------
-- Table structure for audit_records
-- ----------------------------
DROP TABLE IF EXISTS "public"."audit_records";
CREATE TABLE "public"."audit_records" (
  "id" int8 NOT NULL,
  "audit_action" int8,
  "audit_data" text COLLATE "pg_catalog"."default",
  "audit_operation" int8,
  "correlation_id" varchar(255) COLLATE "pg_catalog"."default",
  "created_by" varchar(255) COLLATE "pg_catalog"."default",
  "created_on" timestamp(6),
  "platform_name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table audit_records
-- ----------------------------
ALTER TABLE "public"."audit_records" ADD CONSTRAINT "audit_records_pkey" PRIMARY KEY ("id");










-- ----------------------------
-- Table structure for batch_job_instance
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_job_instance";
CREATE TABLE "public"."batch_job_instance" (
  "job_instance_id" int8 NOT NULL,
  "version" int8,
  "job_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "job_key" varchar(32) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Uniques structure for table batch_job_instance
-- ----------------------------
ALTER TABLE "public"."batch_job_instance" ADD CONSTRAINT "job_inst_un" UNIQUE ("job_name", "job_key");

-- ----------------------------
-- Primary Key structure for table batch_job_instance
-- ----------------------------
ALTER TABLE "public"."batch_job_instance" ADD CONSTRAINT "batch_job_instance_pkey" PRIMARY KEY ("job_instance_id");






-- ----------------------------
-- Table structure for batch_job_execution
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_job_execution";
CREATE TABLE "public"."batch_job_execution" (
  "job_execution_id" int8 NOT NULL,
  "version" int8,
  "job_instance_id" int8 NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "start_time" timestamp(6),
  "end_time" timestamp(6),
  "status" varchar(10) COLLATE "pg_catalog"."default",
  "exit_code" varchar(2500) COLLATE "pg_catalog"."default",
  "exit_message" varchar(2500) COLLATE "pg_catalog"."default",
  "last_updated" timestamp(6),
  "job_configuration_location" varchar(2500) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table batch_job_execution
-- ----------------------------
ALTER TABLE "public"."batch_job_execution" ADD CONSTRAINT "batch_job_execution_pkey" PRIMARY KEY ("job_execution_id");

-- ----------------------------
-- Foreign Keys structure for table batch_job_execution
-- ----------------------------
ALTER TABLE "public"."batch_job_execution" ADD CONSTRAINT "job_inst_exec_fk" FOREIGN KEY ("job_instance_id") REFERENCES "public"."batch_job_instance" ("job_instance_id") ON DELETE NO ACTION ON UPDATE NO ACTION;






-- ----------------------------
-- Table structure for batch_job_execution_context
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_job_execution_context";
CREATE TABLE "public"."batch_job_execution_context" (
  "job_execution_id" int8 NOT NULL,
  "short_context" varchar(2500) COLLATE "pg_catalog"."default" NOT NULL,
  "serialized_context" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table batch_job_execution_context
-- ----------------------------
ALTER TABLE "public"."batch_job_execution_context" ADD CONSTRAINT "batch_job_execution_context_pkey" PRIMARY KEY ("job_execution_id");

-- ----------------------------
-- Foreign Keys structure for table batch_job_execution_context
-- ----------------------------
ALTER TABLE "public"."batch_job_execution_context" ADD CONSTRAINT "job_exec_ctx_fk" FOREIGN KEY ("job_execution_id") REFERENCES "public"."batch_job_execution" ("job_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;






-- ----------------------------
-- Table structure for batch_job_execution_params
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_job_execution_params";
CREATE TABLE "public"."batch_job_execution_params" (
  "job_execution_id" int8 NOT NULL,
  "type_cd" varchar(6) COLLATE "pg_catalog"."default" NOT NULL,
  "key_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "string_val" varchar(2500) COLLATE "pg_catalog"."default",
  "date_val" timestamp(6),
  "long_val" int8,
  "double_val" float8,
  "identifying" char(1) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Foreign Keys structure for table batch_job_execution_params
-- ----------------------------
ALTER TABLE "public"."batch_job_execution_params" ADD CONSTRAINT "job_exec_params_fk" FOREIGN KEY ("job_execution_id") REFERENCES "public"."batch_job_execution" ("job_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;








-- ----------------------------
-- Table structure for batch_step_execution
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_step_execution";
CREATE TABLE "public"."batch_step_execution" (
  "step_execution_id" int8 NOT NULL,
  "version" int8 NOT NULL,
  "step_name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "job_execution_id" int8 NOT NULL,
  "start_time" timestamp(6) NOT NULL,
  "end_time" timestamp(6),
  "status" varchar(10) COLLATE "pg_catalog"."default",
  "commit_count" int8,
  "read_count" int8,
  "filter_count" int8,
  "write_count" int8,
  "read_skip_count" int8,
  "write_skip_count" int8,
  "process_skip_count" int8,
  "rollback_count" int8,
  "exit_code" varchar(2500) COLLATE "pg_catalog"."default",
  "exit_message" varchar(2500) COLLATE "pg_catalog"."default",
  "last_updated" timestamp(6)
)
;

-- ----------------------------
-- Primary Key structure for table batch_step_execution
-- ----------------------------
ALTER TABLE "public"."batch_step_execution" ADD CONSTRAINT "batch_step_execution_pkey" PRIMARY KEY ("step_execution_id");

-- ----------------------------
-- Foreign Keys structure for table batch_step_execution
-- ----------------------------
ALTER TABLE "public"."batch_step_execution" ADD CONSTRAINT "job_exec_step_fk" FOREIGN KEY ("job_execution_id") REFERENCES "public"."batch_job_execution" ("job_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;








-- ----------------------------
-- Table structure for batch_step_execution_context
-- ----------------------------
DROP TABLE IF EXISTS "public"."batch_step_execution_context";
CREATE TABLE "public"."batch_step_execution_context" (
  "step_execution_id" int8 NOT NULL,
  "short_context" varchar(2500) COLLATE "pg_catalog"."default" NOT NULL,
  "serialized_context" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table batch_step_execution_context
-- ----------------------------
ALTER TABLE "public"."batch_step_execution_context" ADD CONSTRAINT "batch_step_execution_context_pkey" PRIMARY KEY ("step_execution_id");

-- ----------------------------
-- Foreign Keys structure for table batch_step_execution_context
-- ----------------------------
ALTER TABLE "public"."batch_step_execution_context" ADD CONSTRAINT "step_exec_ctx_fk" FOREIGN KEY ("step_execution_id") REFERENCES "public"."batch_step_execution" ("step_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;








-- ----------------------------
-- Table structure for flyway_schema_history_dataflow
-- ----------------------------
DROP TABLE IF EXISTS "public"."flyway_schema_history_dataflow";
CREATE TABLE "public"."flyway_schema_history_dataflow" (
  "installed_rank" int4 NOT NULL,
  "version" varchar(50) COLLATE "pg_catalog"."default",
  "description" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "script" varchar(1000) COLLATE "pg_catalog"."default" NOT NULL,
  "checksum" int4,
  "installed_by" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "installed_on" timestamp(6) NOT NULL DEFAULT now(),
  "execution_time" int4 NOT NULL,
  "success" bool NOT NULL
)
;

-- ----------------------------
-- Indexes structure for table flyway_schema_history_dataflow
-- ----------------------------
CREATE INDEX "flyway_schema_history_dataflow_s_idx" ON "public"."flyway_schema_history_dataflow" USING btree (
  "success" "pg_catalog"."bool_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table flyway_schema_history_dataflow
-- ----------------------------
ALTER TABLE "public"."flyway_schema_history_dataflow" ADD CONSTRAINT "flyway_schema_history_dataflow_pk" PRIMARY KEY ("installed_rank");








-- ----------------------------
-- Table structure for stream_definitions
-- ----------------------------
DROP TABLE IF EXISTS "public"."stream_definitions";
CREATE TABLE "public"."stream_definitions" (
  "definition_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "definition" text COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "original_definition" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table stream_definitions
-- ----------------------------
ALTER TABLE "public"."stream_definitions" ADD CONSTRAINT "stream_definitions_pkey" PRIMARY KEY ("definition_name");








-- ----------------------------
-- Table structure for task_definitions
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_definitions";
CREATE TABLE "public"."task_definitions" (
  "definition_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "definition" text COLLATE "pg_catalog"."default",
  "description" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table task_definitions
-- ----------------------------
ALTER TABLE "public"."task_definitions" ADD CONSTRAINT "task_definitions_pkey" PRIMARY KEY ("definition_name");








-- ----------------------------
-- Table structure for task_deployment
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_deployment";
CREATE TABLE "public"."task_deployment" (
  "id" int8 NOT NULL,
  "object_version" int8,
  "task_deployment_id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "task_definition_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "platform_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_on" timestamp(6)
)
;

-- ----------------------------
-- Primary Key structure for table task_deployment
-- ----------------------------
ALTER TABLE "public"."task_deployment" ADD CONSTRAINT "task_deployment_pkey" PRIMARY KEY ("id");








-- ----------------------------
-- Table structure for task_execution
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_execution";
CREATE TABLE "public"."task_execution" (
  "task_execution_id" int8 NOT NULL,
  "start_time" timestamp(6),
  "end_time" timestamp(6),
  "task_name" varchar(100) COLLATE "pg_catalog"."default",
  "exit_code" int4,
  "exit_message" varchar(2500) COLLATE "pg_catalog"."default",
  "error_message" varchar(2500) COLLATE "pg_catalog"."default",
  "last_updated" timestamp(6),
  "external_execution_id" varchar(255) COLLATE "pg_catalog"."default",
  "parent_execution_id" int8
)
;

-- ----------------------------
-- Primary Key structure for table task_execution
-- ----------------------------
ALTER TABLE "public"."task_execution" ADD CONSTRAINT "task_execution_pkey" PRIMARY KEY ("task_execution_id");




-- ----------------------------
-- Table structure for task_execution_metadata
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_execution_metadata";
CREATE TABLE "public"."task_execution_metadata" (
  "id" int8 NOT NULL,
  "task_execution_id" int8 NOT NULL,
  "task_execution_manifest" text COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Primary Key structure for table task_execution_metadata
-- ----------------------------
ALTER TABLE "public"."task_execution_metadata" ADD CONSTRAINT "task_execution_metadata_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table task_execution_metadata
-- ----------------------------
ALTER TABLE "public"."task_execution_metadata" ADD CONSTRAINT "task_metadata_fk" FOREIGN KEY ("task_execution_id") REFERENCES "public"."task_execution" ("task_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;







-- ----------------------------
-- Table structure for task_execution_params
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_execution_params";
CREATE TABLE "public"."task_execution_params" (
  "task_execution_id" int8 NOT NULL,
  "task_param" varchar(2500) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Foreign Keys structure for table task_execution_params
-- ----------------------------
ALTER TABLE "public"."task_execution_params" ADD CONSTRAINT "task_exec_params_fk" FOREIGN KEY ("task_execution_id") REFERENCES "public"."task_execution" ("task_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;







-- ----------------------------
-- Table structure for task_lock
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_lock";
CREATE TABLE "public"."task_lock" (
  "lock_key" char(36) COLLATE "pg_catalog"."default" NOT NULL,
  "region" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "client_id" char(36) COLLATE "pg_catalog"."default",
  "created_date" timestamp(6) NOT NULL
)
;

-- ----------------------------
-- Primary Key structure for table task_lock
-- ----------------------------
ALTER TABLE "public"."task_lock" ADD CONSTRAINT "lock_pk" PRIMARY KEY ("lock_key", "region");







-- ----------------------------
-- Table structure for task_task_batch
-- ----------------------------
DROP TABLE IF EXISTS "public"."task_task_batch";
CREATE TABLE "public"."task_task_batch" (
  "task_execution_id" int8 NOT NULL,
  "job_execution_id" int8 NOT NULL
)
;

-- ----------------------------
-- Foreign Keys structure for table task_task_batch
-- ----------------------------
ALTER TABLE "public"."task_task_batch" ADD CONSTRAINT "task_exec_batch_fk" FOREIGN KEY ("task_execution_id") REFERENCES "public"."task_execution" ("task_execution_id") ON DELETE NO ACTION ON UPDATE NO ACTION;


commit;