grant connect on database tplmo_data_flow to tplmo_data_flow_ddl_role,tplmo_data_flow_dml_role,tplmo_data_flow_readonly_role;

-- 修改库的拥有者
alter database tplmo_data_flow owner to tplmo_data_flow_ddl_role;