/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : advance_interest

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:29:04
*/


-- ----------------------------
-- Table structure for t_advance_interest_amount
-- ----------------------------
DROP TABLE IF EXISTS "advance_interest"."t_advance_interest_amount";
CREATE TABLE "advance_interest"."t_advance_interest_amount" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "business_no" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "policy_no" varchar(50) COLLATE "pg_catalog"."default",
  "product_code" varchar(20) COLLATE "pg_catalog"."default",
  "account_type" varchar(50) COLLATE "pg_catalog"."default",
  "pre_type" varchar(50) COLLATE "pg_catalog"."default",
  "balance" numeric(20,2) DEFAULT NULL::numeric,
  "current_interest" numeric(20,2) DEFAULT NULL::numeric,
  "policy_year" int2,
  "policy_currency" varchar(20) COLLATE "pg_catalog"."default",
  "dividend_option" varchar(50) COLLATE "pg_catalog"."default",
  "interest_amount" numeric(20,2) DEFAULT NULL::numeric,
  "balance_amount" numeric(20,2) DEFAULT NULL::numeric,
  "invok_status" varchar(1) COLLATE "pg_catalog"."default",
  "date_param" date,
  "create_by" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "create_time" timestamp(6) NOT NULL,
  "update_by" varchar COLLATE "pg_catalog"."default" NOT NULL,
  "update_time" timestamp(6) NOT NULL
)
;
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."id" IS '主键UUID';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."business_no" IS '业务编号';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."policy_no" IS '保单号';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."product_code" IS '产品编号';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."account_type" IS '账户类型';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."pre_type" IS '预缴利息类型：年缴YEAR，日缴DAYS';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."balance" IS '预付保费余额';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."current_interest" IS '预付保费利息余额';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."policy_year" IS '保单年度';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."policy_currency" IS '币种';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."dividend_option" IS '红利/现金分配方式';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."interest_amount" IS '发放利息金额';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."balance_amount" IS '发放本金';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."invok_status" IS '调用服务：N失败，P调产品工厂取得利息成功, I调账户服务利息发放成功';
COMMENT ON COLUMN "advance_interest"."t_advance_interest_amount"."date_param" IS '入参日期';
COMMENT ON TABLE "advance_interest"."t_advance_interest_amount" IS '预付利息对账表';

-- ----------------------------
-- Primary Key structure for table t_advance_interest_amount
-- ----------------------------
ALTER TABLE "advance_interest"."t_advance_interest_amount" ADD CONSTRAINT "t_advance_interest_amount_pkey" PRIMARY KEY ("id");
