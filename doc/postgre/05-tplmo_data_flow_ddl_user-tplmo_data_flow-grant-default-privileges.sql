begin;
-- public
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA renewal GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA renewal GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA renewal GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA renewal GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;


ALTER DEFAULT PRIVILEGES IN SCHEMA slq GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA slq GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA slq GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA slq GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;


ALTER DEFAULT PRIVILEGES IN SCHEMA annual GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA annual GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA annual GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA annual GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;


ALTER DEFAULT PRIVILEGES IN SCHEMA provision GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA provision GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA provision GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA provision GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA advance_interest GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA advance_interest GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA advance_interest GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA advance_interest GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA ilog GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA ilog GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA ilog GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA ilog GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA underwriting GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA underwriting GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA underwriting GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA underwriting GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

ALTER DEFAULT PRIVILEGES IN SCHEMA data_flow GRANT SELECT ON TABLES to tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA data_flow GRANT SELECT,UPDATE,DELETE,INSERT ON TABLES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA data_flow GRANT USAGE,SELECT ON SEQUENCES to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA data_flow GRANT EXECUTE ON FUNCTIONS to tplmo_data_flow_ddl_user,tplmo_data_flow_dml_user,tplmo_data_flow_readonly_user;

commit;





