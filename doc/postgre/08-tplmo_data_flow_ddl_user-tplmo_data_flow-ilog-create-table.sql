/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : ilog

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:33:30
*/


-- ----------------------------
-- Table structure for batch_ilog
-- ----------------------------
DROP TABLE IF EXISTS "ilog"."batch_ilog";
CREATE TABLE "ilog"."batch_ilog" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "policy_id" varchar(64) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "success_ind" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "create_time" timestamp(6) DEFAULT NULL::timestamp without time zone,
  "update_time" timestamp(6) DEFAULT NULL::timestamp without time zone,
  "create_by" varchar(8) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "update_by" varchar(8) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "fail_times" int2
)
;
COMMENT ON COLUMN "ilog"."batch_ilog"."policy_id" IS '保单id';
COMMENT ON COLUMN "ilog"."batch_ilog"."success_ind" IS '成功或失败标识';
COMMENT ON COLUMN "ilog"."batch_ilog"."fail_times" IS '失败次数';

-- ----------------------------
-- Primary Key structure for table batch_ilog
-- ----------------------------
ALTER TABLE "ilog"."batch_ilog" ADD CONSTRAINT "batch_ilog_pkey" PRIMARY KEY ("id");
