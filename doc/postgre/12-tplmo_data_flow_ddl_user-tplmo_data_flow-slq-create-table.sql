/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : slq

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:43:47
*/


-- ----------------------------
-- Table structure for slq_letter
-- ----------------------------
DROP TABLE IF EXISTS "slq"."slq_letter";
CREATE TABLE "slq"."slq_letter" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "slq_id" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "success_ind" varchar(64) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT NULL::timestamp without time zone,
  "update_time" timestamp(6) DEFAULT NULL::timestamp without time zone,
  "create_by" varchar(8) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying,
  "update_by" varchar(8) COLLATE "pg_catalog"."default" NOT NULL DEFAULT NULL::character varying
)
;
COMMENT ON COLUMN "slq"."slq_letter"."slq_id" IS '问题件id';
COMMENT ON COLUMN "slq"."slq_letter"."success_ind" IS '成功或者失败标识';
