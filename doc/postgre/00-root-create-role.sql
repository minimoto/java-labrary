-- root执行

-- 创建角色和用户

CREATE ROLE "tplmo_data_flow_ddl_role";
CREATE ROLE "tplmo_data_flow_dml_role";
CREATE ROLE "tplmo_data_flow_readonly_role";

CREATE ROLE "tplmo_data_flow_ddl_user" with password 'Taiping@9999' LOGIN;
CREATE ROLE "tplmo_data_flow_dml_user" with password 'Taiping@9999' LOGIN;
CREATE ROLE "tplmo_data_flow_readonly_user" with password 'Taiping@9999' LOGIN;

GRANT "tplmo_data_flow_ddl_role" TO "tplmo_data_flow_ddl_user";
GRANT "tplmo_data_flow_dml_role" TO "tplmo_data_flow_dml_user";
GRANT "tplmo_data_flow_readonly_role" TO "tplmo_data_flow_readonly_user";


