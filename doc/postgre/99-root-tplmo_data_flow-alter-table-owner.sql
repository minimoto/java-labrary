-- 注意每次都要更新表的拥有者
begin;
alter table public.app_registration owner to tplmo_data_flow_ddl_role;
alter table public.audit_records owner to tplmo_data_flow_ddl_role;
alter table public.batch_job_execution owner to tplmo_data_flow_ddl_role;
alter table public.batch_job_execution_context owner to tplmo_data_flow_ddl_role;
alter table public.batch_job_execution_params owner to tplmo_data_flow_ddl_role;
alter table public.batch_job_instance owner to tplmo_data_flow_ddl_role;
alter table public.batch_step_execution owner to tplmo_data_flow_ddl_role;
alter table public.batch_step_execution_context owner to tplmo_data_flow_ddl_role;
alter table public.flyway_schema_history_dataflow owner to tplmo_data_flow_ddl_role;
alter table public.stream_definitions owner to tplmo_data_flow_ddl_role;
alter table public.task_definitions owner to tplmo_data_flow_ddl_role;
alter table public.task_deployment owner to tplmo_data_flow_ddl_role;
alter table public.task_execution owner to tplmo_data_flow_ddl_role;
alter table public.task_execution_metadata owner to tplmo_data_flow_ddl_role;
alter table public.task_execution_params owner to tplmo_data_flow_ddl_role;
alter table public.task_lock owner to tplmo_data_flow_ddl_role;
alter table public.task_task_batch owner to tplmo_data_flow_ddl_role;

alter table advance_interest.t_advance_interest_amount owner to tplmo_data_flow_ddl_role;

alter table annual.t_annual_benefit_dtl owner to tplmo_data_flow_ddl_role;
alter table annual.t_annual_explanation owner to tplmo_data_flow_ddl_role;
alter table annual.t_annual_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_annual_policy_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_annual_type_tbl owner to tplmo_data_flow_ddl_role;
alter table annual.t_claim_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_financial_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_future_value_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_loan_info owner to tplmo_data_flow_ddl_role;
alter table annual.t_payment_record owner to tplmo_data_flow_ddl_role;

alter table ilog.batch_ilog owner to tplmo_data_flow_ddl_role;

alter table provision.t_collection_order owner to tplmo_data_flow_ddl_role;

alter table renewal.t_non_forfeiture_policy owner to tplmo_data_flow_ddl_role;
alter table renewal.t_non_forfeiture_product owner to tplmo_data_flow_ddl_role;
alter table renewal.t_renewal_bill owner to tplmo_data_flow_ddl_role;
alter table renewal.t_renewal_bill_detail owner to tplmo_data_flow_ddl_role;
alter table renewal.t_renewal_bill_record owner to tplmo_data_flow_ddl_role;

alter table slq.slq_letter owner to tplmo_data_flow_ddl_role;

alter table underwriting.t_expect_underwriting_record owner to tplmo_data_flow_ddl_role;


commit;

