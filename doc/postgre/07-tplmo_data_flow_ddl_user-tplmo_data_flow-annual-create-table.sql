/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : annual

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:29:44
*/

begin;
-- ----------------------------
-- Table structure for t_annual_benefit_dtl
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_annual_benefit_dtl";
CREATE TABLE "annual"."t_annual_benefit_dtl" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "exist_supplementary" varchar(50) COLLATE "pg_catalog"."default",
  "choose_protection_amount" varchar(50) COLLATE "pg_catalog"."default",
  "plan_name" varchar(50) COLLATE "pg_catalog"."default",
  "refer_to_table" numeric(30,2),
  "effective_date" varchar(50) COLLATE "pg_catalog"."default",
  "premium_paidup_date" varchar(50) COLLATE "pg_catalog"."default",
  "maturity_date" varchar(50) COLLATE "pg_catalog"."default",
  "modal_premium" numeric(30,2),
  "basic" char(1) COLLATE "pg_catalog"."default",
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "status" int4,
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."exist_supplementary" IS '是否存在附加契约标识';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."choose_protection_amount" IS '身故保障 /基本保額 显示   1：显示基本保额  2：显示名義金額';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."plan_name" IS '计划名称';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."refer_to_table" IS '表头：身故保障 /基本保額 对应的值';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."effective_date" IS '生效日期';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."premium_paidup_date" IS '保费付清日';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."maturity_date" IS '保费期满日';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."modal_premium" IS '每期保費';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."basic" IS '是否basicPlan Y 表示basicPlan';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_annual_benefit_dtl"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_annual_benefit_dtl
-- ----------------------------
ALTER TABLE "annual"."t_annual_benefit_dtl" ADD CONSTRAINT "t_annual_benefit_dtl_pkey" PRIMARY KEY ("id");








-- ----------------------------
-- Table structure for t_annual_explanation
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_annual_explanation";
CREATE TABLE "annual"."t_annual_explanation" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "prepaid_method" varchar(255) COLLATE "pg_catalog"."default",
  "prepaid_premium_rate" numeric(30,9),
  "select_rate" varchar(50) COLLATE "pg_catalog"."default",
  "sb_rate" numeric(30,9),
  "cb_rate" numeric(30,9),
  "an_rate" numeric(30,9),
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "sequence" varchar(80) COLLATE "pg_catalog"."default",
  "status" int4
)
;
COMMENT ON COLUMN "annual"."t_annual_explanation"."prepaid_method" IS '预缴保费新、旧逻辑判别  1:new  2：old 当处于预缴时，该节点传值';
COMMENT ON COLUMN "annual"."t_annual_explanation"."prepaid_premium_rate" IS '预付保费息率';
COMMENT ON COLUMN "annual"."t_annual_explanation"."select_rate" IS '現金收益或利息率选择';
COMMENT ON COLUMN "annual"."t_annual_explanation"."sb_rate" IS '保證現金獎賞/ 保證現金利率';
COMMENT ON COLUMN "annual"."t_annual_explanation"."cb_rate" IS '现金红利利率';
COMMENT ON COLUMN "annual"."t_annual_explanation"."an_rate" IS '年金利率';
COMMENT ON COLUMN "annual"."t_annual_explanation"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_annual_explanation"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_annual_explanation"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_annual_explanation"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_annual_explanation"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_annual_explanation"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_annual_explanation
-- ----------------------------
ALTER TABLE "annual"."t_annual_explanation" ADD CONSTRAINT "t_annual_explanation_pkey" PRIMARY KEY ("id");






-- ----------------------------
-- Table structure for t_annual_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_annual_info";
CREATE TABLE "annual"."t_annual_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "servicing_producer_code" varchar(30) COLLATE "pg_catalog"."default",
  "servicing_producer_name" varchar(255) COLLATE "pg_catalog"."default",
  "servicing_producer_office_tel" varchar(30) COLLATE "pg_catalog"."default",
  "start_date" varchar(50) COLLATE "pg_catalog"."default",
  "end_date" varchar(50) COLLATE "pg_catalog"."default",
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "exist_loan" varchar(50) COLLATE "pg_catalog"."default",
  "exist_rate_disclosure" varchar(50) COLLATE "pg_catalog"."default",
  "prepayment_interest_rate" numeric(30,9),
  "policy_loan_rate" numeric(30,9),
  "automatic_premium_loan_rate" numeric(30,9),
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "rep_month" varchar(10) COLLATE "pg_catalog"."default",
  "policy_owner_name" varchar(50) COLLATE "pg_catalog"."default",
  "status" int4,
  "data_times" int4,
  "dept_code" varchar(255) COLLATE "pg_catalog"."default",
  "output_type" varchar(255) COLLATE "pg_catalog"."default",
  "template_type" varchar(255) COLLATE "pg_catalog"."default",
  "print_type" varchar(255) COLLATE "pg_catalog"."default",
  "email_id" varchar(10) COLLATE "pg_catalog"."default",
  "watermark_id" varchar(10) COLLATE "pg_catalog"."default",
  "deliver_type" varchar(255) COLLATE "pg_catalog"."default",
  "post_dept_code" varchar(255) COLLATE "pg_catalog"."default",
  "receiver_name" varchar(255) COLLATE "pg_catalog"."default",
  "receiver_address" varchar(255) COLLATE "pg_catalog"."default",
  "receiver_phone" varchar(255) COLLATE "pg_catalog"."default",
  "statement_date" varchar(50) COLLATE "pg_catalog"."default",
  "exist_future_value" varchar(255) COLLATE "pg_catalog"."default",
  "sequence" varchar(80) COLLATE "pg_catalog"."default",
  "is_saved" varchar(10) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_annual_info"."servicing_producer_code" IS '保险中介人编号';
COMMENT ON COLUMN "annual"."t_annual_info"."servicing_producer_name" IS '保险中介人姓名';
COMMENT ON COLUMN "annual"."t_annual_info"."servicing_producer_office_tel" IS '保险中介人联络电话';
COMMENT ON COLUMN "annual"."t_annual_info"."start_date" IS '年报开始时间';
COMMENT ON COLUMN "annual"."t_annual_info"."end_date" IS '年报结束时间';
COMMENT ON COLUMN "annual"."t_annual_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_annual_info"."exist_loan" IS '是否存在贷款交易摘要标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_annual_info"."exist_rate_disclosure" IS '是否存在息率披露标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_annual_info"."prepayment_interest_rate" IS '预付保费利率';
COMMENT ON COLUMN "annual"."t_annual_info"."policy_loan_rate" IS '保单贷款息率';
COMMENT ON COLUMN "annual"."t_annual_info"."automatic_premium_loan_rate" IS '自動保費貸款息率';
COMMENT ON COLUMN "annual"."t_annual_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_annual_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_annual_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_annual_info"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_annual_info"."rep_month" IS '年报月份  格式202012';
COMMENT ON COLUMN "annual"."t_annual_info"."policy_owner_name" IS '保单持有人姓名';
COMMENT ON COLUMN "annual"."t_annual_info"."data_times" IS '打印次数';
COMMENT ON COLUMN "annual"."t_annual_info"."dept_code" IS '机构代码';
COMMENT ON COLUMN "annual"."t_annual_info"."output_type" IS '输出类型，取值说明：1：纸质；2：电子；3：纸质+电子';
COMMENT ON COLUMN "annual"."t_annual_info"."template_type" IS '模板类型 取值说明：Chi：中文繁体；Eng：英文;Chi_Eng：中文繁体及英文';
COMMENT ON COLUMN "annual"."t_annual_info"."print_type" IS '打印类型，0：首次打印；1：补打；2：保全补打';
COMMENT ON COLUMN "annual"."t_annual_info"."email_id" IS '邮件发送标识：1：发送电子邮件；2：不发送电子邮件';
COMMENT ON COLUMN "annual"."t_annual_info"."watermark_id" IS '1：副版；2：原版';
COMMENT ON COLUMN "annual"."t_annual_info"."deliver_type" IS '邮寄方式，1：直邮（邮寄给投保人）；2：非直邮（邮寄给保险公司）';
COMMENT ON COLUMN "annual"."t_annual_info"."post_dept_code" IS '邮寄网点，当 非直邮时 传入';
COMMENT ON COLUMN "annual"."t_annual_info"."receiver_name" IS '邮寄收件人姓名';
COMMENT ON COLUMN "annual"."t_annual_info"."receiver_address" IS '邮寄收件人地址';
COMMENT ON COLUMN "annual"."t_annual_info"."receiver_phone" IS '邮寄收件人电话';
COMMENT ON COLUMN "annual"."t_annual_info"."statement_date" IS '印发日期';
COMMENT ON COLUMN "annual"."t_annual_info"."exist_future_value" IS '是否打印基本预期价值表标识， Y：存在 N：不存在   当不存在时，后续节点均不存在';
COMMENT ON COLUMN "annual"."t_annual_info"."sequence" IS '唯一编码';
COMMENT ON COLUMN "annual"."t_annual_info"."is_saved" IS '年报是否生成 N 未生成 Y 已生成';

-- ----------------------------
-- Indexes structure for table t_annual_info
-- ----------------------------
CREATE UNIQUE INDEX "policy_no_rep_month" ON "annual"."t_annual_info" USING btree (
  "policy_no" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "rep_month" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
COMMENT ON INDEX "annual"."policy_no_rep_month" IS '保单号和年报月份做唯一索引，防止重复';

-- ----------------------------
-- Primary Key structure for table t_annual_info
-- ----------------------------
ALTER TABLE "annual"."t_annual_info" ADD CONSTRAINT "t_annual_info_pkey" PRIMARY KEY ("id");









-- ----------------------------
-- Table structure for t_annual_policy_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_annual_policy_info";
CREATE TABLE "annual"."t_annual_policy_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "owner_name" varchar(255) COLLATE "pg_catalog"."default",
  "policy_status" varchar(50) COLLATE "pg_catalog"."default",
  "insured_name" varchar(255) COLLATE "pg_catalog"."default",
  "insured_sex" varchar(20) COLLATE "pg_catalog"."default",
  "insured_issue_age" varchar(32) COLLATE "pg_catalog"."default",
  "rating_classification" varchar(50) COLLATE "pg_catalog"."default",
  "premium_structure" varchar(50) COLLATE "pg_catalog"."default",
  "policy_date" varchar(50) COLLATE "pg_catalog"."default",
  "assignee_name" varchar(255) COLLATE "pg_catalog"."default",
  "policy_anniversary_date" varchar(50) COLLATE "pg_catalog"."default",
  "premium_status" varchar(50) COLLATE "pg_catalog"."default",
  "policy_currency" varchar(50) COLLATE "pg_catalog"."default",
  "total_modal_premium" numeric(30,2),
  "next_premium_due_date" varchar(50) COLLATE "pg_catalog"."default",
  "exist_cash_dividend" varchar(20) COLLATE "pg_catalog"."default",
  "cash_dividend_payment_option" varchar(50) COLLATE "pg_catalog"."default",
  "exist_guaranteed_cash" varchar(50) COLLATE "pg_catalog"."default",
  "guaranteed_cash" varchar(50) COLLATE "pg_catalog"."default",
  "exist_annuity_payment" varchar(50) COLLATE "pg_catalog"."default",
  "annuity_payment_option" varchar(50) COLLATE "pg_catalog"."default",
  "annuity_payment_frequency" varchar(50) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "data_type" varchar(50) COLLATE "pg_catalog"."default",
  "policy_owner_name" varchar(255) COLLATE "pg_catalog"."default",
  "address" varchar(255) COLLATE "pg_catalog"."default",
  "status" int4,
  "rep_month" varchar(50) COLLATE "pg_catalog"."default",
  "payment_mode" varchar(255) COLLATE "pg_catalog"."default",
  "payment_method" varchar(255) COLLATE "pg_catalog"."default",
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."owner_name" IS '保单持有人姓名';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_status" IS '保单状况  有效、失效、终止、待生效';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."insured_name" IS '受保人姓名';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."insured_sex" IS '受保人性别';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."insured_issue_age" IS '受保人结发年龄';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."rating_classification" IS '费率类别 标准、非标准、Non-Standard、Standard';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."premium_structure" IS '费率结权';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_date" IS '保单日期';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."assignee_name" IS '承让人';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_anniversary_date" IS '保单周年日';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."premium_status" IS '保费状况';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_currency" IS '保单货币';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."total_modal_premium" IS '每期保费总额';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."next_premium_due_date" IS '下期保费到期日';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."exist_cash_dividend" IS '是否存在现金红利领取方式标识';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."cash_dividend_payment_option" IS '现金红利领取方式';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."exist_guaranteed_cash" IS '是否存在保证现金奖赏/保证现金领取方式标识';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."guaranteed_cash" IS '保证現金獎賞/保证现金奖赏';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."exist_annuity_payment" IS '是否存在年金权益标识';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."annuity_payment_option" IS '年金领取方式';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."annuity_payment_frequency" IS '年金领取频率';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."data_type" IS '数据类型 DSSI：金鑽飛凡儲蓄壽險計劃；MCI：安康至尊危疾終身保';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."policy_owner_name" IS '保单持有人姓名';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."address" IS '保单持有人地址';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."rep_month" IS '年报月份  格式202012';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."payment_mode" IS '缴费频率';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."payment_method" IS '缴费方式';
COMMENT ON COLUMN "annual"."t_annual_policy_info"."sequence" IS '唯一编码';

-- ----------------------------
-- Indexes structure for table t_annual_policy_info
-- ----------------------------
CREATE UNIQUE INDEX "policy_no_rep_month_pol" ON "annual"."t_annual_policy_info" USING btree (
  "policy_no" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "rep_month" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table t_annual_policy_info
-- ----------------------------
ALTER TABLE "annual"."t_annual_policy_info" ADD CONSTRAINT "annual_info_pkey" PRIMARY KEY ("id");






-- ----------------------------
-- Table structure for t_annual_type_tbl
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_annual_type_tbl";
CREATE TABLE "annual"."t_annual_type_tbl" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "plan_code" varchar(20) COLLATE "pg_catalog"."default",
  "annual_type" varchar(20) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "status" int4,
  "reversionary_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "special_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "termina_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "cash_divid" varchar(10) COLLATE "pg_catalog"."default",
  "coupon" varchar(10) COLLATE "pg_catalog"."default",
  "annuity" varchar(10) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."plan_code" IS '险种编码';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."annual_type" IS '年报类型';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."reversionary_bonus" IS '复归红利，存在Y ，不存在N';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."special_bonus" IS '特别红利，存在Y ，不存在N';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."termina_bonus" IS '终期红利，存在Y ，不存在N';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."cash_divid" IS '现金红利，存在Y ，不存在N';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."coupon" IS '保证现金奖赏/生存金，存在Y ，不存在N';
COMMENT ON COLUMN "annual"."t_annual_type_tbl"."annuity" IS '年金，存在Y ，不存在N';

-- ----------------------------
-- Records of t_annual_type_tbl
-- ----------------------------
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('d5e4a331-800a-4c6b-b9b0-cb4c66d6a88f', '1521MCI2', 'MCI', '2020-12-30 21:13:07.279934', '', '2020-12-30 21:13:07.279934', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('e6d2d8cd-3b5b-4beb-bb7e-8868d9a29011', '1511MCI2', 'MCI', '2020-12-30 21:13:34.537617', '', '2020-12-30 21:13:34.537617', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('8f70e898-489a-4220-8b12-1610c78d5bc8', '1531MCI2', 'MCI', '2020-12-30 21:13:48.951291', '', '2020-12-30 21:13:48.951291', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('eaa86923-0ecc-478c-9f6c-b0ff3b8c5eaa', '1121DSSI2', 'DSSI', '2020-12-30 21:14:17.761432', '', '2020-12-30 21:14:17.761432', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('7b94e796-5f43-4456-be4c-14c57e117cf4', '1111DSSI2', 'DSSI', '2021-01-04 09:38:57.808825', '', '2021-01-04 09:38:57.808825', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('cddb3935-6dd9-4f33-9698-df2c1886acb8', '1131DSSI2', 'DSSI', '2021-01-04 09:39:08.557782', '', '2021-01-04 09:39:08.557782', '', NULL, 'N', 'N', 'Y', 'N', 'N', 'N');
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('5940fa39-c285-48d3-ad40-c55213002376', '1311MRTA1', 'mortgage', '2021-04-21 22:53:49.836483', '', '2021-04-21 22:53:49.836483', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('2201b1cc-c781-4f3b-914b-21ff8e61d46a', '1331MRTA1', 'mortgage', '2021-04-21 22:53:49.836483', '', '2021-04-21 22:53:49.836483', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('f8d2239f-5915-4193-a9ce-87b7f4883442', '11115P61', '5P6', '2021-04-21 22:53:49.836483', '', '2021-04-21 22:53:49.836483', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('188c75b2-2b86-4e69-bed2-7d49bbc6b77c', '11215P61', '5P6', '2021-04-21 22:53:49.836483', '', '2021-04-21 22:53:49.836483', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO "annual"."t_annual_type_tbl" VALUES ('f236f838-4024-40df-a6d0-ec14db3ecd44', '11315P61', '5P6', '2021-04-21 22:53:49.836483', '', '2021-04-21 22:53:49.836483', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Primary Key structure for table t_annual_type_tbl
-- ----------------------------
ALTER TABLE "annual"."t_annual_type_tbl" ADD CONSTRAINT "t_annual_type_tbl_pkey" PRIMARY KEY ("id");








-- ----------------------------
-- Table structure for t_claim_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_claim_info";
CREATE TABLE "annual"."t_claim_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "status" int4,
  "sum_amount" numeric(30,2),
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying
)
;
COMMENT ON COLUMN "annual"."t_claim_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_claim_info"."sum_amount" IS '理赔金额';
COMMENT ON COLUMN "annual"."t_claim_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_claim_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_claim_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_claim_info"."update_by" IS '修改人';

-- ----------------------------
-- Primary Key structure for table t_claim_info
-- ----------------------------
ALTER TABLE "annual"."t_claim_info" ADD CONSTRAINT "t_claim_pkey" PRIMARY KEY ("id");







-- ----------------------------
-- Table structure for t_financial_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_financial_info";
CREATE TABLE "annual"."t_financial_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "current_value" numeric(30,2),
  "exist_reversionary_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "reversionary_bonus" numeric(30,2),
  "exist_special_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "special_bonus" numeric(30,2),
  "exist_termina_bonus" varchar(10) COLLATE "pg_catalog"."default",
  "termina_bonus" numeric(30,2),
  "exist_cash_dividend" varchar(10) COLLATE "pg_catalog"."default",
  "opening_balance_div" numeric(30,2),
  "current_year_div" numeric(30,2),
  "interest_div" numeric(30,2),
  "deduction_div" numeric(30,2),
  "current_value_div" numeric(30,2),
  "exist_coupon" varchar(10) COLLATE "pg_catalog"."default",
  "opening_balance_cou" numeric(30,2),
  "current_year_cou" numeric(30,2),
  "interest_cou" numeric(30,2),
  "deduction_cou" numeric(30,2),
  "current_value_cou" numeric(30,2),
  "exist_annuity" varchar(10) COLLATE "pg_catalog"."default",
  "opening_balance_ann" numeric(30,2),
  "current_year_ann" numeric(30,2),
  "interest_ann" numeric(30,2),
  "deduction_ann" numeric(30,2),
  "current_value_ann" numeric(30,2),
  "total_balance" numeric(30,2),
  "exist_prepaid" varchar(10) COLLATE "pg_catalog"."default",
  "prepayment" numeric(30,2),
  "premium_suspensevalue" numeric(30,2),
  "total_load_balance" numeric(30,2),
  "total_policy_value" numeric(30,2),
  "total_load_balance_remark" varchar(255) COLLATE "pg_catalog"."default",
  "total_policy_value_remark" varchar(255) COLLATE "pg_catalog"."default",
  "surrender_charge" numeric(30,2),
  "claw_back_charge" numeric(30,2),
  "total_surrender_benefit" numeric(30,2),
  "status" int4,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_financial_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_financial_info"."current_value" IS '本年度现金价值';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_reversionary_bonus" IS '是否存在复归红利标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."reversionary_bonus" IS '复归红利（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_special_bonus" IS '是否存在特别红利标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."special_bonus" IS '特别红利（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_termina_bonus" IS '是否存在终期红利标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."termina_bonus" IS '终期红利（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_cash_dividend" IS '是否存在现金红利标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."opening_balance_div" IS '现金红利（上年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_year_div" IS '现金红利（本年度）';
COMMENT ON COLUMN "annual"."t_financial_info"."interest_div" IS '现金红利（利息）';
COMMENT ON COLUMN "annual"."t_financial_info"."deduction_div" IS '现金红利（扣除）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_value_div" IS '现金红利（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_coupon" IS '是否存在保證現金獎賞/ 保證現金标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."opening_balance_cou" IS '保證現金獎賞/ 保證現金（上年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_year_cou" IS '保證現金獎賞/ 保證現金（本年度）';
COMMENT ON COLUMN "annual"."t_financial_info"."interest_cou" IS '保證現金獎賞/ 保證現金（利息）';
COMMENT ON COLUMN "annual"."t_financial_info"."deduction_cou" IS '保證現金獎賞/ 保證現金（扣除）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_value_cou" IS '保證現金獎賞/ 保證現金（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_annuity" IS '是否存在年金标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."opening_balance_ann" IS '年金（上年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_year_ann" IS '年金（本年度）';
COMMENT ON COLUMN "annual"."t_financial_info"."interest_ann" IS '年金（利息）';
COMMENT ON COLUMN "annual"."t_financial_info"."deduction_ann" IS '年金（扣除）';
COMMENT ON COLUMN "annual"."t_financial_info"."current_value_ann" IS '年金（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."total_balance" IS '金额总计';
COMMENT ON COLUMN "annual"."t_financial_info"."exist_prepaid" IS '是否存在预付保费标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_financial_info"."prepayment" IS '預付保費利息註2及保費餘額(本年度结余)';
COMMENT ON COLUMN "annual"."t_financial_info"."premium_suspensevalue" IS '预付保费（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."total_load_balance" IS '贷款总金额（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."total_policy_value" IS '总保单价值（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."total_load_balance_remark" IS '贷款总金额 标注';
COMMENT ON COLUMN "annual"."t_financial_info"."total_policy_value_remark" IS '总保单价值（计算公式）';
COMMENT ON COLUMN "annual"."t_financial_info"."surrender_charge" IS '退保手续费（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."claw_back_charge" IS '预缴回报费用（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."total_surrender_benefit" IS '退保权益（本年度结余）';
COMMENT ON COLUMN "annual"."t_financial_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_financial_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_financial_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_financial_info"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_financial_info"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_financial_info
-- ----------------------------
ALTER TABLE "annual"."t_financial_info" ADD CONSTRAINT "t_financial_info_pkey" PRIMARY KEY ("id");








-- ----------------------------
-- Table structure for t_future_value_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_future_value_info";
CREATE TABLE "annual"."t_future_value_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "template_no" varchar(10) COLLATE "pg_catalog"."default",
  "future_value_type" varchar(10) COLLATE "pg_catalog"."default",
  "policy_end_year" varchar(30) COLLATE "pg_catalog"."default",
  "total_premiums_paid" numeric(30,2),
  "prepaid_premium" numeric(30,2),
  "prepaid_premium_rate" numeric(30,2),
  "guaranteed_cash_surrender" numeric(30,2),
  "terminal_bonus" numeric(30,2),
  "total_surrender" numeric(30,2),
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "status" int4,
  "guaranteed_cash_death" numeric(30,2),
  "total_death" numeric(30,2),
  "age_of_life" int4,
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_future_value_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_future_value_info"."template_no" IS '模板编号';
COMMENT ON COLUMN "annual"."t_future_value_info"."future_value_type" IS '价值表类型 1：身故权益 2：退保权益';
COMMENT ON COLUMN "annual"."t_future_value_info"."policy_end_year" IS '保单年度完结';
COMMENT ON COLUMN "annual"."t_future_value_info"."total_premiums_paid" IS '已付总保费';
COMMENT ON COLUMN "annual"."t_future_value_info"."prepaid_premium" IS '预付保费';
COMMENT ON COLUMN "annual"."t_future_value_info"."prepaid_premium_rate" IS '预付保费息率';
COMMENT ON COLUMN "annual"."t_future_value_info"."guaranteed_cash_surrender" IS '保证金额退保';
COMMENT ON COLUMN "annual"."t_future_value_info"."terminal_bonus" IS '终期红利';
COMMENT ON COLUMN "annual"."t_future_value_info"."total_surrender" IS '总额 退保';
COMMENT ON COLUMN "annual"."t_future_value_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_future_value_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_future_value_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_future_value_info"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_future_value_info"."guaranteed_cash_death" IS '保证金额身故';
COMMENT ON COLUMN "annual"."t_future_value_info"."total_death" IS '总额 身故';
COMMENT ON COLUMN "annual"."t_future_value_info"."age_of_life" IS '利益演示 年龄';
COMMENT ON COLUMN "annual"."t_future_value_info"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_future_value_info
-- ----------------------------
ALTER TABLE "annual"."t_future_value_info" ADD CONSTRAINT "t_future_value_info_pkey" PRIMARY KEY ("id");







-- ----------------------------
-- Table structure for t_loan_info
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_loan_info";
CREATE TABLE "annual"."t_loan_info" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "exist_pl" varchar(10) COLLATE "pg_catalog"."default",
  "opening_balance_loan" numeric(30,2),
  "accrual_interest_loan" numeric(30,2),
  "load_this_year_loan" numeric(30,2),
  "load_repay" numeric(30,2),
  "load_balance" numeric(30,2),
  "exist_apl" varchar(10) COLLATE "pg_catalog"."default",
  "opening_balance_auto" numeric(30,2),
  "accrual_interest_auto" numeric(30,2),
  "load_this_year_auto" numeric(30,2),
  "load_repay_auto" numeric(30,2),
  "load_balance_auto" numeric(30,2),
  "total_amount" numeric(30,2),
  "ply" varchar(50) COLLATE "pg_catalog"."default",
  "status" int4,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_loan_info"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_loan_info"."exist_pl" IS '是否存在保单贷款标识， Y：存在 N：不存在';
COMMENT ON COLUMN "annual"."t_loan_info"."opening_balance_loan" IS '保单贷款（承接往年贷款结余）';
COMMENT ON COLUMN "annual"."t_loan_info"."accrual_interest_loan" IS '保单贷款（贷款利息金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_this_year_loan" IS '保单贷款（本年度贷款金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_repay" IS '保单贷款（偿还贷款金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_balance" IS '保单贷款（贷款结余）';
COMMENT ON COLUMN "annual"."t_loan_info"."exist_apl" IS '是否存在自动保费贷款标识， Y：存在 N：不存在 ';
COMMENT ON COLUMN "annual"."t_loan_info"."opening_balance_auto" IS '自动保费贷款（承接往年贷款结余）';
COMMENT ON COLUMN "annual"."t_loan_info"."accrual_interest_auto" IS '自动保费贷款（贷款利息金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_this_year_auto" IS '自动保费贷款（本年度贷款金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_repay_auto" IS '自动保费贷款（偿还贷款金额）';
COMMENT ON COLUMN "annual"."t_loan_info"."load_balance_auto" IS '自动保费贷款（贷款结余）';
COMMENT ON COLUMN "annual"."t_loan_info"."total_amount" IS '总金额';
COMMENT ON COLUMN "annual"."t_loan_info"."ply" IS '预计时效年限（重要事项部分）';
COMMENT ON COLUMN "annual"."t_loan_info"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_loan_info"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_loan_info"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_loan_info"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_loan_info"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_loan_info
-- ----------------------------
ALTER TABLE "annual"."t_loan_info" ADD CONSTRAINT "t_loan_info_pkey" PRIMARY KEY ("id");






-- ----------------------------
-- Table structure for t_payment_record
-- ----------------------------
DROP TABLE IF EXISTS "annual"."t_payment_record";
CREATE TABLE "annual"."t_payment_record" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default",
  "transaction_date" varchar(50) COLLATE "pg_catalog"."default",
  "premium" numeric(30,2),
  "remarks" varchar(255) COLLATE "pg_catalog"."default",
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "status" int4,
  "is_primary" varchar(255) COLLATE "pg_catalog"."default",
  "sequence" varchar(80) COLLATE "pg_catalog"."default"
)
;
COMMENT ON COLUMN "annual"."t_payment_record"."policy_no" IS '保单号';
COMMENT ON COLUMN "annual"."t_payment_record"."transaction_date" IS '交易日期';
COMMENT ON COLUMN "annual"."t_payment_record"."premium" IS '保费';
COMMENT ON COLUMN "annual"."t_payment_record"."remarks" IS '备注';
COMMENT ON COLUMN "annual"."t_payment_record"."create_time" IS '创建时间';
COMMENT ON COLUMN "annual"."t_payment_record"."create_by" IS '创建人';
COMMENT ON COLUMN "annual"."t_payment_record"."update_time" IS '修改时间';
COMMENT ON COLUMN "annual"."t_payment_record"."update_by" IS '修改人';
COMMENT ON COLUMN "annual"."t_payment_record"."is_primary" IS '是否主险 1 主险 2 非主险';
COMMENT ON COLUMN "annual"."t_payment_record"."sequence" IS '唯一编码';

-- ----------------------------
-- Primary Key structure for table t_payment_record
-- ----------------------------
ALTER TABLE "annual"."t_payment_record" ADD CONSTRAINT "t_payment_record_pkey" PRIMARY KEY ("id");



commit;