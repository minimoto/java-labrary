begin;


-- ----------------------------
-- Table structure for t_non_forfeiture_policy
-- ----------------------------
DROP TABLE IF EXISTS "renewal"."t_non_forfeiture_policy";
CREATE TABLE "renewal"."t_non_forfeiture_policy" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "next_payment_date" date,
  "policy_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "policy_id" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "execute_step" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "deal_type" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "deal_ind" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "receivable_order_id" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::numeric,
  "renewal_business_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "business_opt_date" date,
  "gs_amount" numeric(30,2) DEFAULT NULL::numeric,
  "ps_amount" numeric(30,2) DEFAULT NULL::numeric,
  "cs_amount" numeric(30,2) DEFAULT NULL::numeric,
  "short_pay_max_amount" numeric(30,2) DEFAULT NULL::numeric,
  "gs_last_amount" numeric(30,2) DEFAULT NULL::numeric,
  "ps_last_amount" numeric(30,2) DEFAULT NULL::numeric,
  "cs_last_amount" numeric(30,2) DEFAULT NULL::numeric,
  "short_pay_last_amount" numeric(30,2) DEFAULT NULL::numeric
)
;
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."create_time" IS '创建时间';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."create_by" IS '创建人';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."update_time" IS '修改时间';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."update_by" IS '修改人';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."next_payment_date" IS '下期缴费日';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."policy_no" IS '保单号';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."policy_id" IS '保单ID';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."execute_step" IS '执行步骤,执行到了job的哪一步';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."deal_type" IS '处理类型，1，续期成功，2，部分失效,  3,全部失效';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."deal_ind" IS '是否已经处理标识';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."receivable_order_id" IS '应收订单ID';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."renewal_business_no" IS '应收订单的业务编号，比如续期抽档的业务编号等';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."business_opt_date" IS '业务操作日期，实际可能从运行参数获取';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."gs_amount" IS 'gs通用账户余额';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."ps_amount" IS 'ps续期账户余额';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."cs_amount" IS 'cs保全账户余额';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."short_pay_max_amount" IS '短缴最大额度';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."gs_last_amount" IS 'gs账户剩余额度';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."ps_last_amount" IS 'ps账户剩余额度';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."cs_last_amount" IS 'cs账户剩余额度';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_policy"."short_pay_last_amount" IS '短缴剩余额度';
COMMENT ON TABLE "renewal"."t_non_forfeiture_policy" IS 'Non-forfeiture保单表';

-- ----------------------------
-- Primary Key structure for table t_non_forfeiture_policy
-- ----------------------------
ALTER TABLE "renewal"."t_non_forfeiture_policy" ADD CONSTRAINT "t_non_forfeiture_policy_pkey" PRIMARY KEY ("id");






-- ----------------------------
-- Table structure for t_non_forfeiture_product
-- ----------------------------
DROP TABLE IF EXISTS "renewal"."t_non_forfeiture_product";
CREATE TABLE "renewal"."t_non_forfeiture_product" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "forfeiture_busi_id" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "policy_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "policy_id" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "product_code" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "main_ind" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "apl_ind" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "currency" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "discount_premium" numeric(30,2) DEFAULT NULL::numeric,
  "term_premium" numeric(30,2) DEFAULT NULL::numeric,
  "extra_premium" numeric(30,2) DEFAULT NULL::numeric,
  "prepaid_amount" numeric(30,2) DEFAULT NULL::numeric,
  "prepaid_interest" numeric(30,2) DEFAULT NULL::numeric,
  "cb_amount" numeric(30,2) DEFAULT NULL::numeric,
  "cb_interest" numeric(30,2) DEFAULT NULL::numeric,
  "cb_unsettlement_interest" numeric(30,2) DEFAULT NULL::numeric,
  "sb_amount" numeric(30,2) DEFAULT NULL::numeric,
  "sb_interest" numeric(30,2) DEFAULT NULL::numeric,
  "sb_unsettlement_interest" numeric(30,2) DEFAULT NULL::numeric,
  "apl_amount" numeric(30,2) DEFAULT NULL::numeric,
  "pl_amount" numeric(30,2) DEFAULT NULL::numeric,
  "mortgage_ind" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "gcv_amount" numeric(30,2) DEFAULT NULL::numeric,
  "deal_way" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "status" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "deal_order" int2,
  "deal_detail" varchar(1000) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "bill_detail_id" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "apl_interest" numeric(30,2) DEFAULT NULL::numeric,
  "apl_unsettlement_interest" numeric(30,2) DEFAULT NULL::numeric,
  "pl_interest" numeric(30,2) DEFAULT NULL::numeric,
  "pl_unsettlement_interest" numeric(30,2) DEFAULT NULL::numeric,
  "max_loan_percentage" numeric(8,2) DEFAULT NULL::numeric,
  "annualized_premium" numeric(30,2) DEFAULT NULL::numeric,
  "standard_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "prepaid_ind" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "discounting_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_discount_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_discount_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "em_loading_rate" numeric(30,2) DEFAULT NULL::numeric,
  "annual_discount_premium" numeric(30,4) DEFAULT NULL::numeric
)
;
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."create_time" IS '创建时间';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."create_by" IS '创建人';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."update_time" IS '修改时间';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."update_by" IS '修改人';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."forfeiture_busi_id" IS '业务ID，t_non_forfeiture_policy的ID';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."policy_no" IS '保单号';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."policy_id" IS '保单ID';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."product_code" IS '产品编号';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."main_ind" IS '主险标识';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."apl_ind" IS 'apl标识';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."currency" IS '币种';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."discount_premium" IS '折扣后保费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."term_premium" IS '险种期缴保费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."extra_premium" IS '加费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."prepaid_amount" IS '预付账户本金';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."prepaid_interest" IS '预付账户利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."cb_amount" IS '现金红利账户本金';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."cb_interest" IS '现金红利账户利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."cb_unsettlement_interest" IS '现金红利账户未结算利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."sb_amount" IS '生存红利账户本金';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."sb_interest" IS '生存红利账户利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."sb_unsettlement_interest" IS '生存红利账户未结算利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."apl_amount" IS '自动抵押贷款金额';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."pl_amount" IS '手动抵押贷款金额';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."mortgage_ind" IS '支持贷款pl标识';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."gcv_amount" IS 'gcv价值';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."deal_way" IS '1:抵缴（无需APL）, 2:apl， 3:失效';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."status" IS '0：未处理，1：已处理';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."deal_order" IS '处理顺序，数字升序优先处理';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."deal_detail" IS '处理详情';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."bill_detail_id" IS '支付账单险种详情ID';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."apl_interest" IS '自动贷款账户利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."apl_unsettlement_interest" IS '自动贷款账户未结算利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."pl_interest" IS '手动贷款账户利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."pl_unsettlement_interest" IS '手动贷款账户未结算利息';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."max_loan_percentage" IS '最大可贷款额度比率';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_premium" IS '年化保费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."standard_premium" IS '标准保费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."modal_em_loading_premium" IS '健康加费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."modal_flat_loading_premium" IS '职业加费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."modal_temp_loading_premium" IS '临时加费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."prepaid_ind" IS '预缴标识';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."discounting_premium" IS '折扣保费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_discount_premium" IS '折后保费（年化）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annual_loading_premium" IS '总加费（年度）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_loading_premium" IS '总加费（年化）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."modal_discount_loading_premium" IS '每期折后保费 + 每期加费';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annual_em_loading_premium" IS 'Em加费（年度）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annual_flat_loading_premium" IS 'Flat加费（年度）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annual_temp_loading_premium" IS 'Temp加费（年度）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_em_loading_premium" IS 'Em加费（年化）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_flat_loading_premium" IS 'Flat加费（年化）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annualized_temp_loading_premium" IS 'Temp加费（年化）';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."em_loading_rate" IS 'EM率';
COMMENT ON COLUMN "renewal"."t_non_forfeiture_product"."annual_discount_premium" IS '折后保费（年度）';
COMMENT ON TABLE "renewal"."t_non_forfeiture_product" IS 'Non-Forfeiture产品表';

-- ----------------------------
-- Primary Key structure for table t_non_forfeiture_product
-- ----------------------------
ALTER TABLE "renewal"."t_non_forfeiture_product" ADD CONSTRAINT "t_non_forfeiture_product_pkey" PRIMARY KEY ("id");










-- ----------------------------
-- Table structure for t_renewal_bill
-- ----------------------------
DROP TABLE IF EXISTS "renewal"."t_renewal_bill";
CREATE TABLE "renewal"."t_renewal_bill" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "policy_currency" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "payment_requency" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "policy_year" int4,
  "policy_type" varchar(255) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "next_payment_date" date,
  "policy_term" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "business_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "bill_status" varchar(20) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "total_premium" numeric(30,2) DEFAULT NULL::numeric,
  "renewal_offset_date" date,
  "business_date" date
)
;
COMMENT ON COLUMN "renewal"."t_renewal_bill"."policy_no" IS '保单号';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."create_time" IS '创建时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."create_by" IS '创建人';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."update_time" IS '修改时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."update_by" IS '修改人';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."policy_currency" IS '保单币种';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."payment_requency" IS '缴费频率';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."policy_year" IS '保单年度';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."policy_type" IS '保单类型';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."next_payment_date" IS '下期缴费日';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."policy_term" IS '保障年期';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."business_no" IS '业务编号';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."bill_status" IS '账单状态';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."total_premium" IS '总保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."renewal_offset_date" IS '下次抽档日';
COMMENT ON COLUMN "renewal"."t_renewal_bill"."business_date" IS '业务操作日期';
COMMENT ON TABLE "renewal"."t_renewal_bill" IS '续期应收账单表';

-- ----------------------------
-- Primary Key structure for table t_renewal_bill
-- ----------------------------
ALTER TABLE "renewal"."t_renewal_bill" ADD CONSTRAINT "t_renewal_bill_pkey" PRIMARY KEY ("id");












-- ----------------------------
-- Table structure for t_renewal_bill_detail
-- ----------------------------
DROP TABLE IF EXISTS "renewal"."t_renewal_bill_detail";
CREATE TABLE "renewal"."t_renewal_bill_detail" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "policy_no" varchar(36) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "product_code" varchar(36) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "main_ind" varchar(8) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "term_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "currency" varchar(16) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "next_payment_date" date,
  "renewal_bill_id" varchar(36) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "discount_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_premium" numeric(30,2) DEFAULT NULL::numeric,
  "total_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "standard_premium" numeric(32,2) DEFAULT NULL::numeric,
  "discounting_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_discount_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "modal_discount_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annual_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_em_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_flat_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "annualized_temp_loading_premium" numeric(30,2) DEFAULT NULL::numeric,
  "em_loading_rate" numeric(30,4) DEFAULT NULL::numeric,
  "annual_discount_premium" numeric(30,2) DEFAULT NULL::numeric
)
;
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."create_time" IS '创建时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."create_by" IS '创建人';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."update_time" IS '修改时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."update_by" IS '修改人';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."policy_no" IS '保单编号';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."product_code" IS '险种编号';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."main_ind" IS '主险种标识';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."term_premium" IS '期缴保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."modal_em_loading_premium" IS '健康加费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."modal_flat_loading_premium" IS '职业加费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."modal_temp_loading_premium" IS '临时加费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."currency" IS '币种';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."next_payment_date" IS '下次缴费日';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."renewal_bill_id" IS '续期账单ID';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."discount_premium" IS '折后保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_premium" IS '年化保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."total_loading_premium" IS '总加费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."standard_premium" IS '标准保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."discounting_premium" IS '折扣保费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_discount_premium" IS '折后保费（年化）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annual_loading_premium" IS '总加费（年度）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_loading_premium" IS '总加费（年化）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."modal_discount_loading_premium" IS '每期折后保费 + 每期加费';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annual_em_loading_premium" IS 'Em加费（年度）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annual_flat_loading_premium" IS 'Flat加费（年度）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annual_temp_loading_premium" IS 'Temp加费（年度）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_em_loading_premium" IS 'Em加费（年化）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_flat_loading_premium" IS 'Flat加费（年化）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annualized_temp_loading_premium" IS 'Temp加费（年化）';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."em_loading_rate" IS 'EM率';
COMMENT ON COLUMN "renewal"."t_renewal_bill_detail"."annual_discount_premium" IS '折后保费（年度）';
COMMENT ON TABLE "renewal"."t_renewal_bill_detail" IS '续期缴费详情表';

-- ----------------------------
-- Primary Key structure for table t_renewal_bill_detail
-- ----------------------------
ALTER TABLE "renewal"."t_renewal_bill_detail" ADD CONSTRAINT "t_renewal_bill_detail_pkey" PRIMARY KEY ("id");








-- ----------------------------
-- Table structure for t_renewal_bill_record
-- ----------------------------
DROP TABLE IF EXISTS "renewal"."t_renewal_bill_record";
CREATE TABLE "renewal"."t_renewal_bill_record" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "policy_no" varchar(30) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "bill_ind" varchar(8) COLLATE "pg_catalog"."default" DEFAULT NULL::numeric,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "policy_status" varchar(16) COLLATE "pg_catalog"."default" DEFAULT NULL::numeric,
  "premium_status" varchar(16) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "next_payment_date" date,
  "renewal_offset_date" date,
  "business_date" date
)
;
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."policy_no" IS '保单号';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."bill_ind" IS '抽档标识';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."create_time" IS '创建时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."create_by" IS '创建人';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."update_time" IS '修改时间';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."update_by" IS '修改人';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."policy_status" IS '保单状态';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."premium_status" IS '保单缴费状态';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."next_payment_date" IS '下期缴费日';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."renewal_offset_date" IS '下次抽档日';
COMMENT ON COLUMN "renewal"."t_renewal_bill_record"."business_date" IS '业务操作日期';
COMMENT ON TABLE "renewal"."t_renewal_bill_record" IS '续期抽档记录表';

-- ----------------------------
-- Primary Key structure for table t_renewal_bill_record
-- ----------------------------
ALTER TABLE "renewal"."t_renewal_bill_record" ADD CONSTRAINT "t_renewal_bill_record_pkey" PRIMARY KEY ("id");





commit;