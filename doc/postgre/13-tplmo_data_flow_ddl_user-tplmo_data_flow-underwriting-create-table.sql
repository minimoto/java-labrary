/*
 Navicat Premium Data Transfer

 Source Server         : uat-data-flow
 Source Server Type    : PostgreSQL
 Source Server Version : 100001
 Source Host           : 10.29.49.26:5432
 Source Catalog        : tplmo_data_flow
 Source Schema         : underwriting

 Target Server Type    : PostgreSQL
 Target Server Version : 100001
 File Encoding         : 65001

 Date: 02/06/2021 15:44:13
*/


-- ----------------------------
-- Table structure for t_expect_underwriting_record
-- ----------------------------
DROP TABLE IF EXISTS "underwriting"."t_expect_underwriting_record";
CREATE TABLE "underwriting"."t_expect_underwriting_record" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL DEFAULT (uuid_generate_v4())::character varying,
  "create_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "create_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "update_time" timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "update_by" varchar(200) COLLATE "pg_catalog"."default" NOT NULL DEFAULT ''::character varying,
  "business_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "policy_no" varchar(40) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying,
  "expected_issue_date" date,
  "status" varchar(10) COLLATE "pg_catalog"."default" DEFAULT NULL::character varying
)
;
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."create_time" IS '创建时间';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."create_by" IS '创建人';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."update_time" IS '修改时间';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."update_by" IS '修改人';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."business_no" IS '业务编号';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."policy_no" IS '保单号';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."expected_issue_date" IS '期望承保日';
COMMENT ON COLUMN "underwriting"."t_expect_underwriting_record"."status" IS '承保结果';
COMMENT ON TABLE "underwriting"."t_expect_underwriting_record" IS '期望承保日承保记录表';

-- ----------------------------
-- Primary Key structure for table t_expect_underwriting_record
-- ----------------------------
ALTER TABLE "underwriting"."t_expect_underwriting_record" ADD CONSTRAINT "t_expect_underwriting_record_pkey" PRIMARY KEY ("id");
