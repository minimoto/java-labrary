## 实验室

#### 1. spring batch
[文档说明](doc/readme/spring-batch.md)

---------
#### 2. 规则框架 drools
[文档说明](doc/readme/drools.md)

---------
#### 3. if-else
* 通过 “ 工厂模型 + 注解 " 实现多个 if 

---------
#### 4. jvm
* 用于学习 jvm 的测试程序

---------
#### 5. redis
示例1

* 通过 jedis 实现分布式锁

* 通过 redisson 实现分布式锁 (推荐)

---------
#### 6. stream
* jdk8 开发中遇到的业务场景

  * 两个 List 运算：根据 A list 中的对象属性，在 B list 查找符合要求的集合
  
---------
#### 7. thread
[文档说明](doc/readme/thread.md)

---------
#### 8. valid
* 学习自定义验证,分组验证

---------
#### 9. dataflow
[文档说明](doc/readme/dataflow.md)

---------
####  10. mq-stream
[文档说明](doc/readme/mq-stream.md)

---------
####  11. spring-security
[文档说明](doc/readme/spring-security.md)
    
#### 12. 表达式引擎 aviator && aviator-source
[文档说明](doc/readme/aviator.md)

#### 13. generic
* 泛型










    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
                          
---------
