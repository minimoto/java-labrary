package com.tplhk.function;

import com.tplhk.function.mockFrameWork.CoreHandle;
import com.tplhk.function.mockFrameWork.DataVo;
import com.tplhk.stream.vo.*;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@SpringBootTest(classes = FunctionTests.class)
class FunctionTests {

    // 框架回调业务代码（写法1）
    @Test
    void test() {
        CoreHandle coreHandle = new CoreHandle();
        coreHandle.handle(dataVo -> {
            dataVo.setId(UUID.randomUUID().toString());
            System.out.println("test dataVo:" + dataVo);
        } );
    }


    /**
     * 框架回调业务代码（写法2）
     * 相比于写法1，写法2更加简洁，框架省去 DataHandleInterface 接口
     */
    @Test
    void test2() {
        CoreHandle coreHandle = new CoreHandle();
        coreHandle.handle2(dataVo -> {
            dataVo.setId(UUID.randomUUID().toString());
            System.out.println("test dataVo:" + dataVo);
        } );
    }

    /**
     * 框架调用业务方法
     *
     * 相比以前需要一个接口，现在只需要一个 supplier 即可
     *
     * Supplier：供给型接口，没有入参，只有一个返回值
     */
    @Test
    void test3() {
        CoreHandle coreHandle = new CoreHandle();
        coreHandle.handle3(() -> {
            DataVo dataVo = new DataVo();
            dataVo.setId(UUID.randomUUID().toString());
            System.out.println("test dataVo:" + dataVo);
            return dataVo;
        } );
    }

    /**
     * 框架调用业务方法，有输入输出
     *
     * 相比以前需要一个接口，现在只需要一个 Function 即可
     *
     * Function：函数型接口，有入参和返回值
     */
    @Test
    void test4() {
        CoreHandle coreHandle = new CoreHandle();
        coreHandle.handle4(dataVo -> {
            dataVo.setId(UUID.randomUUID().toString());
            System.out.println("test dataVo:" + dataVo);
            return "success: " + dataVo;
        } );
    }

}
