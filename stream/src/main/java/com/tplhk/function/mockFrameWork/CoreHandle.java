package com.tplhk.function.mockFrameWork;

import com.tplhk.function.interfaces.DataHandleInterface;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * create by xiao_qiang_01@163.com
 * 2024/6/29
 */
public class CoreHandle {


    /**
     * 框架将数据 DataVo 传给业务服务
     * 框架回调业务代码（写法1）
     */
    public void handle(DataHandleInterface<DataVo> dataHandleInterface) {
        DataVo dataVo = new DataVo();
        dataVo.setAge(24);
        dataVo.setName("xiao_qiang_01");
        System.out.println("CoreHandle handle dataVo:" + dataVo);

        dataHandleInterface.handle(dataVo);

        System.out.println("CoreHandle handle dataVo after handle:" + dataVo);
    }


    /**
     * 框架将数据 DataVo 传给业务服务
     * 框架回调业务代码（写法2）
     *
     * 相比于写法1，写法2更加简洁，框架省去 DataHandleInterface 接口
     *
     * Consumer：消费型接口，只有一个入参，没有返回值
     */
    public void handle2(Consumer<DataVo> consumer) {
        DataVo dataVo = new DataVo();
        dataVo.setAge(24);
        dataVo.setName("xiao_qiang_01");
        System.out.println("CoreHandle handle dataVo:" + dataVo);

        consumer.accept(dataVo);

        System.out.println("CoreHandle handle dataVo after handle:" + dataVo);
    }


    /**
     * 框架调用业务方法
     *
     * 相比以前需要一个接口，现在只需要一个 supplier 即可
     *
     * Supplier：供给型接口，没有入参，只有一个返回值
     */
    public void handle3(Supplier<DataVo> supplier) {

        DataVo dataVo = supplier.get();

        System.out.println("CoreHandle handle dataVo after handle:" + dataVo);
    }

    /**
     * 框架调用业务方法，有输入输出
     *
     * 相比以前需要一个接口，现在只需要一个 Function 即可
     *
     * Function：函数型接口，有入参和返回值
     */
    public void handle4(Function<DataVo,  String> fun) {
        DataVo dataVo = new DataVo();
        dataVo.setAge(24);
        dataVo.setName("xiao_qiang_01");
        System.out.println("CoreHandle handle dataVo:" + dataVo);
        String result = fun.apply(dataVo);

        System.out.println("CoreHandle handle dataVo after handle:" + result);
    }
}
