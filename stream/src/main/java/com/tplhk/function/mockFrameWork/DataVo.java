package com.tplhk.function.mockFrameWork;

import lombok.Data;

/**
 * create by xiao_qiang_01@163.com
 * 2024/6/29
 */
@Data
public class DataVo {


    private String id;

    private String name;

    private int age;

}
