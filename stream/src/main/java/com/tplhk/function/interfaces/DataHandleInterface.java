package com.tplhk.function.interfaces;

/**
 * create by xiao_qiang_01@163.com
 * 2024/6/29
 */
public interface DataHandleInterface<T> {

     void handle(T data);

}
