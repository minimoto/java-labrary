package com.tplhk.stream.vo;

import lombok.Data;

/**
 * @ClassName : Parents
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/8 19:40
 **/
@Data
public class Parents {
    private String id;
    private String name;
    private String chirldId;
    private String email;
}
