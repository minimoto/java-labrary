package com.tplhk.stream.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @ClassName : Teacher
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/8 19:38
 **/
@Data
@AllArgsConstructor
public class Teacher {
    private String id;
    private String name;
    private String subject;
}
