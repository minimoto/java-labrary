package com.tplhk.stream.vo;

import lombok.Data;

/**
 * @ClassName : Subject
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/8 19:40
 **/
public enum Subject {

    Math("1", "数学"),
    Chinese("2", "汉语"),
    Music("3", "音乐"),
    English("4", "英语");

    private String code;

    private String value;

    Subject(String code, String value) {
        this.code = code;
        this.value = value;
    }

    public String getCode() {
        return this.code;
    }

    public String getValue() {
        return this.value;
    }
}