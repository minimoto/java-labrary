package com.tplhk.stream.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * @ClassName : Student
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/8 19:39
 **/
@Data
@ToString
@AllArgsConstructor
public class Student {
    private String id;
    private String name;
    private String techId;
}
