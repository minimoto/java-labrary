package com.tplhk.stream.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by xiao_qiang_01@163.com
 * 2022/4/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LessonInteractionResult {
    private String studentName;
    private Integer studentNum;
    private String interactionType;
}
