package com.tplhk.thread.completeableFuture.service;

/**
 * @ClassName : UserInfoService
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/4/27 9:13
 **/
public class UserInfoService {

    public UserInfo getUserInfo(String userId) {
        //模拟调用耗时
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //一般是查数据库，或者远程调用返回的
        return new UserInfo("666", "捡田螺的小男孩", 27);
    }

}
