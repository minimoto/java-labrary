package com.tplhk.thread.completeableFuture.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName : UserInfo
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/4/27 9:15
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CarInfo {
    private Integer id;
    private String carName;
    private Integer score;
    private String colorMapper;
}
