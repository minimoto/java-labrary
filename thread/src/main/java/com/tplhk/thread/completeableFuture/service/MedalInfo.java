package com.tplhk.thread.completeableFuture.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName : MedalInfo
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/4/27 9:17
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedalInfo {

    private String id;
    private String medalName;

}
