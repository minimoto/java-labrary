package com.tplhk.thread.countdown;

import java.util.concurrent.CountDownLatch;

/**
 * @ClassName : Worker
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/10/30 14:40
 **/
public class Worker implements Runnable {

    private final CountDownLatch startSignal;
    private final CountDownLatch doneSignal;

    public Worker(CountDownLatch startSignal, CountDownLatch doneSignal) {
        this.startSignal = startSignal;
        this.doneSignal = doneSignal;
    }

    @Override
    public void run() {
        try {
            startSignal.await(); // 放置倒数门栓。等待Driver线程执行完毕，获得开始信号
            System.out.println("Working now ...");
            doneSignal.countDown(); // 倒数门栓减1。当前worker执行完毕，释放一个完成信号
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
