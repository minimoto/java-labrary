package com.tplhk.thread.countdown;

import com.tplhk.thread.countdown.Worker;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName : TestCountDownLatch
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/10/30 14:35
 **/
public class TestCountDownLatch {


    public static void main(String[] args) throws InterruptedException {
        CountDownLatch startSignal = new CountDownLatch(1);
        CountDownLatch doneSignal = new CountDownLatch(5);
        // 依次创建并启动5个worker线程
        for (int i = 0; i < 5; ++i) {
            new Thread(new Worker(startSignal, doneSignal)).start();
        }

        System.out.println("Driver is doing something...");
        System.out.println("Driver is Finished, start all workers ...");
        startSignal.countDown(); // 倒数门栓减1。Driver执行完毕，发出开始信号，使所有的worker线程开始执行
        doneSignal.await(); // 放置另一个倒数门栓。等待所有的worker线程执行结束
        System.out.println("Finished.");
    }
}
