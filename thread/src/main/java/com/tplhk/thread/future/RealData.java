package com.tplhk.thread.future;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * @ClassName : RealData
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/4/20 10:42
 **/
public class RealData implements Callable<String> {

    private String userId;

    public RealData(String userId) {
        this.userId = userId;
    }

    @Override
    public String call() throws InterruptedException {
        System.out.println("开始处理耗时任务");
        Random random = new Random();
        int i = random.nextInt();
        System.out.println(":::i==" + i);
        Thread.sleep(1000 * 10);
        System.out.println("结束处理耗时任务");
        return String.valueOf(i);
    }
}
