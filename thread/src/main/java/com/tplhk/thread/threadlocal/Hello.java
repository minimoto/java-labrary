package com.tplhk.thread.threadlocal;

/**
 * @ClassName : Hello
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/6/12 18:00
 **/
public class Hello {

    private String name;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
