package com.tplhk.thread.cyclicbarrier;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;

/**
 * @ClassName : Writer
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/10/30 14:48
 **/
public class Writer extends Thread {

    private CyclicBarrier cyclicBarrier;

    public Writer(CyclicBarrier cyclicBarrier) {
        this.cyclicBarrier = cyclicBarrier;
    }

    @Override
    public void run() {
        System.out.println("线程 " + Thread.currentThread().getName() + " 正在写入数据");
        try {
            TimeUnit.SECONDS.sleep(3);
            System.out.println("线程" + Thread.currentThread().getName() + " ok");
            cyclicBarrier.await();

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }

    }
}
