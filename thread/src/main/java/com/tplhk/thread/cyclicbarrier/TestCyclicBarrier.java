package com.tplhk.thread.cyclicbarrier;

import java.util.concurrent.CyclicBarrier;

/**
 * @ClassName : TestCyclicBarrier
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/10/30 14:47
 **/
public class TestCyclicBarrier {

    public static void main(String[] args) {
        int n = 5;
        CyclicBarrier cyclicBarrier = new CyclicBarrier(n, new Runnable() {
            @Override
            public void run() {
                System.out.println("所有线程都写完数据库啦!!");
            }
        });

        for (int i = 0; i < n; i++) {
            new Writer(cyclicBarrier).start();
        }
    }
}
