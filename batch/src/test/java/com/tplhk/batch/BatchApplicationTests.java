package com.tplhk.batch;

import com.tplhk.batch.readStr.repository.OrderEntity;
import com.tplhk.batch.readStr.repository.task.OrderDomainService;
import org.junit.jupiter.api.Test;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = BatchApplication.class)
class BatchApplicationTests {
    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job processJob;
    @Autowired
    Job jobDemoJob;
    @Autowired
    Job flowDemoJob;
    @Autowired
    Job splitDemoJob;
    @Autowired
    Job deciderDemoJob;
    @Autowired
    Job nestedJobDemoJob;
    @Autowired
    Job parameterDemoJob;
    @Autowired
    Job taskletDemoJob;

    @Test
    void testReadStr() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        // 如果 job 执行失败， 可以使用相同的 jobParameters 再次执行。此时BATCH_JOB_EXECUTION.JOB_INSTANCE_ID 值是不变的
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(processJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(jobDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseFlow() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(flowDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseSplit() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(splitDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseDecider() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(deciderDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseNestedJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(nestedJobDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testUseParameterJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(parameterDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }

    @Test
    void testTaskletDemo() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(taskletDemoJob, jobParameters);

        System.out.println("Batch job has been invoked");
    }


    @Autowired
    OrderDomainService orderDomainService;

    @Test
    void testDB() {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderNo("kllk");
        orderEntity.setUserId("xjq");
        orderEntity.setCommodityCode("1");
        orderEntity.setAmount(1D);
        orderEntity.setCount(100);
        boolean save = orderDomainService.saveOrUpdate(orderEntity);
        if (save) {
            System.out.println("ok");
        } else {
            System.out.println("no");

        }
    }


}
