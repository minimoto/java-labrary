package com.tplhk.batch.uselisten;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

/**
 * @ClassName : MyJobListener
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 15:00
 **/
public class MyJobListener implements JobExecutionListener {
    @Override
    public void beforeJob(JobExecution jobExecution) {
        System.out.println(jobExecution.getJobInstance().getJobName() + " before.....");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        System.out.println(jobExecution.getJobInstance().getJobName() + " after.....");

    }
}
