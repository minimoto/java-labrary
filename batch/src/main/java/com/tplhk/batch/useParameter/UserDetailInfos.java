package com.tplhk.batch.useParameter;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName : UserInfos
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 16:41
 **/
@Component
@Data
public class UserDetailInfos {
    private List<UserDetailInfo> userDetailInfos = new LinkedList<>();
}
