package com.tplhk.batch.useParameter;

import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
@EnableBatchProcessing
public class ParameterDemo {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    Step parameterDemoStep1;

    @Autowired
    Step parameterDemoStep2;

//    @Autowired
//    Step parameterDemoStep3;

    @Bean
    public Job parameterDemoJob() {
        return jobBuilderFactory.get("jobDemoJob")
                .start(parameterDemoStep1)
                .next(parameterDemoStep2)
//                .next(parameterDemoStep3)
                .build();
    }

}
