package com.tplhk.batch.useParameter;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @ClassName : UserInfo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 16:40
 **/
@Data
@Builder
@ToString
public class UserDetailInfo {
    private String name;
    private String sex;
    private String address;
}
