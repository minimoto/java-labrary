package com.tplhk.batch.usedecider;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
@EnableBatchProcessing
public class DeciderDemo {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    Step commonDemoStep1;

    @Autowired
    Step commonDemoStep2;

    @Autowired
    Step commonDemoStep3;

    @Autowired
    Step errorStep;

    @Bean
    public Job deciderDemoJob() {
        return jobBuilderFactory.get("flowDemoJob")
                .start(errorStep)
                .next(myDecider())
                .from(myDecider()).on("completed").to(commonDemoStep2)
                .from(myDecider()).on("fail").to(commonDemoStep3)
                .from(commonDemoStep3).on("*").to(myDecider())
                .end()
                .build();

    }

    @Bean
    public JobExecutionDecider myDecider() {
        return new MyDecider();
    }


}
