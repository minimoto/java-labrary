package com.tplhk.batch.usedecider;

import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;

import javax.batch.runtime.BatchStatus;

/**
 * @ClassName : MyDecider
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 13:59
 **/
public class MyDecider implements JobExecutionDecider {

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        if (stepExecution.getStatus().getBatchStatus().equals(BatchStatus.COMPLETED)) {
            return new FlowExecutionStatus("completed");
        } else {
            return new FlowExecutionStatus("fail");
        }
    }
}
