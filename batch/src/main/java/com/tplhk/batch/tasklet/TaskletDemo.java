package com.tplhk.batch.tasklet;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
public class TaskletDemo {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    Step errorStep;

    @Autowired
    Step commonDemoStep1;

    @Autowired
    Step commonDemoStep2;

    @Bean
    public Job taskletDemoJob() {
        return jobBuilderFactory.get("taskletDemoJob")
                .start(errorStep)
                .next(commonDemoStep1)
                .next(commonDemoStep2)
                .build();

    }


}
