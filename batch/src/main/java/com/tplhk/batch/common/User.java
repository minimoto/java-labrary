package com.tplhk.batch.common;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

/**
 * @ClassName : User
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/8 11:08
 **/
@Data
@Builder
@ToString
public class User {

    private String name;
    private Integer age;
}
