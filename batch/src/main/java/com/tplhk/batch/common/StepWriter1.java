package com.tplhk.batch.common;

import com.tplhk.batch.useParameter.UserDetailInfo;
import com.tplhk.batch.useParameter.UserDetailInfos;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName : Writer
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:15
 **/
@Component
public class StepWriter1 implements ItemWriter<List<String>> {

    @Autowired
    UserDetailInfos userDetailInfos;


    @Override
    public void write(List<? extends List<String>> list) {
        List<String> tempList = list.get(0);
        tempList.stream()
                .map(item -> UserDetailInfo.builder().name(item).sex("男").address(item + " 在中国").build())
                .forEach(item -> userDetailInfos.getUserDetailInfos().add(item));
    }
}
