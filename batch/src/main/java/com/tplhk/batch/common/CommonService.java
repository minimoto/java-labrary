package com.tplhk.batch.common;

import org.springframework.stereotype.Service;

/**
 * @ClassName : CommonService
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/8 10:15
 **/
@Service
public class CommonService {

    public String testService1() {
        System.out.println("this is testService1 ");
        return "ok";
    }

    public void testServiceParam(String param) {
        System.out.println(param);
    }

}
