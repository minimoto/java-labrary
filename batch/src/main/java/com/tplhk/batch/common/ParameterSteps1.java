package com.tplhk.batch.common;

import com.tplhk.batch.useParameter.UserInfo;
import com.tplhk.batch.useParameter.UserInfos;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sound.midi.Soundbank;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
@EnableBatchProcessing
public class ParameterSteps1 {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    StepReader1 stepReader1;

    @Autowired
    StepProcessor1 stepProcessor1;

    @Autowired
    StepWriter1 stepWriter1;

    @Bean
    public Step parameterDemoStep1() {
        return stepBuilderFactory.get("parameterDemoStep1")
                .<UserInfos, List<String>>chunk(1)
                .reader(stepReader1)
                .faultTolerant().skipLimit(3).skip(Exception.class)
                .processor(stepProcessor1)
                .writer(stepWriter1)
                .build();
    }


}
