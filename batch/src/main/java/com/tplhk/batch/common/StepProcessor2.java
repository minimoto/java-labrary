package com.tplhk.batch.common;

import com.tplhk.batch.useParameter.UserDetailInfos;
import com.tplhk.batch.useParameter.UserInfos;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName : StepProcessor2
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/23 14:10
 **/
@Component
public class StepProcessor2 implements ItemProcessor<List<String>, String> {

    private int tryCount = 1;
    private int errorCount = 0;

    @Override
    public String process(List<String> data) throws Exception {
//        if(tryCount<= 10) {
//            tryCount++;
//            errorCount++;
//            throw new Exception("stepRead2 exception : " + errorCount);
//        }
//        tryCount++;
        data.stream().forEach(item -> System.out.println(item.toUpperCase()));
        return "ok";
    }
}
