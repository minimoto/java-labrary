package com.tplhk.batch.common;

import com.tplhk.batch.useParameter.UserDetailInfos;
import com.tplhk.batch.useParameter.UserInfo;
import com.tplhk.batch.useParameter.UserInfos;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sound.midi.Soundbank;
import java.util.List;
import java.util.Map;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
@EnableBatchProcessing
public class ParameterSteps2 {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    StepReader2 stepReader2;

    @Autowired
    StepProcessor2 stepProcessor2;

    @Autowired
    StepWriter2 stepWriter2;

    @Bean
    public Step parameterDemoStep2() {
        return stepBuilderFactory.get("parameterDemoStep2")
                .<List<String>, String>chunk(3)
                .reader(stepReader2)
                .processor(stepProcessor2)
                .writer(stepWriter2)
//                .faultTolerant().skipLimit(4).skip(Exception.class)
                .build();
    }


}
