package com.tplhk.batch.common;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @ClassName : CommonDemoStep2Tasklet
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/8 14:55
 **/
@StepScope
@Component
public class CommonDemoStep2Tasklet implements Tasklet {

    @Autowired
    CommonService commonService;

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        commonService.testServiceParam("this is CommonDemoStep2Tasklet");

        // 取外部参数
        String time = String.valueOf(chunkContext.getStepContext().getJobParameters().get("time"));
        System.out.println(time);

        return RepeatStatus.FINISHED;
    }
}
