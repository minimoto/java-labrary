package com.tplhk.batch.common;

import com.google.common.collect.Lists;
import com.tplhk.batch.useParameter.UserDetailInfo;
import com.tplhk.batch.useParameter.UserDetailInfos;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName : StepReader2
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/23 14:34
 **/
@Component
public class StepReader2 implements ItemReader<List<String>> {

    @Autowired
    UserDetailInfos userDetailInfos;

    int count = 0;

    private int tryCount = 1;

    private int errcount = 0;

    @Override
    public List<String> read() throws Exception {
//        if(tryCount%2 == 0) {
//            tryCount++;
//            errcount++;
//            throw new Exception("stepRead2 exception : " + errcount);
//        }
//        tryCount++;
        List<String> ret = Lists.newArrayList();
        List<UserDetailInfo> userDetailInfos = this.userDetailInfos.getUserDetailInfos();
        if (count == userDetailInfos.size()) {
            return null;
        }
        UserDetailInfo userDetailInfo = userDetailInfos.get(count);
        ret.add(userDetailInfo.getName() + " " + userDetailInfo.getAddress());
        count++;
        return ret;
    }

}
