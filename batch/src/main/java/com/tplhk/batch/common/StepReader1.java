package com.tplhk.batch.common;

import com.tplhk.batch.useParameter.UserInfo;
import com.tplhk.batch.useParameter.UserInfos;
import org.springframework.batch.core.ItemReadListener;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @ClassName : Reader
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 14:39
 **/
@Component
public class StepReader1 implements ItemReader<UserInfos> {
    private String[] messages = {"111111", "222222", "333333",
            "4444444", "55555", "66666", "77777", "88888", "99999", "00000"};
    private int count = 0;

    @Autowired
    UserInfos userInfos;

    private int tryCount = 1;

    @Override
    public UserInfos read() throws Exception {
        if (count == 0) {
            if (tryCount <= 3) {
                tryCount++;
                throw new Exception("stepRead1 exception");
            }
            Arrays.stream(messages)
                    .map(item -> UserInfo.builder().id(item).name(item).build())
                    .forEach(item -> userInfos.getUserInfoList().add(item));
            count++;
            return userInfos;
        } else {
            return null;
        }
    }


//    @Override
//    public String read() throws Exception {
//        if (count < messages.length) {
//            return messages[count++];
//        } else {
//            count = 0;
//        }
//        return null;
//    }


}
