package com.tplhk.batch.common;

import com.google.common.collect.Lists;
import com.tplhk.batch.useParameter.UserInfo;
import com.tplhk.batch.useParameter.UserInfos;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @ClassName : Processor
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:13
 **/
@Component
public class StepProcessor1 implements ItemProcessor<UserInfos, List<String>> {

    private int tryCount = 1;

    @Override
    public List<String> process(UserInfos data) throws Exception {

        return data.getUserInfoList()
                .stream()
                .map(item -> item.getName() + " in StepProcessor1")
                .collect(Collectors.toList());

    }

}