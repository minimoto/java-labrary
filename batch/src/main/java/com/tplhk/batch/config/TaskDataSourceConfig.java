package com.tplhk.batch.config;


import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.baomidou.mybatisplus.core.MybatisXMLLanguageDriver;
import com.baomidou.mybatisplus.extension.spring.MybatisSqlSessionFactoryBean;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.type.JdbcType;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

//表示这个类为一个配置类
@Configuration
// 配置mybatis的接口类放的地方
@MapperScan(basePackages = "com.tplhk.batch.readStr.mapper.task", sqlSessionFactoryRef = "taskSqlSessionFactory")
public class TaskDataSourceConfig {
    // 将这个对象放入Spring容器中
    @Bean(name = "taskDataSource")
    @Qualifier("taskDataSource")
    // 读取application.properties中的配置参数映射成为一个对象
    // prefix表示参数的前缀
    @ConfigurationProperties(prefix = "spring.datasource.task")
    public DataSource getDateSource1() {
        return DataSourceBuilder.create().build();
    }

//	mybatis 使用这个配置
//	@Bean(name = "taskSqlSessionFactory")
//	@Qualifier("taskSqlSessionFactory")
//	// @Qualifier表示查找Spring容器中名字为test1DataSource的对象
//	public SqlSessionFactory taskSqlSessionFactory(@Qualifier("taskDataSource") DataSource datasource)
//			throws Exception {
//		SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
//		bean.setDataSource(datasource);
//		bean.setMapperLocations(
//				// 设置mybatis的xml所在位置
//				new PathMatchingResourcePatternResolver().getResources("classpath:mapper/task/*.xml"));
//		return bean.getObject();
//	}

    //	mybatis-puls 使用这个配置
    @Bean("taskSqlSessionFactory")
    public SqlSessionFactory b2bSqlSessionFactory(@Qualifier("taskDataSource") DataSource datasource) throws Exception {
        MybatisSqlSessionFactoryBean factoryBean = new MybatisSqlSessionFactoryBean();
        factoryBean.setDataSource(datasource);

        MybatisConfiguration configuration = new MybatisConfiguration();
        configuration.setDefaultScriptingLanguage(MybatisXMLLanguageDriver.class);
        configuration.setJdbcTypeForNull(JdbcType.NULL);
        factoryBean.setConfiguration(configuration);
        //指定xml路径.
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:mapper/task/*.xml"));
//		factoryBean.setPlugins(new Interceptor[]{
//				new PaginationInterceptor(),
//				new PerformanceInterceptor(),
//				new OptimisticLockerInterceptor()
//		});
        return factoryBean.getObject();
    }

    @Bean("taskSqlSessionTemplate")
    @Qualifier("taskSqlSessionTemplate")
    public SqlSessionTemplate taskSqlSessionTemplate(
            @Qualifier("taskSqlSessionFactory") SqlSessionFactory sessionfactory) {
        return new SqlSessionTemplate(sessionfactory);
    }

}
