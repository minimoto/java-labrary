package com.tplhk.batch.config;

import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : AppConfig
 * @Description : 全局配置类
 * @Author : taiping
 * @Date: 2020/11/30 14:42
 **/
@Configuration
public class AppConfig {
}
