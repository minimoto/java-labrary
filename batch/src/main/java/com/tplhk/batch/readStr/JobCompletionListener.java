package com.tplhk.batch.readStr;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

/**
 * @ClassName : JobCompletionListener
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:15
 **/
public class JobCompletionListener extends JobExecutionListenerSupport {

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            System.out.println("BATCH JOB COMPLETED SUCCESSFULLY");
        } else if (jobExecution.getStatus() == BatchStatus.FAILED) {
            System.out.println("BATCH JOB FAILED");
        }
    }

}
