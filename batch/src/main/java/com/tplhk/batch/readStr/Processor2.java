package com.tplhk.batch.readStr;

import com.tplhk.batch.readStr.repository.OrderEntity;
import com.tplhk.batch.readStr.repository.task.OrderDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName : Processor
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:13
 **/
@Slf4j
@Component
@StepScope
public class Processor2 implements ItemProcessor<String, String> {

    private int retryTime = 0;

    @Value("#{jobParameters['time']}")
    private String time;

    @Override
    public String process(String data) throws Exception {
        if (data.equals("222222") || data.equals("4444444")) {
            // 相当于过滤
//            return null;
            System.out.println("读取重试数据：" + data + " ; 重试次数 : " + (++retryTime));
            throw new Exception("process 异常 ！");
        }

        log.info("time:::::::::" + time);
        System.out.println("处理数据中: " + data);
        return data;
    }

}