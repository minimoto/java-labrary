package com.tplhk.batch.readStr.repository.task;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tplhk.batch.readStr.repository.OrderEntity;

/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2019/11/21
 * @Describe
 */
public interface OrderDomainService extends IService<OrderEntity> {


}
