package com.tplhk.batch.readStr.mapper.task;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.tplhk.batch.readStr.repository.OrderEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 *
 */
@Component
public interface OrderMapper extends BaseMapper<OrderEntity> {

}