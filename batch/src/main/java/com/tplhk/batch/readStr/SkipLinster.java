package com.tplhk.batch.readStr;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.SkipListener;
import org.springframework.batch.core.listener.SkipListenerSupport;

/**
 * create by xiao_qiang_01@163.com
 * 2020/12/26
 */
@Slf4j
public class SkipLinster implements SkipListener<String, String> {

    @Override
    public void onSkipInRead(Throwable throwable) {

    }

    @Override
    public void onSkipInWrite(String s, Throwable throwable) {
        log.error("Write ： 跳过异常数据 : " + s);
    }

    @Override
    public void onSkipInProcess(String s, Throwable throwable) {
        log.error("Process ： 跳过异常数据 : " + s);

    }
}
