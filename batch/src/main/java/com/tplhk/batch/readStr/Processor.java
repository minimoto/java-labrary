package com.tplhk.batch.readStr;

import org.springframework.batch.item.ItemProcessor;

/**
 * @ClassName : Processor
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:13
 **/
public class Processor implements ItemProcessor<String, String> {

    @Override
    public String process(String data) throws Exception {
        return data.toUpperCase();
    }

}