package com.tplhk.batch.readStr;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2020/12/26
 */
@Slf4j
public class WriteLinster implements ItemWriteListener<String> {
    @Override
    public void beforeWrite(List<? extends String> list) {

    }

    @Override
    public void afterWrite(List<? extends String> list) {

    }

    @Override
    public void onWriteError(Exception e, List<? extends String> list) {

        log.error("写入数据异常：{}, 数据: {} ", e.getMessage(), list.toString());
    }
}
