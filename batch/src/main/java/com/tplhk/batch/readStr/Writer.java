package com.tplhk.batch.readStr;

import com.tplhk.batch.readStr.repository.OrderEntity;
import com.tplhk.batch.readStr.repository.task.OrderDomainService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @ClassName : Writer
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 16:15
 **/
public class Writer implements ItemWriter<String> {

    @Autowired
    OrderDomainService orderDomainService;

//    private int retryTime = 0;


    @Override
    public void write(List<? extends String> messages) throws Exception {

//        if(retryTime %2  == 0){
//            retryTime++;
//            throw new Exception("读取错误");
//        }
//        retryTime++;
//        System.out.println("写入数据: " + messages.toString());

        for (String msg : messages) {
            if (msg.equals("111111") || msg.equals("88888")) {
                throw new Exception("读取错误");
            }
            System.out.println("写入数据: " + msg);
        }

//        for (String msg : messages) {
//            System.out.println("Writing the data " + msg);
//            OrderEntity orderEntity = new OrderEntity();
//            orderEntity.setOrderNo(msg);
//            orderEntity.setCommodityCode(msg);
//            orderEntity.setCount(1);
//            orderEntity.setAmount(1D);
//            System.out.println("写入数据库...");
//            orderDomainService.save(orderEntity);
//        }
    }

}
