package com.tplhk.batch.readStr;

import org.springframework.batch.core.*;
import org.springframework.util.StringUtils;

/**
 * @ClassName : FileItemReadListener
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 17:29
 **/
public class FileItemReadListener implements ItemReadListener<String> {

    private Writer writer;

    public FileItemReadListener(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void beforeRead() {

    }

    @Override
    public void afterRead(String s) {

    }

    @Override
    public void onReadError(Exception e) {
//        writer.write(format("%s%n", e.getMessage()));
        System.out.println("错误信息：" + e.getMessage());
    }
}
