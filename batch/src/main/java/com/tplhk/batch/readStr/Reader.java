package com.tplhk.batch.readStr;

import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @ClassName : Reader
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/6 14:39
 **/
public class Reader implements ItemReader<String> {
    private String[] messages = {"111111", "222222", "333333",
            "4444444", "55555", "66666", "77777", "88888", "99999",
            "aaaaaaa", "bbbbbbb",
            "cccccc", "ddddd", "eeeee", "fffff", "ggggg", "hhhhhh"};
    private int count = 0;

    private int errCount = 0;

    @Override
    public String read() throws Exception {
        if (count < messages.length) {
//            if(errCount<5){
//                errCount++;
//                throw new Exception("读取错误");
//            }
            System.out.println("read 成功数据: " + messages[count]);
            return messages[count++];
        }
        return null;
    }

//    @Override
//    public String read() throws Exception {
//        if (count < messages.length) {
//            return messages[count++];
//        } else {
//            count = 0;
//        }
//        return null;
//    }


}
