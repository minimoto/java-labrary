package com.tplhk.batch.readStr.repository;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.io.Serializable;

/**
 * @ClassName : OrderEntity
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 10:29
 **/
@Data
@TableName("t_order")
public class OrderEntity extends Model<OrderEntity> {

    @TableId(value = "id", type = IdType.AUTO)
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;

    private String orderNo;

    private String userId;

    private String commodityCode;

    private Integer count;

    private Double amount;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }
}
