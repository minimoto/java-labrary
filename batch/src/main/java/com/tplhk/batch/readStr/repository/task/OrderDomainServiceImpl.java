package com.tplhk.batch.readStr.repository.task;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tplhk.batch.readStr.mapper.task.OrderMapper;
import com.tplhk.batch.readStr.repository.OrderEntity;
import com.tplhk.batch.readStr.repository.task.OrderDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2019/11/21
 * @Describe
 */
@Service
public class OrderDomainServiceImpl extends ServiceImpl<OrderMapper, OrderEntity> implements OrderDomainService {

    @Autowired
    OrderMapper orderMapper;


}
