package com.tplhk.batch.usejob;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : JobDemo
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/12/14 11:41
 **/
@Configuration
@EnableBatchProcessing
public class JobDemo {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    Step commonDemoStep1;

    @Autowired
    Step commonDemoStep2;

    @Autowired
    Step commonDemoStep3;

    @Bean
    public Job jobDemoJob() {
        //  执行顺序 step1 -> step2 -> step3
//        return jobBuilderFactory.get("jobDemoJob")
//                .start(step1())
//                .next(step2())
//                .next(step3())
//                .build();

        return jobBuilderFactory.get("jobDemoJob")
                .start(commonDemoStep1).on("COMPLETED").to(commonDemoStep2)
                .from(commonDemoStep2).on("COMPLETED").to(commonDemoStep3)
                .from(commonDemoStep3).end()
                .build();
    }


}
