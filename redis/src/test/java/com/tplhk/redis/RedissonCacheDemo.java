package com.tplhk.redis;


import com.tplhk.redis.lock.redisson.dto.User;
import com.tplhk.redis.lock.redisson.service.RedissonCacheService;
import com.zengtengpeng.annotation.EnableCache;
import com.zengtengpeng.operation.RedissonObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SpringBootTest(classes = RedisApplication.class)
@EnableCache(value = {"cache1", "cache2"})
public class RedissonCacheDemo {

    @Autowired
    RedissonCacheService redissonCacheService;


    @Test
    public void testGetUserInfo() {
        User userInfo = redissonCacheService.getUserInfo("123");
        System.out.println(userInfo.toString());
    }

    @Test
    public void testPutUserInfo() {
        User user = User.builder().userId("123").username("123:77777777777").build();
        redissonCacheService.putUserInfo(user);
        System.out.println(redissonCacheService.getUserInfo("123").toString());
    }

    @Test
    public void testDeleteUserInfo() {
        redissonCacheService.deleteUserInfo("123");
    }

    @Test
    public void testRedissonObject() {
        redissonCacheService.testRedissonObject();

    }

    @Test
    public void testRedissonBinary(HttpServletResponse response) throws IOException {
        redissonCacheService.testRedissonBinary(response);

    }

    @Test
    public void testRedissonCollection() {
        redissonCacheService.testRedissonCollection();
    }

}
