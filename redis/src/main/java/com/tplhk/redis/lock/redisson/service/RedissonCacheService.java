package com.tplhk.redis.lock.redisson.service;

import com.tplhk.redis.lock.redisson.dto.User;
import com.zengtengpeng.operation.RedissonBinary;
import com.zengtengpeng.operation.RedissonCollection;
import com.zengtengpeng.operation.RedissonObject;
import org.redisson.api.RList;
import org.redisson.api.RMap;
import org.redisson.api.RSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


@Service
public class RedissonCacheService {

    @Autowired
    private RedissonObject redissonObject;

    @Autowired
    private RedissonBinary redissonBinary;

    @Autowired
    private RedissonCollection redissonCollection;


    /**
     * 通过返回值加入缓存
     *
     * @param userId
     * @return
     */
    @Cacheable(key = "#userId", value = "cache1")
    public User getUserInfo(String userId) {
        System.out.println("进入 getUserInfo");
        return User.builder().userId(userId).username(userId + ":haha").build();
    }

    /**
     * 通过返回值修改缓存
     *
     * @param user
     * @return
     */
    @CachePut(key = "#user.userId", value = "cache1")
    public User putUserInfo(User user) {
        System.out.println("进入 putUserInfo");
        return user;
    }

    /**
     * 删除缓存
     *
     * @param userId
     * @return
     */
    @CacheEvict(key = "#userId", value = "cache1")
    public void deleteUserInfo(String userId) {
        System.out.println("进入 deleteUserInfo");
    }


    /**
     * 缓存对象 redissonObject 的使用：最常用的方式
     */
    public void testRedissonObject() {
        User user = new User();
        user.setUserId("123");
        user.setUsername("123456789");

        // 设值，多次设值会履盖
        redissonObject.setValue("123", user, -1L);
        // 取值
        System.out.println("取 123 的值 : " + redissonObject.getValue("123"));

        redissonObject.trySetValue("123", "前面设置了123，所以不会再设置");

        redissonObject.trySetValue("456", "这个 456 没有设置，所以会写入缓存");

        System.out.println("取 123 的值 : " + redissonObject.getValue("123"));

        System.out.println("取 456 的值 : " + redissonObject.getValue("456"));

    }

    /**
     * 缓存二进制对象 redissonBinary 的使用：存储文件
     * 未测试
     */
    public void testRedissonBinary(HttpServletResponse response) throws IOException {
        redissonBinary.setValue("binary", new FileInputStream(new File("D:\\BaiduNetdiskDownload\\Istio实战指南.pdf")));
        redissonBinary.getValue("binary", response.getOutputStream());
    }

    /**
     * 缓存容器对象 redissonCollection 的使用：存储 list, map, set
     * 未测试
     */
    public void testRedissonCollection() {
        Map<String, String> map = new HashMap<>();
        map.put("test1", "test11");
        map.put("test2", "test22");
        map.put("test3", "test33");
        map.put("test4", "test44");
        //设置值
        redissonCollection.setMapValues("map", map);

        //获取值
        RMap<String, String> test = redissonCollection.getMap("map");
        System.out.println(test);

        List<String> list = new ArrayList<>();
        list.add("test1");
        list.add("test2");
        list.add("test3");
        list.add("test4");
        list.add("test5");
        //设置值
        redissonCollection.setListValues("list", list);

        //获取值
        RList<Object> list1 = redissonCollection.getList("list");
        System.out.println(list1);


        Set<String> set = new HashSet<>();
        set.add("test1");
        set.add("test2");
        set.add("test3");
        set.add("test4");
        set.add("test5");
        //设置值
        redissonCollection.setSetValues("set", set);

        //获取值
        RSet<Object> set1 = redissonCollection.getSet("set");
        System.out.println(set1);

    }


}
