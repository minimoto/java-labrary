package com.tplhk.redis.lock.jedis.util;

import cn.hutool.core.thread.threadlocal.NamedThreadLocal;

public class RedisLockThreadLocalContext {
    private static ThreadLocal<String> threadLocal = new NamedThreadLocal<>("REDIS-LOCK-LOCAL-CONTEXT");

    public static ThreadLocal<String> getThreadLocal() {
        return threadLocal;
    }
}
