/**
 * Copyright (C) 2010 dennis zhuang (killme2008@gmail.com)
 * <p>
 * This library is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Lesser General Public License as published by the Free Software Foundation; either version
 * 2.1 of the License, or (at your option) any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 * <p>
 * You should have received a copy of the GNU Lesser General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 **/
package com.googlecode.aviator;

import java.util.HashMap;
import java.util.Map;


/**
 * A expression
 *
 * @author xujunqiang
 *
 */
public enum ExpressionCustomizeManager {

    INSTANCE;

    private Map<String, ExpressCustomize> expressHashMap = new HashMap();

    public void addExpress(String name, String express) {
        ExpressCustomize expressCustomize = new ExpressCustomize();
        expressCustomize.setExpress(express);
        expressHashMap.put(name, expressCustomize);
    }

    public void addExpress(String name, ExpressCustomize expressCustomize) {
        expressHashMap.put(name, expressCustomize);
    }

    public void deleteExpress(String name) {
        expressHashMap.remove(name);
    }

    public ExpressCustomize getExpress(String name) {
        return expressHashMap.getOrDefault(name, null);
    }

}
