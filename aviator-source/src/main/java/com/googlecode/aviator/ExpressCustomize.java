package com.googlecode.aviator;


/**
 * @ClassName : ExpressCustomize
 * @Description : todo add by xujunqiang
 * @Author : xujunqiang
 * @Date: 2021/9/3 12:38
 **/
public class ExpressCustomize {
    private String name;
    private String express;

    public static ExpressCustomize build() {
        return new ExpressCustomize();
    }

    public String getExpress() {
        return express;
    }

    public ExpressCustomize setExpress(String express) {
        this.express = express;
        return this;
    }

    public String getName() {
        return name;
    }

    public ExpressCustomize setName(String name) {
        this.name = name;
        return this;
    }
}
