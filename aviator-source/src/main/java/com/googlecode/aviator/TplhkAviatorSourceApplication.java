package com.googlecode.aviator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TplhkAviatorSourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(TplhkAviatorSourceApplication.class, args);
    }

}
