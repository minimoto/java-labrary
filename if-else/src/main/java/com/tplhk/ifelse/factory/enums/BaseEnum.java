package com.tplhk.ifelse.factory.enums;

/**
 * 顶层枚举接口
 *
 * @author wangzhijun
 * @date 16:27 2020/5/13
 **/
public interface BaseEnum<V> {

    /**
     * 返回code
     *
     * @return
     */
    V getCode();

    /**
     * 返回value
     *
     * @return
     */
    String getMessage();


}
