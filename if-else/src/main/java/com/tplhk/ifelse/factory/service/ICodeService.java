package com.tplhk.ifelse.factory.service;

import java.util.List;
import java.util.Map;


/**
 * 数据字典服务 最顶层的接口
 *
 * @author wangzhijun
 * @date 2020/5/25 14:04
 **/
public interface ICodeService {


    /**
     * @return
     */
    void dosomething();
}
