package com.tplhk.ifelse.factory.enums;

/**
 * 码库服务
 *
 * @author xiao_qiang_01@163.com
 * @date 2020/5/25 17:25
 **/
public enum CodeServiceEnums implements BaseEnum<Integer> {

    /**
     * 服务类型
     */
    CODE_SERVICE(1, "码库服务"),
    CUSTOMER_SERVICE(2, "客户服务"),
    POLICYNO_SERVICE(3, "保单服务");


    private Integer code;

    private String message;


    CodeServiceEnums(Integer code, String message) {
        this.code = code;
        this.message = message;
    }


    @Override
    public Integer getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }
}
