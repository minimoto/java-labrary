package com.tplhk.ifelse.factory.processor;

import cn.hutool.core.lang.ClassScanner;
import com.tplhk.ifelse.factory.annotation.CodeType;
import com.tplhk.ifelse.factory.enums.CodeServiceEnums;
import com.tplhk.ifelse.factory.service.impl.CodeFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 码表处理
 *
 * @author wangzhijun
 * @date 2020/5/25 14:30
 **/
@Component
@Slf4j
public class HandCodeProcessor implements BeanFactoryPostProcessor {

    public static final String SCAN_PACKEAGE = "com.tplhk.ifelse.factory.service.impl.strategy";

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        Map<Integer, Class> handsMap = new HashMap<>(5);
        ClassScanner.scanPackageByAnnotation(SCAN_PACKEAGE, CodeType.class).forEach(classz -> {
            CodeServiceEnums type = classz.getAnnotation(CodeType.class).value();
            handsMap.put(type.getCode(), classz);
        });
        //初始化类，将数据注册到容器中
        CodeFactory codeFactory = new CodeFactory(handsMap);
        beanFactory.registerSingleton(CodeFactory.class.getName(), codeFactory);
    }


}
