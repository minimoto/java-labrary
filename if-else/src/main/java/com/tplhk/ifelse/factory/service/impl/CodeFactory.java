package com.tplhk.ifelse.factory.service.impl;

import com.tplhk.ifelse.factory.service.ICodeService;
import com.tplhk.ifelse.factory.util.SpringUtil;

import java.util.Map;

/**
 * 处理上下文
 *
 * @author wangzhijun
 * @date 2020/5/25 14:42
 **/
public class CodeFactory {

    Map<Integer, Class> handMap;

    public CodeFactory(Map<Integer, Class> map) {
        this.handMap = map;
    }

    public ICodeService getInstance(Integer type) {
        Class classz = handMap.get(type);
        if (classz == null) {
            throw new IllegalArgumentException("not found hand type:" + type);
        }
        return (ICodeService) SpringUtil.getBean(classz);

    }
}
