package com.tplhk.ifelse.factory.service.impl.strategy;

import com.tplhk.ifelse.factory.annotation.CodeType;
import com.tplhk.ifelse.factory.enums.CodeServiceEnums;
import com.tplhk.ifelse.factory.service.ICodeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 码表服务 处理数据字典
 *
 * @author wangzhijun
 * @date 2020/5/13 16:00
 **/
@Service
@CodeType(CodeServiceEnums.CUSTOMER_SERVICE)
@Slf4j
public class CustormerServiceImpl implements ICodeService {


    @Override
    public void dosomething() {
        log.info("this is " + CodeServiceEnums.CUSTOMER_SERVICE.getMessage());
    }
}
