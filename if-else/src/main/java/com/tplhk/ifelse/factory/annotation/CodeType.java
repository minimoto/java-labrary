package com.tplhk.ifelse.factory.annotation;


import com.tplhk.ifelse.factory.enums.CodeServiceEnums;

import java.lang.annotation.*;

/**
 * 码表类型
 *
 * @author wangzhijun
 * @date 2020/5/25 14:20
 **/
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CodeType {

    CodeServiceEnums value();
}
