package com;

import com.tplhk.ifelse.factory.IfelseApplication;
import com.tplhk.ifelse.factory.enums.CodeServiceEnums;
import com.tplhk.ifelse.factory.service.impl.CodeFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @ClassName : test
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/10/30 10:37
 **/
@SpringBootTest(classes = IfelseApplication.class)
public class test {

    @Autowired
    CodeFactory codeFactory;

    @Test
    public void testIfelse() {
        codeFactory.getInstance(CodeServiceEnums.CODE_SERVICE.getCode()).dosomething();
        codeFactory.getInstance(CodeServiceEnums.CUSTOMER_SERVICE.getCode()).dosomething();
        codeFactory.getInstance(CodeServiceEnums.POLICYNO_SERVICE.getCode()).dosomething();
    }
}
