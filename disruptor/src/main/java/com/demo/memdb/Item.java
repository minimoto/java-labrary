package com.demo.memdb;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商品
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Item implements Serializable {

  private static final long serialVersionUID = -873268150277605569L;
  /**
   * ID
   */
  private String id;

  /**
   * 库存
   */
  private int amount;

  /**
   * 减库存，如果库存不足，则扣减失败
   *
   * @return
   */
  public boolean decreaseAmount() {

    if (!hasRemaining()) {
      return false;
    }
    amount--;
    return true;

  }

  /**
   * 是否还有库存
   *
   * @return
   */
  public boolean hasRemaining() {
    return amount > 0;
  }


}
