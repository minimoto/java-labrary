package com.demo.request;

import cn.hutool.core.collection.CollectionUtil;
import com.lmax.disruptor.EventHandler;
import com.demo.command.infras.Command;
import com.demo.command.infras.CommandDispatcher;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 将结果RequestDtoEvent的结果输出到数据库
 */
@Slf4j
public class RequestDtoEventDbOutputer implements EventHandler<RequestDtoEvent> {

  private CommandDispatcher commandDispatcher;

  @Override
  public void onEvent(RequestDtoEvent event, long sequence, boolean endOfBatch) throws Exception {
    log.info("将结果RequestDtoEvent 通过分发类 commandDispatcher ,发到对应的环环");
    if (event.hasErrorOrException()) {
      return;
    }

    List<Command> commandList = event.getCommandCollector().getCommandList();
    if (CollectionUtil.isEmpty(commandList)) {
      return;
    }

    for (Command command : commandList) {
      commandDispatcher.dispatch(command);
    }
  }

  public void setCommandDispatcher(CommandDispatcher commandDispatcher) {
    this.commandDispatcher = commandDispatcher;
  }
}
