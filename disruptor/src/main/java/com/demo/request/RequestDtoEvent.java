package com.demo.request;

import com.demo.dto.RequestDto;
import com.demo.command.infras.CommandCollector;
import com.demo.dto.ResponseDto;
import lombok.Data;

@Data
public class RequestDtoEvent {

  private RequestDto requestDto;

  /**
   * 数据库操作Command收集器
   */
  private final CommandCollector commandCollector = new CommandCollector();

  /**
   * 响应结果
   */
  private ResponseDto responseDto;


  public void clearForGc() {
    this.requestDto = null;
    this.commandCollector.getCommandList().clear();
    this.responseDto = null;
  }

  public boolean hasErrorOrException() {
    return responseDto != null && responseDto.getErrorMessage() != null;
  }

}
