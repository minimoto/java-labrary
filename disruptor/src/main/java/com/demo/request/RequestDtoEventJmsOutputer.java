package com.demo.request;

import com.demo.dto.ResponseDto;
import com.lmax.disruptor.EventHandler;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 将结果RequestDtoEvent的结果输出到jms的handler
 */
@Slf4j
public class RequestDtoEventJmsOutputer implements EventHandler<RequestDtoEvent> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RequestDtoEventJmsOutputer.class);

  // 简化：去掉 mq 操作 by xujunqiang
//  private JmsMessageSender messageSender;

  @Override
  public void onEvent(RequestDtoEvent event, long sequence, boolean endOfBatch) throws Exception {
    log.info("将结果RequestDtoEvent 输出到 mq");
    ResponseDto responseDto = event.getResponseDto();
    if (responseDto == null) {
      return;
    }
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("Send Response for Request {}. Id: {}", responseDto.getRequestId(), responseDto.getId());
    }
    // 简化：去掉 mq 操作 by xujunqiang
    // 这里这么做没有问题, 因为实际的调用方是单线程调用的, 多线程下则会出现并发问题
//    messageSender.sendMessage(responseDto);
    log.info("将结果输出到 mq: {}", responseDto.toString());

  }

//  public void setMessageSender(JmsMessageSender messageSender) {
//    this.messageSender = messageSender;
//  }

}
