package com.demo.request;

import com.demo.dto.ResponseDto;
import com.lmax.disruptor.ExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
public class RequestDtoEventExceptionHandler implements ExceptionHandler<RequestDtoEvent> {

  @Override
  public void handleEventException(Throwable ex, long sequence, RequestDtoEvent event) {
    event.setResponseDto(
        createExceptionResponseDto(event.getRequestDto().getId(), ex.getMessage())
    );
    log.error("{} : {}. {} ", event.getRequestDto().getClass().getName(), event.getRequestDto().getId(), ex.getMessage());
  }

  private ResponseDto createExceptionResponseDto(String requestId, String exception) {
    ResponseDto responseDto = new ResponseDto(requestId);
    responseDto.setErrorMessage(exception);
    responseDto.setSuccess(false);
    return responseDto;
  }

  @Override
  public void handleOnStartException(Throwable ex) {
    log.error("Exception during onStart()", ex);
  }

  @Override
  public void handleOnShutdownException(Throwable ex) {
    log.error("Exception during onShutdown()", ex);
  }

}
