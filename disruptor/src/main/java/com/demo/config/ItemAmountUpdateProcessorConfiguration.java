package com.demo.config;

import com.demo.command.infras.disruptor.*;
import com.demo.config.lifecycle.DisruptorLifeCycleContainer;
import com.lmax.disruptor.dsl.Disruptor;
import com.xjq.common.context.SpringContextHolder;
import com.xjq.common.util.BeanUtil;
import lombok.Data;
import com.demo.StartupOrderConstants;
import com.demo.command.infras.CommandDispatcher;
import com.demo.command.item.ItemAmountUpdateCommand;
import com.demo.command.item.ItemAmountUpdateCommandBuffer;
import com.demo.command.item.ItemAmountUpdateCommandExecutor;
import com.demo.command.item.ItemAmountUpdateCommandProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.concurrent.Executors;

@Configuration
@EnableConfigurationProperties(ItemAmountUpdateProcessorConfiguration.Conf.class)
public class ItemAmountUpdateProcessorConfiguration {

  @Autowired
  private Conf conf;

  @Autowired
  private CommandDispatcher commandDispatcher;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  @Bean
  public ItemAmountUpdateCommandProcessor lessonStdCountUpdateCmdProcessor() {
    CommandEventProducer<ItemAmountUpdateCommand>[] commandEventProducerList = new CommandEventProducer[conf.getNum()];

    for (int i = 0; i < conf.getNum(); i++) {
      ItemAmountUpdateCommandBuffer cmdBuffer = new ItemAmountUpdateCommandBuffer(conf.getSqlBufferSize());
      ItemAmountUpdateCommandExecutor cmdExecutor = new ItemAmountUpdateCommandExecutor(jdbcTemplate);

      Disruptor<CommandEvent<ItemAmountUpdateCommand>> disruptor = new Disruptor<>(
          new CommandEventFactory(),
          conf.getQueueSize(),
          Executors.defaultThreadFactory());

      disruptor
          .handleEventsWith(new CommandEventDbHandler(cmdBuffer, cmdExecutor))
          .then(new CommandEventGcHandler())
      ;
      // disruptor 的异常处理是这样的,
      // 不论这种形式 A->B, 还是这种形式 A,B->C,D, 只有抛出异常的那个handler会中断执行
      disruptor.setDefaultExceptionHandler(new CommandEventExceptionHandler());
      commandEventProducerList[i] = new CommandEventProducer<>(disruptor.getRingBuffer());

      BeanUtil.registerSingleton(
              SpringContextHolder.getContext(),
              "CommandEvent<ItemAmountUpdateCommand>_DisruptorLifeCycleContainer_" + i,
              new DisruptorLifeCycleContainer("CommandEvent<ItemAmountUpdateCommand>_Disruptor_" + i, disruptor,
                      StartupOrderConstants.DISRUPTOR_ITEM_UPDATE));
    }

    ItemAmountUpdateCommandProcessor cmdProcessor = new ItemAmountUpdateCommandProcessor(commandEventProducerList);
    commandDispatcher.registerCommandProcessor(cmdProcessor);
    return cmdProcessor;

  }


  @ConfigurationProperties(prefix = "item-update.proc")
  @Data
  public static class Conf {

    /**
     * 处理器数量
     */
    private int num;

    /**
     * 单次执行的SQL条数 (将多条SQL放到一起执行比分多次执行效率高)
     */
    private int sqlBufferSize;

    /**
     * disruptor队列长度, 值必须是2的次方
     */
    private int queueSize;

  }

}

