package com.demo.config;

import com.lmax.disruptor.dsl.Disruptor;
import com.xjq.common.context.SpringContextHolder;
import com.xjq.common.exception.AppException;
import com.demo.RequestDtoListener;
import com.demo.request.RequestDtoEventProducer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;



@Configuration
public class JmsServerConfiguration {

  @Bean
  public RequestDtoListener requestJmsMessageListener() throws AppException {
    RequestDtoListener messageListener = new RequestDtoListener();
    messageListener.setRequestDtoEventProducer(SpringContextHolder.getBean(RequestDtoEventProducer.class));
    return messageListener;
  }




}
