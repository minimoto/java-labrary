package com.demo.dataloader;

import com.demo.memdb.Item;
import com.demo.memdb.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 启动时，将数据库中的数据，加载到内存中
 */
@Slf4j
@Component
public class ItemDataStartupLoader extends DataStartupLoader {

  private JdbcTemplate jdbcTemplate;

  private ItemRepository itemRepository;

  public ItemDataStartupLoader(JdbcTemplate jdbcTemplate, ItemRepository itemRepository) {
    this.jdbcTemplate = jdbcTemplate;
    this.itemRepository = itemRepository;
  }

  @Override
  protected void doLoad() {
    List<Item> items = jdbcTemplate.query("select id, amount from item",
        (rs, rowNum) -> new Item(rs.getString(1), rs.getInt(2)));

    items.stream().forEach(item -> {
      itemRepository.put(item);
      log.info("Load Item from database: {}", item.toString());
    });

  }

  @Override
  public int getPhase() {
    return Ordered.LOWEST_PRECEDENCE;
  }


}
