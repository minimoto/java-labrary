package com.demo;

import com.demo.dto.RequestDto;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import com.demo.request.RequestDtoEventProducer;
//import org.springframework.jms.support.converter.MessageConverter;
//import org.springframework.jms.support.converter.SimpleMessageConverter;
//
//import javax.jms.JMSException;
//import javax.jms.Message;
//import javax.jms.MessageListener;
//import javax.jms.ObjectMessage;

/**
 * 简化：屏蔽 RequestDtoListener, 测试时，直接调用 onMessage 发送数据
 */
@Slf4j
@Data
public class RequestDtoListener {

  private RequestDtoEventProducer requestDtoEventProducer;

  public void onMessage(RequestDto requestDto) {
      this.requestDtoEventProducer.onData(requestDto);
  }


}


/**
 * 消费 MQ ,把数据发送到 disruptor ( requestDtoEventProducer )
 */
//@Slf4j
//public class RequestDtoListener implements MessageListener {
//
//
//  private RequestDtoEventProducer requestDtoEventProducer;
//
//  private MessageConverter messageConverter = new SimpleMessageConverter();
//
//  @Override
//  public void onMessage(Message message) {
//    if (!(message instanceof ObjectMessage)) {
//      log.error("Not ObjectMessage but actually {}", message.getClass().getSimpleName());
//      return;
//    }
//    try {
//      RequestDto requestDto = MessageConvertUtils.fromMessage(messageConverter, message);
//      requestDtoEventProducer.onData(requestDto);
//    } catch (AppException e) {
//      log.error("Error when onMessage", e);
//      throw new RuntimeException(e);
//    }
//  }
//
//  public void setRequestDtoEventProducer(RequestDtoEventProducer requestDtoEventProducer) {
//    this.requestDtoEventProducer = requestDtoEventProducer;
//  }
//
//  public void setMessageConverter(MessageConverter messageConverter) {
//    this.messageConverter = messageConverter;
//  }
//
//
//}
