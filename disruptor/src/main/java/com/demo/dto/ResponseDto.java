package com.demo.dto;


import cn.hutool.core.lang.UUID;
import lombok.Data;

/**
 * 购买请求的响应结果
 */
@Data
public class ResponseDto {

  private String id;

  /**
   * 关联的RequestDto的id
   */
  private String requestId;

  /**
   * 错误消息
   */
  protected String errorMessage;

  /**
   * 是否成功处理请求
   */
  protected boolean success;

  public ResponseDto(String requestId) {
    this.id = UUID.fastUUID().toString();
    this.requestId = requestId;
  }


}
