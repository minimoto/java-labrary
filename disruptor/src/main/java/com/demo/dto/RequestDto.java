package com.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 购买商品的请求
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RequestDto {

  private static final long serialVersionUID = 5515305970509119810L;

  private String id;

  /**
   * 商品ID
   */
  private String itemId;

  /**
   * 用户ID
   */
  private String userId;


}
