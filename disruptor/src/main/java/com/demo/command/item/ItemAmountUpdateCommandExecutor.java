package com.demo.command.item;

import cn.hutool.core.collection.CollectionUtil;
import com.demo.command.infras.CommandExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
public class ItemAmountUpdateCommandExecutor implements CommandExecutor<ItemAmountUpdateCommandBuffer> {

  private static final String SQL = "UPDATE ITEM SET AMOUNT = ? WHERE ID = ?";

  private JdbcTemplate jdbcTemplate;

  public ItemAmountUpdateCommandExecutor(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public void execute(ItemAmountUpdateCommandBuffer commandBuffer) {
    List<ItemAmountUpdateCommand> commands = commandBuffer.get();
    if (CollectionUtil.isEmpty(commands)) {
      return;
    }

    List<Object[]> args = commands.stream().map(cmd -> new Object[] { cmd.getAmount(), cmd.getItemId() })
        .collect(toList());
    try {
      jdbcTemplate.batchUpdate(SQL, args);
      commands.forEach(command -> log.info("Executed:{}", command.toString()));
    } catch (Exception e) {
      commands.forEach(command -> log.error("Failed:{}", command.toString()));
      log.error(e.getMessage());
    }
  }

}
