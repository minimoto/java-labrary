package com.demo.command.item;

import com.demo.command.infras.Command;
import lombok.Data;
import lombok.ToString;

/**
 * 更新商品库存的命令
 */
@Data
@ToString
public class ItemAmountUpdateCommand extends Command {

  private static final long serialVersionUID = 7896607558242859910L;
  private final String itemId;

  private final int amount;

  /**
   * @param requestId Command来源的requestId
   * @param itemId    商品ID
   * @param amount    库存
   */
  public ItemAmountUpdateCommand(String requestId, String itemId, int amount) {
    super(requestId);
    this.itemId = itemId;
    this.amount = amount;
  }



}
