package com.demo.command.item;

import cn.hutool.core.util.RandomUtil;
import com.demo.command.infras.CommandProcessor;
import com.demo.command.infras.disruptor.CommandEventProducer;
import lombok.extern.slf4j.Slf4j;

import java.util.Random;

/**
 * Created by qianjia on 16/5/17.
 */
@Slf4j
public class ItemAmountUpdateCommandProcessor implements CommandProcessor<ItemAmountUpdateCommand> {

  private final CommandEventProducer<ItemAmountUpdateCommand>[] commandEventProducerList;

  private final int producerCount;

  public ItemAmountUpdateCommandProcessor(CommandEventProducer<ItemAmountUpdateCommand>[] commandEventProducerList) {
    this.commandEventProducerList = commandEventProducerList;
    this.producerCount = commandEventProducerList.length;
  }

  @Override
  public Class<ItemAmountUpdateCommand> getMatchClass() {
    return ItemAmountUpdateCommand.class;
  }

  @Override
  public void process(ItemAmountUpdateCommand command) {
    int index = RandomUtil.randomInt(producerCount);
    commandEventProducerList[index].onData(command);
  }

}
