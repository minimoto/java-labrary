package com.demo.command.order;

import com.demo.command.infras.Command;
import lombok.Data;
import lombok.ToString;

/**
 * 保存订单的命令
 */
@Data
@ToString
public class OrderInsertCommand extends Command {

  private static final long serialVersionUID = -1844388054958673686L;
  private final String itemId;

  private final String userId;

  /**
   * @param requestId Command来源的requestId
   * @param itemId    商品ID
   * @param userId    用户ID
   */
  public OrderInsertCommand(String requestId, String itemId, String userId) {
    super(requestId);
    this.itemId = itemId;
    this.userId = userId;
  }

}
