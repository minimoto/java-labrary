package com.demo.command.order;

import cn.hutool.core.util.RandomUtil;
import com.demo.command.infras.CommandProcessor;
import com.demo.command.infras.disruptor.CommandEventProducer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderInsertCommandProcessor implements CommandProcessor<OrderInsertCommand> {

  private final CommandEventProducer<OrderInsertCommand>[] commandEventProducerList;

  private final int producerCount;

  public OrderInsertCommandProcessor(CommandEventProducer<OrderInsertCommand>[] commandEventProducerList) {
    this.commandEventProducerList = commandEventProducerList;
    this.producerCount = commandEventProducerList.length;
  }

  @Override
  public Class<OrderInsertCommand> getMatchClass() {
    return OrderInsertCommand.class;
  }

  @Override
  public void process(OrderInsertCommand command) {
    int index = RandomUtil.randomInt(producerCount);
    commandEventProducerList[index].onData(command);
  }

}
