package com.demo.command.order;


import com.demo.command.infras.CommandBuffer;
import com.xjq.common.exception.AppException;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class OrderInsertCommandBuffer implements CommandBuffer<OrderInsertCommand> {

  private final List<OrderInsertCommand> commandList;

  private final int capacity;

  public OrderInsertCommandBuffer(int capacity) {
    this.capacity = capacity;
    this.commandList = new ArrayList<>(capacity);
  }

  @Override
  public boolean hasRemaining() {
    return commandList.size() < this.capacity;
  }

  /**
   * @param command
   * @throws AppException
   */
  @Override
  public void put(OrderInsertCommand command) {

    if (!hasRemaining()) {
      throw new AppException();
    }

    this.commandList.add(command);
    log.info("Put:{}", command.toString());

  }

  @Override
  public void clear() {
    commandList.clear();
  }

  @Override
  public List<OrderInsertCommand> get() {
    return new ArrayList<>(commandList);
  }

}
