package com.demo.command.infras;

/**
 * {@link Command}执行器
 */
public interface CommandExecutor<T extends CommandBuffer> {

  void execute(T commandBuffer);

}
