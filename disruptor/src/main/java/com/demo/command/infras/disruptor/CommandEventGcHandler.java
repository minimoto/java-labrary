package com.demo.command.infras.disruptor;

import com.lmax.disruptor.EventHandler;
import com.demo.command.infras.Command;

/**
 * DbCommandEvent的GC处理器
 */
public class CommandEventGcHandler<T extends Command> implements EventHandler<CommandEvent<T>> {

  @Override
  public void onEvent(CommandEvent<T> event, long sequence, boolean endOfBatch) throws Exception {
    event.clearForGc();
  }

}
