package com.demo.command.infras.disruptor;

import com.demo.command.infras.Command;
import com.lmax.disruptor.EventFactory;

public class CommandEventFactory<T extends Command> implements EventFactory<CommandEvent<T>> {
  @Override
  public CommandEvent<T> newInstance() {
    return new CommandEvent<>();
  }
}
