package com.demo.command.infras.disruptor;

import com.demo.command.infras.Command;

public class CommandEvent<T extends Command> {

  private T command;

  public T getCommand() {
    return command;
  }

  public void setCommand(T command) {
    this.command = command;
  }

  public void clearForGc() {
    this.command = null;
  }

}
