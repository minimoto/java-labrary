package com.demo.controller;


import cn.hutool.core.lang.UUID;
import com.demo.RequestDtoListener;
import com.demo.dto.RequestDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2019/11/21
 * @Describe
 */
@Slf4j
@RestController
public class SentMqController {

    @Autowired
    private RequestDtoListener requestDtoListener;


    @GetMapping("/sendmq/{id}")
    public void insertStorage(@PathVariable("id") String id) {
        RequestDto requestDto = new RequestDto();
        requestDto.setId(UUID.fastUUID().toString());
        requestDto.setItemId(id);
        requestDto.setUserId("xiaoqiang");
        requestDtoListener.onMessage(requestDto);
    }


}
