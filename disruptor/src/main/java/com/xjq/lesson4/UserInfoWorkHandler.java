package com.xjq.lesson4;


import com.lmax.disruptor.EventHandler;
import com.lmax.disruptor.WorkHandler;
import com.xjq.lession2.UserInfoEvent;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoWorkHandler implements WorkHandler<UserInfoEvent> {


    @Override
    public void onEvent(UserInfoEvent userInfoEvent) {
        System.out.println("[ UserInfoWorkHandler ] thread: " + Thread.currentThread().getName() + ":" +  userInfoEvent.getUserInfo().getUsername() + " : " + userInfoEvent.getUserInfo().getPwd());
    }
}
