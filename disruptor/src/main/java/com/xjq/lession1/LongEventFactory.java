package com.xjq.lession1;

import com.lmax.disruptor.EventFactory;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class LongEventFactory implements EventFactory {

    @Override
    public Object newInstance() {
        return new LongEvent();
    }

}
