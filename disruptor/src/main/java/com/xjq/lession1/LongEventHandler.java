package com.xjq.lession1;


import com.lmax.disruptor.EventHandler;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class LongEventHandler implements EventHandler<LongEvent> {

    @Override
    public void onEvent(LongEvent longEvent, long l, boolean b) throws Exception {
        System.out.println(longEvent.getValue());
    }
}
