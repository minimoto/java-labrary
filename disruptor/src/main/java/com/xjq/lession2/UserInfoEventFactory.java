package com.xjq.lession2;

import com.lmax.disruptor.EventFactory;
import com.xjq.lession1.LongEvent;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEventFactory implements EventFactory<UserInfoEvent> {

    @Override
    public UserInfoEvent newInstance() {
        return new UserInfoEvent();
    }

}
