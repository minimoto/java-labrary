package com.xjq.lession2;

import com.lmax.disruptor.EventTranslator;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;
import com.xjq.lession1.LongEvent;

import java.nio.ByteBuffer;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEventProducer {

    private static final EventTranslatorOneArg<UserInfoEvent, UserInfo> TRANSLATOR = new EventTranslatorOneArg<UserInfoEvent, UserInfo>() {
        @Override
        public void translateTo(UserInfoEvent userInfoEvent, long l, UserInfo userInfo) {
            userInfoEvent.setUserInfo(userInfo);
        }
    };


    private final RingBuffer<UserInfoEvent> ringBuffer;

    public UserInfoEventProducer(RingBuffer<UserInfoEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void onData(UserInfo userInfo) {
        ringBuffer.publishEvent(TRANSLATOR, userInfo);
    }
}
