package com.xjq.lession2;


import com.lmax.disruptor.EventHandler;
import com.xjq.lession1.LongEvent;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEventHandler implements EventHandler<UserInfoEvent> {

    @Override
    public void onEvent(UserInfoEvent longEvent, long sequence, boolean endOfBatch) throws Exception {
        System.out.println("[ UserInfoEventHandler ] thread: " + Thread.currentThread().getName() + ":" +  longEvent.getUserInfo().getUsername() + " : " + longEvent.getUserInfo().getPwd());
    }
}
