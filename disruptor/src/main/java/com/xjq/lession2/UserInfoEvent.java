package com.xjq.lession2;

import com.xjq.lesson5.ExtentInfo;
import lombok.Data;
import lombok.ToString;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
@ToString
@Data
public class UserInfoEvent {

    private UserInfo userInfo;

    private ExtentInfo extentInfo ;

}
