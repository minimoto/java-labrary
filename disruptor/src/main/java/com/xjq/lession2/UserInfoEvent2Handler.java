package com.xjq.lession2;


import com.lmax.disruptor.EventHandler;
import com.xjq.lesson5.ExtentInfo;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEvent2Handler implements EventHandler<UserInfoEvent> {

    @Override
    public void onEvent(UserInfoEvent longEvent, long sequence, boolean endOfBatch) throws Exception {
        System.out.println("[ UserInfoEvent2Handler ] thread: " + Thread.currentThread().getName() + ":" + longEvent.getUserInfo().getUsername() + " : " + longEvent.getUserInfo().getPwd());
        longEvent.setExtentInfo(new ExtentInfo());
        longEvent.getExtentInfo().setAddress("address:" + longEvent.getUserInfo().getUsername());

    }
}
