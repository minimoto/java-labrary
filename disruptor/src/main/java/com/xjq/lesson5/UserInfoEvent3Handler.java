package com.xjq.lesson5;


import com.lmax.disruptor.EventHandler;
import com.xjq.lession2.UserInfoEvent;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEvent3Handler implements EventHandler<UserInfoEvent> {

    @Override
    public void onEvent(UserInfoEvent longEvent, long sequence, boolean endOfBatch) throws Exception {
        System.out.println("[ UserInfoEvent3Handler ] thread: " + Thread.currentThread().getName() + ":" +  longEvent.toString());
    }
}
