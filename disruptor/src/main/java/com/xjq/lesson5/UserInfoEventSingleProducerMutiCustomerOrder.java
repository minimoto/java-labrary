package com.xjq.lesson5;

import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;
import com.xjq.lession2.UserInfo;
import com.xjq.lession2.UserInfoEvent;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
public class UserInfoEventSingleProducerMutiCustomerOrder {

    private static final EventTranslatorOneArg<UserInfoEvent, UserInfo> TRANSLATOR = new EventTranslatorOneArg<UserInfoEvent, UserInfo>() {
        @Override
        public void translateTo(UserInfoEvent userInfoEvent, long l, UserInfo userInfo) {
            userInfoEvent.setUserInfo(userInfo);
        }
    };


    private final RingBuffer<UserInfoEvent> ringBuffer;

    public UserInfoEventSingleProducerMutiCustomerOrder(RingBuffer<UserInfoEvent> ringBuffer) {
        this.ringBuffer = ringBuffer;
    }

    public void onData(UserInfo userInfo) {
        ringBuffer.publishEvent(TRANSLATOR, userInfo);
    }
}
