package com.xjq;

import cn.hutool.core.thread.ThreadUtil;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import com.xjq.lession1.LongEventProducer;
import com.xjq.lession2.*;
import com.xjq.lesson3.UserInfoEventMutiProducer;
import com.xjq.lesson4.UserInfoEventSingleProducerMutiCustomerNoRepeat;
import com.xjq.lesson5.UserInfoEventSingleProducerMutiCustomerOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.nio.ByteBuffer;
import java.util.concurrent.Executors;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/29
 */
@SpringBootTest
public class EventTest {

    @Autowired
    LongEventProducer longEventProducer;

    @Autowired
    UserInfoEventProducer userInfoEventProducer;

    @Autowired
    UserInfoEventMutiProducer userInfoEventMutiProducer;

    @Autowired
    UserInfoEventSingleProducerMutiCustomerNoRepeat userInfoEventSingleProducerMutiCustomerNoRepeat;

    @Autowired
    UserInfoEventSingleProducerMutiCustomerOrder userInfoEventSingleProducerMutiCustomerOrder;


    /**
     * 生产者: 接收基本数据类型 Long
     */
    @Test
    public void testLesson1(){
        ByteBuffer byteBuffer = ByteBuffer.allocate(8);
        for(long l = 0; l<100; l++){
            byteBuffer.putLong(0, l);
            longEventProducer.onData(byteBuffer);
            //Thread.sleep(1000);
        }

//        disruptor.shutdown();//关闭 disruptor，方法会堵塞，直至所有的事件都得到处理；
    }

    /**
     * 单生产者多消费者场景
     * 生产者: 接收复杂对象
     */
    @Test
    public void testLesson2(){
        for(long l = 0; l<100; l++){
            UserInfo userInfo = new UserInfo();
            userInfo.setUsername("username" + l);
            userInfo.setPwd("pwd" + l);
            userInfoEventProducer.onData(userInfo);
            //Thread.sleep(1000);
        }

//        disruptor.shutdown();//关闭 disruptor，方法会堵塞，直至所有的事件都得到处理；
    }

    /**
     * 多生产者单消费者场景
     */
    @Test
    public void testLesson3(){
        Thread thread = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t1】username" + l);
                    userInfo.setPwd("【t1】pwd" + l);
                    userInfoEventMutiProducer.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t2】username" + l);
                    userInfo.setPwd("【t2】pwd" + l);
                    userInfoEventMutiProducer.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        thread.start();
        thread2.start();
        ThreadUtil.sleep(1000*5);
//        disruptor.shutdown();//关闭 disruptor，方法会堵塞，直至所有的事件都得到处理；
    }

    /**
     * 单生产者多消费者竞争场景
     */
    @Test
    public void testLesson4(){
        Thread thread = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t1】username" + l);
                    userInfo.setPwd("【t1】pwd" + l);
                    userInfoEventSingleProducerMutiCustomerNoRepeat.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t2】username" + l);
                    userInfo.setPwd("【t2】pwd" + l);
                    userInfoEventSingleProducerMutiCustomerNoRepeat.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        thread.start();
        thread2.start();
        ThreadUtil.sleep(1000*5);

//        disruptor.shutdown();//关闭 disruptor，方法会堵塞，直至所有的事件都得到处理；
    }

    /**
     * 单生产者多消费者竞争场景
     */
    @Test
    public void testLesson5(){
        Thread thread = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t1】username" + l);
                    userInfo.setPwd("【t1】pwd" + l);
                    userInfoEventSingleProducerMutiCustomerOrder.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        Thread thread2 = new Thread(){
            @Override
            public void  run(){
                for(long l = 0; l<50; l++){
                    UserInfo userInfo = new UserInfo();
                    userInfo.setUsername("【t2】username" + l);
                    userInfo.setPwd("【t2】pwd" + l);
                    userInfoEventSingleProducerMutiCustomerOrder.onData(userInfo);
                    //Thread.sleep(1000);
                }
            }
        };
        thread.start();
        thread2.start();
        ThreadUtil.sleep(1000*500);

//        disruptor.shutdown();//关闭 disruptor，方法会堵塞，直至所有的事件都得到处理；
    }

}
