###   demo 模拟企业业务
#### 主要数据流程
1. RequestDtoListener, 数据入口, 模拟接收 MQ 的处理
2. 将接收 MQ 的数据发到 RequestDisruptor （RequestDisruptorConfiguration) 

    它有三个主要消费者：
    
    处理业务消费（RequestDtoEventBusinessHandler）
    
    分类消费 (RequestDtoEventDbOutputer), 通过分发类（CommandDispatcher） 发到 Disruptor<CommandEvent<ItemAmountUpdateCommand>> （ItemAmountUpdateProcessorConfiguration）
    Disruptor<CommandEvent<OrderInsertCommand>>（OrderInsertProcessorConfiguration）

    消费结果发到 MQ (RequestDtoEventJmsOutputer)

#### 模拟 MQ 消费
http://localhost:8080/sendmq/1

#### 表结构
CREATE TABLE "yun"."item" (
  "id" varchar(36) COLLATE "pg_catalog"."default" NOT NULL,
  "amount" int2 DEFAULT 0
)
;

ALTER TABLE "yun"."item" 
  OWNER TO "root";
  
  
  CREATE TABLE "yun"."item_order" (
    "id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
    "item_id" varchar(36) COLLATE "pg_catalog"."default",
    "user_id" varchar(36) COLLATE "pg_catalog"."default",
    CONSTRAINT "item_order_pkey" PRIMARY KEY ("id")
  )
  ;
  
  ALTER TABLE "yun"."item_order" 
    OWNER TO "root";
