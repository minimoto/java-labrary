## disruptor 

### 
# 1. 策略
* ①BlockingWaitStrategy
     * Disruptor的默认策略是BlockingWaitStrategy。
     * 在BlockingWaitStrategy内部是使用锁和condition来控制线程的唤醒。
     * BlockingWaitStrategy是最低效的策略，但其对CPU 的消耗最小并且在各种不同部署环境中能提供更加一致的性能表现。
* ②SleepingWaitStrategy
     * SleepingWaitStrategy 的性能表现跟 BlockingWaitStrategy 差不多，对 CPU 的消耗也类似，
     * 但其对生产者线程的影响最小，通过使用LockSupport.parkNanos(1)来实现循环等待。
     * 一般来说Linux系统会暂停一个线程约60µs，
     * 这样做的好处是，生产线程不需要采取任何其他行动就可以增加适当的计数器，也不需要花费时间信号通知条件变量。
     * 但是，在 生产者线程和使用者线程之间移动事件的平均延迟会更高。它在不需要低延迟并且对生产线程的影响较小的情况最好。
     * 一个常见的用例是异步日志记录。
 
* ③YieldingWaitStrategy
     * YieldingWaitStrategy是可以使用在低延迟系统的策略之一。
     * YieldingWaitStrategy 将自旋以等待序列增加到适当的值。
     * 在循环体内，将调用Thread.yield（），以允许其他排 队的线程运行。
     * 在要求极高性能且事件处理线数小于 CPU 逻辑核心数的场景中，推荐使用此策略；例如，CPU开启超线程的特性。

* ④BusySpinWaitStrategy
     * 性能最好，适合用于低延迟的系统。
     * 在要求极高性能且事件处理线程数小于CPU逻辑核 心数的场景中，推荐使用此策略；例如，CPU开启超线程的特性。

# 2. 使用细分场景
* 单生产者多消费者场景
  * 并发系统中提高性能最好的方式之一
  * 多个消费者通过线程并发处理
  * 消费顺序取决于消费者线程速度
  ![单生产者多消费者场景.png](./doc/单生产者多消费者场景.png)
  * 事件编排代码
  ```java
    disruptor.handleEventsWith(
      new UserInfoEventHandler(), 
      new UserInfoEvent2Handler()
    );
  ```
  * lesson1
    * 接收 Long 类型数据
  * lesson2
    * 接收复杂对象类型数据
  
* 多生产者单消费者场景
  * 创建Disruptor 的时候，将ProducerType.SINGLE改为ProducerType.MULTI, 否则并发生产时会丢失数据
  * 编写多线程生产者的相关代码
  * 消费顺序取决于生产者速度
  ![多生产者单消费者场景.png](./doc/多生产者单消费者场景.png)
  * lesson3

    
* 单生产者多消费者竞争场景
  * 多个消费者不重复消费,即多个消费者只有一个消费者成功消费
  * 消费者需要 实现 WorkHandler 接口，而不是 EventHandler 接口
  * 使用 handleEventsWithWorkerPool 设置 disruptor的 消费者，而不是 handleEventsWith 方法
  * 事件编排代码
  ```java
    disruptor.handleEventsWithWorkerPool(
      new UserInfoWorkHandler(), 
      new UserInfoWork2Handler()
    );
  ```
  * lesson4
  
* 多个消费者串行消费场景
  * 多个消费者，可以按照次序，消费消息
  ![多个消费者串行消费场景.png](./doc/多个消费者串行消费场景.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWith(
                  new UserInfoEventHandler())
                .then(new UserInfoEvent2Handler())
                .then(new UserInfoEvent3Handler());
  ```
  * lesson5
  
* 菱形方式执行场景
  ![菱形方式执行场景.png](./doc/菱形方式执行场景.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWith(
                  new UserInfoEventHandler(), new UserInfoEvent2Handler())
                .then(new UserInfoEvent3Handler()
        );
  ```
* 链式并行执行场景
  ![链式并行执行场景.png](./doc/链式并行执行场景.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWith(
                  new 消费者1-1())
                .then(new 消费者1-2()
        );
        disruptor.handleEventsWith(
                          new 消费者2-1())
                        .then(new 消费者2-2()
                );  
        disruptor.start();
  ```  
  
* 多组消费者相互隔离场景
  * 组内 相互竞争
  * 组间 相互隔离
  ![多组消费者相互隔离场景.png](./doc/多组消费者相互隔离场景.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWithWorkerPool(
          new UserInfoWorkHandler(), 
          new UserInfoWork2Handler()
        );
        disruptor.handleEventsWithWorkerPool(
          new UserInfoWork3Handler(), 
          new UserInfoWork4Handler()
        );
        disruptor.handleEventsWithWorkerPool(
          new UserInfoWork5Handler(), 
          new UserInfoWork6Handler()
        );  
        disruptor.start();
  ```  


* 多组消费者航道执行模式
  * 组内 相互竞争
  * 组之间串行依次执行
  ![多组消费者航道执行模式.png](./doc/多组消费者航道执行模式.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWithWorkerPool(
                  new UserInfoWork1Handler(), 
                  new UserInfoWork1Handler()
                )
        .thenHandleEventsWithWorkerPool(
                  new UserInfoWork2Handler(), 
                  new UserInfoWork2Handler()
        );
        disruptor.start();
  ```  
  

* 六边形执行顺序
  * 组内部是有序的
  * 组之间是并行的
  ![六边形执行顺序.png](./doc/六边形执行顺序.png)
  * 事件编排代码
  ```java
        disruptor.handleEventsWith(consumer1,consumer2);
        disruptor.after(consumer1).handleEventsWith(consumer3);
        disruptor.after(consumer2).handleEventsWith(consumer4);
        disruptor.after(consumer3,consumer4).handleEventsWith(consumer5);
        disruptor.start();
  ```  
