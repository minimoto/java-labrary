package com.service;

/**
 * @ClassName : Singleton
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 15:20
 **/
public class Singleton {

    private Singleton(){
    }

    public static enum SingletonEnum {

        SINGLETON;

        private Singleton instance = null;

        private SingletonEnum(){
            instance = new Singleton();
        }

        public Singleton getInstance(){
            return instance;
        }
    }
}