package com;

import com.service.Singleton;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SingletonApplication {

    public static void main(String[] args) {
        SpringApplication.run(SingletonApplication.class, args);

        Singleton s1 = Singleton.SingletonEnum.SINGLETON.getInstance();
        Singleton s2 = Singleton.SingletonEnum.SINGLETON.getInstance();
        System.out.println(s1==s2);
    }

}
