package com;


import cn.hutool.core.collection.ListUtil;
import com.stragegy.bizservice.StragegyProxyBizService;
import com.stragegy.enums.BusinessTypeEnum;
import com.stragegy.enums.PaymentTypeEnum;
import com.stragegy.vo.*;
import com.stragegy2.bussness.ItemAmountUpdateCommand;
import com.stragegy2.bussness.OrderInsertCommand;
import com.stragegy2.bussness.OrderInsertCommandProcessor;
import com.stragegy2.command.Command;
import com.stragegy2.command.DefaultCommandDispatcher;
import lombok.extern.slf4j.Slf4j;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * 两种策略模式的套路都是一样
 * 定义模型，实现模型处理器，最后把模型处理器注入到分发器。
 *
 * strategy 和strategy2 区别就是，strategy 是自动注册到工厂。
 * 而strategy2 需要手动注册到分发器。
 *
 * 推荐使用 strategy2，因为它可读快更好(没有使用泛型), 而且在处理器中，模型的数据类型不需要转换
 *
 * @author xjq
 * @date 2022/12/5
 */

@SpringBootTest(classes = {StragegyApplication.class})
@Slf4j
public class TestStragegy {

    @Autowired
    StragegyProxyBizService stragegyProxyBizService;

    @Autowired
    DefaultCommandDispatcher dispatcher;

    @Test
    public void testStragegy(){
        BuessnessCodeVo codeVo = new BuessnessCodeVo();
        codeVo.setCode("123456");
        codeVo.setBusinessTypeEnum(BusinessTypeEnum.CODE_SERVICE);
        stragegyProxyBizService.businessProxy(codeVo);

        BuessnessCustomerVo customerVo = new BuessnessCustomerVo();
        customerVo.setCustomerName("xjq");
        customerVo.setBusinessTypeEnum(BusinessTypeEnum.CUSTOMER_SERVICE);
        stragegyProxyBizService.businessProxy(customerVo);

        BuessnessPolicyVo policyVo = new BuessnessPolicyVo();
        policyVo.setPolicyId("p123456");
        policyVo.setBusinessTypeEnum(BusinessTypeEnum.POLICYNO_SERVICE);
        stragegyProxyBizService.businessProxy(policyVo);




        PaymentVo paymentVo = new PaymentVo();
        paymentVo.setPaymentTypeEnum(PaymentTypeEnum.BANK_SERVICE);
        stragegyProxyBizService.paymentProxy(paymentVo);

    }

    @Test
    public void testStragegy2(){
        ItemAmountUpdateCommand itemAmountUpdateCommand = new ItemAmountUpdateCommand("1", 100);
        ItemAmountUpdateCommand itemAmountUpdateCommand2 = new ItemAmountUpdateCommand("2", 200);
        OrderInsertCommand orderInsertCommand = new OrderInsertCommand("A001", "aaa");
        OrderInsertCommand orderInsertCommand2 = new OrderInsertCommand("B001", "bbb");


        List<Command> commands = ListUtil.toList(itemAmountUpdateCommand, itemAmountUpdateCommand2, orderInsertCommand, orderInsertCommand2);

        for (Command command : commands) {
           dispatcher.dispatch(command);
        }


    }

}
