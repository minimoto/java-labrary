package com.stragegy.vo;

import com.stragegy.enums.BusinessTypeEnum;
import com.stragegy.enums.PaymentTypeEnum;
import lombok.Data;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
@Data
public class PaymentVo {

    private PaymentTypeEnum paymentTypeEnum;
}
