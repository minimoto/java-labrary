package com.stragegy.enums;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public enum BusinessTypeEnum {
    /**
     * 服务类型
     */
    CODE_SERVICE("1", "码库服务"),
    CUSTOMER_SERVICE("2", "客户服务"),
    POLICYNO_SERVICE("3", "保单服务");


    private String code;

    private String message;


    BusinessTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }


    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
