package com.stragegy.enums;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public enum PaymentTypeEnum {
    /**
     * 支付类型
     */
    WX_SERVICE("1", "微信"),
    ALI_SERVICE("2", "支付宝"),
    BANK_SERVICE("3", "银行卡");


    private String code;

    private String message;


    PaymentTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }


    public String getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
