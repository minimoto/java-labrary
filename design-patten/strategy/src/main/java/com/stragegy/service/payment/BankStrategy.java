package com.stragegy.service.payment;


import com.google.common.collect.Sets;
import com.stragegy.enums.PaymentTypeEnum;
import com.stragegy.vo.PaymentVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * @author jqxu
 * @date 2022/10/18
 * @description: 申购发起(承保):  underWriting
 */
@Slf4j
@Service
public class BankStrategy extends PaymentStrategy {

    private static final PaymentTypeEnum BUSINESS_TYPE_ENUM = PaymentTypeEnum.BANK_SERVICE;

    /**
     * 承保发起
     * @param request 请求参数
     */
//    @Transactional(rollbackOn = Exception.class)
    @Override
    public void process(PaymentVo request) {
        System.out.println("this is payment::bank");
    }

    @Override
    public PaymentTypeEnum matchBizSource() {
        return BUSINESS_TYPE_ENUM;
    }


}
