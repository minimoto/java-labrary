package com.stragegy.service;

import cn.hutool.core.collection.CollectionUtil;

import java.util.Set;

/**
 * @author xujunqiang
 * @date 2022/10/25
 * @description: 根据业务来源执行不同存储逻辑的策略接口
 */
public interface IStrategy<T,E> {

    /**
     * 按照不同策略执行存储
     *
     * @param request 请求参数
     */
    void process(T request);

    /**
     * 对应策略要处理的业务来源
     *
     * @return 对应策略要处理的业务来源
     */
    E matchBizSource();

    /**
     * 当前策略是否匹配当前业务
     *
     * @param bizSource 业务
     * @return 匹配则返回 true
     */
    default boolean matchBizSource(E bizSource) {
        return matchBizSource().equals(bizSource);
    }
}
