package com.stragegy.service.payment;


import com.stragegy.service.IStrategy;
import com.stragegy.enums.PaymentTypeEnum;
import com.stragegy.service.StragegyBaseService;
import com.stragegy.vo.PaymentVo;

/**
 * @author jqxu
 * @date 2022/10/18
 * @description: 申购发起接口
 */
public abstract class PaymentStrategy extends StragegyBaseService implements IStrategy<PaymentVo, PaymentTypeEnum> {

}
