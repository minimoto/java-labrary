package com.stragegy.service.bussness;

import com.google.common.collect.Sets;
import com.stragegy.enums.BusinessTypeEnum;
import com.stragegy.vo.BuessnessCodeVo;
import com.stragegy.vo.BuessnessVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author jqxu
 * @date 2022/10/18
 */
@Slf4j
@Service
public class CodeStrategy extends BussnessStrategy {

    private static final BusinessTypeEnum BUSINESS_TYPE_ENUM_SET = BusinessTypeEnum.CODE_SERVICE;


    /**
     * CODE服务
     * @param request 请求参数
     */
    @Override
//    @Transactional(rollbackOn = Exception.class)
    public void process(BuessnessVo request) {
        BuessnessCodeVo buessnessVo = (BuessnessCodeVo)request;
        System.out.println("这是CODE服务:{}" + buessnessVo.toString());
    }


    @Override
    public BusinessTypeEnum matchBizSource() {
        return BUSINESS_TYPE_ENUM_SET;
    }




}
