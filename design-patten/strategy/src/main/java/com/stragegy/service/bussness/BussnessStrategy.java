package com.stragegy.service.bussness;


import com.stragegy.service.IStrategy;
import com.stragegy.enums.BusinessTypeEnum;
import com.stragegy.service.StragegyBaseService;
import com.stragegy.vo.BuessnessVo;

/**
 * @author jqxu
 * @date 2022/10/18
 * @description: 申购提交接口
 */
public abstract class BussnessStrategy extends StragegyBaseService implements IStrategy<BuessnessVo, BusinessTypeEnum> {



}
