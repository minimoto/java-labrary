package com.stragegy.bizservice;


import com.stragegy.vo.BuessnessVo;
import com.stragegy.vo.PaymentVo;

/**
 * @ClassName : FundTransactionProxyBizService
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/10/24 17:22
 **/
public interface StragegyProxyBizService {


    void businessProxy(BuessnessVo request);

    void paymentProxy(PaymentVo request);
}
