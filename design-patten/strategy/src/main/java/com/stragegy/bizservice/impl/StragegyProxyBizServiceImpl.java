package com.stragegy.bizservice.impl;


import com.stragegy.bizservice.StragegyProxyBizService;
import com.stragegy.enums.BusinessTypeEnum;
import com.stragegy.enums.PaymentTypeEnum;
import com.stragegy.service.bussness.BussnessStrategy;
import com.stragegy.service.bussness.BussnessStrategyFactory;
import com.stragegy.service.payment.PaymentStrategy;
import com.stragegy.service.payment.PaymentStrategyFactory;
import com.stragegy.vo.BuessnessVo;
import com.stragegy.vo.PaymentVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


/**
 * @ClassName : FundTransactionProxyBizServiceImpl
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/10/24 17:23
 **/
@Slf4j
@Service
public class StragegyProxyBizServiceImpl implements StragegyProxyBizService {

    /**
     *
     * @param request
     */
    @Override
    public void businessProxy(BuessnessVo request) {
        BusinessTypeEnum businessTypeEnum = request.getBusinessTypeEnum();
        BussnessStrategy strategy = BussnessStrategyFactory.getStrategy(businessTypeEnum);
        if (strategy == null) {
            log.info("不支持的业务来源: {}", businessTypeEnum);
            throw new RuntimeException("不支持的业务来源");
        }
        strategy.process(request);
    }

    /**
     *
     * @param request
     */
    @Override
    public void paymentProxy(PaymentVo request) {
        PaymentTypeEnum paymentTypeEnum = request.getPaymentTypeEnum();
        PaymentStrategy strategy = PaymentStrategyFactory.getStrategy(paymentTypeEnum);
        if (strategy == null) {
            log.info("不支持的业务来源: {}", paymentTypeEnum);
            throw new RuntimeException("不支持的业务来源");
        }
        strategy.process(request);
    }


}
