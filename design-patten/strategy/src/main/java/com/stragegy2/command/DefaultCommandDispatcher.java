package com.stragegy2.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by qianjia on 16/5/17.
 */
@Slf4j
@Service
public class DefaultCommandDispatcher implements CommandDispatcher {

  private final Map<Class, CommandProcessor> commandProcessorMap = new HashMap<>();

  @Override
  public void dispatch(Command command) {
    commandProcessorMap.get(command.getClass()).process(command);
  }

  @Override
  public void registerCommandProcessor(CommandProcessor commandProcessor) {
    log.info("Register CommandDispatcher: [{}] for [{}]", commandProcessor.getClass().getName(), commandProcessor.getMatchClass().getName());
    this.commandProcessorMap.put(commandProcessor.getMatchClass(), commandProcessor);
  }

}
