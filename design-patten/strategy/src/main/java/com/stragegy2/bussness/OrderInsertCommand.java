package com.stragegy2.bussness;

import com.stragegy2.command.Command;
import lombok.Data;
import lombok.ToString;

/**
 * 保存订单的命令
 */
@Data
@ToString
public class OrderInsertCommand extends Command {

  private final String itemId;

  private final String userId;

  /**
   * @param itemId    商品ID
   * @param userId    用户ID
   */
  public OrderInsertCommand(String itemId, String userId) {
    this.itemId = itemId;
    this.userId = userId;
  }

}
