package com.stragegy2.bussness;

import cn.hutool.core.util.RandomUtil;

import com.stragegy2.command.CommandProcessor;
import com.stragegy2.command.DefaultCommandDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.util.stereotypes.Lazy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by qianjia on 16/5/17.
 */
@Slf4j
@Component
public class ItemAmountUpdateCommandProcessor implements CommandProcessor<ItemAmountUpdateCommand> {


  @Autowired
  DefaultCommandDispatcher dispatcher;

  @PostConstruct
  public void regisger() {
    dispatcher.registerCommandProcessor(this);
  }

  @Override
  public Class<ItemAmountUpdateCommand> getMatchClass() {
    return ItemAmountUpdateCommand.class;
  }

  @Override
  public void process(ItemAmountUpdateCommand command) {
    log.info("item amount update command received: {}", command.toString());
  }

}
