package com.stragegy2.bussness;

import com.stragegy2.command.Command;
import lombok.Data;
import lombok.ToString;

/**
 * 更新商品库存的命令
 */
@Data
@ToString
public class ItemAmountUpdateCommand extends Command {

  private final String itemId;

  private final int amount;

  /**
   * @param itemId    商品ID
   * @param amount    库存
   */
  public ItemAmountUpdateCommand(String itemId, int amount) {
    this.itemId = itemId;
    this.amount = amount;
  }



}
