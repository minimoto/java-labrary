package com.stragegy2.bussness;

import cn.hutool.core.util.RandomUtil;
import com.stragegy2.command.CommandProcessor;
import com.stragegy2.command.DefaultCommandDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class OrderInsertCommandProcessor implements CommandProcessor<OrderInsertCommand> {

  @Autowired
  DefaultCommandDispatcher dispatcher;

  @PostConstruct
  public void regisger() {
    dispatcher.registerCommandProcessor(this);
  }

  @Override
  public Class<OrderInsertCommand> getMatchClass() {
    return OrderInsertCommand.class;
  }

  @Override
  public void process(OrderInsertCommand command) {
    log.info("OrderInsertCommandProcessor process: {}", command.toString());
  }

}
