package com;

import cn.hutool.json.JSONUtil;
import com.service.impl.ExtendInfoTemplateService;
import com.service.impl.FNATemplateService;
import com.service.impl.RPQTemplateService;
import com.vo.UserResponseCardVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @ClassName : TemplateApplication
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 16:23
 **/
@Slf4j
@SpringBootApplication
public class TemplateApplication {

    public static void main(String[] args) {
        SpringApplication.run(TemplateApplication.class, args);
        // 可以通过策略模式路由取得 ExtendInfoTemplateService 具体类
//        ExtendInfoTemplateService extendInfoService = SpringContextHolder.getBean(serviceName);
        ExtendInfoTemplateService fnaTemplateService = new FNATemplateService();
        fnaTemplateService.setUserResponseCardId("100000");
        fnaTemplateService.getExtendInfo();
        UserResponseCardVo vo = new UserResponseCardVo();
        fnaTemplateService.toUserResponseCardVo(vo);
        log.info("FNATemplateService::::{}", JSONUtil.toJsonStr(vo));


        ExtendInfoTemplateService rpqTemplateService = new RPQTemplateService();
        rpqTemplateService.setUserResponseCardId("100000");
        rpqTemplateService.getExtendInfo();

        UserResponseCardVo vo1 = new UserResponseCardVo();
//        rpqTemplateService.toUserResponseCardVo(vo1);
        BeanUtils.copyProperties(rpqTemplateService.getUserResponseCardExtentVo(), vo1);
        log.info("RPQTemplateService::::{}", JSONUtil.toJsonStr(vo1));

    }


}
