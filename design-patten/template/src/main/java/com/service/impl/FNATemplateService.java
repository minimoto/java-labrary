package com.service.impl;

import com.vo.FNAExtentVo;
import com.vo.UserResponseCardVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName : RPQServiceImpl
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/9/23 9:17
 **/
@Service
@Slf4j
public class FNATemplateService extends ExtendInfoTemplateService<FNAExtentVo> {

    public FNATemplateService() {
        super.setUserResponseCardExtentVo(new FNAExtentVo());
    }

    @Override
    public boolean isExtendInfo(){
        return false;
    }

    @Override
    public void toUserResponseCardVo(UserResponseCardVo userResponseCardVo) {
        // 可以用 struct mapper 映射
//        userResponseCardMapper.toUserResponseCardEntity(super.getUserResponseCardExtentVo(), userResponseCardVo);
        userResponseCardVo.setScore(super.getUserResponseCardExtentVo().getScore());
        userResponseCardVo.setRiskLevel(super.getUserResponseCardExtentVo().getRiskLevel());
    }


}
