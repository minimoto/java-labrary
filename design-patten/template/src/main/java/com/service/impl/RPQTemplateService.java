package com.service.impl;

import com.vo.RPQExtentVo;
import com.vo.UserResponseCardVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @ClassName : RPQServiceImpl
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/9/23 9:17
 **/
@Service
@Slf4j
public class RPQTemplateService extends ExtendInfoTemplateService<RPQExtentVo> {

    public RPQTemplateService() {
        super.setUserResponseCardExtentVo(new RPQExtentVo());
    }

    @Override
    public boolean isExtendInfo(){
        return true;
    }

    @Override
    public void customize(){
        log.info("::::::RPQService ExtendInfoTemplateService.customize");
        if(super.getUserResponseCardExtentVo().getScore() > 80){
            super.getUserResponseCardExtentVo().setRiskLevel("优");
        }else {
            super.getUserResponseCardExtentVo().setRiskLevel("一般");
        }
    }

    @Override
    public void toUserResponseCardVo(UserResponseCardVo userResponseCardVo) {
        // 可以用 struct mapper 映射
//        userResponseCardMapper.toUserResponseCardEntity(super.getUserResponseCardExtentVo(), userResponseCardVo);
        userResponseCardVo.setScore(super.getUserResponseCardExtentVo().getScore());
        userResponseCardVo.setRiskLevel(super.getUserResponseCardExtentVo().getRiskLevel());
        userResponseCardVo.setExtentInfo(super.getUserResponseCardExtentVo().getExtentInfo());
    }


}
