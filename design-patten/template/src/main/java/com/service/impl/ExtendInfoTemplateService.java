package com.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.json.JSONUtil;
import com.vo.BaseExtentVo;
import com.vo.TempVo;
import com.vo.UserResponseCardVo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @Author xujunqiang
 * @Date 2021/11/29
 */
@Slf4j
@Data
@Service
public abstract class ExtendInfoTemplateService<T extends BaseExtentVo> {

    /**
     *  业务数据 ：用户卡片id
     *  还可以定义其它入参的业务数据
      */
    private String userResponseCardId;

    /**
     * 业务数据 ：子类的入参，用来保存计算结果
     */
    private T userResponseCardExtentVo;

    // 模板方法
    public void getExtendInfo(){
        this.extentScore();
        if(isExtendInfo()) {
            this.extendInfo();
        }
        this.customize();
    }

    /**
     * 计算公共结果：分数. 这个方法一定由父类完成。
     */
    private void extentScore() {
        // 可以根据 userResponseCardId 查询数据库，得到分数
        this.userResponseCardExtentVo.setScore(new Random().nextInt( 100));
    }

    /**
     * 计算扩展信息：这个方法可由子类重载
     */
    protected void extendInfo(){
        // 这里业务处理，保存扩展信息
        TempVo vo = new TempVo();
        vo.setExtentInfo("hahahah");
        BeanUtils.copyProperties(vo, userResponseCardExtentVo);
        log.info("这里业务处理，保存扩展信息: {}", JSONUtil.toJsonStr(userResponseCardExtentVo));
    }

    /**
     * 自定义方法：这个方法可由子类重载
     */
    protected void customize(){
        log.info("::::::default ExtendInfoTemplateService.customize");
    }

    protected boolean isExtendInfo(){
        return false;
    }

    /**
     * 模型转换：将不同的子类模型 (T)userResponseCardExtentVo 转为统一的 UserResponseCardVo 模型，以便入库操作
     * 感觉可以不用这个类，在 main 测试中可知，通过 beanUtil 也是可以模型转换。
     * 可以在业务层根据需要转为自己需要的模型。
     * @param userResponseCardVo
     * @return
     */
    public abstract void toUserResponseCardVo(UserResponseCardVo userResponseCardVo);


}
