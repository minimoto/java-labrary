package com.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


/**
 * @Author jqxu
 * @Date 2021/11/29
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class RPQExtentVo extends BaseExtentVo{

    private String riskLevel;

    private String extentInfo;

}
