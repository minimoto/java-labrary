package com.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @ClassName : UserResponseCardVo
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 16:04
 **/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseCardVo {

    private Integer score;

    private String riskLevel;

    private String extentInfo;
}
