package com.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

/**
 * @ClassName : BaseExtentVo
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 15:41
 **/
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class BaseExtentVo {

    private Integer score;

}
