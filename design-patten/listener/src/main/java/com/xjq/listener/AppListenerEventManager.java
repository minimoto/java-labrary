/*
 * Copyright 2020-2099 sa-token.cc
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xjq.listener;


import com.xjq.common.exception.AppException;

import java.util.ArrayList;
import java.util.List;

/**
 * Sa-Token 事件中心 事件发布器
 *
 * <p> 提供侦听器注册、事件发布能力 </p>
 * 
 * @author click33
 * @since 1.31.0
 */
public class AppListenerEventManager {

	// --------- 注册侦听器 
	
	private static List<AppListener> listenerList = new ArrayList<>();
	
	static {
		// 默认添加控制台日志侦听器 
		listenerList.add(new AppListenerForLog());
	}

	/**
	 * 获取已注册的所有侦听器
	 * @return / 
	 */
	public static List<AppListener> getListenerList() {
		return listenerList;
	}

	/**
	 * 重置侦听器集合
	 * @param listenerList / 
	 */
	public static void setListenerList(List<AppListener> listenerList) {
		if(listenerList == null) {
			throw new AppException("重置的侦听器集合不可以为空");
		}
		AppListenerEventManager.listenerList = listenerList;
	}

	/**
	 * 注册一个侦听器 
	 * @param listener / 
	 */
	public static void registerListener(AppListener listener) {
		if(listener == null) {
			throw new AppException("注册的侦听器不可以为空");
		}
		listenerList.add(listener);
	}

	/**
	 * 注册一组侦听器 
	 * @param listenerList / 
	 */
	public static void registerListenerList(List<AppListener> listenerList) {
		if(listenerList == null) {
			throw new AppException("注册的侦听器集合不可以为空");
		}
		for (AppListener listener : listenerList) {
			if(listener == null) {
				throw new AppException("注册的侦听器不可以为空");
			}
		}
		AppListenerEventManager.listenerList.addAll(listenerList);
	}

	/**
	 * 移除一个侦听器 
	 * @param listener / 
	 */
	public static void removeListener(AppListener listener) {
		listenerList.remove(listener);
	}

	/**
	 * 移除指定类型的所有侦听器 
	 * @param cls / 
	 */
	public static void removeListener(Class<? extends AppListener> cls) {
		ArrayList<AppListener> listenerListCopy = new ArrayList<>(listenerList);
		for (AppListener listener : listenerListCopy) {
			if(cls.isAssignableFrom(listener.getClass())) {
				listenerList.remove(listener);
			}
		}
	}

	/**
	 * 清空所有已注册的侦听器 
	 */
	public static void clearListener() {
		listenerList.clear();
	}

	/**
	 * 判断是否已经注册了指定侦听器 
	 * @param listener / 
	 * @return / 
	 */
	public static boolean hasListener(AppListener listener) {
		return listenerList.contains(listener);
	}

	/**
	 * 判断是否已经注册了指定类型的侦听器 
	 * @param cls / 
	 * @return / 
	 */
	public static boolean hasListener(Class<? extends AppListener> cls) {
		for (AppListener listener : listenerList) {
			if(cls.isAssignableFrom(listener.getClass())) {
				return true;
			}
		}
		return false;
	}
	
	
	// --------- 事件发布 
	
	/**
	 * 事件发布：xx 账号登录
	 * @param loginType 账号类别
	 * @param loginId 账号id
	 * @param tokenValue 本次登录产生的 token 值 
	 */
	public static void doLogin(String loginType, Object loginId, String tokenValue) {
		for (AppListener listener : listenerList) {
			listener.doLogin(loginType, loginId, tokenValue);
		}
	}
			
	/**
	 * 事件发布：xx 账号注销
	 * @param loginType 账号类别
	 * @param loginId 账号id
	 * @param tokenValue token值
	 */
	public static void doLogout(String loginType, Object loginId, String tokenValue) {
		for (AppListener listener : listenerList) {
			listener.doLogout(loginType, loginId, tokenValue);
		}
	}
	


}
