package com;


import com.service.event.UserRegisterEvent;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

/**
 * @author macong
 * @date 2022/12/5
 */

@SpringBootTest(classes = {EventApplication.class})
@Slf4j
public class TestEvent implements ApplicationContextAware, ApplicationEventPublisherAware {

    private ApplicationContext applicationContext;
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void test() throws InterruptedException {
        String username = "xjq";
        System.out.println("用户"+username+"注册成功");
        //可以用applicationContext发送事件
//        applicationContext.publishEvent(new UserRegisterEvent(this, username));
        //也可以用applicationEventPublisher，二者等价
        applicationEventPublisher.publishEvent(new UserRegisterEvent(this, username));
        
        Thread.sleep(8*1000);

    }


    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }
}
