package com.service.listener;

import com.service.event.UserRegisterEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @ClassName : MailUserRegisterListener
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/17 14:18
 **/
@Component
@Order(2)//可以使用order指定顺序，越小优先级越高。我自己测试没有生效
@Slf4j
public class MailUserRegisterListener implements ApplicationListener<UserRegisterEvent> {

    @SneakyThrows
    @Override
    public void onApplicationEvent(UserRegisterEvent event) {
        log.info(".......");
        Thread.sleep(5*1000);
        log.info(Thread.currentThread().getName()+"-给用户"+event.getUsername()+"发送邮件！");
    }

}
