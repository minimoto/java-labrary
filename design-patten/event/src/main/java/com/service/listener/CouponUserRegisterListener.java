package com.service.listener;

import com.service.event.UserRegisterEvent;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @ClassName : CouponUserRegisterListener
 * @Description : 使用@EventListener监听事件
 * @Author : jqxu
 * @Date: 2022/12/17 14:18
 **/
@Component
@Order(10)
@Slf4j
public class CouponUserRegisterListener {


    @Async  // 可以异步处理
    @SneakyThrows
    @EventListener
    public void sendCoupon(UserRegisterEvent event) {
        log.info(".......");
        Thread.sleep(5*1000);
        log.info(Thread.currentThread().getName()+"-给用户"+event.getUsername()+"发送优惠券!");
    }

}
