package com.service.event;

import org.springframework.context.ApplicationEvent;

/**
 * @ClassName : UserRegisterEvent
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/17 14:15
 **/

/**
 * 自定义事件需要继承ApplicationEvent
 * 业务层只需要使用即可：
 * applicationContext.publishEvent(new UserRegisterEvent(this, username));
 */
public class UserRegisterEvent extends ApplicationEvent {

    private String username;

    public UserRegisterEvent(Object source, String username) {
        super(source);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

}