package com;

import com.service.event.UserRegisterEvent;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class EventApplication  {


    public static void main(String[] args) {
        SpringApplication.run(EventApplication.class, args);
    }

}
