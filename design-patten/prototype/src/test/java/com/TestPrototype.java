package com;


import com.prototype.vo.DeepCloneableTarget;
import com.prototype.vo.DeepProtoType;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author jqxu
 * @date 2022/12/5
 */

@RunWith(SpringRunner.class)
@Slf4j
public class TestPrototype {


    @Test
    public void test() throws CloneNotSupportedException {
        DeepProtoType p = new DeepProtoType();
        p.name = "植物";
        p.deepCloneableTarget = new DeepCloneableTarget("百合", "鲜花");

        //方式1 完成深拷贝
        DeepProtoType p1 = (DeepProtoType) p.clone();
        System.out.println("p.name=" + p.name + "p.deepCloneableTarget=" + p.deepCloneableTarget.hashCode());
        System.out.println("p1.name=" + p.name + "p1.deepCloneableTarget=" + p1.deepCloneableTarget.hashCode());

        //方式2 完成深拷贝
        DeepProtoType p2 = (DeepProtoType) p.deepClone();
        System.out.println("p.name=" + p.name + "p.deepCloneableTarget=" + p.deepCloneableTarget.hashCode());
        System.out.println("p2.name=" + p.name + "p2.deepCloneableTarget=" + p2.deepCloneableTarget.hashCode());

    }



}
