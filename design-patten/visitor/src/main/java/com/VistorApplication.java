package com;

import com.service.BusinessReport;
import com.service.vistor.CEOVisitor;
import com.service.vistor.CTOVisitor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VistorApplication {

    /**
     * 员工属性（数据结构）和CEO、CTO访问者（数据操作）的解耦
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(VistorApplication.class, args);

        // 构建报表
        BusinessReport report = new BusinessReport();
        System.out.println("=========== CEO看报表 ===========");
        report.showReport(new CEOVisitor());
        System.out.println("=========== CTO看报表 ===========");
        report.showReport(new CTOVisitor());


    }

}
