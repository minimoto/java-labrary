package com.service.employee;

import com.service.vistor.Visitor;

import java.util.Random;

/**
 * @ClassName : Staff
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 13:56
 **/
// 员工基类
public abstract class Staff {

    public String name;
    public int kpi;// 员工KPI

    public Staff(String name) {
        this.name = name;
        kpi = new Random().nextInt(10);
    }
    // 核心方法，接受Visitor的访问
    public abstract void accept(Visitor visitor);
}