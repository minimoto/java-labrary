package com.service.employee;

import com.service.vistor.Visitor;

import java.util.Random;

/**
 * @ClassName : Manager
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 13:58
 **/
// 经理
public class Manager extends Staff {

    public Manager(String name) {
        super(name);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
    // 一年做的产品数量
    public int getProducts() {
        return new Random().nextInt(10);
    }
}