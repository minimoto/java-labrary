package com.service.employee;

import com.service.vistor.Visitor;

import java.util.Random;

/**
 * @ClassName : Engineer
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 13:58
 **/
// 工程师
public class Engineer extends Staff {

    public Engineer(String name) {
        super(name);
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    // 工程师一年的代码数量
    public int getCodeLines() {
        return new Random().nextInt(10 * 10000);
    }
}
