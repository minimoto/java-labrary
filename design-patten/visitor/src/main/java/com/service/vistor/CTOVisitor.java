package com.service.vistor;

import com.service.employee.Engineer;
import com.service.employee.Manager;

/**
 * @ClassName : CTOVisitor
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 14:02
 **/
public class CTOVisitor implements Visitor {
    @Override
    public void visit(Engineer engineer) {
        System.out.println("工程师: " + engineer.name + ", 代码行数: " + engineer.getCodeLines());
    }

    @Override
    public void visit(Manager manager) {
        System.out.println("经理: " + manager.name + ", 产品数量: " + manager.getProducts());
    }
}
