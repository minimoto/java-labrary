package com.service.vistor;

import com.service.employee.Engineer;
import com.service.employee.Manager;

/**
 * @ClassName : CEOVisitor
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/12/16 14:01
 **/
// CEO访问者
public class CEOVisitor implements Visitor {
    @Override
    public void visit(Engineer engineer) {
        System.out.println("工程师: " + engineer.name + ", KPI: " + engineer.kpi);
    }

    @Override
    public void visit(Manager manager) {
        System.out.println("经理: " + manager.name + ", KPI: " + manager.kpi +
                ", 新产品数量: " + manager.getProducts());
    }
}
