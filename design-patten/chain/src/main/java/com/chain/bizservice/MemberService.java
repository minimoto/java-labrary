package com.chain.bizservice;

import com.chain.service.AbstractHandler;
import com.chain.service.AuthHandler;
import com.chain.service.LoginHandler;
import com.chain.service.ValidateHandler;
import com.chain.vo.Member;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public class MemberService {

    public void login(String loginName,String loginPass){
        AbstractHandler.Builder builder = new AbstractHandler.Builder();
        builder.addHandler(new ValidateHandler())
                .addHandler(new LoginHandler())
                .addHandler(new AuthHandler());
        //用过Netty的人，肯定见过
        builder.build().doHandler(new Member(loginName,loginPass));
    }


}
