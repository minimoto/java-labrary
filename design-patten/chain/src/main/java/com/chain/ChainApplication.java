package com.chain;

import com.chain.bizservice.MemberService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChainApplication {

    public static void main(String[] args) {
        SpringApplication.run(ChainApplication.class, args);
        MemberService memberService = new MemberService();
        memberService.login("xiaoqiang", "");
    }

}
