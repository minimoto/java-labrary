package com.chain.service;

import com.chain.vo.Member;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public abstract class AbstractHandler {

    protected AbstractHandler next;

    private void next(AbstractHandler next){
        this.next = next;
    }

    public abstract void doHandler(Member member);

    public static class Builder{
        private AbstractHandler head;
        private AbstractHandler tail;

        public Builder addHandler(AbstractHandler abstractHandler){
            // do {
            if (this.head == null) {
                this.head = this.tail = abstractHandler;
                return this;
            }
            this.tail.next(abstractHandler);
            this.tail = abstractHandler;
            // }while (false);//真正框架中，如果是双向链表，会判断是否已经到了尾部
            return this;
        }

        public AbstractHandler build(){
            return this.head;
        }
    }

}
