package com.chain.service;

import com.chain.vo.Member;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public class AuthHandler extends AbstractHandler {

    @Override
    public void doHandler(Member member) {
        if (!"管理员".equals(member.getRoleName())) {
            System.out.println("您不是管理员，没有操作权限");
            return;
        }
        System.out.println("允许操作");
    }
}
