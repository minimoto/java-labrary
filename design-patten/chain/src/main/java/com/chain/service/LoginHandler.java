package com.chain.service;

import com.chain.vo.Member;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public class LoginHandler extends AbstractHandler {

    @Override
    public void doHandler(Member member) {
        System.out.println("登录成功！");
        member.setRoleName("管理员");
        next.doHandler(member);
    }
}
