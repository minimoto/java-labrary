package com.chain.service;

import com.chain.vo.Member;
import org.springframework.util.StringUtils;

/**
 * create by xiao_qiang_01@163.com
 * 2022/12/4
 */
public class ValidateHandler extends AbstractHandler {

    @Override
    public void doHandler(Member member) {
        if (StringUtils.isEmpty(member.getLoginName()) ||
                StringUtils.isEmpty(member.getLoginPass())) {
            System.out.println("用户名和密码为空,中止运行");
            return;
        }
        System.out.println("用户名和密码不为空，可以往下执行");
        next.doHandler(member);
    }

}
