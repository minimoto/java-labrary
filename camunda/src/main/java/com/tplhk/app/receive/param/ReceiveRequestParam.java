package com.tplhk.app.receive.param;

import com.tplhk.camunda.starter.model.param.task.ReceiveTaskParam;
import lombok.Data;

/**
 * 会议请求参数
 *
 * @author jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class ReceiveRequestParam extends ReceiveTaskParam {
    /**
     *  启动参数：会议主题
     */
    private String meettingName;
    /**
     *  启动参数：参加人数
     */
    private Integer joinNum;

}
