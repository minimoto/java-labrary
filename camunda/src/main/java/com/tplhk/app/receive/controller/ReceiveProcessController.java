package com.tplhk.app.receive.controller;


import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.payment.model.response.Result;
import com.tplhk.app.receive.param.ReceiveRequestParam;
import com.tplhk.app.receive.service.IBizReceiveProcessInfoService;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *  receiveTask (接收任务) 和 messageEvent (消息事件)
 *
 * 本示例技术点：
 * 1. 接收消息
 * 2. receiveTask 是阻塞型的
 * 3. receiveTask/学习ReceiveTask.bpmn ：做为阻塞任务，需要先启动流程实例。
 *      本例通过 startReceiveProcess 模拟启动流程实例， 然后再执行 mockConsume : processInstanceId 必传
 * 4. receiveTask/学习ReceiveTask2.bpmn ：做为事件（message start event），此时 message 名称全局唯一, 不需要启动流程实例。
 *      直接执行 mockConsume: processInstanceId 和 processInstanceKey 都不用传，但建议传 processInstanceKey
 * 5. 接收任务和消息事件区别：一般来说，消息任务代替消息中间件，因为任务可以扩展事件。消息中间件通常与事件网关配合使用。
 * @author jqxu
 * @since 2022-05-30
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/receiveTask")
public class ReceiveProcessController {

    @Resource
    private IBizReceiveProcessInfoService receiveProcessInfoService;


    /**
     * 启动流程
     *
     * @return 响应结果
     */
    @PostMapping
    public Result<String> startReceiveProcess() {
        String processInstanceId = receiveProcessInfoService.startReceiveProcess();
        Result<String> respResult = Result.ok(processInstanceId);
        return respResult;
    }


    /**
     * 模拟消费消息
     *
     * @return 响应结果
     */
    @PostMapping("/mockConsume")
    public Result<String> mockConsume(@RequestBody ReceiveRequestParam param) {
        receiveProcessInfoService.mockConsumeMsg(param);
        Result<String> respResult = Result.ok();
        return respResult;
    }



//    /**
//     * 查询用户待处理任务
//     *
//     * @param taskQueryParam 任务查询参数
//     * @return 响应结果
//     */
//    @GetMapping("/tasks")
//    public Result<List<TaskVo<Object>>> getTasks(TaskQueryParam taskQueryParam) {
//        log.info("get payment tasks, param: {}", taskQueryParam);
//        TwoTuple<List<TaskVo<Object>>, Long> twoTuple = callActivityProcessInfoService.queryTasks(taskQueryParam);
//        Result<List<TaskVo<Object>>> taskVoRespResult = Result.ok();
//        taskVoRespResult.setData(twoTuple.getFirst());
//        taskVoRespResult.setTotal(twoTuple.getSecond());
//        log.info("get payment tasks, param: {}", taskQueryParam);
//        return taskVoRespResult;
//    }
//
//
//    /**
//     * 查询用户历史任务
//     *
//     * @param taskQueryParam 任务查询参数
//     * @return 响应结果
//     */
//    @GetMapping("/tasks/history")
//    public Result<List<HistoricTaskInstance>> getHistoryTasks(TaskQueryParam taskQueryParam) {
//        log.info("get payment tasks history, param: {}", taskQueryParam);
//        TwoTuple<List<HistoricTaskInstance>, Long> listLongTwoTuple = callActivityProcessInfoService.queryHistoryTasks(taskQueryParam);
//        Result<List<HistoricTaskInstance>> taskVoRespResult = Result.ok();
//        taskVoRespResult.setData(listLongTwoTuple.getFirst());
//        taskVoRespResult.setTotal(listLongTwoTuple.getSecond());
//        return taskVoRespResult;
//    }


}
