package com.tplhk.app.receive.service.impl;


import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.callactivity.wrapper.CallActivityProcessVariablesWrapper;
import com.tplhk.app.receive.constants.ReceiveTaskConstants;
import com.tplhk.app.receive.param.ReceiveRequestParam;
import com.tplhk.app.receive.service.IBizReceiveProcessInfoService;
import com.tplhk.camunda.starter.model.param.process.ProcessVariablesQueryParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import com.tplhk.camunda.starter.util.CamundaUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.runtime.MessageCorrelationBuilder;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 支付流程 - 业务数据 服务实现类
 *
 * @author jqxu
 * @since 2022-05-30
 */
@Service
@Slf4j
public class BizReceiveProcessInfoServiceImpl implements IBizReceiveProcessInfoService {

    @Resource
    private CamundaCommonService camundaCommonService;

    @Override
    public String startReceiveProcess() {
        String businessKey = "process" + System.currentTimeMillis();
        ProcessInstance processInstance = this.camundaCommonService.startProcessInstance(ReceiveTaskConstants.Process_ReceiveTask_ID, businessKey);
        return processInstance.getId();
    }

    @Override
    public void mockConsumeMsg(ReceiveRequestParam param) {
        param.setEventName(ReceiveTaskConstants.MessageOrderInfo);
        this.camundaCommonService.startMessageCorrelation(param.getProcessInstanceId(), param.getProcessInstanceKey(), param.getEventName(), param);

    }


}
