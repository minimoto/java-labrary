package com.tplhk.app.receive.constants;

/**
 * 流程常量定义
 *
 * @author jqxu
 * @since 2022-05-30
 */
public class ReceiveTaskConstants {
    /**
     * 流程定义ID
     */
    public static final String Process_ReceiveTask_ID = "ProcessReceiveTask";

    /**
     * 任务ID
     */
    public static final String TaskReceiveOrderInfoID = "TaskReceiveOrderInfo";
    public static final String TaskDoSomeThingID = "TaskDoSomeThing";

    /**
     * 消息
     */
    public static final String MessageOrderInfo = "MessageOrderInfo";


}
