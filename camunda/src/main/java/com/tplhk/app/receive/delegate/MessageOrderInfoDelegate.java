package com.tplhk.app.receive.delegate;

import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Description : 服务任务delegate.bpmn
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Service(value = "messageOrderInfoDelegate")
public class MessageOrderInfoDelegate implements JavaDelegate {

    @Autowired
    CamundaCommonService commonService;

    @Override
    public void execute(DelegateExecution execution) {
        String executionId = execution.getId();
        log.info(" messageOrderInfoDelegate.executionId:::{}", executionId);
        String meettingName = (String) execution.getVariable("meettingName");
        log.info(" messageOrderInfoDelegate.meettingName:::{}", meettingName);
        Integer joinNum = (Integer) execution.getVariable("joinNum");
        log.info(" messageOrderInfoDelegate.joinNum:::{}", joinNum);


        //任务失败
//        execution.getProcessEngine()
//                .getTaskService()
//                .handleBpmnError(execution.get(), "50000", "error!!!~~~");
    }


}
