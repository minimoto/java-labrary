package com.tplhk.app.cancel.constants;

/**
 * 流程常量定义
 *
 * @author jqxu
 * @since 2022-05-30
 */
public class CancelConstants {
    /**
     * 流程定义ID
     */
    public static final String ProcessTransactionID = "ProcessTransaction";

    public static final String START_MSG = "START_MSG";

}
