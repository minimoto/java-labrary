package com.tplhk.app.cancel.param;

import com.tplhk.camunda.starter.model.param.task.ReceiveTaskParam;
import lombok.Data;

/**
 * 会议请求参数
 *
 * @author jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class CancelRequestParam extends ReceiveTaskParam {


}
