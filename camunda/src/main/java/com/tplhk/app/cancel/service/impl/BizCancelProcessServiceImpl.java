package com.tplhk.app.cancel.service.impl;


import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.callactivity.wrapper.CallActivityProcessVariablesWrapper;
import com.tplhk.app.cancel.constants.CancelConstants;
import com.tplhk.app.cancel.param.CancelRequestParam;
import com.tplhk.app.cancel.service.IBizCancelProcessService;
import com.tplhk.app.receive.constants.ReceiveTaskConstants;
import com.tplhk.app.receive.param.ReceiveRequestParam;
import com.tplhk.app.trip.constants.TripConstants;
import com.tplhk.app.trip.service.IBizTripProcessService;
import com.tplhk.camunda.starter.model.param.process.ProcessVariablesQueryParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import com.tplhk.camunda.starter.util.CamundaUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jqxu
 * @since 2022-05-30
 */
@Service
@Slf4j
public class BizCancelProcessServiceImpl implements IBizCancelProcessService {

    @Resource
    private CamundaCommonService camundaCommonService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String startCancelProcess() {
        String busiKey = String.valueOf(System.currentTimeMillis());
        log.info("开启流程，processKey：{}, businessKey：{}", CancelConstants.ProcessTransactionID, busiKey);
        ProcessInstance processInstance = this.camundaCommonService.startProcessInstance(
                CancelConstants.ProcessTransactionID,
                busiKey);
        log.info("开启流程，结果：id = {}, getProcessInstanceId = {}, getProcessDefinitionId = {}, getBusinessKey = {}",
                processInstance.getId(),
                processInstance.getProcessInstanceId(),
                processInstance.getProcessDefinitionId(),
                processInstance.getBusinessKey());
        return processInstance.getId();
    }

    @Override
    public void mockConsumeMsg(CancelRequestParam param) {
        param.setEventName(CancelConstants.START_MSG);
        this.camundaCommonService.startMessageCorrelation(param.getProcessInstanceId(), param.getProcessInstanceKey(), param.getEventName(), param);

    }

}
