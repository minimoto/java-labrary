package com.tplhk.app.cancel.controller;


import com.tplhk.app.cancel.param.CancelRequestParam;
import com.tplhk.app.cancel.service.IBizCancelProcessService;
import com.tplhk.app.payment.model.response.Result;
import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.receive.param.ReceiveRequestParam;
import com.tplhk.app.trip.service.IBizTripProcessService;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import com.tplhk.domain.order.entity.OrderEntity;
import com.tplhk.domain.order.service.OrderDomainService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 *
 * 工作流程图：cancel/取消&错误&补偿事件
 *
 * @author jqxu
 * @since 2022-05-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/cancel")
public class CancelProcessController {

    @Resource
    private IBizCancelProcessService cancelProcessService;

    @Autowired
    OrderDomainService orderDomainService;

    /**
     * 开启流程
     *
     * @return 响应结果
     */
    @PostMapping
    public Result<String> startCancelProcess() {
        String processInstanceId = cancelProcessService.startCancelProcess();
        Result<String> respResult = Result.ok(processInstanceId);
        log.info("start payment, result: {}", respResult);

        OrderEntity entity = OrderEntity.builder().productCode("111").orderNo("aaa").count(100).build();
        orderDomainService.insertOrder(entity);
        return respResult;
    }

    /**
     * 模拟消费消息
     *
     * @return 响应结果
     */
    @PostMapping("/mockConsume")
    public Result<String> mockConsume(@RequestBody CancelRequestParam param) {
        cancelProcessService.mockConsumeMsg(param);
        Result<String> respResult = Result.ok();
        return respResult;
    }

}
