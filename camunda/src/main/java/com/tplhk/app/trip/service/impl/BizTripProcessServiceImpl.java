package com.tplhk.app.trip.service.impl;


import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.callactivity.wrapper.CallActivityProcessVariablesWrapper;
import com.tplhk.app.trip.constants.TripConstants;
import com.tplhk.app.trip.service.IBizTripProcessService;
import com.tplhk.camunda.starter.model.param.process.ProcessVariablesQueryParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import com.tplhk.camunda.starter.util.CamundaUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jqxu
 * @since 2022-05-30
 */
@Service
@Slf4j
public class BizTripProcessServiceImpl implements IBizTripProcessService {

    @Resource
    private CamundaCommonService camundaCommonService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String startTripProcess() {
        String busiKey = String.valueOf(System.currentTimeMillis());
        log.info("开启旅行订票流程，processKey：{}, businessKey：{}", TripConstants.Process_Trip_ID, busiKey);
        ProcessInstance processInstance = this.camundaCommonService.startProcessInstance(
                TripConstants.Process_Trip_ID,
                busiKey);
        log.info("开启旅行订票流程，结果：id = {}, getProcessInstanceId = {}, getProcessDefinitionId = {}, getBusinessKey = {}",
                processInstance.getId(),
                processInstance.getProcessInstanceId(),
                processInstance.getProcessDefinitionId(),
                processInstance.getBusinessKey());
        return processInstance.getId();
    }



}
