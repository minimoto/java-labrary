package com.tplhk.app.trip.delegate;

import com.tplhk.exception.AppExcetion;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

@Component("bookHotelAdapter")
public class BookHotelAdapter implements JavaDelegate {

  @Override
  public void execute(DelegateExecution execution) throws Exception {

    System.out.println("2. book hotel ");
    if (true) {
      throw new AppExcetion("hotel booking did not work");
    }
  }

}
