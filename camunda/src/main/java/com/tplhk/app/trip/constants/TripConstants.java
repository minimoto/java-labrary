package com.tplhk.app.trip.constants;

/**
 * 流程常量定义
 *
 * @author jqxu
 * @since 2022-05-30
 */
public class TripConstants {
    /**
     * 流程定义ID
     */
    public static final String Process_Trip_ID = "ProcessTrip";



}
