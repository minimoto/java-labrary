package com.tplhk.app.trip.service;


import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import org.camunda.bpm.engine.history.HistoricTaskInstance;

import java.util.List;

/**
 * 支付流程 - 业务数据 服务类
 *
 * @author jqxu
 * @since 2022-05-30
 */
public interface IBizTripProcessService {

    /**
     *
     * @return 响应结果
     */
    String startTripProcess();



}
