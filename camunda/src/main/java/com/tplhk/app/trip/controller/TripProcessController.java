package com.tplhk.app.trip.controller;


import com.tplhk.app.payment.model.response.Result;
import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.app.trip.service.IBizTripProcessService;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 旅行订票过程
 *
 * 工作流程图：trip/trip错误事件捕获示例
 * 错误事件：
 * 1. 开始事件
 *      a. 主流程的错误开始事件，只能捕获主流程的异常
 *      b. 子流程的错误开始事件，只能捕获子流程的异常
 *
 *
 * @author jqxu
 * @since 2022-05-19
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/trip")
public class TripProcessController {

    @Resource
    private IBizTripProcessService tripProcessService;
    /**
     * 开启流程
     *
     * @return 响应结果
     */
    @PostMapping
    public Result<String> startTripProcess() {
        String processInstanceId = tripProcessService.startTripProcess();
        Result<String> respResult = Result.ok(processInstanceId);
        log.info("start payment, result: {}", respResult);
        return respResult;
    }



}
