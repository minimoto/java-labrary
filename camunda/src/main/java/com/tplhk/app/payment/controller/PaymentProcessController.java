package com.tplhk.app.payment.controller;


import com.tplhk.app.payment.constants.PaymentProcessConstants;
import com.tplhk.app.payment.model.entity.PaymentProcessInfoEntity;
import com.tplhk.app.payment.model.param.PaymentConfirmParam;
import com.tplhk.app.payment.model.param.PaymentRequestParam;
import com.tplhk.app.payment.model.response.Result;
import com.tplhk.app.payment.service.IBizPaymentProcessInfoService;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 支付流程 - Controller
 * 工作流程图：payment/*.bpmn
 * @author jqxu
 * @date 2022-05-22 18:08
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/payment")
public class PaymentProcessController {



    @Resource
    private IBizPaymentProcessInfoService paymentProcessInfoService;
    /**
     * 开启支付流程
     *
     * @param paymentRequestParam 支付参数
     * @return 响应结果
     */
    @PostMapping
    public Result<String> startPayment(@Validated @RequestBody PaymentRequestParam paymentRequestParam) {
        log.info("start payment, param: {}", paymentRequestParam);
        String processInstanceId = this.paymentProcessInfoService.startPaymentProcess(paymentRequestParam);
        Result<String> respResult = Result.ok(processInstanceId);
        log.info("start payment, result: {}", respResult);
        return respResult;
    }

    /**
     * 确认支付1
     *
     * @param paymentConfirmParam 确认参数
     * @return 响应结果
     */
    @PutMapping("/confirm1")
    public Result<String> confirmPayment1(@Validated @RequestBody PaymentConfirmParam paymentConfirmParam) {
        log.info("confirm payment, param: {}", paymentConfirmParam);
        paymentConfirmParam.setTaskId(PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK1_ID);
        String taskId = this.paymentProcessInfoService.confirmPayment(paymentConfirmParam);
        log.info("confirm payment, result: {}", taskId);
        return Result.ok(taskId);
    }

    /**
     * 确认支付2
     *
     * @param paymentConfirmParam 确认参数
     * @return 响应结果
     */
    @PutMapping("/confirm2")
    public Result<String> confirmPayment2(@Validated @RequestBody PaymentConfirmParam paymentConfirmParam) {
        log.info("confirm payment, param: {}", paymentConfirmParam);
        paymentConfirmParam.setTaskId(PaymentProcessConstants.PAYMENT_USER_CONFIRM_TASK2_ID);
        String taskId = this.paymentProcessInfoService.confirmPayment(paymentConfirmParam);
        log.info("confirm payment, result: {}", taskId);
        return Result.ok(taskId);
    }

    /**
     * 查询用户待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks")
    public Result<List<TaskVo<PaymentProcessInfoEntity>>> getTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks, param: {}", taskQueryParam);
        TwoTuple<List<TaskVo<PaymentProcessInfoEntity>>, Long> twoTuple = this.paymentProcessInfoService.queryTasks(taskQueryParam);
        Result<List<TaskVo<PaymentProcessInfoEntity>>> taskVoRespResult = Result.ok();
        taskVoRespResult.setData(twoTuple.getFirst());
        taskVoRespResult.setTotal(twoTuple.getSecond());
        log.info("get payment tasks, param: {}", taskQueryParam);
        return taskVoRespResult;
    }


    /**
     * 查询用户历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks/history")
    public Result<List<HistoricTaskInstance>> getHistoryTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks history, param: {}", taskQueryParam);
        TwoTuple<List<HistoricTaskInstance>, Long> listLongTwoTuple = this.paymentProcessInfoService.queryHistoryTasks(taskQueryParam);
        Result<List<HistoricTaskInstance>> taskVoRespResult = Result.ok();
        taskVoRespResult.setData(listLongTwoTuple.getFirst());
        taskVoRespResult.setTotal(listLongTwoTuple.getSecond());
        return taskVoRespResult;
    }


}
