package com.tplhk.app.payment.model.vo;

import com.tplhk.camunda.starter.model.param.task.UserTaskConfirmParam;
import lombok.Data;


/**
 * 支付确认参数
 *
 * @Author : jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class PaymentConfirmVo extends UserTaskConfirmParam {

    /**
     * 用户是否同意付款(0:未确认,1:同意, 2:不同意)
     */
    private Integer approvalResult;

}
