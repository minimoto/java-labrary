package com.tplhk.app.payment.delegate;


import com.tplhk.app.payment.model.wrapper.PaymentProcessVariablesWrapper;
import com.tplhk.app.payment.service.IBizPaymentProcessInfoService;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * 支付活动 - Java代理
 *
 * @author jqxu
 * @date 2022-05-31
 */
@Service("paymentDelegate")
@Slf4j
public class PaymentDelegate implements JavaDelegate {

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void execute(DelegateExecution execution) {
        /** 获取流程相关信息 */
        String bizKey = execution.getProcessBusinessKey();
        String processInstanceId = execution.getProcessInstanceId();
        log.info("调用支付服务，processInstanceId: {}, bizKey: {}", processInstanceId, bizKey);

        PaymentProcessVariablesWrapper paymentProcessVariablesWrapper = new PaymentProcessVariablesWrapper(execution.getVariables());
        log.info("RPC调用支付服务, 流程变量：{}", paymentProcessVariablesWrapper);

       // todo 根据 bizKey 更新业务DB (省略)
//        Boolean result = this.bizPaymentProcessInfoService.updateById(bizPaymentProcessInfo);


        //抛出异常，则此task执行失败，回退到上一步
        //throw new MsgRuntimeException("支付异常");
    }

}
