package com.tplhk.app.payment.model.param;

import lombok.Data;

import java.math.BigDecimal;

/**
 * 支付请求参数
 *
 * @author jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class PaymentRequestParam {
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品价格
     */
    private BigDecimal productPrice;
    /**
     * 支付用户ID
     */
    private String paymentAssignee;
}
