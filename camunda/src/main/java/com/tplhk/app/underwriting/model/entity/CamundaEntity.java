package com.tplhk.app.underwriting.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

/**
 * 支付流程 - 业务数据
 *
 * @author jqxu
 * @since 2022-01-30
 */
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class CamundaEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程实例ID
     */
    private String processInstanceId;


}
