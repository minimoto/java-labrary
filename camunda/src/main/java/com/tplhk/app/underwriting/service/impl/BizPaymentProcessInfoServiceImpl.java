package com.tplhk.app.underwriting.service.impl;


import com.tplhk.app.underwriting.constants.UnderwritingProcessConstants;
import com.tplhk.app.underwriting.model.entity.PaymentProcessInfoEntity;
import com.tplhk.app.underwriting.model.param.ActivityJobParam;
import com.tplhk.app.underwriting.model.param.RegisterRequestParam;
import com.tplhk.app.underwriting.model.wrapper.PaymentProcessVariablesWrapper;
import com.tplhk.app.underwriting.service.IBizPaymentProcessInfoService;
import com.tplhk.camunda.starter.model.param.process.ProcessVariablesQueryParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import com.tplhk.camunda.starter.servcie.CamundaCommonService;
import com.tplhk.camunda.starter.util.CamundaUtils;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * 支付流程 - 业务数据 服务实现类
 *
 * @author jqxu
 * @since 2022-05-30
 */
@Service
@Slf4j
public class BizPaymentProcessInfoServiceImpl implements IBizPaymentProcessInfoService {

    @Resource
    private CamundaCommonService camundaCommonService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String startRegisterProcess(RegisterRequestParam registerRequestParam) {
        /** 插入业务数据 */
        // todo registerRequestParam 转 entity (略)
        // todo save(entity);
        // 取 entity.getId();
        log.info("插入业务数据:{}", registerRequestParam.toString());
        String id = String.valueOf(System.currentTimeMillis());

        /** 启动流程 */
        log.info("开启流程处理，processKey：{}, businessKey：{}", UnderwritingProcessConstants.UNDERWRITING_PROCESS_ID, id);
        ProcessInstance processInstance = this.camundaCommonService.startProcessInstance(
                UnderwritingProcessConstants.UNDERWRITING_PROCESS_ID,
                id,
                registerRequestParam);
        log.info("开启支付流程处理，结果：id = {}, getProcessInstanceId = {}, getProcessDefinitionId = {}, getBusinessKey = {}",
                processInstance.getId(),
                processInstance.getProcessInstanceId(),
                processInstance.getProcessDefinitionId(),
                processInstance.getBusinessKey());

        /** 更新业务数据中的流程实例ID */
        // todo entity.setProcessInstanceId(processInstance.getId());
        // todo update(paymentProcessInfoEntity);

        this.camundaCommonService.completeTask(UnderwritingProcessConstants.ACTIVITY_REGISTER);

        return id;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String job(ActivityJobParam activityJobParam) {
        String taskId = camundaCommonService.getTaskId(activityJobParam.getProcessInstanceId(), activityJobParam.getTaskId());
        if(null == taskId){
            log.error("找不到 taskId, activityJobParam 参数：{}, taskId: {}", activityJobParam, taskId);
            return null;
        }
        activityJobParam.setTaskId(taskId);
        log.info("activityJobParam 参数：{}", activityJobParam);

        //todo 通过 activityJobParam.getBizKey() 更新表记录
        log.info("通过 activityJobParam.getBizKey() = {} 更新表记录", activityJobParam.getBizKey());

        this.camundaCommonService.completeTask(activityJobParam.getTaskId());
        return activityJobParam.getTaskId();
    }

    @Override
    public TwoTuple<List<TaskVo<PaymentProcessInfoEntity>>, Long> queryTasks(TaskQueryParam taskQueryParam) {
//        Consumer<TaskQuery> extendTaskQuery = (x) -> {
//            // 这里设置额外的查询条件
//            x.caseDefinitionKey("kkkkkkkk");
//            x.caseDefinitionId("2eeeeee");
//        };
        TwoTuple<List<TaskVo<PaymentProcessInfoEntity>>, Long> twoTuple = this.camundaCommonService.queryRuntimeTasks(
                taskQueryParam,
                null,
                taskQueryParam.getTaskBizKeyVariableName(),
                (productName) -> {
                    // todo 根据 productName 值查询业务数据，存到 setTaskVoTaskData
                    log.info("productName::::::{}", productName);
        //            return this.getById(Long.valueOf(bizKey));
                    return new PaymentProcessInfoEntity();
                },
                (bizKey) -> {
                    // todo 根据 BusinessKey 返回业务数据
                    log.info("BusinessKey::::::{}", bizKey);
        //            return this.getById(Long.valueOf(bizKey));
                    return new PaymentProcessInfoEntity();
                });
        log.info("查询待处理任务列表，结果：{}", twoTuple.getFirst());

        return twoTuple;
    }


    @Override
    public TwoTuple<List<HistoricTaskInstance>, Long> queryHistoryTasks(TaskQueryParam taskQueryParam) {
        return this.camundaCommonService.queryHistoryTasks(taskQueryParam);
    }

}
