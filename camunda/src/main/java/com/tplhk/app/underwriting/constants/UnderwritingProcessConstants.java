package com.tplhk.app.underwriting.constants;

/**
 * 流程常量定义
 *
 * @author jqxu
 * @date 2022-05-25 10:47
 */
public class UnderwritingProcessConstants {
    /**
     * 流程定义
     */
    public static final String UNDERWRITING_PROCESS_ID = "Process_underwriting_input";

    /**
     * 任务
     */
    public static final String ACTIVITY_REGISTER = "Activity_register";
    public static final String ACTIVITY_JOB = "Activity_job";
    public static final String ACTIVITY_UNDERWRITING_START = "Activity_underwriting_start";
    public static final String ACTIVITY_UNDERWRITING_SAVE = "Activity_underwriting_save";
    public static final String ACTIVITY_UNDERWRITING_CHECK = "Activity_underwriting_check";

    /**
     * 流程变量名
     */
    public static final String VAR_UNDERWRITINGRESULT = "underwritingResult";
    public static final String VAR_CHECKRESULT  = "checkResult";


}
