package com.tplhk.app.underwriting.model.wrapper;

import com.tplhk.app.payment.constants.PaymentProcessConstants;
import com.tplhk.camunda.starter.util.CamundaUtils;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Optional;

/**
 * 支付流程 - 流程变量访问器
 *
 * @Author : jqxu
 * @Date: 2022/5/6 16:27
 */
public class PaymentProcessVariablesWrapper {

    private Map<String, Object> processVariables;

    public PaymentProcessVariablesWrapper(Map<String, Object> processVariables) {
        this.processVariables = processVariables;
    }

    public String getProductName() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(PaymentProcessConstants.PAYMENT_PRODUCT_NAME_VAR_NAME))
                .orElse(null);
    }

    public BigDecimal getProductPrice() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> processVariables.get(PaymentProcessConstants.PAYMENT_PRODUCT_PRICE_VAR_NAME))
                .map(CamundaUtils::convertDecimal)
                .orElse(null);
    }

    public BigDecimal getProductDiscount() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> processVariables.get(PaymentProcessConstants.PAYMENT_PRODUCT_DISCOUNT_RESULT_VAR_NAME))
                .map(productDiscountResultMap -> ((Map) productDiscountResultMap).get(PaymentProcessConstants.PAYMENT_PRODUCT_DISCOUNT_VAR_NAME))
                .map(CamundaUtils::convertDecimal)
                .orElse(null);
    }

    public BigDecimal getProductDiscountPrice() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> processVariables.get(PaymentProcessConstants.PAYMENT_PRODUCT_DISCOUNT_PRICE_VAR_NAME))
                .map(CamundaUtils::convertDecimal)
                .orElse(null);
    }

    @Override
    public String toString() {
        return "PaymentProcessVariablesWrapper{" +
                "productName=" + this.getProductName() +
                ",productPrice=" + this.getProductPrice() +
                ",productDiscount=" + this.getProductDiscount() +
                ",productDiscountPrice=" + this.getProductDiscountPrice() +
                '}';
    }
}
