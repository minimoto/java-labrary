package com.tplhk.app.underwriting.model.param;

import com.tplhk.camunda.starter.model.param.task.UserTaskConfirmParam;
import lombok.Data;


/**
 * 支付确认参数
 *
 * @author luohq
 * @date 2022-01-22 18:05
 */
@Data
public class ActivityJobParam extends UserTaskConfirmParam{

}
