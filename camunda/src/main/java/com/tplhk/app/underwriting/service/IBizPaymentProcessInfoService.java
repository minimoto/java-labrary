package com.tplhk.app.underwriting.service;


import com.tplhk.app.underwriting.model.entity.PaymentProcessInfoEntity;
import com.tplhk.app.underwriting.model.param.ActivityJobParam;
import com.tplhk.app.underwriting.model.param.RegisterRequestParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import org.camunda.bpm.engine.history.HistoricTaskInstance;

import java.util.List;

/**
 * 支付流程 - 业务数据 服务类
 *
 * @author jqxu
 * @since 2022-05-30
 */
public interface IBizPaymentProcessInfoService  {

    /**
     * 开启支付流程
     *
     * @param registerRequestParam 初审注册参数
     * @return 响应结果
     */
    String startRegisterProcess(RegisterRequestParam registerRequestParam);

    /**
     * 查询待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    TwoTuple<List<TaskVo<PaymentProcessInfoEntity>>, Long> queryTasks(TaskQueryParam taskQueryParam);

    /**
     * 查询历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    TwoTuple<List<HistoricTaskInstance>, Long> queryHistoryTasks(TaskQueryParam taskQueryParam);

    /**
     * 作业
     *
     * @param activityJobParam 确认参数
     * @return 响应结果
     */
    String job(ActivityJobParam activityJobParam);


}
