package com.tplhk.app.underwriting.model.param;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 初审注册参数
 *
 * @author jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class RegisterRequestParam {
    /**
     * 保单号码
     */
    private String policyNo;
    /**
     * 初审日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime registerTime;

}
