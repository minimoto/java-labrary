package com.tplhk.app.underwriting.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.*;

/**
 * @author zhanglu
 * @date 2020-02-02
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Result<T> implements Serializable {

    /**
     * 关于用户的状态码，全部在SSO项目中已定义4001-4099，分别有：
     * 4011 登录失败
     * 4017 Token无效
     * 4031 已登录，但权限不足
     */

    /**
     * 成功返回
     */
    public static final int OK = 0;

    /**
     * 参数错误
     */
    public static final int PARAM_ERROR = 4000;
    /**
     * 未知异常
     */
    public static final int INTERNAL_SERVER_ERROR = 5000;
    /**
     * 服务器维护中
     */
    public static final int SERVER_UPGRADE = 5100;


    /**
     * 返回响应码
     */
    private int code;
    /**
     * 返回消息
     */
    private String message;

    /**
     * 返回数据
     */
    private T data;

    /**
     * 记录总数（分页时会有值返回）
     */
    private Long total;

    /**
     * 成功返回，无数据
     *
     * @return Result
     */
    public static <T> Result<T> ok() {
        ResultBuilder<T> builder = Result.builder();
        return builder.code(OK).build();
    }

    /**
     * 成功返回，有数据
     * 为了统一规范 当data为null时 做了额外处理
     *
     * @param <T> data
     * @return Result
     */
    public static <T> Result<T> ok(T data) {
        ResultBuilder<T> builder = Result.builder();
        return builder.code(OK)
                .data(Optional.ofNullable(data).orElse(null))
                .build();
    }

    public static <T> Result<List<T>> ok(List<T> data) {
        ResultBuilder<List<T>> builder = Result.builder();
        return builder.code(OK)
                .data(Optional.ofNullable(data).orElse(new ArrayList<>()))
                .build();
    }

    public static <T> Result<Map<String, T>> ok(Map<String, T> data) {
        ResultBuilder<Map<String, T>> builder = Result.builder();
        return builder.code(OK)
                .data(Optional.ofNullable(data).orElse(new HashMap<>()))
                .build();
    }


    /**
     * 成功返回，有数据，有分页记录值
     *
     * @param total 总数
     * @param data  分页数据
     * @return Result
     */
    public static <T> Result<List<T>> ok(List<T> data, Long total) {
        ResultBuilder<List<T>> builder = Result.builder();
        return builder.code(OK)
                .data(Optional.ofNullable(data).orElse(new ArrayList<>()))
                .total(total)
                .build();
    }

    /**
     * 失败返回
     *
     * @param <T>
     * @return
     */
    public static <T> Result<T> failed(int code, String message) {
        ResultBuilder<T> builder = Result.builder();
        return builder.code(code).message(message).build();
    }

}
