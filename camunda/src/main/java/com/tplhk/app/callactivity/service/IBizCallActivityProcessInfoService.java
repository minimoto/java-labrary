package com.tplhk.app.callactivity.service;


import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import org.camunda.bpm.engine.history.HistoricTaskInstance;

import java.util.List;

/**
 * 支付流程 - 业务数据 服务类
 *
 * @author jqxu
 * @since 2022-05-30
 */
public interface IBizCallActivityProcessInfoService  {

    /**
     * 开启会议流程
     *
     * @param callActivityRequestParam 请求参数
     * @return 响应结果
     */
    String startCallActivityProcess(CallActivityRequestParam callActivityRequestParam);


    /**
     * 公文起草
     *
     * @param taskDraftOfficialDocumentsConfirmParam
     * @return 响应结果
     */
    String TaskDraftOfficialDocuments(TaskDraftOfficialDocumentsConfirmParam taskDraftOfficialDocumentsConfirmParam);

    /**
     * 会签
     *
     * @param taskCounterSignatureSubConfirmParam
     * @return 响应结果
     */
    String TaskCounterSignatureSub(TaskCounterSignatureSubConfirmParam taskCounterSignatureSubConfirmParam);

    /**
     * 领导签发
     *
     * @param leaderConfirmParam
     * @return 响应结果
     */
    String TaskLeaderSign(TaskLeaderSignConfirmParam leaderConfirmParam);

    /**
     * 查询待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    TwoTuple<List<TaskVo<Object>>, Long> queryTasks(TaskQueryParam taskQueryParam);

    /**
     * 查询历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    TwoTuple<List<HistoricTaskInstance>, Long> queryHistoryTasks(TaskQueryParam taskQueryParam);



}
