package com.tplhk.app.callactivity.constants;

/**
 * 流程常量定义
 *
 * @author jqxu
 * @since 2022-05-30
 */
public class CallActivityConstants {
    /**
     * 流程定义ID
     */
    public static final String Process_CallActivity_ID = "ProcessCallActivity";

    /**
     * 用户确认任务ID
     */
    public static final String Task_DraftOfficialDocuments_ID = "TaskDraftOfficialDocuments";
    public static final String Task_CounterSignature_ID = "TaskCounterSignature";
    public static final String Task_LeaderSign_ID = "TaskLeaderSign";
    public static final String Task_CounterSignature_Sub_Id = "TaskCounterSignatureSub";

    /**
     * 流程变量名
     */
    public static final String VAR_APPROVAL_RESULT = "approvalResult";
    public static final String VAR_MEETTING_NAME = "meettingName";
    public static final String VAR_JOIN_NUM = "joinNum";
    public static final String VAR_USER_NAME = "userName";
    public static final String VAR_NOTICE_INFO = "noticeInfo";
    public static final String VAR_DRAFT_USER_NAME = "draftUserName";
    public static final String VAR_COUNTER_SIGN_DATE_TIME = "counterSignDateTime";


}
