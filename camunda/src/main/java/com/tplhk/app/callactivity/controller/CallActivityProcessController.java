package com.tplhk.app.callactivity.controller;


import com.tplhk.app.payment.model.response.Result;
import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.app.callactivity.model.param.CallActivityRequestParam;
import com.tplhk.app.callactivity.model.param.TaskLeaderSignConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskCounterSignatureSubConfirmParam;
import com.tplhk.app.callactivity.model.param.TaskDraftOfficialDocumentsConfirmParam;
import com.tplhk.app.callactivity.service.IBizCallActivityProcessInfoService;
import com.tplhk.camunda.starter.model.param.process.TaskQueryParam;
import com.tplhk.camunda.starter.model.vo.TaskVo;
import com.tplhk.camunda.starter.model.vo.TwoTuple;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.history.HistoricTaskInstance;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 调用子流程
 *
 * 工作流程图：callActivity/*.bpmn
 *
 * 本示例技术点：
 * 1. 子流程可以取得主流程变量，但子流程变量需要在模型的 out mappings 映射出去才可以
 * 2. 子流程无法修改主流程的变量值
 * @author jqxu
 * @since 2022-05-30
 */
@Slf4j
@Validated
@RestController
@RequestMapping("/callactivity")
public class CallActivityProcessController {

    @Resource
    private IBizCallActivityProcessInfoService callActivityProcessInfoService;
    /**
     * 开启会议流程
     *
     * @param callActivityRequestParam
     * @return 响应结果
     */
    @PostMapping
    public Result<String> startPayment(@Validated @RequestBody CallActivityRequestParam callActivityRequestParam) {
        log.info("start payment, param: {}", callActivityRequestParam);
        String processInstanceId = callActivityProcessInfoService.startCallActivityProcess(callActivityRequestParam);
        Result<String> respResult = Result.ok(processInstanceId);
        log.info("start payment, result: {}", respResult);
        return respResult;
    }

    /**
     * 公文起草
     * @param confirmParam
     * @return 响应结果
     */
    @PutMapping("/taskDraftOfficialDocuments")
    public Result<String> taskDraftOfficialDocuments(@Validated @RequestBody TaskDraftOfficialDocumentsConfirmParam confirmParam) {
        log.info("confirm payment, param: {}", confirmParam);
        confirmParam.setTaskId(CallActivityConstants.Task_DraftOfficialDocuments_ID);
        String taskId = callActivityProcessInfoService.TaskDraftOfficialDocuments(confirmParam);
        log.info("confirm payment, result: {}", taskId);
        return Result.ok(taskId);
    }

    /**
     * 会签
     * @param confirmParam 确认参数
     * @return 响应结果
     */
    @PutMapping("/taskCounterSignatureSub")
    public Result<String> taskCounterSignatureSub(@Validated @RequestBody TaskCounterSignatureSubConfirmParam confirmParam) {
        log.info("confirm payment, param: {}", confirmParam);
        confirmParam.setTaskId(CallActivityConstants.Task_CounterSignature_Sub_Id);
        String taskId = callActivityProcessInfoService.TaskCounterSignatureSub(confirmParam);
        log.info("confirm payment, result: {}", taskId);
        return Result.ok(taskId);
    }

    /**
     * 领导签发
     * @param confirmParam 确认参数
     * @return 响应结果
     */
    @PutMapping("/taskLeaderSign")
    public Result<String> taskLeaderSign(@Validated @RequestBody TaskLeaderSignConfirmParam confirmParam) {
        log.info("confirm payment, param: {}", confirmParam);
        confirmParam.setTaskId(CallActivityConstants.Task_LeaderSign_ID);
        String taskId = callActivityProcessInfoService.TaskLeaderSign(confirmParam);
        log.info("confirm payment, result: {}", taskId);
        return Result.ok(taskId);
    }

    /**
     * 查询用户待处理任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks")
    public Result<List<TaskVo<Object>>> getTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks, param: {}", taskQueryParam);
        TwoTuple<List<TaskVo<Object>>, Long> twoTuple = callActivityProcessInfoService.queryTasks(taskQueryParam);
        Result<List<TaskVo<Object>>> taskVoRespResult = Result.ok();
        taskVoRespResult.setData(twoTuple.getFirst());
        taskVoRespResult.setTotal(twoTuple.getSecond());
        log.info("get payment tasks, param: {}", taskQueryParam);
        return taskVoRespResult;
    }


    /**
     * 查询用户历史任务
     *
     * @param taskQueryParam 任务查询参数
     * @return 响应结果
     */
    @GetMapping("/tasks/history")
    public Result<List<HistoricTaskInstance>> getHistoryTasks(TaskQueryParam taskQueryParam) {
        log.info("get payment tasks history, param: {}", taskQueryParam);
        TwoTuple<List<HistoricTaskInstance>, Long> listLongTwoTuple = callActivityProcessInfoService.queryHistoryTasks(taskQueryParam);
        Result<List<HistoricTaskInstance>> taskVoRespResult = Result.ok();
        taskVoRespResult.setData(listLongTwoTuple.getFirst());
        taskVoRespResult.setTotal(listLongTwoTuple.getSecond());
        return taskVoRespResult;
    }


}
