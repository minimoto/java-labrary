package com.tplhk.app.callactivity.model.param;

import com.tplhk.camunda.starter.model.param.task.UserTaskConfirmParam;
import lombok.Data;


/**
 * 公文起草确认参数
 *
 * @author jqxu
 * @date 2022-01-22 18:05
 */
@Data
public class TaskDraftOfficialDocumentsConfirmParam extends UserTaskConfirmParam{

    /**
     * 提醒进入会议信息
     */
    private String noticeInfo;

}
