package com.tplhk.app.callactivity.wrapper;

import com.tplhk.app.callactivity.constants.CallActivityConstants;
import com.tplhk.util.DateJdk8Util;

import java.time.LocalDate;
import java.util.Map;
import java.util.Optional;

/**
 *  流程变量访问器
 *
 * @Author : jqxu
 * @Date: 2022/5/6 16:27
 */
public class CallActivityProcessVariablesWrapper {

    private Map<String, Object> processVariables;

    public CallActivityProcessVariablesWrapper(Map<String, Object> processVariables) {
        this.processVariables = processVariables;
    }

    public String getMeettingName() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(CallActivityConstants.VAR_MEETTING_NAME))
                .orElse(null);
    }

    public Integer getJoinNum() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (Integer) processVariables.get(CallActivityConstants.VAR_JOIN_NUM))
                .orElse(null);
    }

    public String getNoticeInfo() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(CallActivityConstants.VAR_NOTICE_INFO))
                .orElse(null);
    }

    public String getDraftUserName() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(CallActivityConstants.VAR_DRAFT_USER_NAME))
                .orElse(null);
    }

    public String getUserName() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(CallActivityConstants.VAR_USER_NAME))
                .orElse(null);
    }

    public String getCounterSignDateTime() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> {
                    LocalDate localDate = (LocalDate) processVariables.get(CallActivityConstants.VAR_COUNTER_SIGN_DATE_TIME);
                    return DateJdk8Util.localDateToDateStr(localDate);
                })
                .orElse(null);
    }

    public String getApprovalResult() {
        return Optional.ofNullable(this.processVariables)
                .map(processVariables -> (String) processVariables.get(CallActivityConstants.VAR_APPROVAL_RESULT))
                .orElse(null);
    }
}
