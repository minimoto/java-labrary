package com.tplhk.exception;

/**
 * @ClassName : AppExcetion
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/19 17:19
 **/
public class TplhkExcetion extends RuntimeException {

    public TplhkExcetion(String message) {
        super(message);
    }
}
