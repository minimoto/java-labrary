package com.tplhk.exception;

/**
 * @ClassName : AppExcetion
 * @Description : TODO
 * @Author : jqxu
 * @Date: 2022/5/19 17:19
 **/
public class AppExcetion extends RuntimeException {

    public AppExcetion(String message) {
        super(message);
    }
}
