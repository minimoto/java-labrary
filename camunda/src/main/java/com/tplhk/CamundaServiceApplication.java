package com.tplhk;

import com.tplhk.camunda.starter.anno.EnableCamundaCommon;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@EnableCamundaCommon
public class CamundaServiceApplication {

    public static void main(String... args) {
        SpringApplication.run(CamundaServiceApplication.class, args);

    }

}
