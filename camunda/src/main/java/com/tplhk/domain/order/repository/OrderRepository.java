package com.tplhk.domain.order.repository;

import com.tplhk.domain.order.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

/**
 * create by xiao_qiang_01@163.com
 * 2020/2/11
 */
@Component
public interface OrderRepository extends JpaRepository<OrderEntity, String> {

}
