package com.tplhk.domain.order.service.impl;

import com.tplhk.domain.order.entity.OrderEntity;
import com.tplhk.domain.order.repository.OrderRepository;
import com.tplhk.domain.order.service.OrderDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2019/11/21
 * @Describe
 */
@Slf4j
@Service
public class OrderDomainServiceImpl implements OrderDomainService {
    private Random random;
    private final OrderRepository orderRepository;

    public OrderDomainServiceImpl(OrderRepository orderRepository) {
        this.random = new Random();
        this.orderRepository = orderRepository;
    }

    @Override
    public int insertOrder(OrderEntity orderEntity){
        orderRepository.save(orderEntity);
        return 1;
    }

}
