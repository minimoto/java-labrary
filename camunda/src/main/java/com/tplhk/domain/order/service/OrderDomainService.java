package com.tplhk.domain.order.service;


import com.tplhk.domain.order.entity.OrderEntity;

/**
 * create by xiao_qiang_01@163.com
 * 2020/7/14
 */
public interface OrderDomainService {

    int insertOrder(OrderEntity orderEntity);

}
