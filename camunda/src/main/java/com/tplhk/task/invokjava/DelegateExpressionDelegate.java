package com.tplhk.task.invokjava;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * @Description : 服务任务delegate.bpmn
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Service(value = "delegateExpressionDelegate")
public class DelegateExpressionDelegate implements JavaDelegate {

    //定义待注入的Field（Camunda建议配置Setter方法进行注入）
//    private Expression item;
//    private Expression text2;

    @Override
    public void execute(DelegateExecution execution) {
//        Long score = (Long) execution.getVariable("score");
//        log.info("2. DelegateExpressionDelegate.score:::{}", score);
//        String item = (String) execution.getVariable("item");
//        log.info("2. DelegateExpressionDelegate.item:::{}", item);
        execution.setVariable("score", 200L);
        execution.setVariable("item", "haha200");
        Long score = (Long) execution.getVariable("score");
        log.info("2. DelegateExpressionDelegate.score:::{}", score);
        String item = (String) execution.getVariable("item");
        log.info("2. DelegateExpressionDelegate.item:::{}", item);
        log.info(":::::execution.getId() = {}", execution.getId());
        //任务失败
//        execution.getProcessEngine()
//                .getTaskService()
//                .handleBpmnError(execution.get(), "50000", "error!!!~~~");
    }


}
