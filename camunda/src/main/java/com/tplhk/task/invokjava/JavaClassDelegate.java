package com.tplhk.task.invokjava;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.Expression;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @Description : 服务任务javaclass.bpmn
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Component
public class JavaClassDelegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution execution) {
        execution.setVariable("score", 100L);
        execution.setVariable("item", "haha");
        Long score = (Long) execution.getVariable("score");
        log.info("1. JavaClassDelegate.score:::{}", score);
        String item = (String) execution.getVariable("item");
        log.info("1. JavaClassDelegate.item:::{}", item);


    }
}
