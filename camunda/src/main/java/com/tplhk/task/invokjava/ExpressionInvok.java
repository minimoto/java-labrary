package com.tplhk.task.invokjava;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/**
 * @Description : 服务任务express.bpmn
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Component("expression")
public class ExpressionInvok  {


    public void test() {
        log.info("3. ExpressionDelegate.test().....");

    }


}
