//package com.tplhk.task.external;
//
//import lombok.extern.slf4j.Slf4j;
//import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
//import org.camunda.bpm.client.task.ExternalTaskHandler;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import java.math.BigDecimal;
//
//
///**
// * 支付信用卡.bpmn
// */
//@Configuration
//@Slf4j
//public class ChargeCardTask {
//
//    private final String ITEM = "item";
//    private final String AMOUNT = "amount";
//
//    @ExternalTaskSubscription(topicName = "charge-card")
//    @Bean
//    public ExternalTaskHandler paymentBizHandler() {
//        return (externalTask, externalTaskService) -> {
//            //获取流程变量
//            String item = externalTask.getVariable(ITEM);
//            Long amount = externalTask.getVariable(AMOUNT);
//            log.info("Charging credit card with an amount of '" + amount + "'€ for the item '" + item + "'...");
//            log.info("The External Task {} has been checked!", externalTask.getId());
////            //完成任务
//            externalTaskService.complete(externalTask);
//
////            //任务失败，可以设置重试: 重试 3 次，间隔 5 秒
////            externalTaskService.handleFailure(
////                    externalTask,
////                    "errorMsg",
////                    "errorDetails",
////                    externalTask.getRetries()==null ? 2 : externalTask.getRetries()-1,
////                    1 * 5L * 1000L);
//        };
//    }
//
//}
