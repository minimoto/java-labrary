package com.tplhk.task.delegate;

import com.tplhk.exception.AppExcetion;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * @Description : 抛个错误2(测试用）
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Service(value = "doAppException1Delegate")
public class DoAppException1Delegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution execution) {
        log.info("抛个错误 AppExcetion1 ........");
        if(true){
            throw new AppExcetion("抛个错误 AppExcetion1");
        }
    }


}
