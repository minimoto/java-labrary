package com.tplhk.task.delegate;

import com.tplhk.exception.TplhkExcetion;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * @Description : 抛个错误3(测试用）
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Service(value = "doTplhkException1Delegate")
public class DoTplhkException1Delegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution execution) {
        log.info("抛个错误 TplhkException1 ........");
        if(true){
            throw new TplhkExcetion("抛个错误 TplhkException1");
        }
    }


}
