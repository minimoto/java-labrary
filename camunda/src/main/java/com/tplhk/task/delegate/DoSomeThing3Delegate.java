package com.tplhk.task.delegate;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Service;

/**
 * @Description : 搞点事件3(测试用）
 * @Author : jqxu
 * @Date: 2022/5/7 17:13
 **/
@Slf4j
@Service(value = "doSomething3")
public class DoSomeThing3Delegate implements JavaDelegate {


    @Override
    public void execute(DelegateExecution execution) {
        log.info("搞点事件3........");
    }


}
