package com.tplhk.temp;

import cn.hutool.extra.spring.SpringUtil;
import com.tplhk.temp.test1.config.FenBaoConfig;
import com.tplhk.temp.test1.config.FenBaoItemConfig;
import com.tplhk.temp.test1.service.FenBaoAbstractService;
import com.tplhk.temp.test1.service.FetchPolicyDataService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Comparator;
import java.util.Map;

@SpringBootTest
class TempDemoApplicationTests {

    @Autowired
    FenBaoConfig fenBaoConfig;

    @Test
    void contextLoads() {
        fenBaoConfig.getFenBao().entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparing(FenBaoItemConfig::getOrder)))
                .forEach((k) -> ((FenBaoAbstractService) SpringUtil.getBean(k.getKey())).invok());
    }

}
