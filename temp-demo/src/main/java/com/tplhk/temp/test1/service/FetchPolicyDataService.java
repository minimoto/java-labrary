package com.tplhk.temp.test1.service;

import com.tplhk.temp.test1.config.FenBaoConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * create by xiao_qiang_01@163.com
 * 2021/9/4
 */
@Slf4j
@Service("fetchPolicyData")
public class FetchPolicyDataService extends FenBaoAbstractService {


    @Override
    public void run() {
        log.info("[{}] this is FetchPolicyDataService", getServiceName());
    }

    @Override
    protected String getServiceName() {
        return "fetchPolicyData";
    }


}
