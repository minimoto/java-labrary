package com.tplhk.temp.test1.entity;

import com.xjq.jpa.model.SoftDeleteEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;
import org.hibernate.envers.Audited;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author: Administrator
 * @Date: 2021/12/21
 * @Describe
 */
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "t_storage")
public class StorageEntity extends SoftDeleteEntity {

    private String productName;

    private Integer count;


}
