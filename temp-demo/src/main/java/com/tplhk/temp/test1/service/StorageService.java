package com.tplhk.temp.test1.service;

import com.tplhk.temp.test1.entity.StorageEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Map;

/**
 * create by xiao_qiang_01@163.com
 * 2023/9/10
 */
@Slf4j
@Service
public class StorageService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    StorageDomainService storageDomainService;

    @Transactional
    public void saveStorage(String id) {
//        jdbcTemplate.execute(" INSERT INTO yun.t_storage(id, product_name, count, type_name, create_by, create_time, update_by, update_time, deleted) VALUES ('" + id + "', 'bbd', -114, NULL, 'system', now(), 'system', now(), 0);");\
        jdbcTemplate.update(" UPDATE yun.t_storage SET product_name =  ?, count = -114, type_name = NULL, " +
                "create_by = 'system', create_time = now(), update_by = 'system', update_time = now(), deleted = 0 WHERE id = ?;",
                id, id);
        StorageEntity storageEntity = jdbcTemplate.queryForObject("select * from t_storage where id=?", new BeanPropertyRowMapper<>(StorageEntity.class), id);
        log.info("::::{}", storageEntity.toString());

    }


    @Transactional
    public void saveStorage1(String id) {
        StorageEntity storageEntity = StorageEntity.builder()
                .productName(id)
                .count(3214)
                .build();
        storageDomainService.save(storageEntity);

        StorageEntity byId = storageDomainService.findByProductName(storageEntity.getProductName());
        log.info("byid::::::{}", byId);
        StorageEntity storageEntity1 = jdbcTemplate.queryForObject("select * from t_storage where product_name=?", new BeanPropertyRowMapper<>(StorageEntity.class), id);
        log.info("::::{}", storageEntity.toString());

    }
}
