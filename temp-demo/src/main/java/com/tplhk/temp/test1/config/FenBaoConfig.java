package com.tplhk.temp.test1.config;

import lombok.Data;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * create by xiao_qiang_01@163.com
 * 2021/9/4
 */
@Configuration
@Data
public class FenBaoConfig {

    private Map<String, FenBaoItemConfig> fenBao = new HashMap<>();

    public FenBaoConfig() {
        FenBaoItemConfig fenBaoItemConfig1 = new FenBaoItemConfig();
        fenBaoItemConfig1.setIsEnable(true);
        fenBaoItemConfig1.setOrder(1);
        fenBao.put("fetchPolicyData", fenBaoItemConfig1);

        FenBaoItemConfig fenBaoItemConfig2 = new FenBaoItemConfig();
        fenBaoItemConfig2.setIsEnable(true);
        fenBaoItemConfig2.setOrder(2);
        fenBao.put("fetchProductData", fenBaoItemConfig2);
    }
}
