package com.tplhk.temp.test1.service.impl;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.tplhk.temp.test1.entity.StorageEntity;
import com.tplhk.temp.test1.repository.StorageRepository;
import com.tplhk.temp.test1.service.StorageDomainService;
import org.springframework.stereotype.Service;

/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2021/12/21
 * @Describe
 */
@Service
public class StorageDomainServiceImpl  implements StorageDomainService {

    private final StorageRepository storageRepository;

    public StorageDomainServiceImpl(StorageRepository storageRepository) {
        this.storageRepository = storageRepository;
    }

    @Override
    public StorageEntity findById(String id) {
        return storageRepository.findById(id).get();
    }

    @Override
    public StorageEntity save(StorageEntity storageEntity) {
        return storageRepository.save(storageEntity);
    }

    @Override
    public StorageEntity findByProductName(String productName) {
        return storageRepository.findByProductName(productName);
    }

}
