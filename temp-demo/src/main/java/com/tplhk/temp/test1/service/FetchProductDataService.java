package com.tplhk.temp.test1.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * create by xiao_qiang_01@163.com
 * 2021/9/4
 */
@Slf4j
@Service("fetchProductData")
public class FetchProductDataService extends FenBaoAbstractService {


    @Override
    public void run() {
        log.info("[{}] this is FetchProductDataService", getServiceName());
    }

    @Override
    protected String getServiceName() {
        return "fetchProductData";
    }


}
