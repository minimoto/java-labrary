package com.tplhk.temp.test1.service;


import com.tplhk.temp.test1.entity.StorageEntity;

/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2021/12/21
 * @Describe
 */
public interface StorageDomainService {

    StorageEntity findById(String id);

    StorageEntity save(StorageEntity storageEntity);

    StorageEntity findByProductName(String productName);
}
