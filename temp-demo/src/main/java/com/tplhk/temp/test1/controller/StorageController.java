package com.tplhk.temp.test1.controller;


import com.tplhk.temp.test1.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2019/11/21
 * @Describe
 */
@Slf4j
@RestController
public class StorageController {


    private final StorageService storageService;

    public StorageController(final StorageService storageService) {
        this.storageService = storageService;
    }

    @GetMapping("/insertStorage/{id}")
    public void insertStorage(@PathVariable("id") String id) {
        storageService.saveStorage(id);
        return ;
    }


}
