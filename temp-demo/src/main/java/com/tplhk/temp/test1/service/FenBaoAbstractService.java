package com.tplhk.temp.test1.service;

import com.tplhk.temp.test1.config.FenBaoConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * create by xiao_qiang_01@163.com
 * 2021/9/4
 */
@Slf4j
public abstract class FenBaoAbstractService {

    @Autowired
    FenBaoConfig fenBaoConfig;

    public void invok() {
        if (!isEnable()) {
            log.warn("{} 未启用运行", getServiceName());
            return;
        }
        this.run();
    }

    protected abstract void run();

    protected abstract String getServiceName();

    private boolean isEnable() {
        return fenBaoConfig.getFenBao().get(getServiceName()).getIsEnable();
    }

}
