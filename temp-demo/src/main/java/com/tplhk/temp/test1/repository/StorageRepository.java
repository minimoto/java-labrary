package com.tplhk.temp.test1.repository;

import com.tplhk.temp.test1.entity.StorageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface StorageRepository extends JpaRepository<StorageEntity, String> {


    StorageEntity findByProductName(String productName);
}
