package com.tplhk.temp.test1.config;

import lombok.Data;

/**
 * create by xiao_qiang_01@163.com
 * 2021/9/4
 */
@Data
public class FenBaoItemConfig {
    private Boolean isEnable;
    private Integer order;
}
