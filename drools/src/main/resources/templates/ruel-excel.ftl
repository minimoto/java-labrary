package droolrule;
import com.tplhk.drool.entity.RuleExcel ;
import com.tplhk.drool.entity.RuleResult;
import java.util.*;
import org.slf4j.Logger
import org.slf4j.LoggerFactory ;

rule "${rulename}"
    salience ${ruleno}
    when ruleExcel : RuleExcel(${condition})
       resultParam : List();
    then
        final Logger LOGGER = LoggerFactory.getLogger("${rulename} 规则引擎") ;
        resultParam.add("${result}");
        LOGGER.info("resultParam = ${result}");
end