package com.tplhk.drool.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author zhouhong
 * @version 1.0
 * @title: ResponseData
 * @description: TODO
 * @date 2019/11/27 9:13
 */
@ToString
public class ResponseData<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int SUCCESS = 0;
    public static final int FAIL = 1;

    @Getter
    @Setter
    private String msg = "success";

    @Getter
    @Setter
    private int code = SUCCESS;

    @Getter
    @Setter
    private T data;

    public ResponseData() {
        super();
    }

    public ResponseData(T data) {
        super();
        this.data = data;
    }

    public ResponseData(T data, String msg) {
        super();
        this.code = FAIL;
        this.data = data;
        this.msg = msg;
    }

    public ResponseData(Throwable e) {
        super();
        this.msg = e.getMessage();
        this.code = FAIL;
    }

    public ResponseData(int code, String msg) {
        super();
        this.code = code;
        this.msg = msg;
    }

    public ResponseData(int code, String msg, T data) {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

}
