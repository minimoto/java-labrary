package com.tplhk.drool.entity;

import lombok.Data;

@Data
public class ClaimQueryVo {
    private String sex;
    private int age;
    private double salary;
    private double money;

}
