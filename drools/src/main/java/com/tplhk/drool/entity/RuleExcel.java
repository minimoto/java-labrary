package com.tplhk.drool.entity;

import lombok.Builder;
import lombok.Data;
import org.apache.logging.log4j.util.Strings;

import java.beans.Transient;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class RuleExcel {
    private Integer ruleId;
    private String sex;
    private Integer age;
    private Integer driver;
    private String websiteId;
    private String result;

    private Integer ruleno;

    public String toRuleString() {
        List<String> condition = new LinkedList<>();
        if (!Strings.isBlank(sex)) {
            condition.add("sex.equalsIgnoreCase(\"" + sex + "\")");
        }
        if (null != age) {
            condition.add("age<=" + age);
        }
        if (null != driver) {
            condition.add("driver<=" + driver);
        }
        if (null != websiteId) {
            condition.add("websiteId.equalsIgnoreCase(\"" + websiteId + "\")");
        }

        return condition.stream().collect(Collectors.joining(" && "));
    }
}
