package com.tplhk.drool.entity;

import cn.hutool.json.JSONUtil;
import com.tplhk.drool.service.IRuleVoResult;
import lombok.Data;


@Data
public class RuleVo2Result implements IRuleVoResult {
    public Integer defaultValue = 0;
    public int score;
    public Sex sex;
    public Age age;
    public Salary salary;
    Object paramValue;

    public RuleVo2Result(Object paramValue) {
        this.paramValue = paramValue;
        init();
    }

    public RuleVo2Result(Integer defaultValue) {
        this.defaultValue = defaultValue;
        init();
    }

    public RuleVo2Result(Object paramValue, Integer defaultValue) {
        this.defaultValue = defaultValue;
        this.paramValue = paramValue;
        init();
    }

    private void init() {
        this.sex = new Sex();
        this.age = new Age();
        this.salary = new Salary();
    }

    @Override
    public RuleBase getSubClass(String className) {
        switch (className) {
            case "sex":
                return this.getSex();
            case "age":
                return this.getAge();
            case "salary":
                return this.getSalary();
            default:
                return null;
        }
    }


    @Data
    public class Sex extends RuleBase {

        public Sex() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("sex", "").toString());
            }
        }
    }

    @Data
    public class Age extends RuleBase {

        public Age() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("age", "").toString());
            }
        }
    }


    @Data
    public class Salary extends RuleBase {

        public Salary() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("salary", "").toString());
            }
        }
    }


}
