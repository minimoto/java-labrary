package com.tplhk.drool.entity;

import lombok.Data;

@Data
public class QueryParam {

    private String paramId;
    private String paramSign;

}
