package com.tplhk.drool.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Builder
@ToString
@Data
public class RuleVoResult {
    private Object param;
    private String fieldName;
    private String result;


}
