package com.tplhk.drool.entity;

import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
@Builder
public class CustomerEntity {
    private String customerId;
    private String sex;
    private Integer age;
    private List<String> orderList;
    private Set<String> orderSet;
    private Map<String, String> orderMap;
}
