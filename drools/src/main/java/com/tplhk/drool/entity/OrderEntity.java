package com.tplhk.drool.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderEntity {
    private String id;
    private String customerId;
    private int price;
}
