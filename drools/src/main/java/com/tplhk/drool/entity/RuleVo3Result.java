package com.tplhk.drool.entity;

import cn.hutool.json.JSONUtil;
import com.tplhk.drool.service.IRuleVoResult;
import lombok.*;

@Data
public class RuleVo3Result implements IRuleVoResult {
    private Integer defaultValue = 0;
    private int score;
    private Money money;
    private Age age;
    private Salary salary;
    private Object paramValue;

    public RuleVo3Result(Object paramValue) {
        this.paramValue = paramValue;
        init();
    }

    public RuleVo3Result(Object paramValue, Integer defaultValue) {
        this.defaultValue = defaultValue;
        this.paramValue = paramValue;
        init();
    }

    private void init() {
        this.money = new Money();
        this.age = new Age();
        this.salary = new Salary();
    }

    @Override
    public RuleBase getSubClass(String className) {
        switch (className) {
            case "money":
                return this.getMoney();
            case "age":
                return this.getAge();
            case "salary":
                return this.getSalary();
            default:
                return null;
        }
    }


    @Data
    public class Money extends RuleBase {

        public Money() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("money", "").toString());
            }
        }
    }

    @Data
    public class Age extends RuleBase {

        public Age() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("age", "").toString());
            }
        }
    }

    @Data
    public class Salary extends RuleBase {

        public Salary() {
            super(defaultValue);
            if (null != paramValue) {
                this.setValue(JSONUtil.parseObj(paramValue).getOrDefault("salary", "").toString());
            }
        }
    }

}
