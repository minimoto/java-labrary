package com.tplhk.drool.entity;

import com.tplhk.drool.service.IRuleVoResult;
import lombok.Data;

@Data
public class RuleBase {
    private Object value;
    private Integer score;
    private Integer defaultValue;


    public RuleBase(Integer defaultValue) {
        if (null == this.defaultValue) {
            this.defaultValue = defaultValue;
        }
        this.score = this.defaultValue;
    }

    public void setScore(Object value, Integer score) {
        this.score = score;
        this.value = value;
    }


}