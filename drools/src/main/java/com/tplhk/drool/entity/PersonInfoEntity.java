package com.tplhk.drool.entity;

import lombok.Data;

@Data
public class PersonInfoEntity {
    private String sex;
    private int age;
    private double salary;

}
