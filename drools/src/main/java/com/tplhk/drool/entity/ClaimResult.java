package com.tplhk.drool.entity;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class ClaimResult extends RuleVo2Result {

    private Money money;

    public ClaimResult() {
        super(0);
        this.money = new Money();
    }

    @Data
    public class Money extends RuleBase {

        public Money() {
            super(defaultValue);
        }
    }
}
