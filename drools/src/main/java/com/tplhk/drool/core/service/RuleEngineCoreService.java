package com.tplhk.drool.core.service;

import org.kie.api.command.Command;

import java.util.List;
import java.util.Map;

public interface RuleEngineCoreService {
    void loadRule(Map<String, String> ruleMap);

    void invokRule(List<Command> commandList);

    void updateRule(Map<String, String> ruleMap);
}
