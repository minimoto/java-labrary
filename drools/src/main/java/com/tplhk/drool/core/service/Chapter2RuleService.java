package com.tplhk.drool.core.service;

import org.kie.api.command.Command;

import java.util.List;

public interface Chapter2RuleService {

    void testData(List<Command> commandList);

}
