package com.tplhk.drool.core.service.impl;

import com.tplhk.drool.core.service.AmlRuleService;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.command.Command;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : AmlRuleService
 * @Description : xiaoqiang
 * @Author : taiping
 * @Date: 2021/5/13 15:41
 **/
@Slf4j
@Service
public class AmlRuleServiceImpl extends RuleEngineCoreServiceImpl implements AmlRuleService {


    public AmlRuleServiceImpl() {
        this.PATHt = "chapter13";
        this.PATH = "src/main/resources/chapter13";
        Map<String, String> ruleFileMap = super.loadRuleContentFromFile(PATHt);
        log.info("chapter13。。。。。。。。。");
        ruleFileMap.entrySet().stream().forEach(item -> log.info(item.getKey()));
        super.loadRule(ruleFileMap);

    }

    @Override
    public void testData(List<Command> commandList) {
        super.invokRule(commandList);
    }

    @Override
    public void updateRule() {
        Map<String, String> ruleFileUpdateMap = loadRuleContentFromDB(null);
        super.updateRule(ruleFileUpdateMap);
    }

}
