package com.tplhk.drool.core.service.impl;

import com.tplhk.drool.core.service.AmlRuleService;
import com.tplhk.drool.core.service.Chapter2RuleService;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.command.Command;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @ClassName : AmlRuleService
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/5/13 15:41
 **/
@Slf4j
@Service
public class Chapter2RuleServiceImpl extends RuleEngineCoreServiceImpl implements Chapter2RuleService {


    public Chapter2RuleServiceImpl() {
        this.PATHt = "chapter2";
        this.PATH = "src/main/resources/chapter2";
        Map<String, String> ruleFileMap = super.loadRuleContentFromFile(PATHt);
        log.error("chapter2。。。。。。。。。");
        ruleFileMap.entrySet().stream().forEach(item -> log.info(item.getKey()));
        super.loadRule(ruleFileMap);
    }

    @Override
    public void testData(List<Command> commandList) {
        super.invokRule(commandList);

    }

}
