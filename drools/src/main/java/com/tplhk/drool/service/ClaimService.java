package com.tplhk.drool.service;


import com.tplhk.drool.entity.RuleBase;

import java.util.List;

public interface ClaimService {

    RuleBase getBaseByField(String fieldName, Object value);

    RuleBase getBaseByField(String fieldName, Object ParamValue, Integer defaultScore);

    List<RuleBase> getBaseByField(String fieldName, List<Object> ListValue, Integer defaultScore);
}
