package com.tplhk.drool.service.impl;

import com.tplhk.drool.entity.QueryParam;
import com.tplhk.drool.service.RuleEngineService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

@Slf4j
@Service
public class RuleEngineServiceImpl implements RuleEngineService {


    @Override
    public void executeAddRule(QueryParam param) {
        log.info("参数数据:" + param.getParamId() + ";" + param.getParamSign());
        log.info("参数数据 写入数据库 ");
    }

    @Override
    public void executeRemoveRule(QueryParam param) {
        log.info("参数数据:" + param.getParamId() + ";" + param.getParamSign());
        log.info("参数数据 移出数据库 ");
    }
}
