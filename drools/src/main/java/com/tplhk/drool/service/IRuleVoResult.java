package com.tplhk.drool.service;


import com.tplhk.drool.entity.RuleBase;

public interface IRuleVoResult {
    /**
     * excle 中被调用
     *
     * @param className
     * @return
     */
    RuleBase getSubClass(String className);

//    /**
//     * 如果传入的对象参数无法匹配任何规则，那么， excel 中的 action 是不会通过  buildRuleResultVo 调用 setScore 方法的，
//     * 所以 value 无法保存原始值。
//     *
//     * 本方法是为了补充这个缺陷
//     * @param o
//     */
//    void fillValue(Object o);

}
