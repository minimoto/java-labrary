package com.tplhk.drool.service;


import com.tplhk.drool.entity.QueryParam;

public interface RuleEngineService {
    void executeAddRule(QueryParam param);

    void executeRemoveRule(QueryParam param);
}
