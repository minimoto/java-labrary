package com.tplhk.drool.controller;

import com.google.common.collect.Lists;
import com.tplhk.drool.entity.QueryParam;
import com.tplhk.drool.entity.RuleExcel;
import com.tplhk.drool.entity.RuleResult;
import com.tplhk.drool.util.FreemarkerUtil;
import com.tplhk.drool.util.KieHelpUtil;
import com.tplhk.drool.vo.ResponseData;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.definition.rule.Rule;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequestMapping(value = "/freemaker")
@RestController
public class FreemakerRuleController {

    @PostMapping(value = "/import")
    public ResponseData importRules() {
        List<RuleExcel> ruleExcelList = readRuleFromExcel();
        for (RuleExcel ruleExcel : ruleExcelList) {
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("rulename", "rulename-" + ruleExcel.getRuleId());
            dataModel.put("ruleno", ruleExcel.getRuleno());
            dataModel.put("condition", ruleExcel.toRuleString());
            dataModel.put("result", ruleExcel.getResult());
            try {
                String context = FreemarkerUtil.createFileByTemplate("ruel-excel.ftl", dataModel);

                KieFileSystem kfs = KieHelpUtil.kieHelper.kfs;
                kfs.delete("src/main/resources/" + "rulename-" + ruleExcel.getRuleId() + ".drl");
                log.info("Drools DRL was deleted sucessfully " + "rulename-" + ruleExcel.getRuleId() + ".drl");
                kfs.write("src/main/resources/" + "rulename-" + ruleExcel.getRuleId() + ".drl", context);
                log.info("Drools DRL was created sucessfully " + "rulename-" + ruleExcel.getRuleId() + ".drl");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        }
        ;
        return new ResponseData("导入成功");
    }

    @PostMapping(value = "/test")
    public ResponseData test() {
        KieSession kieSession = KieHelpUtil.kieHelper.build().newKieSession();

        RuleExcel excel = RuleExcel.builder().sex("男").age(2).driver(2).websiteId("10003").result("拒绝").build();
        kieSession.insert(excel);

        // 返参
        RuleResult resultParam = new RuleResult();
        kieSession.insert(resultParam);
        kieSession.fireAllRules();

        System.out.println(resultParam.toString());
        return new ResponseData(resultParam.toString());
    }

    private List<RuleExcel> readRuleFromExcel() {
        RuleExcel rule1 = RuleExcel.builder().ruleId(1).ruleno(999).sex("男").age(60).driver(20).websiteId("10001").result("拒绝").build();
        RuleExcel rule2 = RuleExcel.builder().ruleId(2).ruleno(998).sex("男").age(30).driver(15).websiteId("10002").result("转人工").build();
        RuleExcel rule3 = RuleExcel.builder().ruleId(3).ruleno(997).sex("男").age(20).driver(10).websiteId("10003").result("转人工").build();
        List<RuleExcel> ruleExcelList = Lists.newLinkedList();
        ruleExcelList.add(rule1);
        ruleExcelList.add(rule2);
        ruleExcelList.add(rule3);
        return ruleExcelList;
    }
}
