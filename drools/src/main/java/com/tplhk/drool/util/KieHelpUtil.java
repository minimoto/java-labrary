package com.tplhk.drool.util;

import org.kie.api.KieBase;
import org.kie.api.io.ResourceType;
import org.kie.internal.utils.KieHelper;

public class KieHelpUtil {
    public static KieHelper kieHelper;
    public static KieBase kieBase;

    static {
        kieHelper = new ExKieHelper();
        kieBase = kieHelper.getKieContainer().getKieBase();
    }

    public static void delete(String name, ResourceType type) {
        String path = ((ExKieHelper) kieHelper).getPath(name, type);
        kieHelper.kfs.delete(path);
    }

    public static void addRule(String content, String name) {
        kieHelper.addContent(content, name + "." + ResourceType.DRL);
    }

    public static void addRuleFlow(String content, String name) {
        kieHelper.addContent(content, name + "." + ResourceType.BPMN2.getDefaultExtension());
    }

    public static KieBase getKieBase() {
        return kieBase;
    }

    public static void setKieBase(KieBase base) {
        kieBase = base;
    }

    private static class ExKieHelper extends KieHelper {
        public ExKieHelper() {
            super();
        }

        public String getPath(String name, ResourceType type) {
            return "src/main/resources/" + name + "." + type.getDefaultExtension();
        }
    }
}
