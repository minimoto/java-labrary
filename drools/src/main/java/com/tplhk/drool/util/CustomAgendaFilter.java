package com.tplhk.drool.util;

import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;

import java.util.Set;

public class CustomAgendaFilter implements AgendaFilter {
    private final Set<String> ruleNames;

    public CustomAgendaFilter(Set<String> ruleNames) {
        this.ruleNames = ruleNames;
    }

    @Override
    public boolean accept(Match match) {
        return ruleNames.contains(match.getRule().getName());
    }
}
