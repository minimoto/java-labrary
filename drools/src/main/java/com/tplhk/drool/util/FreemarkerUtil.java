package com.tplhk.drool.util;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class FreemarkerUtil {


    public static void createFileByTemplate(String templateFile, Map<String, Object> dataModel, String outputFile) throws IOException, TemplateException {
        //创建配置对象
        Configuration configuration = new Configuration(Configuration.getVersion());
        //设置默认生成文件编码
        configuration.setDefaultEncoding("utf-8");
        //设置模板路径
        configuration.setClassForTemplateLoading(FreemarkerUtil.class, "/templates");
        //获取模板
        Template template = configuration.getTemplate(templateFile);

//        创建输出对象,将文件输出到D盘根目录下
        FileWriter fileWriter = new FileWriter(outputFile);

        //渲染模板和数据
        template.process(dataModel, fileWriter);

        //关闭输出
        fileWriter.close();
    }

    public static String createFileByTemplate(String templateFile, Map<String, Object> dataModel) throws IOException, TemplateException {
        //创建配置对象
        Configuration configuration = new Configuration(Configuration.getVersion());
        //设置默认生成文件编码
        configuration.setDefaultEncoding("utf-8");
        //设置模板路径
        configuration.setClassForTemplateLoading(FreemarkerUtil.class, "/templates");
        //获取模板
        Template template = configuration.getTemplate(templateFile);

        // 直接返回字符串
        return FreeMarkerTemplateUtils.processTemplateIntoString(template, dataModel);


    }
}
