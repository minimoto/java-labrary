import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import com.google.common.collect.Maps;
import com.tplhk.drool.core.service.AmlRuleService;
import com.tplhk.drool.core.service.Chapter2RuleService;
import com.tplhk.drool.entity.*;
import com.tplhk.drool.service.IRuleVoResult;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.kie.api.command.Command;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.command.CommandFactory;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.PipedReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * 模拟读取数据库规则，动态写入 kfs，可以更新规则文件
 * 支持多个规则模型
 */
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestRuleEngineCoreSerivce {

    @Autowired
    private AmlRuleService amlRuleService;

    @Autowired
    private Chapter2RuleService chapter2RuleService;

    @Test
    public void testRuleService() {
        RuleExcel excelParam = RuleExcel.builder().sex("男").age(2).driver(2).websiteId("10003").build();
        List<String> result = new LinkedList<>();
        List<Command> cmds1 = new ArrayList<>();
        cmds1.add(CommandFactory.newInsert(excelParam));
        cmds1.add(CommandFactory.newInsert(result));
        amlRuleService.testData(cmds1);
        System.out.println("结果：：：：" + result.toString());
        amlRuleService.updateRule();
        amlRuleService.testData(cmds1);
        System.out.println("结果：：：：" + result.toString());

        Map<String, String> orderMap = Maps.newHashMap();
        orderMap.put("1", "10");
        orderMap.put("2", "20");
        orderMap.put("3", "30");
        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("abc001")
                .age(30)
                .sex("男")
                .orderList(Lists.list("1", "2", "3", "4"))
                .orderSet(Sets.newSet("1", "2", "3", "4"))
                .orderMap(orderMap)
                .build();
        List<Command> cmds2 = new ArrayList<>();
        cmds2.add(CommandFactory.newInsert(customerEntity));


        chapter2RuleService.testData(cmds2);


    }


    private final String rule3 = "package droolrule;\n" +
            "import com.tplhk.drool.entity.RuleExcel ;\n" +
            "import com.tplhk.drool.entity.RuleResult;\n" +
            "import java.util.*;\n" +
            "import org.slf4j.Logger\n" +
            "import org.slf4j.LoggerFactory ;\n" +
            "\n" +
            "rule \"rulename-3\"\n" +
            "    salience 995\n" +
            "    when ruleExcel : RuleExcel(sex.equalsIgnoreCase(\"男\") && age<=20 && driver<=10 && websiteId.equalsIgnoreCase(\"10003\"))\n" +
            "       resultParam : List();\n" +
            "    then\n" +
            "        final Logger LOGGER = LoggerFactory.getLogger(\"rulename-333 规则引擎\") ;\n" +
            "        resultParam.add(\"转人工333\");\n" +
            "        LOGGER.info(\"resultParam = 转人工333\");\n" +
            "end";

    private final String rule4 = "package droolrule;\n" +
            "import com.tplhk.drool.entity.RuleExcel ;\n" +
            "import com.tplhk.drool.entity.RuleResult;\n" +
            "import java.util.*;\n" +
            "import org.slf4j.Logger\n" +
            "import org.slf4j.LoggerFactory ;\n" +
            "\n" +
            "rule \"rulename-4\"\n" +
            "    salience 996\n" +
            "    when ruleExcel : RuleExcel(sex.equalsIgnoreCase(\"男\") && age<=20 && driver<=10 && websiteId.equalsIgnoreCase(\"10003\"))\n" +
            "       resultParam : List();\n" +
            "    then\n" +
            "        final Logger LOGGER = LoggerFactory.getLogger(\"rulename-4 规则引擎\") ;\n" +
            "        resultParam.add(\"转人工4\");\n" +
            "        LOGGER.info(\"resultParam = 转人工4\");\n" +
            "end";
}

