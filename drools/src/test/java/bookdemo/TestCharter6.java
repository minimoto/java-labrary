package bookdemo;

import com.tplhk.drool.entity.CustomerEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter6 {

    @Test
    public void test1() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter6");
        CustomerEntity customerEntity = CustomerEntity.builder()
                .age(20)
                .build();
        kieSession.setGlobal("count", 1);
        kieSession.setGlobal("customer", customerEntity);
        kieSession.setGlobal("list", new LinkedList<String>());

        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        log.info("包装类并没有改变值：count = " + kieSession.getGlobal("count"));
        log.info("javabean age 值被修改：customer.age = " + kieSession.getGlobal("customer").toString());
        log.info("list 值被修改：list.size = " + ((List) kieSession.getGlobal("list")).size());

        kieSession.dispose();
    }


}
