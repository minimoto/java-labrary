package bookdemo;

import com.google.common.collect.Maps;
import com.tplhk.drool.entity.CustomerEntity;
import com.tplhk.drool.entity.OrderEntity;
import com.tplhk.drool.entity.PersonInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;
import org.mockito.internal.util.collections.Sets;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter2 {

    @Test
    public void test1() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("001")
                .age(30)
                .sex("男")
                .build();
        OrderEntity orderEntity = OrderEntity.builder()
                .customerId("001")
                .price(2000)
                .build();
        kieSession.insert(customerEntity);
        kieSession.insert(orderEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test2() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("001")
                .age(40)
                .sex("男")
                .build();

        kieSession.insert(customerEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test4() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("001")
                .age(30)
                .sex("男")
                .build();
        OrderEntity orderEntity = OrderEntity.builder()
                .customerId("001")
                .price(2000)
                .build();
        kieSession.insert(customerEntity);
        kieSession.insert(orderEntity);

        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test5() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("001")
                .age(30)
                .sex("男")
                .orderList(Lists.list("1", "2", "3", "4"))
                .build();
        OrderEntity orderEntity = OrderEntity.builder()
                .id("1")
                .customerId("001")
                .price(2000)
                .build();
        kieSession.insert(customerEntity);
        kieSession.insert(orderEntity);

        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test6() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("abc001")
                .age(30)
                .sex("男")
                .orderList(Lists.list("1", "2", "3", "4"))
                .build();

        kieSession.insert(customerEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }


    @Test
    public void test7() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter2");

        Map<String, String> orderMap = Maps.newHashMap();
        orderMap.put("1", "10");
        orderMap.put("2", "20");
        orderMap.put("3", "30");
        CustomerEntity customerEntity = CustomerEntity.builder()
                .customerId("abc001")
                .age(30)
                .sex("男")
                .orderList(Lists.list("1", "2", "3", "4"))
                .orderSet(Sets.newSet("1", "2", "3", "4"))
                .orderMap(orderMap)
                .build();

        FactHandle insert = kieSession.insert(customerEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
//        需要WorkingMemory调用retract方法将其从内存中清除掉，
//        不然，即使走完了规则变成了垃圾对象，也无法被垃圾回收器回收
        kieSession.delete(insert);

    }

}
