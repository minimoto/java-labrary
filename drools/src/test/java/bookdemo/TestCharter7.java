package bookdemo;

import com.tplhk.drool.entity.CustomerEntity;
import com.tplhk.drool.util.CustomAgendaFilter;
import lombok.extern.slf4j.Slf4j;
import org.drools.core.base.RuleNameEndsWithAgendaFilter;
import org.drools.core.base.RuleNameEqualsAgendaFilter;
import org.drools.core.base.RuleNameMatchesAgendaFilter;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.mockito.internal.util.collections.Sets;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter7 {

    @Test
    public void test1() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter7");

        // 执行所有规则
//        int count = kieSession.fireAllRules();
        // 精确匹配
//        int count = kieSession.fireAllRules(new RuleNameEqualsAgendaFilter("指定规则名称1"));
        // 匹配结尾的字符串
//        int count = kieSession.fireAllRules(new RuleNameEndsWithAgendaFilter("1"));
        // 匹配开始的字符串
//        int count = kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("指"));
        // 正规表达式
//        int count = kieSession.fireAllRules(new RuleNameMatchesAgendaFilter("\\w{0,10}"));
        // 自定义
        Set set = Sets.newSet("指定规则名称1", "abc");
        int count = kieSession.fireAllRules(new CustomAgendaFilter(set));

        log.info("共执行了 {} 条规则", count);

        kieSession.dispose();
    }


}
