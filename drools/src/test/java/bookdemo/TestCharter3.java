package bookdemo;

import com.google.common.collect.Maps;
import com.tplhk.drool.entity.CustomerEntity;
import com.tplhk.drool.entity.OrderEntity;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.mockito.internal.util.collections.Sets;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter3 {

    @Test
    public void test1() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("chapter3");
        CustomerEntity customerEntity = CustomerEntity.builder()
                .age(20)
                .sex("男")
                .build();

        kieSession.insert(customerEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }


}
