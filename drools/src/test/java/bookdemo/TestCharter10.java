package bookdemo;

import com.tplhk.drool.entity.PersonInfoEntity;
import com.tplhk.drool.entity.RuleVo2Result;
import com.tplhk.drool.service.IRuleVoResult;
import com.tplhk.drool.util.CustomAgendaFilter;
import lombok.extern.slf4j.Slf4j;
import org.drools.decisiontable.InputType;
import org.drools.decisiontable.SpreadsheetCompiler;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Set;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter10 {

    /**
     * 生成规则文件
     *
     * @throws FileNotFoundException
     */
    @Test
    public void test1() throws FileNotFoundException {
        File file = new File("D:\\workspaces\\tplhk-cloud\\learning\\doc\\testRule3.xls");
        InputStream is = new FileInputStream(file);
        SpreadsheetCompiler compiler = new SpreadsheetCompiler();
        String drl = compiler.compile(is, InputType.XLS);
        System.out.println(drl);
    }

    /**
     * 占位符使用
     */
    @Test
    public void test2() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("xlstest1");
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
    }

    /**
     * xls 合并单位格
     */
    @Test
    public void test3() throws FileNotFoundException {
        File file = new File("D:\\workspaces\\tplhk-cloud\\learning\\drools\\src\\main\\resources\\chapter10\\test2\\chapter10-test2.xls");
        InputStream is = new FileInputStream(file);
        SpreadsheetCompiler compiler = new SpreadsheetCompiler();
        String drl = compiler.compile(is, InputType.XLS);
        System.out.println(drl);

    }

}
