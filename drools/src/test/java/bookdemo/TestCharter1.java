package bookdemo;

import com.tplhk.drool.entity.PersonInfoEntity;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.boot.test.context.SpringBootTest;


@Slf4j
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestCharter1 {

    @Test
    public void test1() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("testhelloworld");
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test2() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("testhelloworld");

        PersonInfoEntity personInfoEntity = new PersonInfoEntity();
        kieSession.insert(personInfoEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

    @Test
    public void test3() {
        KieServices kieServices = KieServices.Factory.get();
        KieContainer kc = kieServices.getKieClasspathContainer();
        KieSession kieSession = kc.newKieSession("testhelloworld");

        PersonInfoEntity personInfoEntity = new PersonInfoEntity();
        personInfoEntity.setSex("男");
        personInfoEntity.setAge(18);
        kieSession.insert(personInfoEntity);
        int count = kieSession.fireAllRules();
        log.info("共执行了 {} 条规则", count);
        kieSession.dispose();
    }

}
