import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.tplhk.drool.entity.*;
import com.tplhk.drool.util.FreemarkerUtil;
import com.tplhk.drool.util.KieHelpUtil;
import freemarker.template.TemplateException;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.drools.core.io.impl.ReaderResource;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderError;
import org.kie.internal.builder.KnowledgeBuilderErrors;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.runtime.StatefulKnowledgeSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.sun.javafx.fxml.expression.Expression.add;

/**
 * 适用于直接加载 /resouces/droolRule/ 目录下的规则文件.  需要配置类 RuleEngineConfig
 * 测试用例：TestDrool
 */
@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestDrool {

    @Autowired
    private KieSession kieSession;

    @Test
    public void simpleExample() {
        QueryParam queryParam1 = new QueryParam();
        queryParam1.setParamId("1");
        queryParam1.setParamSign("+");

        QueryParam queryParam2 = new QueryParam();
        queryParam2.setParamId("2");
        queryParam2.setParamSign("-");

        // 入参
        kieSession.insert(queryParam1);
        kieSession.insert(queryParam2);

        // 返参
        RuleResult resultParam = new RuleResult();
        kieSession.insert(resultParam);
        kieSession.fireAllRules();

        System.out.println(resultParam.toString());
    }


}
