import com.google.common.collect.Lists;
import com.tplhk.drool.entity.QueryParam;
import com.tplhk.drool.entity.RuleExcel;
import com.tplhk.drool.entity.RuleResult;
import com.tplhk.drool.util.FreemarkerUtil;
import com.tplhk.drool.util.KieHelpUtil;
import freemarker.template.TemplateException;
import org.drools.core.impl.InternalKnowledgeBase;
import org.drools.core.impl.KnowledgeBaseFactory;
import org.junit.jupiter.api.Test;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.kie.internal.utils.KieHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 1. 模型从数据库读取数据，再通过 freemarker 生成规则文件
 * 2. 直接加载 数据库 规则，写入 kfs.  不需要配置类 RuleEngineConfig
 * 测试用例：TestDrool2
 */
//@SpringBootTest(classes = com.tplhk.drool.DroolApplication.class)
public class TestDrool2 {

    /**
     * 创建 rule 文件
     */
    @Test
    public void createFileByTemplate() {
        List<RuleExcel> ruleExcelList = readRuleFromExcel();
        ruleExcelList.stream().forEach(ruleExcel -> {
            Map<String, Object> dataModel = new HashMap<>();
            dataModel.put("rulename", "rulename-" + ruleExcel.getRuleId());
            dataModel.put("ruleno", ruleExcel.getRuleno());
            dataModel.put("condition", ruleExcel.toRuleString());
            dataModel.put("result", ruleExcel.getResult());
            try {
                FreemarkerUtil.createFileByTemplate("ruel-excel.ftl", dataModel, "src/main/resources/ruleFreemarker/" + "rulename-" + ruleExcel.getRuleId() + ".drl");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (TemplateException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 模拟从数据库加载规则，并测试
     */
    @Test
    public void simpleExample() {
        KieHelpUtil.kieHelper.addContent(rule1, ResourceType.DRL);
        KieHelpUtil.kieHelper.addContent(rule2, ResourceType.DRL);
        KieHelpUtil.kieHelper.addContent(rule3, ResourceType.DRL);

        KieSession kieSession = KieHelpUtil.kieHelper.build().newKieSession();

        RuleExcel excel = RuleExcel.builder().sex("男").age(2).driver(2).websiteId("10003").build();
        kieSession.insert(excel);

        // 返参
        RuleResult resultParam = new RuleResult();
        kieSession.insert(resultParam);
        kieSession.fireAllRules();

        System.out.println(resultParam.toString());
    }

    private List<RuleExcel> readRuleFromExcel() {
        RuleExcel rule1 = RuleExcel.builder().ruleId(1).ruleno(999).sex("男").age(60).driver(20).websiteId("10001").result("拒绝").build();
        RuleExcel rule2 = RuleExcel.builder().ruleId(2).ruleno(998).sex("男").age(30).driver(15).websiteId("10003").result("转人工").build();
        RuleExcel rule3 = RuleExcel.builder().ruleId(3).ruleno(997).sex("男").age(20).driver(10).websiteId("10003").result("转人工").build();
        List<RuleExcel> ruleExcelList = Lists.newLinkedList();
        ruleExcelList.add(rule1);
        ruleExcelList.add(rule2);
        ruleExcelList.add(rule3);
        return ruleExcelList;
    }


    private final String rule1 = "package droolrule;\n" +
            "import com.tplhk.drool.entity.RuleExcel ;\n" +
            "import com.tplhk.drool.entity.RuleResult;\n" +
            "import org.slf4j.Logger\n" +
            "import org.slf4j.LoggerFactory ;\n" +
            "\n" +
            "rule \"rulename-1\"\n" +
            "    salience 999\n" +
            "    when ruleExcel : RuleExcel(sex.equalsIgnoreCase(\"男\") && age<=60 && driver<=20 && websiteId.equalsIgnoreCase(\"10001\"))\n" +
            "       resultParam : RuleResult()\n" +
            "    then\n" +
            "        final Logger LOGGER = LoggerFactory.getLogger(\"rulename-1 规则引擎\") ;\n" +
            "        resultParam.setResultCode(\"拒绝\");\n" +
            "        LOGGER.info(\"resultParam = 拒绝\");\n" +
            "end";

    private final String rule2 = "package droolrule;\n" +
            "import com.tplhk.drool.entity.RuleExcel ;\n" +
            "import com.tplhk.drool.entity.RuleResult;\n" +
            "import org.slf4j.Logger\n" +
            "import org.slf4j.LoggerFactory ;\n" +
            "\n" +
            "rule \"rulename-2\"\n" +
            "    salience 998\n" +
            "    when ruleExcel : RuleExcel(sex.equalsIgnoreCase(\"男\") && age<=30 && driver<=15 && websiteId.equalsIgnoreCase(\"10002\"))\n" +
            "       resultParam : RuleResult()\n" +
            "    then\n" +
            "        final Logger LOGGER = LoggerFactory.getLogger(\"rulename-2 规则引擎\") ;\n" +
            "        resultParam.setResultCode(\"转人工\");\n" +
            "        LOGGER.info(\"resultParam = 转人工\");\n" +
            "end";

    private final String rule3 = "package droolrule;\n" +
            "import com.tplhk.drool.entity.RuleExcel ;\n" +
            "import com.tplhk.drool.entity.RuleResult;\n" +
            "import org.slf4j.Logger\n" +
            "import org.slf4j.LoggerFactory ;\n" +
            "\n" +
            "rule \"rulename-3\"\n" +
            "    salience 997\n" +
            "    when ruleExcel : RuleExcel(sex.equalsIgnoreCase(\"男\") && age<=20 && driver<=10 && websiteId.equalsIgnoreCase(\"10003\"))\n" +
            "       resultParam : RuleResult()\n" +
            "    then\n" +
            "        final Logger LOGGER = LoggerFactory.getLogger(\"rulename-3 规则引擎\") ;\n" +
            "        resultParam.setResultCode(\"转人工\");\n" +
            "        LOGGER.info(\"resultParam = 转人工\");\n" +
            "end";
}
