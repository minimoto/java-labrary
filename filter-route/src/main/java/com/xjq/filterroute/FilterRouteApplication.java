package com.xjq.filterroute;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FilterRouteApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilterRouteApplication.class, args);
    }

}
