package com.xjq.filterroute.config;


import com.xjq.common.context.servlet.ServletContextHolder;
import com.xjq.common.exception.ServerError;
import com.xjq.starter.config.BaseRouteFilterConfiguration;
import com.xjq.starter.filter.RouteServletFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2024/06/30
 * @Describe
 */
@Slf4j
@Configuration
public class BussnessRouteFilterConfigure extends BaseRouteFilterConfiguration {

	/**
     * 注册 [Sa-Token 全局过滤器] 
     */
    @Bean
    public RouteServletFilter servletFilterConfig() {
		RouteServletFilter filter = new RouteServletFilter();
		super.servletFilterConfig(filter);

        return filter
        		// 指定 [拦截路由] 与 [放行路由]
        		.addInclude("/**")
//				.addExclude("/test/postInfo")


        		;
    }

}
