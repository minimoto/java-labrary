package com.xjq.filterroute.config;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.setting.SettingUtil;
import com.google.common.collect.Sets;
import com.xjq.common.context.servlet.ServletContextHolder;
import com.xjq.starter.filter.CheckPathInvalidCharFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2024/06/30
 * @Describe
 */
@Slf4j
@Configuration
public class PathCheckFilterConfigure  {


    @Bean
    public CheckPathInvalidCharFilter pathCheckFilter() {
        CheckPathInvalidCharFilter filter = new CheckPathInvalidCharFilter();
		filter.setInvalidCharacter(Sets.newHashSet("="));
        return filter;
    }

}
