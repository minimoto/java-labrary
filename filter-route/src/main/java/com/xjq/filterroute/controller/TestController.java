package com.xjq.filterroute.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;


/**
 * @author: xiao_qiang_01@163.com
 * @Date: 2024/06/30
 * @Describe
 */
@Slf4j
@RestController
@RequestMapping("/test")
public class TestController {


    @GetMapping("/getInfo")
    public String getInfo() {
        log.info("这是 get 请求");
        return "这是 get 请求";
    }

    @PostMapping("/postInfo")
    public String postInfo() {
        log.info("这是 post 请求");
        return "这是 post 请求";
    }

    @DeleteMapping("/deleteInfo")
    public String deleteInfo() {
        log.info("这是 delete 请求");
        return "这是 delete 请求";
    }


}
