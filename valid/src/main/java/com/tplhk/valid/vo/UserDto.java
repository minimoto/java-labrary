package com.tplhk.valid.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * @ClassName : UserDto
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/11/6 15:25
 **/
@Data
public class UserDto {

    @NotBlank(message = "用户名不能为空")
    private String name;

    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String mobileNo;

    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式有误")
    private String email;

    @NotNull(message = "性别不能为空")
    private Integer sex;
}
