package com.tplhk.valid.vo;

import com.tplhk.valid.interfaces.CreateValid;
import com.tplhk.valid.interfaces.UpdateValid;
import com.tplhk.valid.validator.StrLength;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @ClassName : UserDto
 * @Description : TODO
 * @Author : taiping
 * @Date: 2020/11/6 15:25
 **/
@Data
public class UserValidatorDto {

    @StrLength(min = 1, max = 4, message = "用户名长度不正确")
    private String name;

    @NotBlank(message = "手机号不能为空")
    @Pattern(regexp = "^[1][3,4,5,6,7,8,9][0-9]{9}$", message = "手机号格式有误")
    private String mobileNo;

    @NotBlank(message = "邮箱不能为空")
    @Email(message = "邮箱格式有误")
    private String email;

    @NotNull(message = "性别不能为空")
    private Integer sex;
}
