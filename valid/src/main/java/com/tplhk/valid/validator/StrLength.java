package com.tplhk.valid.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 字符串长度校验扩展（为空不做校验长度，不为空校验长度）
 *
 * @author wangzhijun
 * @date 2020/6/30 15:04
 **/
@Target({ElementType.FIELD, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = StrLengthValidator.class)
public @interface StrLength {

    int min() default 0;

    int max();

    String message() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
