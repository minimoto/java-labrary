package com.tplhk.valid.config;


import com.tplhk.valid.vo.ResponseData;
import lombok.extern.slf4j.Slf4j;
import org.omg.CORBA.UserException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 统一的异常处理
 *
 * @author wangzhijun
 **/
@RestControllerAdvice
@Slf4j
public class WebExceptionHandler {

    public static final String STR_PRE = "{";

    public static final String STR_SUFFIX = "}";

    /**
     * BindException 校验异常统一处理
     *
     * @param ex
     * @return Result
     * @author wangzhijun
     * @date 14:49 2020/4/20
     **/
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseData handlerMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult re = ex.getBindingResult();
        //获取错误的列
        String resultErrorField = re.getFieldErrors().stream().map(FieldError::getField).collect(Collectors.joining(","));
        String resultErrorMsg = re.getAllErrors().stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining(","));

        return new ResponseData(ResponseData.VALID_SERVER_ERROR, resultErrorMsg, resultErrorField);
    }


    /**
     * 自定义 服务异常
     * @author wangzhijun
     * @date 14:08 2020/5/6
     * @param ex
     * @return com.cntaiping.tplhk.common.core.dto.Result
     **/
//    public Result handServiceException(ServiceException ex){
//        String code = ex.getMessage();
//        return Result.failed(ex.getCode(),messages.getMessage(code));
//    }


    /**
     * 自定义校验框架 ，校验异常
     * @author wangzhijun
     * @date 16:09 2020/5/15
     * @param ex
     * @return com.cntaiping.tplhk.common.core.dto.Result
     **/
//    public Result handValidException(ValidException ex){
//        String code = ex.getMessage();
//        return Result.failed(ex.getCode(),messages.getMessage(code));
//    }


    /**
     * 处理类型转换异常
     * @author wangzhijun
     * @date 15:29 2020/6/18
     * @param ex
     * @return Result
     **/
//    public Result handTypeMissException(TypeMismatchException ex){
//        String code = ex.getPropertyName();
//        String message = messages.getMessage(ResponseStatusEnum.type_miss_match_error.getMessage());
//        return Result.failed(Result.INTERNAL_SERVER_ERROR,code+message);
//    }


    /**
     * 请求参数格式不正确处理
     * @param Exception
     * @return Result
     */
//    public Result requestInvalid(HttpMessageNotReadableException Exception) {
//        Integer code=ResponseStatusEnum.param_format_error.getCode();
//        String message = messages.getMessage(ResponseStatusEnum.param_format_error.getMessage());
//        return Result.failed(code,message);
//    }


    /**
     * 统一异常处理
     *
     * @return com.cntaiping.tplhk.common.core.dto.Result
     * @author wangzhijun
     * @date 14:04 2020/5/6
     **/
    @ExceptionHandler(value = Exception.class)
    public ResponseData exception(Exception ex) {
        return new ResponseData(ResponseData.INTERNAL_SERVER_ERROR, "系统异常");
    }


}
