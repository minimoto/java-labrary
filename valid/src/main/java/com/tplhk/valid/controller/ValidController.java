package com.tplhk.valid.controller;


import com.tplhk.valid.interfaces.CreateValid;
import com.tplhk.valid.interfaces.UpdateValid;
import com.tplhk.valid.validator.StrLength;
import com.tplhk.valid.vo.ResponseData;
import com.tplhk.valid.vo.UserDto;
import com.tplhk.valid.vo.UserGroupDto;
import com.tplhk.valid.vo.UserValidatorDto;
import lombok.extern.slf4j.Slf4j;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;


@Slf4j
@RequestMapping(value = "/valid")
@RestController
public class ValidController {


    /**
     * 使用 validator 框架自带的注解验证
     * <p>
     * http://localhost:8080/valid/user
     * <p>
     * {
     * "name":"a",
     * "mobileNo":"13123456789"
     * }
     *
     * @param userDto
     * @return
     */
    @GetMapping("/byValidator")
    public ResponseData userByValidator(@RequestBody @Validated UserDto userDto) {
        return new ResponseData();
    }


    /**
     * 分组验证。由于 UserGroupDto 使用了 group，所以，使用 @Validated 也必须使用 group ，结果是不会对任何属性校验
     *
     * @param userGroupDto
     * @return
     */
    @PostMapping
    public ResponseData createByValidatorGroup(@RequestBody @Validated({CreateValid.class, UpdateValid.class}) UserGroupDto userGroupDto) {
        return new ResponseData();
    }

    /**
     * 单个字段校验
     *
     * @param userId
     * @return
     */
    @GetMapping("/validByParam")
    public ResponseData validByParam(@NotNull @RequestParam(value = "userId") String userId) {
        return new ResponseData();
    }

    /**
     * 自定义校验
     *
     * @param userValidatorDto
     * @return
     */
    @PostMapping("/validCustomer")
    public ResponseData validByParam1(@Validated @RequestBody UserValidatorDto userValidatorDto) {
        return new ResponseData();
    }

}
