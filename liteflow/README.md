####编写一个复杂的例子

![liteflow-complex.png](./doc/liteflow-complex.png)

```xml
<chain name="chain1">
    item1 = THEN(D, E, F).id("t1");
    
    item2_1 = THEN(
        SWITCH(G).to(
            THEN(H, I).id("t2"),
            J
        ),
        K
    );
    
    item2_2 = THEN(L, M);
    
    item2 = THEN(C, WHEN(item2_1, item2_2)).id("t3");
    
    THEN(
        A,
        SWITCH(B).to(item1, item2),
        Z
    );
</chain>


```

