package com.xjq.liteflow;

import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
class ThenTests {

	@Resource
	private FlowExecutor flowExecutor;

	/**
	 * 串行编排
	 * 实时刷新规则
	 * */
	@Test
	public void testThen() throws InterruptedException {
//		执行类中每2秒运行一次，循环是为了方便你测试热刷新。你直接在Nacos里改规则即可。会实时刷新规则
		log.info("    <chain name=\"then\">\n" +
				"        THEN(node3sCmp,node6sCmp,node8sCmp);\n" +
				"    </chain>");
		while (true) {
			LiteflowResponse response = flowExecutor.execute2Resp("then");
			if (response.isSuccess()){
				log.info("执行成功");
			}else{
				log.info("执行失败");
			}
			Thread.sleep(2000L);
		}
	}




}
