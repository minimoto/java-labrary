package com.xjq.liteflow;

import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
class IfTests {

	@Resource
	private FlowExecutor flowExecutor;

	/**
	 * 条件编排(2元运算符, 最简单的例子 )
	 * */
	@Test
	public void testIf() throws InterruptedException {
		log.info("<chain name=\"if\">\n" +
				"        THEN(\n" +
				"        IF(nodeBooleanTrueCmp, nodeNoTime1Cmp),\n" +
				"        nodeNoTime2Cmp\n" +
				"        );\n" +
				"    </chain>");
		LiteflowResponse response = flowExecutor.execute2Resp("if");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}


	/**
	 * 条件编排(3元运算符 )
	 * */
	@Test
	public void testIf2() throws InterruptedException {
		log.info("<chain name=\"if2\">\n" +
				"        IF(nodeBooleanFalseCmp, nodeNoTime1Cmp, nodeNoTime2Cmp);\n" +
				"    </chain>");
		LiteflowResponse response = flowExecutor.execute2Resp("if2");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}


	/**
	 * 条件编排(if-else )
	 * */
	@Test
	public void testIfelse() throws InterruptedException {
		log.info("<chain name=\"ifelse\">\n" +
				"        IF(nodeBooleanFalseCmp, nodeNoTime1Cmp).ELIF(nodeBooleanTrueCmp, nodeNoTime2Cmp).ELSE(THEN(node3sCmp, node6sCmp));\n" +
				"    </chain>");
		LiteflowResponse response = flowExecutor.execute2Resp("ifelse");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}


}
