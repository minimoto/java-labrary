package com.xjq.liteflow;

import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
class WhenTests {

	@Resource
	private FlowExecutor flowExecutor;


	/**
	 * 串行编排
	 * 实时刷新规则
	 * */
	@Test
	public void testWhen() throws InterruptedException {
		LiteflowResponse response;
//		log.info("when ==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(node6sCmp, node8sCmp),\n" +
//				"        node10sCmp\n" +
//				"    );");
//		response = flowExecutor.execute2Resp("when");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//
//		log.info("when2 ==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(node6sCmp, THEN(node8sCmp, node10sCmp)),\n" +
//				"        NodeNoTime1Cmp\n" +
//				"    );");
//
//		response = flowExecutor.execute2Resp("when2");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//
//		log.info("whenError ：异常会退出，后续节点 node8sCmp 不执行。==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(node6sCmp, NodeErrorCmp),\n" +
//				"        node8sCmp\n" +
//				"    );");
//
//		response = flowExecutor.execute2Resp("whenError");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//
//
//		log.info("whenIgnoreError : 异常会忽略，后续节点 node8sCmp 也会执行。 ==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(node6sCmp, NodeErrorCmp).ignoreError(true),\n" +
//				"        node8sCmp\n" +
//				"    );");
//
//		response = flowExecutor.execute2Resp("whenIgnoreError");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//
//		log.info("whenAny ：任一节点先执行完，则继续执行下一个节点 （node6sCmp）。==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(NodeLongTimeCmp, THEN(NodeNoTime1Cmp, node10sCmp)).any(true),\n" +
//				"        node6sCmp\n" +
//				"    );");
//
//		response = flowExecutor.execute2Resp("whenAny");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//		Thread.sleep(30* 1000L);
//
//
//		log.info("whenMust : 指定节点 (node6sCmp, NodeNoTime1Cmp) 执行完，继续执行下一个节点 （NodeNoTime2Cmp）。==================================");
//		log.info("THEN(\n" +
//				"        node3sCmp,\n" +
//				"        WHEN(node6sCmp, node8sCmp, NodeNoTime1Cmp).must(node6sCmp, NodeNoTime1Cmp),\n" +
//				"        NodeNoTime2Cmp\n" +
//				"    );");
//
//		response = flowExecutor.execute2Resp("whenMust");
//		if (response.isSuccess()){
//			log.info("执行成功");
//		}else{
//			log.info("执行失败");
//		}
//		Thread.sleep(10* 1000L);


		log.info("whenMust2 ：指定节点 (node6sCmp, t1) 执行完，继续执行下一个节点 （node10sCmp）。==================================");
		log.info("id 的作用相当于给 then 节点起个别名，方便后续引用。");
		log.info("THEN(\n" +
				"        node3sCmp,\n" +
				"        WHEN(node6sCmp, THEN(NodeNoTime1Cmp, NodeNoTime2Cmp).id(\"t1\"), NodeLongTimeCmp).must(node6sCmp, \"t1\"),\n" +
				"        node10sCmp\n" +
				"    );");

		response = flowExecutor.execute2Resp("whenMust2");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}
		Thread.sleep(30* 1000L);


	}


}
