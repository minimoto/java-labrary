package com.xjq.liteflow;

import com.xjq.liteflow.vo.ComplexVo;
import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
class ForTests {

	@Resource
	private FlowExecutor flowExecutor;

	/**
	 * FOR循环：固定次数的循环
	 * 如果需要异步执行：
	 * <chain name="chain1">
	 *    FOR(2).parallel(true).DO(THEN(a,b,c));
	 * </chain>
	 * 配置异步线程池参数：
	 * <!--配置默认线程池的worker数目-->
	 * liteflow.parallel-max-workers=16
	 * <!--配置默认线程池的队列长度-->
	 * liteflow.parallel-queue-limit=512
	 * */
	@Test
	public void testFor() throws InterruptedException {
		LiteflowResponse response;
		log.info("在 el 表达式中指定 FOR 循环次数");
		log.info("<chain name=\"for3\">\n" +
				"        FOR(3).DO(THEN(nodeNoTimeForCmp));\n" +
				"    </chain>");

		response = flowExecutor.execute2Resp("for3");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

		log.info("===========================================================");
		log.info("在组件中指定 FOR 循环次数");
		log.info("<chain name=\"forByNodeForCmp\">\n" +
				"        FOR(nodeForCmp).DO(nodeNoTimeForCmp);\n" +
				"    </chain>");

		response = flowExecutor.execute2Resp("forByNodeForCmp");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}

	/**
	 * While 循环：通过上下文修改循环条件
	 * 如果需要异步执行：
	 * <chain name="chain6">
	 *    WHILE(x).parallel(true).DO(THEN(a,b,c));
	 * </chain>
	 * */
	@Test
	public void testWhile() {
		LiteflowResponse response;
		log.info("<chain name=\"whileByNodeWhileCmp\">\n" +
				"        WHILE(nodeWhileCmp).DO(THEN(nodeNoTimeWhileCmp));\n" +
				"    </chain>");

		ComplexVo complexVo = new ComplexVo();
		complexVo.setWhileFlag(true);
		response = flowExecutor.execute2Resp("whileByNodeWhileCmp", null, complexVo);
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}


	/**
	 * Itertor循环：遍历对象
	 * 如果需要异步执行：
	 * <chain name="chain7">
	 *    ITERATOR(x).parallel(true).DO(THEN(a,b,c));
	 * </chain>
	 * */
	@Test
	public void testItertor() {
		LiteflowResponse response;
		log.info("<chain name=\"iterator\">\n" +
				"        ITERATOR(nodeIteratorCmp).DO(nodeNoTimeItertorCmp);\n" +
				"    </chain>");

		response = flowExecutor.execute2Resp("iterator");
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}

	}

	/**
	 * break 语句：它是一个布尔组件，跟在FOR和WHILE后面, 每次循环的末尾进行判断
	 * 在异步执行时，break 表达式为 false 时，会停止向线程池提交新的任务，但是已经提交的任务会继续执行
	 * */
	@Test
	public void testBreak() {
		LiteflowResponse response;
		log.info("    <chain name=\"break\">\n" +
				"        THEN(\n" +
				"            FOR(3).DO(THEN(nodeNoTimeForCmp)).BREAK(nodeBooleanTrueCmp),\n" +
				"            WHILE(nodeWhileCmp).DO(THEN(nodeNoTimeWhileCmp)).BREAK(nodeBooleanTrueCmp)\n" +
				"        );\n" +
				"    </chain>");
		ComplexVo complexVo = new ComplexVo();
		complexVo.setWhileFlag(true);
		response = flowExecutor.execute2Resp("break", null, complexVo);
		if (response.isSuccess()){
			log.info("执行成功");
		}else{
			log.info("执行失败");
		}
	}




}
