package com.xjq.liteflow;

import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.LiteflowResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
class SwichTests {

    @Resource
    private FlowExecutor flowExecutor;

    /**
     * 选择编排(基础例子)
     */
    @Test
    public void testSwitch1() throws InterruptedException {
        log.info("    <chain name=\"switch1\">\n" +
                "        SWITCH(switchNode3sCmp).to(node3sCmp, node6sCmp, node8sCmp);\n" +
                "    </chain>");
        LiteflowResponse response = flowExecutor.execute2Resp("switch1");
        if (response.isSuccess()) {
            log.info("执行成功");
        } else {
            log.info("执行失败");
        }
        Thread.sleep(2000L);
    }



	/**
	 * 选择编排（复杂一点的例子）
	 */
	@Test
	public void testSwitch2() throws InterruptedException {
		log.info("<chain name=\"switch2\">\n" +
				"        THEN(\n" +
				"        nodeNoTime1Cmp,\n" +
				"        WHEN(node8sCmp, SWITCH(switchNode3sCmp).to(node3sCmp, node6sCmp)),\n" +
				"        nodeNoTime2Cmp\n" +
				"        );\n" +
				"    </chain>");
		LiteflowResponse response = flowExecutor.execute2Resp("switch2");
		if (response.isSuccess()) {
			log.info("执行成功");
		} else {
			log.info("执行失败");
		}
		Thread.sleep(2000L);
	}




	/**
	 * 选择编排（id 语法）
	 * 建议用 tag ，因为返回的无关业务字符串，可以在 el 脚本中灵活使用
	 */
	@Test
	public void testSwitchId() throws InterruptedException {
		log.info("<chain name=\"switchId\">\n" +
				"        THEN(\n" +
				"        nodeNoTime1Cmp,\n" +
				"        SWITCH(nodeSwitchT1Cmp).to(node3sCmp, THEN(node6sCmp, node8sCmp).id（\"t1\")),\n" +
				"        nodeNoTime2Cmp\n" +
				"        );\n" +
				"    </chain>");
		LiteflowResponse response = flowExecutor.execute2Resp("switchId");
		if (response.isSuccess()) {
			log.info("执行成功");
		} else {
			log.info("执行失败");
		}
		Thread.sleep(2000L);
	}








}





