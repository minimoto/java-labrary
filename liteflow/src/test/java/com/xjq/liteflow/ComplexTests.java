package com.xjq.liteflow;

import com.xjq.liteflow.vo.ComplexVo;
import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.FlowBus;
import com.yomahub.liteflow.flow.LiteflowResponse;
import com.yomahub.liteflow.flow.element.Node;
import com.yomahub.liteflow.flow.entity.CmpStep;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
public class ComplexTests {

    @Resource
    private FlowExecutor flowExecutor;


    /**
     * 与或非表达式 : AND，OR，NOT表达式
     * 用于 IF，WHILE，BREAK
     * */
    @Test
    public void testAndOrNot() {
        LiteflowResponse response;
        log.info("================== and ===================");
        log.info("<chain name=\"and\">\n" +
                "        IF(AND(nodeBooleanTrueCmp, nodeBooleanFalseCmp), nodeNoTime1Cmp, nodeNoTime2Cmp);\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("and");
        if (response.isSuccess()){
            log.info("执行成功");
        }else{
            log.info("执行失败");
        }

        log.info("================== or =========");
        log.info("<chain name=\"or\">\n" +
                "        IF(OR(nodeBooleanTrueCmp, nodeBooleanFalseCmp), nodeNoTime1Cmp, nodeNoTime2Cmp);\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("or");
        if (response.isSuccess()) {
            log.info("执行成功");
        }else{
            log.info("执行失败");
        }

        log.info("================== not =========");
        log.info("<chain name=\"not\">\n" +
                "        IF(NOT(nodeBooleanTrueCmp), nodeNoTime1Cmp, nodeNoTime2Cmp);\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("not");
        if (response.isSuccess()) {
            log.info("执行成功");
        }else{
            log.info("执行失败");
        }
    }


    /**
     * 数据上下文：用于传递数据，在组件间传递数据
     */
    @Test
    public void testContext() {
        LiteflowResponse response;
        log.info("<chain name=\"context\">\n" +
                "        THEN(nodeContent1Cmp, nodeContent2Cmp);\n" +
                "    </chain>");


        User user = new User("xjq", "123");
        ComplexVo complexVo = new ComplexVo();
        complexVo.setWhileFlag(true);
        complexVo.setCount(100);
        response = flowExecutor.execute2Resp("context", user, complexVo);
        if (response.isSuccess()) {
            log.info("执行成功");
        } else {
            log.info("执行失败");

        }
    }

    /**
     * LiteflowResponse对象
     * 常用方法：
     * isSuccess：判断是否执行成功
     * getCause：获取异常信息，如果isSuccess为false的话。
     * getExecuteSteps：获得执行步骤信息，返回Map<String, List<CmpStep>>，key为chainId，value为List<CmpStep>。
     *
     * 打印步骤信息
     * isSuccess：此组件是否执行成功
     * getNodeId：获得组件Id
     * getNodeName：获得组件名称
     * getTag：获得组件标签值
     * getTimeSpent：获得组件的耗时，单位为毫秒
     * getStartTime：获取组件开始执行的时间，为Date对象(从v2.11.4开始支持)
     * getEndTime：获取组件结束执行的时间，为Date对象(从v2.11.4开始支持)
     * getException：获取此组件抛出的异常，如果isSuccess为false的话。但是这里要注意下：有exception，success一定为false，但是success为false，不一定有exception，因为有可能没执行到，或者没执行结束(any的情况)。
     *
     */
    @Test
    public void testPrintStepInfo() {
        LiteflowResponse response;
        log.info("<chain name=\"when2\">\n" +
                "        THEN(\n" +
                "        node3sCmp,\n" +
                "        WHEN(node6sCmp, THEN(node8sCmp, node10sCmp)),\n" +
                "        nodeNoTime1Cmp\n" +
                "        );\n" +
                "    </chain>");

        List<Node> when2 = FlowBus.getNodesByChainId("when2");
        for (Node node : when2) {
            log.info("节点信息：{} {}, {}", node.getId(), node.getName(), node.getType());
        }

        response = flowExecutor.execute2Resp("when2");
        if (response.isSuccess()) {
            log.info("执行成功");
        } else {
            log.info("执行失败");
            log.error("异常信息：{}", response.getCause());
        }

        Map<String, List<CmpStep>> stepMap = response.getExecuteSteps();
        for (String key : stepMap.keySet()) {
            log.info("所有步骤信息：key= {}, List<CmpStep>= {}", key, stepMap.get(key).toString());
            for (CmpStep step : stepMap.get(key)) {
                log.info("所有步骤信息(详细)：nodeId= {}, nodeName= {}, tag= {}, timeSpent= {}, startTime= {}, endTime= {}, exception= {}", step.getNodeId(), step.getNodeName(), step.getTag(), step.getTimeSpent(), step.getStartTime(), step.getEndTime(), step.getException());
            }
        }

        log.info("或者用以下方式打印步骤信息，效果是一样的：");
        Queue<CmpStep> stepQueue = response.getExecuteStepQueue();
            for (CmpStep step : stepQueue) {
                log.info("步骤信息：nodeId= {}, nodeName= {}, tag= {}, timeSpent= {}, startTime= {}, endTime= {}, exception= {}", step.getNodeId(), step.getNodeName(), step.getTag(), step.getTimeSpent(), step.getStartTime(), step.getEndTime(), step.getException());
            }

    }


    /**
     * 超时表达式见 demo.el.xml
     */
    @Test
    public void testTimeOut() {
        // todo 没有写测试用例
    }


    /**
     * privatedelivery
     */
    @Test
    public void testPrivateDelivery() {
        LiteflowResponse response = flowExecutor.execute2Resp("privatedelivery");
        if (response.isSuccess()) {
            log.info("执行成功");
        } else {
            log.info("执行失败");

        }
    }
}
