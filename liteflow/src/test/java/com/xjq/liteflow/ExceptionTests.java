package com.xjq.liteflow;

import com.xjq.liteflow.vo.ComplexVo;
import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.flow.FlowBus;
import com.yomahub.liteflow.flow.LiteflowResponse;
import com.yomahub.liteflow.flow.element.Node;
import com.yomahub.liteflow.flow.entity.CmpStep;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.Queue;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@Slf4j
@SpringBootTest(classes = LiteflowApplication.class)
public class ExceptionTests {

    @Resource
    private FlowExecutor flowExecutor;

    /**
     * 处理异常日志
     * 如果你的业务中有获取异常 Code 的需求，则你自定义的异常需要实现 LiteFlowException 接口
     * 参考 {@link com.xjq.liteflow.exception.LiteFlowCustomException}
     * */
    @Test
    public void testException() {
        LiteflowResponse response;
        log.info("<chain name=\"exception\">\n" +
                "        THEN(nodeErrorCmp);\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("exception");
        if (response.isSuccess()){
            log.info("执行成功");
        }else{
            log.info("如果你的业务中有获取异常 Code 的需求，则你自定义的异常需要实现 LiteFlowException 接口");
            Exception cause = response.getCause();
            log.info("执行失败:{}", cause.toString());
            String code = response.getCode();
            String message = response.getMessage();
            log.info("code:{}, message:{}", code, message);
        }

    }


    /**
     * 异常处理：通常抛异常是会中断流程的，但有些情况下，我们希望流程可以处理异常,并继续执行
     * 1. 语法 CATCH...DO...
     * 2. DO 可以省略，表示没有异常处理逻辑。
     * 3. 异常处理后，最后执行结果是 "执行成功"
     * 4. CATCH包裹的组件有异常抛出, 只会中止该组件所在的关键字包裹流程(分支), 并不会影响其他分支的执行
     * 比如本例中，最后执行的链路为：
     * node3sCmp<3003>==>nodeErrorCmp<0>==>node6sCmp<6000>==>nodeNoTime1Cmp<0>==>nodeNoTime2Cmp<0>==>node8sCmp<8001>
     * */
    @Test
    public void testCatch() {
        LiteflowResponse response;
        log.info("<chain name=\"catch\">\n" +
                "        THEN(node3sCmp,\n" +
                "        CATCH(\n" +
                "        WHEN(THEN(nodeErrorCmp,node10sCmp), THEN(node6sCmp,nodeNoTime1Cmp))\n" +
                "        ).DO(nodeNoTime2Cmp),\n" +
                "        node8sCmp\n" +
                "        );\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("catch");
        if (response.isSuccess()){
            log.info("执行成功");
        }else{
            log.info("执行失败");
        }

    }

    /**
     * 模拟 For 循环的 continue .
     * 利用 catch 的特性：发生异常时，会中止该组件所在的关键字包裹流程(分支)
     * */
    @Test
    public void testContinue() {
        LiteflowResponse response;
        log.info("    <chain name=\"mockContinue\">\n" +
                "        FOR(5).DO(\n" +
                "        CATCH(\n" +
                "        THEN(node3sCmp,nodeErrorCmp,node6sCmp)\n" +
                "        )\n" +
                "        )\n" +
                "    </chain>");

        response = flowExecutor.execute2Resp("mockContinue");
        if (response.isSuccess()){
            log.info("执行成功");
        }else{
            log.info("执行失败");
        }
    }

    /**
     * 异常重试
     * 1. 配置重试次数
     * 1.1 全局配置 liteflow.retry-count=3
     * 1.2 组件配置 @LiteflowRetry(3)
     * 1.3 指定异常重试 @LiteflowRetry(retry = 3, forExceptions = {NullPointerException.class,IllegalArgumentException.class})
     *
     * 2. 重试后仍失败，则抛出异常并中止流程。但以下情况会继续执行
     * 2.1 组件里覆盖了isContinueOnError，设为true
     * 2.2 在 xml 中, WHEN上配置了ignoreError为true(默认为false)
     *
     * 3. 在 el 中重试
     * 3.1 单个组件的重试
     * <chain id="chain1">
     *     THEN(a, b.retry(3));
     * </chain>
     *
     * 3.2 表达式的重试
     * <chain id="chain1">
     *     THEN(a, b).retry(3);
     * </chain>
     *
     * <chain id="chain2">
     *     FOR(c).DO(a).retry(3);
     * </chain>
     *
     * <chain id="chain3">
     *     exp = SWITCH(x).to(m,n,p);
     *     IF(f, exp.retry(3), b);
     * </chain>
     *
     * 3.3 整个Chain的重试
     * <chain id="sub">
     *     THEN(a, b);
     * </chain>
     *
     * <chain id="main">
     *     WHEN(x, y, sub.retry(3));
     * </chain>
     *
     * 3.4 指定异常的重试
     * <chain id="chain1">
     *     THEN(a, b).retry(3, "java.lang.NullPointerException", "java.lang.ArrayIndexOutOfBoundsException");
     * </chain>
     *
     * */
    @Test
    public void testRetry() {
        // todo 没有写测试用例
    }







}
