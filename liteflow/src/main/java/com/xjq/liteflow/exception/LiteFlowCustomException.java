package com.xjq.liteflow.exception;

import com.yomahub.liteflow.exception.LiteFlowException;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/19
 */
public class LiteFlowCustomException extends LiteFlowException {
    public LiteFlowCustomException(String code, String message) {
        super(code, message);
    }
}
