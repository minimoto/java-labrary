package com.xjq.liteflow.booleans;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeBooleanTrueCmp")
public class NodeBooleanTrueCmp extends NodeBooleanComponent {


    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("nodeBooleanTrueCmp executed!");
        return true;
    }
}
