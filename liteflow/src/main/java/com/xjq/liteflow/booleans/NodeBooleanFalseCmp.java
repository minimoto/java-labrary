package com.xjq.liteflow.booleans;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeBooleanFalseCmp")
public class NodeBooleanFalseCmp extends NodeBooleanComponent {


    @Override
    public boolean processBoolean() throws Exception {
        System.out.println("nodeBooleanFalseCmp executed!");
        return false;
    }
}
