package com.xjq.liteflow.fors;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.core.NodeForComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeForCmp")
public class NodeForCmp extends NodeForComponent {


    @Override
    public int processFor()  {
        System.out.println("通过this.getLoopIndex()来获得下标");

        return 5;
    }
}
