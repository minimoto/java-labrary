package com.xjq.liteflow.fors;

import cn.hutool.core.collection.ListUtil;
import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.core.NodeIteratorComponent;

import java.util.Iterator;
import java.util.List;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeIteratorCmp")
public class NodeIteratorCmp extends NodeIteratorComponent {


    @Override
    public Iterator<?> processIterator() throws Exception {
        User jack = new User("jack", "123456");
        User mary = new User("mary", "234567");
        User tom = new User("tom", "345678");


        List<User> list = ListUtil.toList(jack, mary, tom);
        return list.iterator();
    }
}
