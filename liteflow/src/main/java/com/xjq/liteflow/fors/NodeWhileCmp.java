package com.xjq.liteflow.fors;

import com.xjq.liteflow.vo.ComplexVo;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.core.NodeForComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeWhileCmp")
public class NodeWhileCmp extends NodeBooleanComponent {


    @Override
    public boolean processBoolean() {
        ComplexVo complexVo = this.getContextBean(ComplexVo.class);

        return complexVo.isWhileFlag();
    }
}
