/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.privatedelivery;

import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@LiteflowComponent("privateDeliveryNode2")
public class PrivateDeliveryNode2Cmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("privateDeliveryNode2 executed!");
		User user = this.getPrivateDeliveryData();
		log.info("user: {}", user);
		//do your biz
	}


}
