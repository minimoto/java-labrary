/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.privatedelivery;

import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@LiteflowComponent("privateDeliveryNode1")
public class PrivateDeliveryNode1Cmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("privateDeliveryNode1 executed!");
		List<User> userList = new ArrayList<>();
		// 假设这里是从数据库中查询出来的用户列表，这里只是模拟10个用户
		for (int i = 0; i < 10; i++) {
			User user = new User();
			user.setUserName("user"+(i+1));
			user.setPassword("user"+(i+1));
			userList.add(user);
		}


		for (User user : userList) {
			// sendPrivateDeliveryData 第二个参数，只有对应的组件才能获取到，其他组件是获取不到的。并且这个投递的参数(一个或多个)只能被取一次。
			this.sendPrivateDeliveryData("privateDeliveryNode2",user);
		}
	}


}
