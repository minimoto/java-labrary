/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("node6sCmp")
public class Node6sCmp extends NodeComponent {

	@Override
	public void process() throws InterruptedException {
		Thread.sleep(1000*6);
		System.out.println("Node6sCmp executed!");
	}

}
