/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("nodeNoTimeItertorCmp")
public class NodeNoTimeItertorCmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("nodeNoTimeItertorCmp executed!");
		System.out.println("当前遍历对象是：" + this.getCurrLoopObj());
	}

}
