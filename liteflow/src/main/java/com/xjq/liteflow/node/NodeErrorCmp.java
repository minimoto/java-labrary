/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.xjq.liteflow.exception.LiteFlowCustomException;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("nodeErrorCmp")
public class NodeErrorCmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("NodeErrorCmp executed!");
		throw new LiteFlowCustomException("4000", "NodeErrorCmp exception");
	}

}
