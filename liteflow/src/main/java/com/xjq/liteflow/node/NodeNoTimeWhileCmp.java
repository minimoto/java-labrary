/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.xjq.liteflow.vo.ComplexVo;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("nodeNoTimeWhileCmp")
public class NodeNoTimeWhileCmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("nodeNoTimeWhileCmp executed!");
		int randomNum = (int)(Math.random() * 4);
		System.out.println("生成0-2之间的随机数：" + randomNum);
		if(randomNum == 0){
			System.out.println("随机数为0，跳出循环");
			ComplexVo complexVo = this.getContextBean(ComplexVo.class);
			complexVo.setWhileFlag(false);
		}

	}

}
