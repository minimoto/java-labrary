/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.xjq.liteflow.vo.ComplexVo;
import com.xjq.liteflow.vo.User;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@LiteflowComponent(id = "nodeContent2Cmp", name = "上下文测试节点2")
public class NodeContent2Cmp extends NodeComponent {

	@Override
	public void process()  {
		System.out.println("nodeContent2Cmp executed!");

		log.info("获取上下文数据");
		ComplexVo complexVo = this.getContextBean(ComplexVo.class);
		System.out.println(complexVo.toString());


		log.info("获取入参数据");
		User user = this.getRequestData();
		System.out.println(user.toString());



	}

}
