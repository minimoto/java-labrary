/**
 * <p>Title: liteflow</p>
 * <p>Description: 轻量级的组件式流程框架</p>
 * @author Bryan.Zhang
 * @email weenyc31@163.com
 * @Date 2020/4/1
 */
package com.xjq.liteflow.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@LiteflowComponent("node10sCmp")
public class Node10sCmp extends NodeComponent {

	@Override
	public void process() throws InterruptedException {
		Thread.sleep(1000*10);
		System.out.println("Node10sCmp executed!");
	}

}
