package com.xjq.liteflow.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {

    private String userName;

    private String password;

}
