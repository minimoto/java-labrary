package com.xjq.liteflow.vo;

import lombok.Data;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/18
 */
@Data
public class ComplexVo {

    private User user;

    private boolean  whileFlag;

    private int count;


}
