package com.xjq.liteflow.switchs;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("switchNode3sCmp")
public class NodeSwitchNode3sCmp extends NodeSwitchComponent {
    @Override
    public String processSwitch() {
        System.out.println("NodeSwitchNode3sCmp executed!");
        return "node3sCmp";
    }
}
