package com.xjq.liteflow.switchs;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeSwitchComponent;

/**
 * create by xiao_qiang_01@163.com
 * 2024/7/17
 */
@LiteflowComponent("nodeSwitchT1Cmp")
public class NodeSwitchT1Cmp extends NodeSwitchComponent {
    @Override
    public String processSwitch()  {
        return "t1";
    }
}
