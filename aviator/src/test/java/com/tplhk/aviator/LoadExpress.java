package com.tplhk.aviator;

import com.googlecode.aviator.ExpressCustomize;
import com.googlecode.aviator.ILoadExpressCustomize;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * @ClassName : LoadExpress
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/3 18:12
 **/
public class LoadExpress implements ILoadExpressCustomize {

    @Override
    public List<ExpressCustomize> loadExpressCustomize() {
        ExpressCustomize e1 = ExpressCustomize.build().setName("fun1").setExpress("1+2");
        ExpressCustomize e2 = ExpressCustomize.build().setName("fun2").setExpress("min(x,y,z,d)");
        ExpressCustomize e3 = ExpressCustomize.build().setName("fun3").setExpress("fun1+fun2");
        ExpressCustomize e4 = ExpressCustomize.build().setName("fun4").setExpress("if(fun3>3) {return 100;}else{return 200;}");
        ExpressCustomize e5 = ExpressCustomize.build().setName("residentialLoadingFactor").setExpress("if(string.length(planCode)<8){return 0;} else { if(string.substring(toUpperCase(planCode),7,8)==\"P\" && toUpperCase(countryName)==\"CHINA\"){return 20;} elsif(string.substring(toUpperCase(planCode),7,8)==\"O\" && toUpperCase(countryName)==\"CHINA\"){return 50;} else {return 0;} }");
        ExpressCustomize e6 = ExpressCustomize.build().setName("substring").setExpress("string.substring(planCode,8,1)");

        List<ExpressCustomize> list = new LinkedList<>();
        list.add(e1);
        list.add(e2);
        list.add(e3);
        list.add(e4);
        list.add(e5);
        list.add(e6);
        return list;
    }
}
