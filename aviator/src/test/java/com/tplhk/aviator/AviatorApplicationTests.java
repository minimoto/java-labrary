package com.tplhk.aviator;

import com.googlecode.aviator.AviatorEvaluator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Map;

@SpringBootTest
class AviatorApplicationTests {

    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {
        Map<String, Object> env1 = AviatorEvaluator.newEnv(
                "x", 1, "y", 4, "z", 8,
                "a", 20, "b", 40, "c", 80, "d", 2);
        Map<String, Object> residentialLoadingFactorEnv1 = AviatorEvaluator.newEnv(
                "planCode", "3311TM", "countryName", "MACAU SAR,CHINA");
        Map<String, Object> residentialLoadingFactorEnv2 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLF1", "countryName", "CHINA");
        Map<String, Object> residentialLoadingFactorEnv3 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLp1", "countryName", "CHINA");
        Map<String, Object> residentialLoadingFactorEnv4 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLP1", "countryName", "CHINA");
        Map<String, Object> residentialLoadingFactorEnv5 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLo1", "countryName", "CHINA");
        Map<String, Object> residentialLoadingFactorEnv6 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLO1", "countryName", "CHINA");
        Map<String, Object> residentialLoadingFactorEnv7 = AviatorEvaluator.newEnv(
                "planCode", "3311TMLO1", "countryName", "china");
//        System.out.println(AviatorExecutor.execute(AviatorContext.builder().expression("fun1").env(env1).build()));
//        System.out.println(AviatorExecutor.execute(AviatorContext.builder().expression("fun2").env(env1).build()));
//        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("min(x)+fun2").env(env1).build()));


        System.out.println("residentialLoadingFactorEnv1 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv1).build()));
        System.out.println("residentialLoadingFactorEnv2 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv2).build()));
        System.out.println("residentialLoadingFactorEnv3 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv3).build()));
        System.out.println("residentialLoadingFactorEnv4 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv4).build()));
        System.out.println("residentialLoadingFactorEnv5 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv5).build()));
        System.out.println("residentialLoadingFactorEnv6 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv6).build()));
        System.out.println("residentialLoadingFactorEnv7 = " + aviatorExecutor.execute(AviatorContext.builder().expression("residentialLoadingFactor").env(residentialLoadingFactorEnv7).build()));


    }

}
