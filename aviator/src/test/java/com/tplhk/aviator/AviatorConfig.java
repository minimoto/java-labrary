package com.tplhk.aviator;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @ClassName : AviatorConfig
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/11 16:13
 **/
@Configuration
public class AviatorConfig {

    @Bean
    AviatorExecutor aviatorExecutor() {
        AviatorExecutor aviatorExecutor = new AviatorExecutor(new LoadExpress());
        aviatorExecutor.setTraceFlag(true);
        return aviatorExecutor;
    }
}
