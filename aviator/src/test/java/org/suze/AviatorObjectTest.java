package org.suze;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 基于Aviator测试类
 * @Date : 2018/9/7 下午6:00
 * @Author : 石冬冬-Seig Heil(shiyongxin2010@163.com)
 */
@Slf4j
public class AviatorObjectTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void object_instance() {
        Teacher teacher = new Teacher() {{
            setName("im");
            setScore(20);
        }};

        Student student = new Student() {{
            setName("Jack");
            setScore(22);
        }};
        Map<String, Object> env = new HashMap<String, Object>() {{
            put("teacher", teacher);
            put("student", student);
        }};

        Object value = aviatorExecutor.execute(AviatorContext.builder().env(env).expression("student.name").build());
        System.out.println(value);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class Colleague {
        private String name;
        private double score;
    }

    @Data
    @NoArgsConstructor
    public class Student extends Colleague {

        public Student(String name, double score) {
            super(name, score);
        }
    }

    @Data
    @NoArgsConstructor
    public class Teacher extends Colleague {
    }

}
