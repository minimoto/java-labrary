package org.suze.function.collection;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 判断一个字符串是否不在一个字符串区间集合中
 * @Date : 2021/4/11 9:43 AM
 * @Author : 石冬冬-Seig Heil
 */
public class NotExsitTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("x", 15);
        }};
        String expression = "notin(x, ',' , '15,34,22')";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(evn).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
