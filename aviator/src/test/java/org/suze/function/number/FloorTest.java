package org.suze.function.number;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * @description: 向下取整
 * @Date : 2021/4/11 9:43 AM
 * @Author : 石冬冬-Seig Heil
 */
public class FloorTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("floor(1.2)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("floor(1.6)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("floor(201)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("floor(196)").env(Collections.emptyMap()).build()));
    }
}
