package org.suze.function.number;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 对数字取整方式 和 精度进位 处理
 * @Date : 2021/4/11 9:43 AM
 * @Author : 石冬冬-Seig Heil
 */
public class ScaleTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("x", 15.344);
        }};
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("scale(x,0,0)").env(evn).build()));
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("scale(x,0,1)").env(evn).build()));
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("scale(x,0,2)").env(evn).build()));
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("scale(x,-2,1)").env(evn).build()));
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("scale(x,-2,2)").env(evn).build()));
    }
}
