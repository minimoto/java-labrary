package org.suze.function.number;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * @description: 四舍五入
 * @Date : 2021/4/11 9:43 AM
 * @Author : 石冬冬-Seig Heil
 */
public class RoundTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("round(1.2)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("round(1.6)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("round(201)").env(Collections.emptyMap()).build()));

        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("round(196)").env(Collections.emptyMap()).build()));
    }
}
