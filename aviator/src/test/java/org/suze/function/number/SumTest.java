package org.suze.function.number;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 多个数字求和计算(其中任何一个数字为空则作为0处理)
 * @Date : 2021/4/11 9:43 AM
 * @Author : 石冬冬-Seig Heil
 */
public class SumTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @Test
    public void test() {
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("x", 2);
            put("y", 4);
            put("z", 8);
        }};
        System.out.println(aviatorExecutor.execute(AviatorContext.builder().expression("sum(x,y,z,h)").env(evn).build()));
    }
}
