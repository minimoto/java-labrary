package org.suze.function.object;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 根据出生年月日计算年龄, 出生年月日类型为字符串
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class NvlTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        String expression = "nvl(a,0)";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(Collections.emptyMap()).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
