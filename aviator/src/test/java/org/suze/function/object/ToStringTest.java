package org.suze.function.object;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * @description: 数字转字符串，如果数字为空，则使用默认值作为返回值，objects.toString(value,defaultValue)
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class ToStringTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        String expression = "objects.toString(1,'0')";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(Collections.emptyMap()).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
