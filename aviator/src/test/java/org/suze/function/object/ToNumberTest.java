package org.suze.function.object;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collections;

/**
 * @description: 字符串转数字，如果字符串为空，则使用默认值作为返回值，objects.toNumber(value,defaultValue)
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class ToNumberTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        String expression = "objects.toNumber('1',0)";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(Collections.emptyMap()).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
