package org.suze.function.general;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * @description: 截取字符串中的数字求和
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class StringNumbersSumTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        String str = "保费借款金额：人民币（第一年：5600.00元；第二年：1110.00元；第三年：1000.00元）";
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("str", str);
        }};
        String expression = "string.numbers.sum(str, '-', '：', '元')";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(evn).cached(false).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
