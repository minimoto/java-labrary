package org.suze.function.general;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 数字转大写金额
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class ChineseNumberUpperTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        BigDecimal num = new BigDecimal("11111.01");
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("num", num);
        }};
        String expression = "chinese.number.upper(num)";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(evn).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
