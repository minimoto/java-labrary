package org.suze.function.general;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 根据出生年月日计算年龄, 出生年月日类型为字符串
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class AgeTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("birth", "19880808");
        }};
        String expression = "age(birth)";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(evn).build();
        System.out.println("age:" + aviatorExecutor.execute(ctx));
    }
}
