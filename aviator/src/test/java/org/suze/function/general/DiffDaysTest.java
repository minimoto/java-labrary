package org.suze.function.general;

import com.tplhk.aviator.AviatorContext;
import com.tplhk.aviator.AviatorExecutor;
import lombok.SneakyThrows;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: 两日期相差天数, 参数类型为date
 * @Date : 2021/4/11 10:19 AM
 * @Author : 石冬冬-Seig Heil
 */
public class DiffDaysTest {
    @Autowired
    AviatorExecutor aviatorExecutor;

    @SneakyThrows
    @Test
    public void test() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Map<String, Object> evn = new HashMap<String, Object>() {{
            put("current", simpleDateFormat.parse("2021-04-11"));
            put("destination", simpleDateFormat.parse("2020-04-11"));
        }};
        String expression = "days.diff(current,destination)";
        AviatorContext ctx = AviatorContext.builder().expression(expression).env(evn).build();
        System.out.println(aviatorExecutor.execute(ctx));
    }
}
