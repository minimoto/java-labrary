package com.tplhk.aviator.function.object;

import com.googlecode.aviator.runtime.function.AbstractFunction;
import com.googlecode.aviator.runtime.type.AviatorNil;
import com.googlecode.aviator.runtime.type.AviatorObject;
import com.googlecode.aviator.runtime.type.AviatorString;

import java.util.Map;

/**
 * @description: 字符串转大写
 * @Date : 2021/8/31
 * @Author : xujunqiang
 */
public class ToUpperCase extends AbstractFunction {

    @Override
    public AviatorObject call(Map<String, Object> env, AviatorObject arg1) {
        Object originValue = arg1.getValue(env);
        boolean isNull = null == originValue || originValue.toString().length() == 0;
        return isNull ? AviatorNil.NIL : new AviatorString(originValue.toString().toUpperCase());
    }

    @Override
    public String getName() {
        return "toUpperCase";
    }

}
