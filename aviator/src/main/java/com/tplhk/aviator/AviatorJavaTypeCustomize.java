package com.tplhk.aviator;

import com.googlecode.aviator.lexer.SymbolTable;
import com.googlecode.aviator.runtime.type.AviatorJavaType;

/**
 * @ClassName : AviatorJavaTypeCustomize
 * @Description : TODO
 * @Author : taiping
 * @Date: 2021/9/3 12:21
 **/
public class AviatorJavaTypeCustomize extends AviatorJavaType {
    public AviatorJavaTypeCustomize(String name) {
        super(name);
    }

    public AviatorJavaTypeCustomize(String name, SymbolTable symbolTable) {
        super(name, symbolTable);
    }

}
