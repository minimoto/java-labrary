package com.cntaiping.dataflow.util;

import org.springframework.batch.core.BatchStatus;

/**
 * create by xiao_qiang_01@163.com
 * 2021/3/23
 */
public class SingletonExitCodeUtil {
    public BatchStatus exitCode = BatchStatus.COMPLETED;
    private static SingletonExitCodeUtil instance = new SingletonExitCodeUtil();

    private SingletonExitCodeUtil() {
    }

    public static SingletonExitCodeUtil getInstance() {
        return instance;
    }

    public void setExitCode(BatchStatus exitCode) {
        this.exitCode = exitCode;
    }
}
