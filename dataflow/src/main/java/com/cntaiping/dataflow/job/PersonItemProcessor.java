package com.cntaiping.dataflow.job;

import com.cntaiping.dataflow.util.SingletonExitCodeUtil;
import com.cntaiping.dataflow.dto.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.task.TaskExecutor;

/**
 * @Author: zhangxinxin
 * @Date: 2020/11/22 11:56
 */
public class PersonItemProcessor implements ItemProcessor<Person, String> {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(PersonItemProcessor.class);

    @Autowired
    ApplicationArguments applicationArguments;

    @Autowired
    TaskExecutor taskExecutor;

    @Override
    public String process(Person person) throws Exception {
        applicationArguments.getOptionNames().stream().forEach(item -> System.out.println(item));
        String exitcode = applicationArguments.getOptionValues("exitcode").get(0);
        System.out.println("exitcode:::::::::::::" + exitcode);
        if (exitcode.equals("fail")) {
            throw new JobExecutionException("我故意抛出一个异常");
        }

        String greeting = "Hello " + person.getFirstName() + " "
                + person.getLastName() + "!";

        LOGGER.info("converting '{}' into '{}'", person, greeting);
        return greeting;
    }


}
