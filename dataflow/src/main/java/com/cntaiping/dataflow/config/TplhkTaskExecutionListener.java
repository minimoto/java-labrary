package com.cntaiping.dataflow.config;

import com.cntaiping.dataflow.util.SingletonExitCodeUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.task.listener.TaskExecutionListener;
import org.springframework.cloud.task.repository.TaskExecution;
import org.springframework.context.annotation.Configuration;

/**
 * create by xiao_qiang_01@163.com
 * 2021/3/24
 */
@Configuration
public class TplhkTaskExecutionListener implements TaskExecutionListener {
    private static final Logger log = LoggerFactory.getLogger(TplhkTaskExecutionListener.class);


    @Override
    public void onTaskStartup(TaskExecution taskExecution) {
        log.info("--------------------------------{} task startup--------------------------------", taskExecution.getTaskName());
    }


    @Override
    public void onTaskEnd(TaskExecution taskExecution) {
        taskExecution.setExitMessage(SingletonExitCodeUtil.getInstance().exitCode.name());
        log.info("--------------------------------{} task end--------------------------------", taskExecution.getTaskName());
    }

    // 下面的监听方法不知道如何才能被调用：抛异常也没有执行
    @Override
    public void onTaskFailed(TaskExecution taskExecution, Throwable throwable) {
        String taskName = taskExecution.getTaskName();
        log.info(taskName + " ----- task failed = {}----------------------", taskExecution.getErrorMessage());
        log.info(taskName + " ----- task throwable = {}----------------------", throwable.getMessage());
        taskExecution.setExitMessage("FAILED");

    }
}