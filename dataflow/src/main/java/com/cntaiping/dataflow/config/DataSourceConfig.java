package com.cntaiping.dataflow.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.cloud.task.configuration.DefaultTaskConfigurer;
import org.springframework.cloud.task.configuration.TaskConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;


@Configuration
public class DataSourceConfig {


    @Primary
    @Bean(name = "platform")
    @Qualifier("platform")
    @ConfigurationProperties(prefix = "spring.datasource.platform")
    public DataSource platformDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "task")
    @Qualifier("task")
    @ConfigurationProperties(prefix = "spring.datasource.task")
    public DataSource taskDataSource() {
        return DataSourceBuilder.create().build();
    }


    @Primary
    @Bean(name = "platformJdbcTemplate")
    public JdbcTemplate platformJdbcTemplate(@Qualifier("platform") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }


    @Bean(name = "taskJdbcTemplate")
    public JdbcTemplate taskJdbcTemplate(@Qualifier("task") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }

    @Bean
    public TaskConfigurer taskConfigurer(@Qualifier("platform") DataSource dataSource) {
        TaskConfigurer taskConfigurer = new DefaultTaskConfigurer(dataSource);
        return taskConfigurer;
    }


}
