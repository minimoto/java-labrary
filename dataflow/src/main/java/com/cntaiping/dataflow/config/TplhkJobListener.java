package com.cntaiping.dataflow.config;


import com.cntaiping.dataflow.util.DateJdk8Util;
import com.cntaiping.dataflow.util.SingletonExitCodeUtil;
import com.cntaiping.dataflow.util.SpringContextHolder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.batch.core.ExitStatus.COMPLETED;

@Slf4j
@Component
public class TplhkJobListener implements JobExecutionListener {

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("----------------------" + jobExecution.getJobInstance().getJobName() + " 任务开始执行------------->");
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        log.info("----------------------" + jobExecution.getJobInstance().getJobName() + " 任务执行结束------------->");
        // 实际应用中，业务异常中设置 SingletonExitCodeUtil.exitCode
//        jobExecution.setStatus(SingletonExitCodeUtil.getInstance().exitCode);
        jobExecution.setStatus(BatchStatus.COMPLETED);
        // 不要企图设置ExitStatus来自定义退出状态码，否则异常时 springbatch 不会记录 error_message 信息
//        jobExecution.setExitStatus(ExitStatus.NOOP);
        if (!jobExecution.getExitStatus().equals(COMPLETED)) {
            SingletonExitCodeUtil.getInstance().setExitCode(BatchStatus.FAILED);
            jobExecution.setStatus(BatchStatus.FAILED);
            String subject = "【" + SpringContextHolder.getActiveProfile() + "】批处理执行【" + jobExecution.getStatus() + "】:" + jobExecution.getJobInstance().getJobName();
            log.info("发邮件啦，标题: {}, 内容: {}", subject, jobExecution.getExitStatus().getExitDescription());
            Map map = new HashMap(16);
            map.put("jobId", jobExecution.getJobInstance().getId());
            map.put("jobName", jobExecution.getJobInstance().getJobName());
            map.put("jobStart", DateJdk8Util.getDateStr(DateJdk8Util.UDateToLocalDate(jobExecution.getStartTime())));
            map.put("jobParameters", jobExecution.getJobParameters().getParameters().toString());
            map.put("status", jobExecution.getStatus());
            map.put("exitCode", jobExecution.getExitStatus().getExitCode());
            map.put("exitDescription", jobExecution.getExitStatus().getExitDescription());

//            EmailModel emailModel = EmailModel.builder()
//                    .systemID("batch_service")
//                    .serviceID("batch_job_fail")
//                    .to(emailConfig.getTo())
//                    .subject(subject)
//                    .variables(map)
//                    .locate("zh_cn")
//                    .build();
//            this.sendEmail(emailModel);
        }

    }

//    private void sendEmail(EmailModel emailModel){
//        String result = remoteNotifyService.sendEmail(emailModel);
//        log.info("发送邮件结果, result = {}", result);
//
//    }
}
