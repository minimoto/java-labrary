package com.cntaiping.dataflow;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;

/**
 * @Author: xujunqiang
 * @Date: 2020/12/2 19:14
 */
@SpringBootApplication
@EnableBatchProcessing
@EnableTask
public class BatchDemoApp {


    public static void main(String[] args) {
        SpringApplication.run(BatchDemoApp.class, args);
    }

}
