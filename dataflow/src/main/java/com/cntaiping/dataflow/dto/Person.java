package com.cntaiping.dataflow.dto;

import lombok.Data;

/**
 * @Author: zhangxinxin
 * @Date: 2020/11/22 11:43
 */
@Data
public class Person {

    private String firstName;
    private String lastName;

}
