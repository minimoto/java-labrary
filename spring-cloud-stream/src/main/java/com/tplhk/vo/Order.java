package com.tplhk.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class Order {

    private Long orderId;

    private String orderNum;

    private String type;

    private int num;

    private Date createAt;
}
