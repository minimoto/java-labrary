//package com.tplhk.rabbitmq;
//
//import lombok.Data;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.stream.annotation.EnableBinding;
//import org.springframework.cloud.stream.annotation.Input;
//import org.springframework.cloud.stream.annotation.Output;
//import org.springframework.cloud.stream.annotation.StreamListener;
//import org.springframework.cloud.stream.messaging.Processor;
//import org.springframework.context.annotation.Bean;
//import org.springframework.integration.annotation.InboundChannelAdapter;
//import org.springframework.integration.annotation.Poller;
//import org.springframework.integration.core.MessageSource;
//import org.springframework.integration.support.MessageBuilder;
//import org.springframework.messaging.Message;
//import org.springframework.messaging.MessageChannel;
//import org.springframework.messaging.SubscribableChannel;
//
//import java.util.Date;
//
///**
// * Created by xw on 2017/2/20.
// * 2017-02-20 16:51
// */
//@SpringBootApplication
//@EnableBinding({Processor.class, OrderProcessor.class, ProductProcessor.class, MytestInput.class})
//public class Application implements CommandLineRunner {
//
//    /**
//     * 普通的用法，发送字符串
//     */
//    @Autowired
//    @Qualifier("output")
//    MessageChannel output;
//
//
//    @Autowired
//    @Qualifier("outputOrder")
//    MessageChannel outputOrder;
//
//    /**
//     * 通过接口的方式使用，发送字符串
//     */
//    @Autowired
//    ProductProcessor productProcessor;
//
//
//    /**
//     * 我自己学习的例子，注意的是，发送消息 RoutingKey = qqqsss
//     */
//    @Autowired
//    MytestInput myinputtest;
//
//    // 定时轮询发送消息到 binding 为 Processor.OUTPUT
//    @Bean
//    @InboundChannelAdapter(value = Processor.OUTPUT, poller = @Poller(fixedDelay = "3000", maxMessagesPerPoll = "1"))
//    public MessageSource<String> timerMessageSource() {
//        return () -> MessageBuilder.withPayload("每 3 秒发送一条字符串短消息 [此消息由 input 接收] -" + new Date()).build();
//    }
//
//    public static void main(String[] args) {
//        System.setProperty("spring.profiles.active","rabbitmq");
//        System.setProperty("server.port","9000");
//        SpringApplication.run(Application.class);
//    }
//
//    @Override
//    public void run(String... strings) {
////        // 字符串类型发送MQ
////        System.out.println("字符串信息发送");
////        output.send(MessageBuilder.withPayload("这是一条字符串 [ 此消息由 input 接收 ]").build());
////
////        // 实体类型发送MQ
////        System.out.println("发送一个 order 对象");
////        Order appleOrder = new Order();
////        appleOrder.setOrderNum("0000001");
////        appleOrder.setNum(10);
////        appleOrder.setType("APPLE");
////        appleOrder.setCreateAt(new Date());
////        // 使用 注入 MessageChannel 方式发送
////        outputOrder.send(MessageBuilder.withPayload(appleOrder).build());
////
////
////        // 使用 定义的接口的方式来发送
//        System.out.println("新增产品发送(OUTPUT_PRODUCT_ADD)");
//        productProcessor.outputProductAdd().send(MessageBuilder.withPayload("添加一个产品 [ 此消息由 inputProductAdd 接收]").build());
//
////        System.out.println("Kkk1111");
////        myinputtest.outputProductAdd().send(MessageBuilder.withPayload("haha哈哈11111111111111").setHeader("subject","1qqqsss").build());
//        System.out.println("Kkk");
//        myinputtest.outputProductAdd().send(MessageBuilder.withPayload("haha哈哈").setHeader("subject","qqqsss").build());
//    }
//
//
//    // 普通的用法，接收字符串。监听 binding 为 Processor.INPUT 的消息
//    @StreamListener(Processor.INPUT)
//    public void input(Message<String> message) {
//        System.out.println("[input]一般监听收到：" + message.getPayload());
//    }
//
//    // 通过接口的方式使用。接收字符串
//    @StreamListener(ProductProcessor.INPUT_PRODUCT_ADD)
//    public void inputProductAdd(Message<String> message) {
//        System.out.println("[inputProductAdd]新增产品监听收到：" + message.getPayload());
//    }
//
//    // 监听 binding 为 OrderIntf.INPUT_ORDER 的消息
//    @StreamListener(OrderProcessor.INPUT_ORDER)
//    public void inputOrder(Order order) {
//        System.out.println("=====[inputOrder]订单监听收到=====");
//        System.out.println("订单编号：" + order.getOrderNum());
//        System.out.println("订单类型：" + order.getType());
//        System.out.println("订单数量：" + order.getNum());
//        System.out.println("=====订单处理完成=====");
//    }
//
//
//    @StreamListener(MytestInput.mytestInput)
//    public void mytestInput(Message<String> message) {
//        System.out.println("[mytestInput] 接收 header消息：" + message.getPayload());
//    }
//
//
//
//
//
//
//
//}
//
//interface ProductProcessor {
//
//    String INPUT_PRODUCT_ADD = "inputProductAdd";
//    String OUTPUT_PRODUCT_ADD = "outputProductAdd";
//
//    @Input(INPUT_PRODUCT_ADD)
//    SubscribableChannel inputProductAdd();
//
//    @Output(OUTPUT_PRODUCT_ADD)
//    MessageChannel outputProductAdd();
//
//}
//
//interface OrderProcessor {
//
//    String INPUT_ORDER = "inputOrder";
//    String OUTPUT_ORDER = "outputOrder";
//
//    @Input(INPUT_ORDER)
//    SubscribableChannel inputOrder();
//
//    @Output(OUTPUT_ORDER)
//    MessageChannel outputOrder();
//}
//
//interface MytestInput {
//
//    String mytestInput = "mytestInput";
//    String outputmytestInput = "outputmytestInput";
//
//    @Input(mytestInput)
//    SubscribableChannel inputProductAdd();
//
//    @Output(outputmytestInput)
//    MessageChannel outputProductAdd();
//
//}
//
//@Data
//class Order {
//
//    private String orderNum;
//
//    private String type;
//
//    private int num;
//
//    private Date createAt;
//
//    public String getOrderNum() {
//        return orderNum;
//    }
//
//    public void setOrderNum(String orderNum) {
//        this.orderNum = orderNum;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public int getNum() {
//        return num;
//    }
//
//    public void setNum(int num) {
//        this.num = num;
//    }
//
//    public Date getCreateAt() {
//        return createAt;
//    }
//
//    public void setCreateAt(Date createAt) {
//        this.createAt = createAt;
//    }
//}