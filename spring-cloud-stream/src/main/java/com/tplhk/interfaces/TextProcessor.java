package com.tplhk.interfaces;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TextProcessor {

    String TEXT_INPUT = "textInput";
    String TEXT_OUTPUT = "textOutput";

    @Input(TEXT_INPUT)
    SubscribableChannel textInput();

    @Output(TEXT_OUTPUT)
    MessageChannel textOutput();
}
