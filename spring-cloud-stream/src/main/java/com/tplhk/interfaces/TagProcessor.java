package com.tplhk.interfaces;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface TagProcessor {

    String TAG_INPUT = "tagInput";
    String TAG_OUTPUT = "tagOutput";

    @Input(TAG_INPUT)
    SubscribableChannel tagInput();

    @Output(TAG_OUTPUT)
    MessageChannel tagOutput();
}
