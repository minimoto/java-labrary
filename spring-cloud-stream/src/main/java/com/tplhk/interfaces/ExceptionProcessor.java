package com.tplhk.interfaces;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface ExceptionProcessor {
    String EXCEPTION_INPUT = "exceptionInput";
    String EXCEPTION_OUTPUT = "exceptionOutput";

    @Input(EXCEPTION_INPUT)
    SubscribableChannel exceptionInput();

    @Output(EXCEPTION_OUTPUT)
    MessageChannel exceptionOutput();
}
