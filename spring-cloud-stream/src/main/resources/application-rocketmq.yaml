server:
  port: 18080
spring:
  application:
    name: stream-rocketmq-producer-application
  cloud:
    # Spring Cloud Stream 配置项，对应 BindingServiceProperties 类
    stream:
      # Binding 配置项，对应 BindingProperties Map
      bindings:
        textOutput:
          destination: text-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
        textInput:
          destination: text-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
          group: text-consumer-group-text-TOPIC # 消费者分组,配合 broadcasting 集群消费。命名规则：组名+topic名

        orderOutput:
          destination: order-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
          producer:
            # 分区 key 表达式。该表达式基于 Spring EL，从消息中获得分区 key。
            # 相同的 orderId 发送到同一个分区
            partition-key-expression: payload['orderId']
        orderInput:
          destination: order-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
          group: order-consumer-group-order-topic # 消费者分组,配合 broadcasting 集群消费。命名规则：组名+topic名

        exceptionOutput:
          destination: exception-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
        exceptionInput:
          destination: exception-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
          group: exception-consumer-group-exception-topic # 消费者分组,配合 broadcasting 集群消费。命名规则：组名+topic名
          consumer:
            # 重试次数
            maxAttempts: 5

        tagOutput:
          destination: tag-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
        tagInput:
          destination: tag-topic # 目的地。这里使用 RocketMQ Topic
          content-type: application/json # 内容格式。这里使用 JSON
          group: tag-consumer-group-tag-topic # 消费者分组,配合 broadcasting 集群消费。命名规则：组名+topic名

      # Spring Cloud Stream RocketMQ 配置项
      rocketmq:
        # RocketMQ Binder 配置项，对应 RocketMQBinderConfigurationProperties 类
        binder:
          #          name-server: 10.29.49.94:9876;10.29.49.101:9876 # RocketMQ Namesrv 地址
          name-server: 10.6.56.57:9876 # RocketMQ Namesrv 地址
        # RocketMQ 自定义 Binding 配置项，对应 RocketMQBindingProperties Map
        bindings:
          # ###################################################################################################
          # textOutput 演示发送普通对象消息，延时消息，事务消息
          # ###################################################################################################
          textOutput:
            # RocketMQ Producer 配置项，对应 RocketMQProducerProperties 类
            producer:
              # group 用于生产者事务，配合 transactional。
              # 如果发送的是事务消息且原始生产者在发送之后崩溃，
              # 则 Broker 服务器会联系同一生产者组的其他生产者实例以提交或回滚消费
              group: text-producer
              sync: true # 是否同步发送消息，默认为 false 异步。
              # 开启事务,必须定义 producer.group 且实现 RocketMQLocalTransactionListener 接口。
              # 即生产者发送消息的方法中，如果抛异常（如写本地数据库失败），则发送消息失败
          #              transactional: true
          textInput:
            # RocketMQ Consumer 配置项，对应 RocketMQConsumerProperties 类
            consumer:
              enabled: true # 是否开启消费，默认为 true
              broadcasting: false # 是否使用广播消费，默认为 false 使用集群消费,集群消费时需要配置消费者 group

          # ###################################################################################################
          # 演示消息异常，异常处理，重试队列，死信队列
          # 消费消息异常时，重试次数配置 5 次（默认 3 次），这个次数是包括首次正常情况的，所以事实上重试次数是 4 次。
          # 在处理异常时，
          # 如果编写异常处理 (handleError，globalHandleError), 则会重试 maxAttempts 后进入异常处理,
          #    并不会发送到 %RETRY% 队列， 也不会进入死信列队， 即该消息处理结束。可以简单的理解为异常消息被直接处理了。
          # 如果没有编写异常处理(handleError，globalHandleError), 则会重试 maxAttempts-1 次后会发送到重试队列,
          #   并会间隔 delay-level 时长重新发送消息，在重试 maxAttempts-1 次后再发送到重试队列。
          #   如果一直不成功，最后会发到死信队列。通过界面修改死信列队TOPIC的 PERM为 6，否则无法写入
          # ###################################################################################################
          exceptionOutput:
            # RocketMQ Producer 配置项，对应 RocketMQProducerProperties 类
            producer:
              sync: true # 是否同步发送消息，默认为 false 异步。
          exceptionInput:
            # RocketMQ Consumer 配置项，对应 RocketMQConsumerProperties 类
            consumer:
              enabled: true # 是否开启消费，默认为 true
              broadcasting: false # 是否使用广播消费，默认为 false 使用集群消费,集群消费时需要配置消费者 group
          #              # 异常后直接放到死信队列
          #              delayLevelWhenNextConsume: -1

          # ###################################################################################################
          # orderOutput 演示顺序消息 并未按顺序，测试未成功
          # ###################################################################################################
          orderOutput:
            # RocketMQ Producer 配置项，对应 RocketMQProducerProperties 类
            producer:
              group: order-producer
              sync: true # 是否同步发送消息，默认为 false 异步。
          orderInput:
            # RocketMQ Consumer 配置项，对应 RocketMQConsumerProperties 类
            consumer:
              enabled: true # 是否开启消费，默认为 true
              broadcasting: false # 是否使用广播消费，默认为 false 使用集群消费,集群消费时需要配置消费者 group
              orderly: true # 是否顺序消费，默认为 false 并发消费。

          # ###################################################################################################
          # tagOutput 演示 tag 消息过滤
          # ###################################################################################################
          tagOutput:
            # RocketMQ Producer 配置项，对应 RocketMQProducerProperties 类
            producer:
              group: tag-producer
              sync: true # 是否同步发送消息，默认为 false 异步。
          tagInput:
            # RocketMQ Consumer 配置项，对应 RocketMQConsumerProperties 类
            consumer:
              enabled: true # 是否开启消费，默认为 true
              broadcasting: false # 是否使用广播消费，默认为 false 使用集群消费,集群消费时需要配置消费者 group
              tags: aaa || bbb # 基于 Tag 订阅，多个 Tag 使用 || 分隔，默认为空

############# 死信队列 (未开发) ###########################
#          inputDlq:
#            destination: '%DLQ%${spring.cloud.stream.bindings.tagInput.group}'
#            content-type: application/json
#            group: tagInput-dlq-group
#            consumer:
#              concurrency: 20

### 消息的ACK确认
#
#  如果生产者需要对消费的结果做确认，可按如下操作进行配置
#
#  1.消费者 ,在消费的方法上，使用@SendTo(ChannelConstant.ACK_INPUT) ACK_INPUT是我们采用自己约定专门处理响应结果的通道
#
#  如下所示：
#
#  注意：返回的结果，业务自己定义，如果只是想确认消息消费成功了，可以用AckResult 确认，如果想要获取消费的结果在做业务处理，请自定义返回结果
#
#  ```
#  @SendTo(ChannelConstant.ACK_INPUT)
#  public String receiveInput1(Object object) {
#  System.out.println("收到普通的消息1：" + MarshalJSONUtil.marshalBeanToJson(object));
#  int sss=12/0;
#  return AckResult.commit;
#}
#  ```
#
#  2.生成者，只需要监听ACK的方法就行了，如果需要针对想要结果做业务处理，请自行处理
#
#  ```
#  /**
#  * AcK应答
#  * @param object
#  */
#  @StreamListener(ChannelConstant.ACK_INPUT)
#  public void ack(Object object) {
#  System.out.println("收到普通的消息ACK：" + MarshalJSONUtil.marshalBeanToJson(object));
#
#}
#  ```